!Create Signature: f2pyA -m CmodulesNeumann3 -h  CmodulesNeumann3.pyf CmodulesNeumann3.f90 --overwrite-signature


subroutine amult3neumann(n, u, v)
!f2py intent(c) amult3neumann
  implicit none
  integer :: n
  real(8) :: u(n), v(n)

!f2py real(8), intent(out), dimension(n) :: v
!f2py real(8), intent(c), dimension(n) :: u
!f2py integer, intent(c) :: n

end subroutine amult3neumann

subroutine trisol3neumann(n, k, d, b, x)
!f2py intent(c) trisol3neumann
  implicit none
  integer :: n
  real(8) :: k, d(n), b(n), s1(n), s2(n), x(n), mk2, k2

!f2py real(8), intent(out), dimension(n) :: x
!f2py real(8), intent(c), dimension(n) :: d
!f2py real(8), intent(c), dimension(n) :: b
!f2py real(8), intent(c) :: k
!f2py integer, intent(c) :: n

end subroutine trisol3neumann

subroutine godunovstepneumann3rd(n, h, k, u, v)
!f2py intent(c) godunovstepneumann3rd
  implicit none
  integer :: n
  real(8) :: h, k, u(n), v(n)

!f2py integer, intent(c) :: n
!f2py real(8), intent(c) :: h
!f2py real(8), intent(c) :: k
!f2py real(8), intent(c), dimension(n) :: u
!f2py real(8), intent(out), dimension(n) :: v

end subroutine godunovstepneumann3rd

subroutine amultneumann(n, u, v)
!f2py intent(c) amultneumann
  implicit none
  integer :: n
  real(8) :: u(n), v(n)

!f2py real(8), intent(out), dimension(n) :: v
!f2py real(8), intent(c), dimension(n) :: u
!f2py integer, intent(c) :: n

end subroutine amultneumann

subroutine trisol0neumann(n, k, b, x)
!f2py intent(c) trisol0neumann
  implicit none
  integer :: n, i, ii
  real(8) :: k, b(n), s1(n), s2(n), x(n), mk4, k2

!f2py real(8), intent(out), dimension(n) :: x
!f2py real(8), intent(c), dimension(n) :: b
!f2py real(8), intent(c) :: k
!f2py integer, intent(c) :: n

end subroutine trisol0neumann

subroutine trisol1neumann(n, k, d, b, x)
!f2py intent(c) trisol1neumann
  implicit none
  integer :: n, i, ii
  real(8) :: k, d(n), b(n), s1(n), s2(n), x(n), mk2


!f2py real(8), intent(out), dimension(n) :: x
!f2py real(8), intent(c), dimension(n) :: d
!f2py real(8), intent(c), dimension(n) :: b
!f2py real(8), intent(c) :: k
!f2py integer, intent(c) :: n

end subroutine trisol1neumann

subroutine godunovstep1(n, h, kcfl, u, v)
!f2py intent(c) godunovstep1
  implicit none
  integer :: n
  real(8) :: h, kcfl, kcfl4, u(n), v(n), ut(n), rhs(n)

!f2py integer, intent(c) :: n
!f2py real(8), intent(c) :: h
!f2py real(8), intent(c) :: kcfl
!f2py real(8), intent(c), dimension(n) :: u
!f2py real(8), intent(out), dimension(n) :: v

end subroutine godunovstep1

subroutine godunovstep2(n, h, k, u, kap0, kaph, v)
!f2py intent(c) godunovstep2
  implicit none
  integer :: n, i
  real(8) :: h, k, u(n), kap0(n), kaph(n), v(n), ut(n), uc(n), uh(n), ue(n), ur(n), dA(n)

!f2py integer, intent(c) :: n
!f2py real(8), intent(c) :: h
!f2py real(8), intent(c) :: k
!f2py real(8), intent(c), dimension(n) :: u
!f2py real(8), intent(c), dimension(n) :: kap0
!f2py real(8), intent(c), dimension(n) :: kaph
!f2py real(8), intent(out), dimension(n) :: v

end subroutine godunovstep2


subroutine test(nx, ny, r_map, s_map)
!f2py intent(c) test

  implicit none
  integer :: nx, ny 
  real(8) :: r_map(nx,ny), s_map(nx,ny)

!f2py integer, intent(c) :: nx
!f2py integer, intent(c) :: ny
!f2py real(8), intent(c), dimension(nx,ny) :: r_map
!f2py real(8), intent(out), dimension(nx,ny) :: s_map

  s_map = r_map

end subroutine test


subroutine smooth7(nx, ny, r_map, w_map, s_map)
!f2py intent(c) smooth7
  implicit none
  logical :: colpflg, colmflg
  integer :: nx, ny, col, row, L, w_map(nx,ny), rowp, rowm, pix_row, pix_col, colp, colm
  real(8) :: r_map(nx,ny), s_map(nx,ny), wtot, w_col, w_row, summ

!f2py integer, intent(c) :: nx
!f2py integer, intent(c) :: ny
!f2py real(8), intent(c), dimension(nx,ny) :: r_map
!f2py integer, intent(c), dimension(nx,ny) :: w_map
!f2py real(8), intent(out), dimension(nx,ny) :: s_map


end subroutine smooth7
