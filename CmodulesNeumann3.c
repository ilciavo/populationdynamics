//Compile with :f2pyA -c --verbose --opt="O2"  CmodulesNeumann3.pyf CmodulesNeumann3.c
/*
!  G.E. Forsythe and C.B. Moler " Computer 
!  solutions to linear algebraic systems,"
!  Prentice-Hall publ., 1967
!  Tridiagonal system is Tx=b, where
!
!  T = [1     0   0    ....          0 ]
!      [-k/2 d(2) -k/2 ....          0 ]
!      [0    -k/2 d(3) -k/2  0 ....  0 ]
!      [0           ....             0 ]
!      [0              -k/2 d(n-1) -k/2]
!      [0                0    0      1 ]
! 
! and k = h/dx^2 is the CFL factor. Diagonals
! d = (1 d(2) d(3) ...  d(n-1) 1) are strong.
! s1 and s2 are work arrays.(Leo: s2 new diagonal, s1 new off-diagonal)  
! In the original F & M notation,
!
!  T = [d(1)  f(1)  0        ...             0 ]
!      [e(2)  d(2)  f(2)  0  ...             0 ]
!      [ 0          ....                     0 ]
!      [ 0                e(n-1) d(n-1) f(n-1) ]
!      [ 0                  0    e(n)   d(n)   ]
*/

#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>
#include<tgmath.h>

///------------NEUMANN SOLVERS------------////
void amult3neumann(int n, double* u, double *v)
{
    int i = 0;
    v[0] = - 2. * u[0] + u[1];
    v[n-1] = u[n-2] - 2. * u[n-1];
    for (i=1; i<n-1; ++i){
        v[i] = u[i-1]+u[i+1]-2*u[i];
    }

}

void trisol3neumann(int n, double k, double* d,double* b,double *x)
{
    int i, ii;
    //Performance counters
    //int muls=0, adds=0;
    //*(x) = *(d);
    //double s1[n];
    //double s2[n];
    //double *s1 = (double*)malloc(sizeof(double)*n);
    double *s1 = malloc(sizeof(double)*n);
    double *s2 = malloc(sizeof(double)*n);

    double mk2 = -k/2;

    //Neumann
    s2[0]=d[0];
    s1[1]=mk2/s2[0];
    s2[1]=d[1]+4.*s1[1]/3; 
    //s1[2]=mk2/s2[0];
    //s2[1]=d[1]+4.*s1[2]/3; //s1[2]???
    //printf("S2[%i]=%g\n", 0, s2[0]);
    //printf("S1[%i]=%g\n", 1, s1[1]); 
    //printf("S2[%i]=%g\n", 1, s2[1]); 

    //muls += 2; 
    //Forward elimination
    for (i=2; i<n-1; ++i){
        s1[i]=mk2/s2[i-1];
        s2[i]=d[i] - mk2*s1[i];

        //muls+=2;
        //++adds;
        //printf("S1[%i]=%g\n", i, s1[i]);
        //printf("S2[%i]=%g\n", i, s2[i]); 
    }

    //Neumann
    s1[n-1]=-4./(3.*s2[n-2]);
    s2[n-1]=d[n-1]-mk2*s1[n-1];

    //*(x) = *(b);
    x[0] = b[0];
    for (i=1; i<n; ++i){
        //*(x+i)=*(b+i)-s1[i]*(*(x+i-1));
        x[i]=b[i]-s1[i]*(x[i-1]);
        //++muls;
        //++adds;
    }

    //Backward substitution
    x[n-1]= x[n-1]/s2[n-1];
    ii = n-2;
    //++muls;

    for(i=0; i<n-2; ++i){
        x[ii]=(x[ii] - mk2*x[ii+1])/s2[ii];
        ii = ii-1;
        //muls+=3;
        //++adds ;
    }
    //x[0]=x[0]/s2[0];
    //++muls;
    //printf("muls=%i\n", muls);
    //printf("adds=%i\n", adds);     
    free(s1);
    free(s2);     
}


void godunovstepneumann3rd(int n, double h, double k, double* u, double* v)
{
    int i;
    double uest;
    //double ut[n], uE[n], ur[n], dA[n];
    double *ut = malloc(sizeof(double)*n);
    double *uE = malloc(sizeof(double)*n);
    double *ur = malloc(sizeof(double)*n);
    double *dA = malloc(sizeof(double)*n);
    

    //dA[0] = 1;
    //dA[n-1] = 1;

    amult3neumann(n, u, ut);
    for (i =0; i<n; ++i){
        //uE = k*ut + h*(1-u)*u;
        uE[i] = k*ut[i] + h*(1-u[i])*u[i];
        //ur = u + 0.5*uE;
        ur[i] = u[i]+ 0.5*uE[i];
        uE[i] = u[i] + uE[i];
        //printf("ut[%i]=%g\n", i, ut[i]);
        //printf("uE[%i]=%g\n", i, uE[i]);
        //printf("ur[%i]=%g\n", i, ur[i]); 

    }
    
    //impose Neumann on euler estimate 
    uE[n-1] = 4.*uE[n-2]/3. - (uE[n-3]/3.);
    uE[0] = (4.*uE[1]/3.) - (uE[2]/3.); //something veerry weird happening here... wrong results 

    //printf("uE[%i]=%g\n", 0, uE[0]);
    //printf("uE[%i]=%g\n", n-1, uE[n-1]);
    //printf("ur[%i]=%g\n", i, ur[i]); 


    uest= u[2] + 0.5*k*(uE[1]-2.*uE[2]+uE[3])+ 0.5*k*(u[1]-2.*u[2]+u[3]) + 0.5*h*((1.-uE[2])*uE[2]+(1.-u[2])*u[2]);
    ur[0]=-uest/3;

    //printf("ur[%i]=%g\n", 0, ur[0]);


    uest= u[n-3] + 0.5*k*(uE[n-4]-2*uE[n-3]+uE[n-2])+ 0.5*k*(u[n-4]-2*u[n-3]+u[n-2]) + 0.5*h*((1-uE[n-3])*uE[n-3]+(1-u[n-3])*u[n-3]);
    ur[n-1]=-uest/3;    

    //printf("ur[%i]=%g\n", n-1, ur[n-1]);


    dA[0] = 1.;
    dA[n-1] = 1.;

    for (i = 1; i<n-1; ++i){
        dA[i] = 1 + k - 0.5*h*(1-uE[i]); //possible error here 
        //printf("dA[%i]=%g\n", i, dA[i]); 
        
    }

    trisol3neumann(n, k, dA, ur, v);

    //impose Neumann on output
    v[0] = 4.*v[1]/3.-v[2]/3.;
    v[n-1] = 4.*v[n-2]/3. - v[n-3]/3;

    free(ut);
    free(uE);
    free(ur);
    free(dA);
}

void amultneumann(int n, double* u, double *v)
{
    int i = 0;
    //int muls=0, adds=0;
    v[0] = -2 * u[0] + u[1];
    v[n-1] = u[n-2] - 2 * u[n-1];
    for (i=1; i<n-1; ++i){
        v[i] = u[i-1] - 2 * u[i] + u[i+1] ;
        //++muls;
        //adds+=2;
    }
    //printf("muls=%i\n", muls);
    //printf("adds=%i\n", adds);
}

//  T = [1        -4/3   0    ....     0 ]
//      [-k/4 1+k/2 -k/4 ....          0 ]
//      [0    -k/4 1+k/2 -k/4  0 ....  0 ]
//      [0           ....              0 ]
//      [0               -k/4 1+k/2  -k/4]
//      [0                 0    -4/3   1 ]
void trisol0neumann(int n, double k, double* b,double *x)
{
    int i, ii;
    //*(x) = *(d);
    //double s1[n];
    //double s2[n];
    double *s1 = malloc(sizeof(double)*n);
    double *s2 = malloc(sizeof(double)*n);

    //First choice: Zero-Flux at coastal cell
    //a = -3/2h
    //b = 2/h
    //c = -1/2h
    //mca = -c/a 
    //ba = b/a
    //mba = -b/a
    const double mca = -1./3.;
    const double ba = -4./3.;
    const double mba = 4./3.;
    
    //Second Choice: Zero-Flux at interface
    //a = -1/h
    //b = 3/2h
    //c = -1/2h
    //mca = -c/a
    //ba = b/a 
    //mba = -b/a
    //const double mca = -1./2.;
    //const double ba = -3./2.;
    //const double mba = 3./2.;
    

    //constants
    const double mk4 = -k/4;
    const double k2 = k/2;

    //Neumann boundary conditions
    s2[0]=1;
    s1[1]=mk4;
    s2[1]= 1 + k2 + mba * s1[1];

    //muls+=3
    //++adds;
    //Forward elimination
    for (i=2; i<n-1; ++i){
        s1[i] = mk4 / s2[i-1];
        s2[i] = 1 + k2 - mk4 * s1[i];
        //muls+=2;
        
        //printf("S1[%i]=%g\n", i, s1[i]);
        //printf("S2[%i]=%g\n", i, s2[i]); 
    }
    s1[n-1]= ba / ( s2[n-2] );
    s2[n-1]= 1. - mk4 * s1[n-1];

    //*(x) = *(b);
    x[0] = b[0];

    for (i=1; i<n; ++i){
        //*(x+i)=*(b+i)-s1[i]*(*(x+i-1));
        x[i] = b[i] - s1[i] * ( x[i-1] ); 
    }

    //Backward substitution
    x[n-1] = x[n-1] / s2[n-1] ;
    ii = n - 2;

    for(i=0; i<n-2; ++i){
        x[ii]=( x[ii] - mk4 * x[ii+1] ) / s2[ii];
        ii = ii - 1;
    }

    //Leo: newline
    x[0] = (x[0] + mba * x[1])/s2[0];

    //x[0]=x[0]/s2[0];

    //Only use if necessary 
    //x[0] = 4.*x[1]/3. - x[2]/3.;
    //x[n-1] = 4.*x[n-2]/3. - x[n-3]/3.; //colosal error here 

    free(s1);
    free(s2);
}

//[1 b/a                  ...]
//[-k/2 d[1] -k/2         ...]
//[                          ]
//[          -k/2 d[n-2] -k/2]
//[                  b/a    1]
void trisol1neumann(int n, double k, double* d,double* b,double *x)
{
    int i, ii;
    //*(x) = *(d);
    //double s1[n];
    //double s2[n];
    double *s1 = malloc(sizeof(double)*n);
    double *s2 = malloc(sizeof(double)*n);

    //First choice: Zero-Flux at coastal cell
    //a = -3/2h
    //b = 2/h
    //c = -1/2h
    //mca = -c/a 
    //ba = b/a
    //mba = -b/a
    const double mca = -1./3.;
    const double ba = -4./3.;
    const double mba = 4./3.;
    
    //Second Choice: Zero-Flux at interface
    //a = -1/h
    //b = 3/2h
    //c = -1/2h
    //mca = -c/a
    //ba = b/a 
    //mba = -b/a
    //const double mca = -1./2.;
    //const double ba = -3./2.;
    //const double mba = 3./2.;

    //constant value 
    const double mk2 = -k/2;

    //Neumann boundary conditions 
    s2[0] = d[0];
    s1[1] = mk2 / s2[0];
    s2[1] = d[1] + mba * s1[1];

    //Forward elimination
    for (i=2; i<n-1; ++i){
        s1[i]=mk2/s2[i-1];
        s2[i]=d[i] - mk2*s1[i];
        //printf("S1[%i]=%g\n", i, s1[i]);
        //printf("S2[%i]=%g\n", i, s2[i]); 
    }
    s1[n-1]= ba / s2[n-2];
    s2[n-1]= d[n-1] - mk2 * s1[n-1];

    //*(x) = *(b);
    x[0] = b[0];
    for (i=1; i<n; ++i){
        //*(x+i)=*(b+i)-s1[i]*(*(x+i-1));
        x[i]= b[i] - s1[i] * x[i-1];
    
    }

    //Backward substitution
    x[n-1]= x[n-1] / s2[n-1];
    ii = n-2;

    for(i=0; i<n-2; ++i){
        x[ii]=( x[ii] - mk2 * x[ii+1] ) / s2[ii];
        ii = ii - 1;
    }

    //Leo: newline 
    x[0] = ( x[0] + mba * x[1] ) / s2[0];
    //x[0]=x[0]/s2[0];
    //Only use if necessary 
    //x[0] = 4.*x[1]/3. - x[2]/3.;
   // x[n-1] = 4.*x[n-2]/3.-x[n-3]/3.; //collosal error fixed  

    free(s1);
    free(s2);
}


void godunovstep1(int np2, double h, double kcfl, double* uw, double* v)
{
    int i;
    int n = np2-2;
    double a, b, c;
    //double ut[n], rhs[n];
    double *ut = malloc(sizeof(double)*n);
    double *rhs = malloc(sizeof(double)*n);
    double *u = malloc(sizeof(double)*n);
    double *v_loc = malloc(sizeof(double)*n);
 
    //First choice: Zero-Flux at coastal cell
    //a = -3/2h
    //b = 2/h
    //c = -1/2h
    //mca = -c/a 
    //ba = b/a
    //mba = -b/a
    const double mca = -1./3.;
    const double ba = -4./3.;
    const double mba = 4./3.;
    
    //Second Choice: Zero-Flux at interface
    //a = -1/h
    //b = 3/2h
    //c = -1/2h
    //mca = -c/a
    //ba = b/a 
    //mba = -b/a
    //const double mca = -1./2.;
    //const double ba = -3./2.;
    //const double mba = 3./2.;
 
    const double kcfl4 = kcfl / 4;
    const double kcfl2 = kcfl / 2;

    double ue, uest;

    //getting land cells
    for (i=0; i<n; ++i){
        u[i] = uw[i+1];
    }

    if (n>3){
        amultneumann(n, u, ut);

        for (i = 0; i<n; ++i){
            rhs[i] = u[i] + kcfl4 * ut[i];
            //printf("ut[%i]=%g\n", i, ut[i]); 
            //printf("rhs[%i]=%g\n", i, rhs[i]); 
        }

        ue = (1. - kcfl) * u[2] + kcfl2 * ( u[1] + u[3] );
        ue = fmax(ue, 0.);
        uest = ( 1 + kcfl4 ) * u[2] + kcfl4 *ue; 
        rhs[0] = mca * uest;

        //printf("rhs[%i]=%g\n", 0, rhs[0]); 

        ue = (1. - kcfl) * u[n-3] + kcfl2 * ( u[n-4] + u[n-2] );
        ue = fmax(ue, 0.);
        uest = ( 1 + kcfl4 )* u[n-3] + kcfl4 *ue; 
        rhs[n-1] = mca * uest; 

        //printf("rhs[%i]=%g\n", n-1, rhs[n-1]); 


        trisol0neumann(n, kcfl, rhs, v_loc);
        
        for (i=0; i<n; ++i){
            //printf("v_loc[%i]=%g\n", i, v_loc[i]);
            v[i+1] = fmax(v_loc[i], 0.);
        }
    }
    if (n==3){
        rhs[0] = (1. - kcfl2) * u[1] + kcfl4 * ( u[0]+ u[2] );
        v[1] = fmax(rhs[0], 0.);
        v[2] = v[1];
        v[3] = v[1];
    }

    if (n==2){
        rhs[0] = ( 1. - kcfl2 ) * u[0] + kcfl4 * u[1]; 
        rhs[1] = ( 1. - kcfl2 ) * u[1] + kcfl4 * u[0];
        a = 1. + kcfl2;
        b = - kcfl4;
        v[1] = ( a * rhs[0] - b * rhs[1] ) / ( a * a - b * b );
        v[2] = v[1];
    }

    if (n==1){
        v[1] = fmax(u[0], 0.);
    }

    free(ut);
    free(rhs);
    free(u);
    free(v_loc);

}


void godunovstep2(int np2, double h, double k, double* uw, double* kap0w, double* kaphw, double* v)
{
    int i;
    int n = np2-2;
    //double ut[n], uE[n], uC[n], uH[n], ur[n], dA[n];
    double *ut = malloc(sizeof(double)*n);
    double *uE = malloc(sizeof(double)*n);
    double *uC = malloc(sizeof(double)*n);
    double *uH = malloc(sizeof(double)*n);
    double *ur = malloc(sizeof(double)*n);
    double *dA = malloc(sizeof(double)*n);

    double *u = malloc(sizeof(double)*n);
    double *kap0 = malloc(sizeof(double)*n);
    double *kaph = malloc(sizeof(double)*n);
    double *v_loc = malloc(sizeof(double)*n);

    //First choice: Zero-Flux at coastal cell
    //a = -3/2h
    //b = 2/h
    //c = -1/2h
    //mca = -c/a 
    //ba = b/a
    //mba = -b/a
    const double mca = -1./3.;
    const double ba = -4./3.;
    const double mba = 4./3.;
    
    //Second Choice: Zero-Flux at interface
    //a = -1/h
    //b = 3/2h
    //c = -1/2h
    //mca = -c/a
    //ba = b/a 
    //mba = -b/a
    //const double mca = -1./2.;
    //const double ba = -3./2.;
    //const double mba = 3./2.;

    double uest, reL, reE, reH, rHS2, S2, kmax, reL1, reL2, reE0, reE1, r1, r2, a, b, c;

    //getting land cells
    for (i=0; i<n; ++i){
        u[i] = uw[i+1];
        kap0[i] = kap0w[i+1];
        kaph[i] = kaphw[i+1];
    }

    if (n>3){
        amultneumann(n, u, ut);

        for (i =0; i<n; ++i){
            uC[i] = u[i] / kap0[i];
            uE[i] = ut[i] + h * ( 1 - uC[i] ) * u[i];
            ur[i] = u[i] + 0.5 * uE[i];
            uE[i] = u[i] + uE[i];
            //printf("ut[%i]=%g\n", i, ut[i]);
            //printf("uE[%i]=%g\n", i, uE[i]);
            //printf("ur[%i]=%g\n", i, ur[i]); 
        }

        //Euler estimate for Neumann 

        uE[0] = fmax(mba * uE[1] + mca * u[2], 0.); //avoiding negative densities
        uE[0] = fmin(uE[0], kaph[0]); //saturation
        
        uE[n-1] =  fmax(mba * uE[n-2] + mca * u[n-3],0.); //avoiding negative values
        uE[n-1] = fmin(uE[n-1], kaph[n-1]); //saturation

        reL = u[2] / kap0[2];
        reH = uE[2] /kaph[2];
        uest = (1.-k)*u[2] + 0.5 * k * (uE[1] - 2*uE[2] + uE[3])
                            + 0.5 * k * (uE[1]+uE[3])
                             + 0.5 * h * ((1.-reH)*uE[2]+(1.-reL)*u[2]);
        uest = fmin(uest,kaph[0]);

        ur[0] = mca * uest; 

        reL = u[n-3] / kap0[n-3];
        reH = uE[n-3] / kaph[n-3];
        uest = (1-k)*u[n-3] + 0.5 * k * (uE[n-4] - 2*uE[n-3] + uE[n-2])
                            + 0.5 * k * (uE[n-4]+uE[n-2])
                             + 0.5 * h * ((1.-reH)*uE[n-3]+(1.-reL)*u[n-3]);
                
        uest = fmin(uest,kaph[n-3]);

        ur[n-1] = mca * uest;

        dA[0] = 1.;
        dA[n-1] = 1.;
        for (i = 1; i<n-1; ++i){
            uH[i] = uE[i]/ kaph[i];
            dA[i] = 1. + k - 0.5 * h * ( 1. - uH[i] );
            //printf("dA[%i]=%g\n", i, dA[i]);         
        }

        trisol1neumann(n, k, dA, ur, v_loc);

        for (i=0; i<n; ++i){
            //printf("v_loc[%i]=%g\n", i, v_loc[i]);
            v[i+1] = fmin(fmax(v_loc[i], 0.), kaph[i]);
        }
    }

    if(n==3){
        reL = u[1] / kap0[1];
        uC[0] = k * ( u[0] - 2 * u[1] + u[2]) + h *( 1 - reL ) * u[1];
        uE[0] = fmax(u[1] + uC[0], 0);
        uE[0] = fmin(uE[0], kaph[1]);
        reE = uE[0] / kaph[1];
        S2 = 1 - 0.5 * h * ( 1 - reE);
        rHS2 = u[1] + 0.5 * uC[0];
        v[2] = fmax(rHS2/S2, 0.);
        kmax = fmin(kaph[0], kaph[1]);
        kmax = fmin(kmax, kaph[2]);
        v[2] = fmin(v[2], kmax);
        v[1] = v[2];
        v[3] = v[2]; 
    }
    
    if(n==2){
        reL1 = u[0] / kap0[0];
        reL2 = u[1] / kap0[1];
        uE[0] = u[0] + k * ( u[1] - 2 * u[0] ) + h * ( 1. - reL1 )*u[0];
        uE[0] = fmax(uE[0],0);
        uE[0] = fmin(uE[0], kaph[0]);
        
        uE[1] = u[1] + k * ( u[0] - 2 * u[1] ) + h * ( 1. - reL1 )*u[1];
        uE[1] = fmax(uE[1],0);
        uE[1] = fmin(uE[1], kaph[1]);
        
        reE0 = uE[0] / kap0[0];
        reE1 = uE[1] / kap0[1];
        r1 = ( 1 - k ) * u[0] + 0.5 * k * u[1] + 0.5 * h * ( 1 - reL1 ) * u[0];
        r2 = ( 1 - k ) * u[1] + 0.5 * k * u[0] + 0.5 * h * ( 1 - reL1 ) * u[1];

        //building matrix
        a = 1 + k - 0.5 * h * (1 - reE0);
        b = - 0.5 * k;
        c = 1 + k - 0.5 * h * (1 - reE1);

        //solving 2x2 system
        v[1] = (a * r1 - b * r2) / ( a * c - b * b) ;
        v[2] = (r2 - b * v[1]) / c ;
        v[1] = fmin(v[1], kaph[0]) ;
        v[2] = fmin(v[2], kaph[1]);
        kmax = fmin(kaph[0], kaph[1]);
        v[1] = fmax(0.5*(v[1]+v[2]),0.); 
        v[1] = fmin(v[1], kmax);
        v[2] = v[1]; 
    }

    if(n==1){
        reL = u[0] / kap0[0];
        uE[0] = ( 1 + h * ( 1 - reL ) ) * u[0];
        uE[0] = fmax(uE[0],0.);
        uE[0] = fmin(uE[0], kaph[0]);
        reE = uE[0] / kaph[0];
        v[0] = ( 1 + 0.5 * h * ( 1. - reL ) ) * u[0] / ( 1 - 0.5 * h * (1-reE));
        v[0] = fmax(v[0],0);
        v[0] = fmin(v[1], kaph[0]);
    }

    free(ut);
    free(uE);
    free(uC);
    free(uH);
    free(ur);
    free(dA);
    free(u);
    free(kap0);
    free(kaph);
    free(v_loc);
}


/////--------------test------------------------

void test(int nx, int ny, double* r_map, double* s_map)
{
    int col, row;
    for (col =0; col<ny; ++col){
        for (row = 0; row<nx; ++row ){
            //s_map[row,col] = r_map[row,col];
            //r_map is read in column mayor order: Fortran 
            //s_map is stored in row mayor order: C
            s_map[row+col*nx] = r_map[col+row*ny];

        }
    }

}

void smooth7(int nx, int ny, double* r_map, int * w_map, double* s_map)
{
    double L = 5;
    double wtot, summ, w_col, w_row;
    bool colpflg, colmflg;
    int col, row, colp, colm, pix_col, pix_row, rowp, rowm; 

    for (col = 0; col<ny; ++col)
    {
        for(row = 0; row <nx; ++ row)
        {
            if (w_map[col+row*ny]>0)
            { //Taking only land points
            //Leo Test
            //s_map[row+col*nx] = 1.;
            
                wtot = 1.; //Total Weight 
                summ = r_map[col+row*ny]; //summing values
    
                //scanning points on a cross 
                //Left/right on X
                for (pix_col = 1; pix_col<L; ++pix_col)
                {
                    //flags to track land
                    colpflg = true; 
                    colmflg = true;
                    //column plus pixel smoother
                    colp = col + pix_col - 1;
                    //column minus pixel smoother
                    colm = col - pix_col +1;

                    if(colp>=nx) colpflg = false; // flag out of the map
 
                    if(colm<0) colmflg = false; // flag out of the map 
 
                    //weighting factor in x 
                    w_col = (1.-(1.*pix_col/L)*(1.*pix_col/L));
 
                    if ( colpflg && (w_map[colp+row*ny]>0))
                    {
                        //inside the map 
                        wtot = wtot+w_col; // updating total weight 
                        summ = summ + w_col*r_map[colp+row*ny]; // updating sum
                    }
                    
                    if ( colmflg && (w_map[colm+row*ny]>0))
                    {
                        //inside the map 
                        wtot= wtot+w_col; // updating total weight 
                        summ = summ + w_col*r_map[colm+row*ny];   // updating sum
                    }

                }  

                //Bottom/Up in Y 
                for (pix_row = 1; pix_row<L; ++pix_row)
                {
                    //flags to track land
                    //colpflg = true 
                    //colmflg = true
                    //column plus pixel smoother
                    rowp = row + pix_row - 1;
                    //column minus pixel smoother
                    rowm = row - pix_row + 1;

                    if(rowp>=ny)
                    {
                        //colpflg = .FALSE. ! flag out of the map
                        rowp = rowp - ny; 
                    }
 
                    if(rowm<0)
                    { 
                        //colmflg = .FALSE. !flag out of the map 
                        rowm = rowm + ny; 
                    }
 
                    //weighting factor in x 
                    w_row =(1.-(1.*pix_row/L)*(1.*pix_row/L));
 
                    if (w_map[col+rowm*ny]>0)
                    {
                        //inside the map 
                        wtot = wtot+w_row; // updating total weight 
                        summ = summ + w_row*r_map[col+rowp*ny];   //updating sum
                    }
                    
                    if (w_map[col+rowm*ny]>0)
                    { 
                        //inside the map 
                        wtot= wtot+w_row; // updating total weight 
                        summ = summ + w_row*r_map[col+rowm*ny];   // updating sum
                    }

                } //End of points on cross
                
                //Now smooth the square
                for(pix_col = 1; pix_col<L; ++pix_col)
                {
                    colpflg = true;
                    colmflg = true;
                    colp = col + pix_col - 1;
                    colm = col - pix_col + 1;

                    if (colp >= nx) colpflg = false; 
                
                    if (colm < 0) colmflg = false;
                    
                    w_col = (1.-(1.*pix_col/L)*(1.*pix_col/L));
                    
                    for (pix_row = 1;pix_row<L;++pix_row)
                    {
                        //rowpflg = .TRUE.
                        //rowmflg = .TRUE.
                        rowp = row + pix_row - 1;
                        rowm = row - pix_row + 1;

                        if (rowp >= ny)
                        {
                            //colpflg = .FALSE.
                            rowp = rowp - ny; 
                        }
                
                        if (rowm < 0)
                        {
                            //colmflg = .FALSE.
                            rowm = rowm + ny; 
                        }
                        
                        w_row =(1.-(1.*pix_row/L)*(1.*pix_row/L));
                        
                        if(colpflg)
                        { //inside the map from the right 
                            if (w_map[colp+rowm*ny]>0)
                            { 
                                wtot = wtot + w_row*w_col;
                                summ = summ + w_row*w_col*r_map[colp +rowm*ny];
                            }
                            
                            if (w_map[colp+rowp*ny]>0)
                            {
                                wtot = wtot + w_row*w_col;
                                summ = summ + w_row*w_col*r_map[colp+rowp*ny];
                            }
                             
                        }
  
                        if (colmflg) 
                        { //inside the map from the left
                            if (w_map[colm+rowm*ny]>0)
                            {
                                wtot = wtot + w_row*w_col;
                                summ = summ + w_row*w_col*r_map[colm+rowm*ny];
                            }
                            
                            if (w_map[colm+rowp*ny]>0) 
                            {
                                wtot = wtot + w_row*w_col;
                                summ = summ + w_row*w_col*r_map[colm+rowp*ny];
                            }                             

                        } 
                                             
                    } 

                }//Finish square scan  
            
                s_map[col*nx+row] = summ/wtot; //average of points in land
            
            }
            else
            {
                s_map[row+col*nx] = 0.; // pixel is in water

            } 
            //s_map(col,row) = L*(r_map(col,row) + w_map(col,row))
        }
    }

}



int  main(){
return 0; 
}
