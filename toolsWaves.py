from numpy import *
#from scipy.integrate import odeint
from matplotlib.pyplot import plot, xlabel, ylabel, figure, axes
import matplotlib.cm as cm
#import time 
from timeit import default_timer as timer
#from Fmodules import smooth7 as smooth7F
#from Cmodules import smooth7 as smooth7F
from Pymodules import  sciSmooth
import Fmodules as modules 
import h5py 
#import netCDF4
 
def createWrlMap(name):
    if name == 'small':
        w_map=genfromtxt('./maps/MAPS100x150/world_100x50.txt')
        #w_map=modules.genfromtxt('/Users/Leo/Desktop/MasterThesis/PopulationDynamics/Code/WesleyCode/mapsimleo/world_100x50.txt',100, 50)
        #x=longitude, y=latitude; circshift adjusts data such that the West coast of Africa is not at the border
        w_map=roll(w_map,2,axis=1)
        
    if name == 'fep1':
        import netCDF4
        data = netCDF4.Dataset('./maps/MAPS360x720x1/fep_test.nc','r')
        w_map = flipud(data.variables['FEP'][:].data)
        w_map = roll(w_map,-310,axis=1)
        w_map[w_map>0]=1
        w_map[w_map<0]=0

    if name == 'testSmall':
        w_map=zeros((50,100))
        w_map[10:40,10:40]=1
        w_map[-40:-10,-40:-10]=1
        w_map

    if name == 'testLarge':
        w_map=zeros((360,720))
        w_map[72:288,72:288]=1
        w_map[-288:-72,-288:-72]=1
        w_map

        
    #set all border pixels to zero
    #Leo: this will probably change 
    w_map[:,0]=0.0
    w_map[:,-1]=0.0
    w_map[0,:]=0.0
    w_map[-1,:]=0.0
    
    #return flipud(w_map)
    return w_map

def initu(u,w_map,orig_x,orig_y,dx,dy):
#initialize to small normal distr. around orig_x, orig_y
    NY,NX = w_map.shape
    orig_y = (orig_y)*NY/50
    orig_x = (orig_x)*NX/100
    
    rx = int(3*NX/100)
    ry = int(3*NY/50)
    
    for j in range(1,2*ry):
        ry2 = ((j-ry)*dy)**2
        for i in range (1,2*rx):
            r2  = ry2 + ((i-rx)*dx)**2 # r^2=dy^2+dx^2
            if r2<(rx*ry): # radius <3
            #Gaussian variant: if distr. is out of range (w_map=0), set to zero
                u[i+orig_y-ry,j+orig_x-rx] = exp(-r2)*w_map[i+orig_y-ry,j+orig_x-rx]
            else:
                u[i+orig_y-ry,j+orig_x-rx] = 0
    
    return u 

def FXscan(mask):
    xstart_size, xend_size=modules.alloc_xscan(mask)
    return modules.xscan(mask,xstart_size,xend_size)

def FYscan(mask):
    ystart_size, yend_size=modules.alloc_yscan(mask)
    return modules.yscan(mask,ystart_size,yend_size)

"""
(nxsegs start_seg end_seg)= Xscan(NX,NY,w_map)
scan through X-direction, enumerate all X-direction land segments
nxsegs: number of pieces of land per row
start_seg: indexes of starting pieces of land 
end_seg: index of ending pieces of land 
"""
def Xscan(w_map):
    NY,NX = w_map.shape
    #NX,NY = w_map.shape #to match matlab code 
    segmentCounter = 0
    nxsegs=zeros(NY,dtype=int)
    start_seg = []
    end_seg = []
    for row in range(0,NY):
        nxsegs[row] = 0
        segmentStarted = False 
        segmentCounterRow = 0
        for column in range(0,NX):
            #print 'i=',i,'j=',j
            if w_map[row,column]>0 or w_map[row,column]: #Leo: check this w_map[row, column]==True:
            #if w_map[column,row]>0 or w_map[column, row]: Leo: this is to match matlab X:N-S Y:W-E
                if not segmentStarted: #Begining of a segment
                    nxsegs[row] = nxsegs[row]+1
                    #xstart_seg[segmentCounter+segmentCounterRow] = j
                    start_seg.append(column)
                    segmentStarted = True 
                 
            else:
                if segmentStarted: #End of the segment
                    #xend_seg[segmentCounter+segmentCounterRow] = j-1
                    end_seg.append(column-1)
                    segmentCounterRow = segmentCounterRow+1
                    segmentStarted = False
                    
        segmentCounter = segmentCounter + nxsegs[row]
        #print segmentCounter
        
    start_seg = array(start_seg)
    end_seg = array(end_seg)
    return (nxsegs,start_seg,end_seg)
    #return (nxsegs,xstart_seg,xend_seg)

"""
(nysegs start_seg end_seg)= Yscan(NX,NY,w_map)
scan through Y-direction, enumerate all Y-direction land segments
nxsegs: number of segments per row
start_seg: starting index for segment
end_seg: ending index for segment
"""
def Yscan(w_map):
    NY,NX = w_map.shape
    #NX,NY = w_map.shape #to match matlab code 
    segmentCounter = 0
    nysegs=zeros(NX,dtype=int)
    start_seg = []
    end_seg = []
    for column in range(0,NX):
        nysegs[column] = 0
        segmentStarted = False 
        segmentCounterColumn = 0
        for row in range(0,NY):
            #print 'i=',i,'j=',j
            if w_map[row,column]>0 or w_map[row, column]: # Leo: check this or w_map[row,column] ==True:
            #if w_map[column,row]>0 or w_map[column, row]: # Leo: This is to match matlab code 
                if not segmentStarted:
                    import numpy as np
                    nysegs[column] = nysegs[column]+1
                    #xstart_seg[segmentCounter+segmentCounterRow] = j
                    start_seg.append(row)
                    segmentStarted = True
            else:
                if segmentStarted:
                    #xend_seg[segmentCounter+segmentCounterRow] = j-1
                    end_seg.append(row-1)
                    segmentCounterColumn = segmentCounterColumn+1
                    segmentStarted = False
        
        segmentCounter = segmentCounter + nysegs[column]
        
    start_seg = array(start_seg)
    end_seg = array(end_seg)
    return (nysegs,start_seg,end_seg)


def yppoints():
    #tests some sigmoid approximation 
    #sigmoid(y) = 1/(1+exp(-y)) with mapping
    # y(t) = -1/y + 1/(1-y)
    yp = linspace(0.0,1.0,101)
    #compute interpolation points yp[0] = 0 yp[100] = 1 
    yp[1:100]=1/(1+exp((1-2*yp[1:100])/(yp[1:100]*(1-yp[1:100]))))
    return yp 

def get_cc_list(name=None):
    import os
    if name is 'small':
        filename = []
        epoch=[]
        folder = './maps/MAPS100x150/' 
        for file in os.listdir(folder):
            if file.endswith(".asc"):
                #appending namefile 
                filename.append(file)
                broken=file.split('_')
                #appending epoch 
                epoch.append(int(broken[2][0:3]))    

        filename, epoch=zip(*sorted(zip(filename, epoch), key = lambda x: x[1], reverse=True ))
        return (filename,epoch)

    #Leo: or name 'fep' use for kaplan map??
    if name is 'large':
        filename = []
        epoch_cc=[]
        mask=[]
        epoch_mask=[]
        folder = './maps/MAPS360x720'
        for file in os.listdir(folder):
            if file.endswith(".asc"):
                broken=file.split('_')
                if broken[0]=='cc':
                    #appending namefile carrying capacity
                    filename.append(file)
                    #appending epoch 
                    epoch_cc.append(int(broken[1][0:3]))
                else:
                    #appending namefile for mask
                    mask.append(file)
                    epoch_mask.append(int(broken[1][0:3]))

        filename, epoch=zip(*sorted(zip(filename, epoch_cc), key = lambda x: x[1], reverse=True ))
        mask, epoch=zip(*sorted(zip(mask, epoch_mask), key = lambda x: x[1], reverse=True ))
        return (filename,mask,epoch)
    
    if name is 'fep1' or name is 'fep2':
        filename = []
        epoch_cc=[]
        mask=[]
        epoch_mask=[]
        folder = './maps/MAPS360x720x1'
        for file in os.listdir(folder):
            if file.endswith(".asc"):
                broken=file.split('_')
                if broken[0]=='cc':
                    #appending namefile carrying capacity
                    filename.append(file)
                    #appending epoch 
                    epoch_cc.append(int(broken[1][0:3]))
                else:
                    #appending namefile for mask
                    mask.append(file)
                    epoch_mask.append(int(broken[1][0:3]))
                    
                    
        filename, epoch=zip(*sorted(zip(filename, epoch_cc), key = lambda x: x[1], reverse=True ))
        mask, epoch=zip(*sorted(zip(mask, epoch_mask), key = lambda x: x[1], reverse=True ))
        return (filename,mask,epoch)
    
    if name is 'huge':
        filename = []
        epoch_cc=[]
        mask=[]
        epoch_mask=[]
        folder = './maps/MAPS720x1440'
        for file in os.listdir(folder):
            if file.endswith(".asc"):
                broken=file.split('_')
                if broken[0]=='cc':
                    #appending namefile carrying capacity
                    filename.append(file)
                    #appending epoch 
                    epoch_cc.append(int(broken[1][0:3]))
                else:
                    #appending namefile for mask
                    mask.append(file)
                    epoch_mask.append(int(broken[1][0:3]))

        filename, epoch=zip(*sorted(zip(filename, epoch_cc), key = lambda x: x[1], reverse=True ))
        mask, epoch=zip(*sorted(zip(mask, epoch_mask), key = lambda x: x[1], reverse=True ))
        return (filename,mask,epoch)
    



def kapepochNew(w_map, yalist, iepoch, mapsize):
    # input iepoch indicates map with name(iepoch)
    # Imports Jody's CC maps, one/time
    # map indexed by iepoch, list of names is in list
    
    ascname = yalist[iepoch]
    kyasuf  = ascname[11:13]     # Kya suffix from asci name
    
    #wj_map=importdata(char(ascname))
    if mapsize is 'small':
        folder='./maps/MAPS100x150/'
        #kmax = 30000
        kmax = 28628.909463
        #wj_map=modules.genfromtxt('/Users/Leo/Desktop/MasterThesis/PopulationDynamics/Code/WesleyCode/mapsimleo/world_100x50.txt',100, 50)
    
    if mapsize is 'large':
        folder='./maps/MAPS360x720/'
        kmax = 172.2810 #This value was destroying everything
        #kmax = 172.28096
        #kmax = 172.280976
        #wj_map=modules.genfromtxt(folder+ascname,720, 360)
        
    if mapsize is 'huge':
        folder='./maps/MAPS720x1440/'
        kmax = 172.2810 #This value was destroying everything
        #kmax = 43.18020
        #kmax = 43.180204 #This value was destroying everything
        #wj_map=modules.genfromtxt(folder+ascname,1440, 720)

    wj_map=genfromtxt(folder+ascname)
    
    #w_cmap = wj_map
    
    rows, cols = wj_map.shape
    #NX = Grid_size_x #rows, cols
    #NY = Grid_size_y
    wtemp=zeros_like(wj_map)
    
    # 1 pixel shift 
    # Jodys maps are shifted by one pixel:
    # Leo: this can change depending on resolution
    wj_map = roll(wj_map,-2,0)
    
    # fill in non-master zeros
    #ijcnt = 0
    thresh = 5.e-7
    w_cmap=zeros_like(wj_map)
    for row in range(0,rows):
        aveLat = 0 
        cnt = 0
        for col in range(0,cols): #Scan row 
            if wj_map[row,col]>0:
                aveLat = aveLat + wj_map[row,col] #sum of values greater than 0 
                cnt = cnt + 1 #number of positive values within a row  
        
        if cnt > 0:
            aveLat = aveLat/cnt #Average value for a row  
            
        for col in range(0,cols): 
            if w_map[row,col]>0: # mask 
                if wj_map[row,col] > 0: #carrying capacities
                    #copying carrying capacities for land with carrying capacity >0 
                    w_cmap[row,col] = wj_map[row,col]  
                else:
                    if aveLat > thresh: #row average greather than threshold 
                        w_cmap[row,col] = aveLat #getting a carrying capacity for new land
                        #ijcnt = ijcnt + 1 # new land count 
                    else:
                        # average value of carrying capacity less than threshold 
                        w_cmap[row,col] = thresh 
            else:
                w_cmap[row,col] = 0 #Carrying capacity 0 

    if mapsize is 'small':
        #w_cmap = smooth7(NX,NY,w_cmap,w_map);    # quadratic low pass filter
        w_cmap = modules.smooth7(w_cmap,w_map);    # quadratic low pass filter
    
    #Remove kinks 
    #w_cmap = modules.smooth7(w_cmap,w_map);    # quadratic low pass filter
        
    kap = w_cmap/kmax                       # smoothed, renormalized CC
    
    return kap

def kapepoch(w_map, yalist, iepoch, mapsize):
    # input iepoch indicates map with name(iepoch)
    # Imports Jody's CC maps, one/time
    # map indexed by iepoch, list of names is in list
    
    ascname = yalist[iepoch]
    #kyasuf  = ascname[11:13]     # Kya suffix from asci name
    
    #wj_map=importdata(char(ascname))
    if mapsize is 'small':
        folder='./maps/MAPS100x150/'
        kmax = 28628.909463
        #kmax = 30000
        #wj_map=modules.genfromtxt('/Users/Leo/Desktop/MasterThesis/PopulationDynamics/Code/WesleyCode/mapsimleo/world_100x50.txt',100, 50)
    
    if mapsize is 'large':
        folder='./maps/MAPS360x720/'
        #kmax = 172.2810 #This value was destroying everything
        #kmax = 172.28096
        kmax = 172.280976
        #wj_map=modules.genfromtxt(folder+ascname,720, 360)
        
    if mapsize is 'fep1':
        folder='./maps/MAPS360x720x1/'
        #kmax = 172.2810 #This value was destroying everything
        #kmax = 172.28096
        kmax = 172.280976
        if iepoch == 61: #Modern age
            #Leo: for FEP
            #kmax = 1.
            #Leo: for NPP
            kmax = 2012.1597
        #wj_map=modules.genfromtxt(folder+ascname,720, 360)
        
    if mapsize is 'fep2':
        ascname = 'cc_000k_720x360.asc'
        folder='./maps/MAPS360x720x2/'
        #kmax = 172.2810 #This value was destroying everything
        #kmax = 172.28096
        #kmax = 172.280976
        #if iepoch == 61: #Modern age
            #Leo: for FEP
            #kmax = 1.
            #Leo: for NPP
            #kmax = 2012.1597
        kmax = 2012.1597
        #wj_map=modules.genfromtxt(folder+ascname,720, 360)
        
    if mapsize is 'huge':
        folder='./maps/MAPS720x1440/'
        #kmax = 43.180
        kmax = 43.180204 #This value was destroying everything
        #kmax = 172.2810 #This value was destroying everything
        #wj_map=modules.genfromtxt(folder+ascname,1440, 720)
        
   

    wj_map=genfromtxt(folder+ascname)
    
    #w_cmap = wj_map
    

    Grid_size_x, Grid_size_y = wj_map.shape
    NX = Grid_size_x
    NY = Grid_size_y
    wtemp=zeros_like(wj_map)
    
    # Jodys maps are shifted by one pixel
    # This is for a small map 
    if mapsize is 'small':
        for i in range(0,NX):
            ip1 = i+1
            if ip1>NX-1: 
                ip1=0
            for j in range(0,NY):
                jm1 = j-1
                if jm1<0:
                    jm1=NY-1

                wtemp[i,j] = wj_map[ip1,jm1] #problem ip1 =50

        wj_map = wtemp
        del wtemp
    else:
        wj_map = roll(wj_map,2,1)
    


    # fill in non-master zeros
    ijcnt = 0
    thresh = 5.e-7
    w_cmap=zeros_like(wj_map)
    for i in range(0,NX):
        avelong = 0 
        cnt = 0
        for j in range(0,NY):
            if wj_map[i,j]>0:
                avelong = avelong + wj_map[i,j]
                cnt = cnt + 1
        
        if cnt > 0:
            avelong = avelong/cnt
            
        for j in range(0,NY):
            if w_map[i,j]>0:
                if wj_map[i,j] > 0:
                    w_cmap[i,j] = wj_map[i,j]
                else:
                    if avelong > thresh:
                        w_cmap[i,j] = avelong
                        ijcnt = ijcnt + 1
                    else:
                        w_cmap[i,j] = thresh
            else:
                w_cmap[i,j] = 0

    if mapsize is 'small':
        #w_cmap = smooth7(NX,NY,w_cmap,w_map);    # quadratic low pass filter
        w_cmap = modules.smooth7(w_cmap,w_map);    # quadratic low pass filter
        #Leo: map is flipped
        #w_cmap = flipud(w_cmap)
    
    #Remove kinks for new pieces of land 
    #w_cmap = modules.smooth7(w_cmap,w_map);    # quadratic low pass filter
    
    kap = w_cmap/kmax                       # smoothed, renormalized CC
    
    if mapsize is 'fep1' or mapsize is 'fep2':        
        #computing food extraction potential 
        x = kap/0.22364030407898464
        fep = 2.*(x)/(x**2+1.)
        return fep
    else:
        return kap

def templateMap(name, masks, kmp):
    if name == 'small':
        t_map=genfromtxt('./maps/MAPS100x150/world_100x50.txt')
        #t_map=modules.genfromtxt('/Users/Leo/Desktop/MasterThesis/PopulationDynamics/Code/WesleyCode/mapsimleo/world_100x50.txt',100, 50)
        #x=longitude, y=latitude; 
        #roll adjusts data such that the West coast of Africa is not at the border
        #similar to  circshift 
        t_map=roll(t_map,2,axis=1)
        
    if name == 'fep1':
        if kmp <= 60:
            ascname = masks[kmp]
            fileName = './maps/MAPS360x720/'+ascname
            #t_map =  flipud(genfromtxt(fileName))        
            t_map =  genfromtxt(fileName)
            #t_map=modules.genfromtxt(fileName,720, 360)
            t_map=roll(t_map,2,axis=1)
        else: 
            import netCDF4
            data = netCDF4.Dataset('./maps/MAPS360x720x1/fep_test.nc','r')
            t_map = flipud(data.variables['FEP'][:].data)
            t_map = roll(t_map,-322,axis=1)
            t_map[t_map>0]=1
            t_map[t_map<0]=0

    if name == 'fep2':
        fileName = './maps/MAPS360x720x1/mask_000k_720x360.asc'
        t_map =  genfromtxt(fileName)
        t_map=roll(t_map,2,axis=1)
        #import netCDF4
        #data = netCDF4.Dataset('./maps/MAPS360x720x1/fep_test.nc','r')
        #t_map = flipud(data.variables['FEP'][:].data)
        #t_map = roll(t_map,-322,axis=1)
        #t_map[t_map>0]=1
        #t_map[t_map<0]=0

    if name == 'large':
        ascname = masks[kmp]
        fileName = './maps/MAPS360x720/'+ascname
        #t_map =  flipud(genfromtxt(fileName))        
        t_map =  genfromtxt(fileName)
        #t_map=modules.genfromtxt(fileName,720, 360)
        t_map=roll(t_map,2,axis=1)

    if name == 'huge':
        ascname = masks[kmp]
        fileName = './maps/MAPS720x1440/'+ascname
        #t_map =  flipud(genfromtxt(fileName))        
        t_map =  genfromtxt(fileName)
        #t_map=modules.genfromtxt(fileName,1440, 720)
        t_map=roll(t_map,2,axis=1)
        
    if name == 'testSmall':
        t_map=zeros((50,100))
        t_map[10:40,10:40]=1
        t_map[-40:-10,-40:-10]=1
        t_map

    if name == 'testFEP':
        t_map=zeros((360,720))
        t_map[72:288,72:288]=1
        t_map[-288:-72,-288:-72]=1
        t_map

        
    #set all border pixels to zero
    #Leo: this will probably change 
    t_map[:,0]=0.0
    t_map[:,-1]=0.0
    t_map[0,:]=0.0
    t_map[-1,:]=0.0
    
    #return flipud(w_map)
    return t_map

def fillins(wI, mI , m_LH, mapsize):
    #This function fills in zeros of wI with threshold values if m_LH ==1 but mI == 0 
    thresh = 5.0e-7
    if mapsize == 'large':
        kmax = 172.2810 #Leo: it depends on mapsize
        
    if mapsize == 'fep1':
        kmax = 172.2810 #Leo: it depends on mapsize
        
    if mapsize == 'fep2':
        kmax = 2012.1597
        
    if mapsize == 'huge':
        kmax = 43.18020
        
    kth = kmax*thresh    
    w0 = wI.copy() 
    mask = (mI==1)^(m_LH==1) #xor
    w0[mask]=kth
    return w0

def storeWrlMap(mapsize):
    w_map = createWrlMap(mapsize)
    #Access file
    f = h5py.File(mapsize+'_maps.hdf5', 'w')
    #Create dataset 
    dset=f.create_dataset(mapsize+'/masks/w_map',w_map.shape,dtype='i')
    #Store data
    dset=w_map
    #Close file
    f.close()

def readWrlMap(mapsize):
    #open file
    f = h5py.File(mapsize+'_maps.hdf5', 'r')
    #read data
    data = f[mapsize+'/masks/w_map']
    mask=array(data)
    f.close()
    return mask

def storeTemplateMaps(mapsize, masks):
    #Access file
    f = h5py.File(mapsize+'_maps.hdf5', 'w')
    for kmp in range(0,len(masks)-1):
        #w_map = templateMap(mapsize, masks, kmp)
        m_L = templateMap(mapsize, masks, kmp)
        m_H = templateMap(mapsize, masks, kmp+1)
        m_LH = array((m_L==1)|(m_H==1),dtype=int) #>0 or ==0 ??
        #Create dataset 
        print('Storing:', masks[kmp])
        #dset=f.create_dataset(mapsize+'/masks/'+masks[kmp],m_LH.shape,dtype='i')
        dset=f.create_dataset(mapsize+'/masks/'+masks[kmp],data=m_LH)
        #Store data
        dset=m_LH
        
    #Close file
    f.close()

def storeCCold(mapsize, masks, kapList):
    #Access file
    f = h5py.File(mapsize+'_kap.hdf5', 'w')
    kmp=0
    #w_map = templateMap(mapsize, masks, kmp)
    m_L = templateMap(mapsize, masks, kmp)
    m_H = templateMap(mapsize, masks, kmp+1)
    m_LH = array((m_L==1)|(m_H==1),dtype=int)
    kapL=kapepoch(m_LH, kapList, kmp, mapsize)
    kapH=kapepoch(m_LH, kapList, kmp+1, mapsize)
    kapL=fillins(kapL, m_L, m_LH, mapsize)
    kapH=fillins(kapH, m_H, m_LH, mapsize)
    #Create dataset 
    print('Storing:', kapList[kmp])
    #Store data
    dsetL=f.create_dataset(mapsize+'/kapL/'+kapList[kmp],data=kapL)
    dsetH=f.create_dataset(mapsize+'/kapH/'+kapList[kmp],data=kapH)
    kmp = kmp + 1
    while(kmp < len(masks)-1):
        m_L = m_H
        kapL = kapH                 # set lower map to prev. upper
        m_H = templateMap(mapsize, masks, kmp+1) #error here !!!! this is supposed to be m_H not w_H 
        m_LH = (m_L==1) | (m_H==1)
        kapH=kapepoch(m_LH, kapList, kmp+1, mapsize)
        kapL=fillins(kapL, m_L, m_LH, mapsize)
        kapH=fillins(kapH, m_H, m_LH, mapsize)
        #Create dataset 
        print('Storing:', kapList[kmp])
        #Store data
        dsetL=f.create_dataset(mapsize+'/kapL/'+kapList[kmp],data=kapL)
        dsetH=f.create_dataset(mapsize+'/kapH/'+kapList[kmp],data=kapH)
        kmp = kmp + 1
        
    #Close file
    f.close()

def storeCC(mapsize, masks, kapList):
    f = h5py.File(mapsize+'_kap.hdf5', 'w')
    for kmp in range(0,len(masks)-1):
        #w_map = templateMap(mapsize, masks, kmp)
        m_L = templateMap(mapsize, masks, kmp)
        m_H = templateMap(mapsize, masks, kmp+1)
        m_LH = array((m_L==1)|(m_H==1),dtype=int)
        kapL=kapepoch(m_LH, kapList, kmp, mapsize)
        kapH=kapepoch(m_LH, kapList, kmp+1, mapsize)
        kapL=fillins(kapL, m_L, m_LH, mapsize)
        kapH=fillins(kapH, m_H, m_LH, mapsize)
        #Smooth kapL and kapH 
        kapL=modules.smooth7(kapL, m_LH)
        kapH=modules.smooth7(kapH, m_LH)
        #kapL=sciSmooth(kapL, m_LH)
        #kapH=sciSmooth(kapH, m_LH)
        #Create dataset 
        print('Storing:', kapList[kmp])
        #Store data
        dsetL=f.create_dataset(mapsize+'/kapL/'+kapList[kmp],data=kapL)
        dsetH=f.create_dataset(mapsize+'/kapH/'+kapList[kmp],data=kapH)
        
    #Close file
    f.close()
        
def readTemplateMap(mapsize, masks, kmp):
    #open file
    f = h5py.File(mapsize+'_maps.hdf5', 'r')
    #read data
    #data = f[mapsize+'/masks/'+masks[kmp]]
    #mask=np.array(data)
    mask = f[mapsize+'/masks/'+masks[kmp]].value
    f.close()
    return mask

def readCC(mapsize, ccList, kmp):
    #open file
    f = h5py.File(mapsize+'_kap.hdf5', 'r')
    #read data
    kapL=f[mapsize+'/kapL/'+ccList[kmp]].value
    kapH=f[mapsize+'/kapH/'+ccList[kmp]].value
    f.close()
    return kapL, kapH

def findIndex(tepoch, Ts, Te):
    kstart = None
    kend = None
    for k, t in enumerate(tepoch):
        #print('index:',k)
        #print('Ts>t:',Ts>=t,'\t', Ts,'\t',t)
        #print('t>Te:',t<=Te,'\t',t,'\t',Te)
        if t<=Ts and kstart == None:
            kstart = k-1 #Leo correction
        if t<=Te and kend == None:
            kend = k
            
    return kstart, kend

def getMaxMin(yalist, iepoch, mapsize):
    ascname = yalist[iepoch]
    kyasuf  = ascname[11:13]     # Kya suffix from asci name

    #wj_map=importdata(char(ascname))
    if mapsize is 'small':
        folder='./maps/MAPS100x150/'
        #wj_map=modules.genfromtxt('/Users/Leo/Desktop/MasterThesis/PopulationDynamics/Code/WesleyCode/mapsimleo/world_100x50.txt',100, 50)

    if mapsize is 'large':
        folder='./maps/MAPS360x720/'
        #wj_map=modules.genfromtxt(folder+ascname,720, 360)


    if mapsize is 'huge':
        folder='./maps/MAPS720x1440/'
        #wj_map=modules.genfromtxt(folder+ascname,1440, 720)

    wj_map=genfromtxt(folder+ascname)

    kmax = wj_map.max()
    kmin = wj_map[wj_map>0].min()
    
    return kmax,kmin

def findGlobalMinMax(mapsize):
    if mapsize is 'small':
        yalist, tepoch = get_cc_list(mapsize)
    else:
        yalist, masks, tepoch = get_cc_list(mapsize)
        
    kmaxGlob = 0
    kminGlob = 10
    for it, time in enumerate(tepoch):
        kmax, kmin = getMaxMin(yalist, it, mapsize)
        kmaxGlob = max(kmax,kmaxGlob)
        kminGlob = min(kmin,kminGlob)
    
    return kmaxGlob, kminGlob

def plotImage(imID, tstr, data):
    #storeData(imID, self.data)
    u_plot = data.w_map-1+log(1+10*data.u)
    fig = pyplot.figure(figsize=(30,20))
    pyplot.subplot(111)
    pyplot.title('Time='+tstr+'kY')
    im = pyplot.imshow(u_plot, cmap = cm.RdBu_r, vmin= -1, vmax = 2)
    #pyplot.clim(0,1)
    cb = pyplot.colorbar()
    #cb.set_clim(-1, 3.0)
    fig.savefig(str(imID)+'-'+tstr+'.png',dpi = 100)
    pyplot.close()
    imID = imID+1
    
def storeData(imID, tstr, data):
    f = h5py.File(data.mapsize+'_results.hdf5', 'a')
    dsetL=f.create_dataset(str(imID)+'-'+tstr+'u',data=data.u)
    f.close() 