################################################################################
# Run behavior interface and behavior implementation classes.
################################################################################
from abc import ABCMeta, abstractmethod
from toolsWaves import *
from mpi4py import MPI
import pandas as pd
import os
import pickle
        
class RunBehavior:
    __metaclass__ = ABCMeta
    @abstractmethod 
    def run(self):
        pass

class RunSingleLayer(RunBehavior):
    def __init__(self, data):
        self.data = data

    def run(self, module = 'C', boundary = 'dirichlet', output = False):
        print("=====Small simulation starting=====")
        #output = False
        #module = 'C'
        #Deciding which booster to use 
        
        #Deciding which booster to use 
        if module == 'Fortran':
            if boundary =='dirichlet':
                import Fmodules as modules
                
            if boundary =='neumann':
                import FmodulesNeumann1 as modules

        elif module =='C':
            if boundary =='dirichlet':
                import Cmodules as modules
                
            if boundary =='neumann1':
                import CmodulesNeumann1 as modules
                
            if boundary =='neumann2':
                import CmodulesNeumann3 as modules   
         
        elif module =='Python':
            if boundary =='dirichlet':
                import Pymodules as modules
                
            if boundary =='neumann1':
                import PymodulesNeumann1 as modules
                
            if boundary =='neumann2':
                import PymodulesNeumann3 as modules
             

        #Python scanners 
        nxsegs, xstart_seg, xend_seg = Xscan(self.data.w_map)
        nysegs, ystart_seg, yend_seg = Yscan(self.data.w_map)
        #Fortran scanners 
        #nxsegs, xstart_seg, xend_seg = FXscan(self.data.w_map)
        #nysegs, ystart_seg, yend_seg = FYscan(self.data.w_map)


        NS = max(self.data.NY,self.data.NX)
        u0 = zeros(NS) #find max size 
        u1 = zeros(NS) #find max size
        kap0 = zeros(NS)
        kaph = zeros(NS)
        Ts = self.data.Ts
        Te = self.data.Te
        tsc = self.data.tsc
        h = self.data.h
        kcfl = self.data.kcfl
        u = self.data.u      
        mapsize = self.data.mapsize

        sigmoid = yppoints() #Interpolation points    

        # read the CC map lists and times=tepoch's for each frame: find first
        # Search for appropriate NPP maps:
        #      from start time Ts (in kya) to end time Te
        #loading carrying capacities from namelist
        yalist, tepoch = get_cc_list(mapsize)

        kstart, kend = findIndex(tepoch,Ts, Te)

        print('   Start time Ts=%5.1f (kya), NPP frame kstart=%d\n'%(Ts,kstart))
        print('   End time   Te=%5.1f (kya), NPP frame kend=%d\n'%(Te,kend))

        # make sure there are at least 2 maps between Ts and Te
        if ((kend-kstart)<1) or ((tepoch[kstart]-tepoch[kend])<(Ts-Te)):
           print('Ts=%e, Te=%e, not space enough: kstart=%d, kend=%d\n'%(Ts,Te,kstart,kend))

        #      start and end maps found
        kmp = kstart # start counting maps here

        #Using Jed's map
        #if self.data.mapsize =='big':
        #    import netCDF4
        #    data = netCDF4.Dataset('/Users/Leo/Desktop/Maps_Leo/fep_test.nc','r')
        #    w_J = flipud(data.variables['FEP'][:].data)
        #    w_J = roll(w_J,-310,axis=1)
        #    w_J[w_J<0]=0
        #    w_L = w_J.copy()
        #    w_H = w_J.copy()
        #    t_L = tepoch[kmp]
        #    kmp = kmp + 1                  # increment map count
        #    t_H = tepoch[kmp]
        #else:
        w_L = kapepoch(self.data.w_map, yalist, kmp, mapsize) # this is the first map
        t_L = tepoch[kmp]
        kmp = kmp + 1                  # increment map count
        w_H = kapepoch(self.data.w_map,yalist,kmp, mapsize) # second map
        t_H = tepoch[kmp]

        s   = (t_L-Ts+tsc*h)/(t_L-t_H)  # first step interpolation parameter
        wt0 = 0.0
        # compute interpoated weight to get to step t=h
        ks  = floor(100*s)
        sd  = 100*s - ks               # residual sd = 100*s mod 1
        wth = sigmoid[0]*(1-sd) + sigmoid[1]*sd  # first interpolant
        t   = Ts                       # this is real time in kya

        # TGL alternates between 1 and 0 
        TGL = True
        imID = 0
        #TGL = False

        # BEGIN main loop
        # self.NT = 1
        start = timer()
        for it in range(0,self.data.NT):
            if TGL: #First choice: half-step1 in Y, and step 2 in X 
                locY = 0
                #half-step1 in Y
                for j in range(0,self.data.NX):
                    nsegs = nysegs[j] #Number of segments 
                    for seg in range(0,nsegs):
                        istart=ystart_seg[locY+seg] #From: segment starts
                        iend=yend_seg[locY+seg] #To:segmend ends
                        ninseg=iend-istart+1 #number of cells inside the segment

                        u0[1:ninseg+1] = u[istart:iend+1,j]

                        #Dirichlet b.c.
                        u0[0] = 0
                        u0[ninseg+1] = 0
                        #Neumann b.c.
                        #u0[0] = u0[1]
                        #u0[ninseg+1] = u0[ninseg]

                        #ut = godunovstep1(ninseg+2,h,kcfl,u0,sc1,sc2)
                        #Booster Subroutine


                        ut = modules.godunovstep1(h,kcfl,u0[0:ninseg+2])

                        #Update u
                        #Leo: Check this 
                        #u[istart:iend,j] = ut[2:ninseg+1]
                        u[istart:iend+1,j] = ut[1:ninseg+1]

                        
                    locY = locy + nsegs
                        
                #Step 2 : X direction updates
                locX = 0
                for i in range(0,self.data.NY):
                    nsegs = nxsegs[i]
                    for seg in range(0,nsegs):
                        jstart=xstart_seg[locX+seg]
                        jend=xend_seg[locX+seg]
                        ninseg=jend-jstart+1

                        u1[1:ninseg+1] = u[i,jstart:jend+1]
                        kap0[0] = 1 
                        kap0[ninseg+1] = 1
                        kaph[0] = 1
                        kaph[ninseg+1] = 1
                        kap0[1:ninseg+1] = w_L[i,jstart:jend+1]*(1-wt0) + w_H[i,jstart:jend+1]*wt0
                        #No interpolation
                        #kap0[1:ninseg+1] = w_L[i,jstart:jend+1]                        
                        kaph[1:ninseg+1] = w_L[i,jstart:jend+1]*(1-wth) + w_H[i,jstart:jend+1]*wth
                        #No interpolation
                        #kaph[1:ninseg+1] = w_L[i,jstart:jend+1]


                        #Dirichlet b.c.
                        u1[0] = 0 
                        u1[ninseg+1] = 0
                        #Neumann b.c.
                        #u1[0] = u1[1]
                        #u1[ninseg+1] = u1[ninseg]
                        
                        #ut = godunovstep2(ninseg+2,h,kcfl,u1,sc1,sc2,kap0,kaph)
                        #Booster Subroutine
                        ut = modules.godunovstep2(h,kcfl,u1[0:ninseg+2],kap0[0:ninseg+2],kaph[0:ninseg+2])
                        #Leo: Check this
                        #u[i,jstart:jend] = ut[2:ninseg+1]
                        u[i,jstart:jend+1] = ut[1:ninseg+1]

                  
                    locX = locX + nsegs
                        
                # half step1 in Y direction
                locY = 0
                for j in range(0,self.data.NX):
                    nsegs = nysegs[j]
                    for seg in range(0,nsegs):
                        istart=ystart_seg[locY+seg]
                        iend=yend_seg[locY+seg]
                        ninseg=iend-istart+1

                        u0[1:ninseg+1] = u[istart:iend+1,j]

                        #Dirichlet b.c.
                        u0[0] = 0
                        u0[ninseg+1] = 0
                        #Neumann b.c.
                        #u0[0] = u0[1]
                        #u0[ninseg+1] = u0[ninseg]

                        #ut = godunovstep1(ninseg+2,h,kcfl,u0,sc1,sc2)
                        #Booster Subroutine
                        ut = modules.godunovstep1(h,kcfl,u0[0:ninseg+2])                        
                        u[istart:iend+1,j] = ut[1:ninseg+1]

                  
                    locY = locY + nsegs

                TGL = False
            else:
                ##Second choice: half-step1 in X, and step 2 in Y 
                locX = 0
                #half-step1 in X
                for i in range(0,self.data.NY):
                    nsegs = nxsegs[i] #Number of segments 
                    for seg in range(0,nsegs):
                        jstart=xstart_seg[locX+seg] #From: segment starts
                        jend=xend_seg[locX+seg] #To:segmend ends
                        ninseg=jend-jstart+1
                        u1[1:ninseg+1] = u[i,jstart:jend+1]

                        #Dirichlet b.c.
                        u1[0] = 0 
                        u1[ninseg+1] = 0
                        #Neumann b.c.
                        #u1[0] = u1[1]
                        #u1[ninseg+1] = u1[ninseg]

                        #ut = godunovstep1(ninseg+2,h,kcfl,u1,sc1,sc2)
                        #Fortran Subroutine
                        ut = modules.godunovstep1(h,kcfl,u1[0:ninseg+2])
                        u[i,jstart:jend+1] = ut[1:ninseg+1]

                        
                    locX = locX + nsegs
                #step2 in Y-Direction 
                locY = 0
                for j in range(0,self.data.NX):
                    nsegs = nysegs[j]
                    for seg in range(0,nsegs):
                        istart=ystart_seg[locY+seg]
                        iend=yend_seg[locY+seg]
                        ninseg=iend-istart+1

                        u0[1:ninseg+1] = u[istart:iend+1,j]
                        kap0[0] = 1 
                        kap0[ninseg+1] = 1
                        kaph[0] = 1 
                        kaph[ninseg+1] = 1

                        kap0[1:ninseg+1] = w_L[istart:iend+1,j]*(1-wt0) + w_H[istart:iend+1,j]*wt0
                        kaph[1:ninseg+1] = w_L[istart:iend+1,j]*(1-wth) + w_H[istart:iend+1,j]*wth

                        #Dirichlet b.c.
                        u0[0] = 0 
                        u0[ninseg+1] = 0
                        #Neumann b.c.
                        #u0[0] = u0[1]
                        #u0[ninseg+1] = u0[ninseg]

                        #ut = godunovstep2(ninseg+2,h,kcfl,u0,sc1,sc2,kap0,kaph)
                        #Booster Subroutine
                        ut = modules.godunovstep2(h,kcfl,u0[0:ninseg+2],kap0[0:ninseg+2],kaph[0:ninseg+2])

                        u[istart:iend+1,j] = ut[1:ninseg+1]
                  
                    locY = locY + nsegs
                #half-step1 in X-Direction
                locX = 0
                for i in range(0,self.data.NY):
                    nsegs = nxsegs[i]
                    for seg in range(0,nsegs):
                        jstart=xstart_seg[locX+seg]
                        jend=xend_seg[locX+seg]
                        ninseg=jend-jstart+1
                        u1[1:ninseg+1] = u[i,jstart:jend+1]

                        #Dirichlet b.c.
                        u1[0] = 0
                        u1[ninseg+1] = 0
                        #Neumann b.c.
                        #u1[0] = u1[1]
                        #u1[ninseg+1] = u1[ninseg]

                        #ut = godunovstep1(ninseg+2,h,kcfl,u1,sc1,sc2)
                        #Booster Subroutine
                        ut = modules.godunovstep1(h,kcfl,u1[0:ninseg+2])
                        u[i,jstart:jend+1] = ut[1:ninseg+1]

                  
                    locX = locX + nsegs
                TGL = True
                
            if kmp > kend: 
                print('break hit, leaving loop')
                break

            # increment t value, reset weights and maps if necessary
            t = t - tsc*h          # decrement t in kya
            #fprintf(' ordinary step: t_L=%e, t=%e, t_H=%e\n',t_L,t,t_H);
            if t > t_H:              # usual case, maps not exhausted
                wt0 = wth           # shift lower weight down
                s   = (t_L-t)/(t_L-t_H)  # interpolation parameter
                ks  = floor(100*s)
                sd  = 100*s - ks    # residual sd = 100*s mod 1
                wth = sigmoid[ks]*(1-sd) + sigmoid[ks+1]*sd  # first interpolant
            else:                   # reached upper map time 
                kmp = kmp +1                      # increment map count
                if kmp <= kend:               # kend is the last map
                    w_L = w_H                 # set lower map to prev. upper
                    t_L = t_H                 # set lower time to prev. upper
                    #if self.data.mapsize =='big':
                    #    w_H = w_J.copy()
                    #else:
                    w_H = kapepoch(self.data.w_map,yalist,kmp, self.data.mapsize)  # read next map
                    t_H = tepoch[kmp]         # time frame of new map

                    wt0 = 0
                    s   = (t_L - t)/(t_L-t_H)  # interpolation parameter
                    ks  = floor(100*s)
                    sd  = 100*s - ks                    # mod1 interpolation 
                    wth = sigmoid[ks]*(1-sd) + sigmoid[ks+1]*sd  # first interpolant

            ##Monitoring data
            if (mod(it,self.data.NPLT)==0)or(it==self.data.NT):
                if t < 10:
                    tstr = '%4.2f'%t
                    print(tstr)
                    if output:
                        storeData(imID, tstr, self.data)

                else:
                    tstr = '%4.1f'%t
                    print(tstr)
                    if output:
                        storeData(imID, tstr, self.data)

        end = timer()

        print("\n=====Small simulation completed=====")
        print('   Main Loop Time:', end-start)


class RunMultiLayer(RunBehavior):
    def __init__(self, data):
        self.data = data

    def run(self, module = 'C', boundary = 'dirichlet', output = False):
        def step1X(rows):
            locX = 0
            for row in rows:
                nSegs = nxsegs[row] #Number of segments 
                for seg in range(0,nSegs):
                    colStart=xstart_seg[locX+seg] #From: segment starts
                    colEnd=xend_seg[locX+seg] #To:segmend ends
                    nInSeg=colEnd-colStart+1 #Number of inner segments

                    u1[1:nInSeg+1] = u[row,colStart:colEnd+1]

                    #Dirichlet B.C.
                    if boundary == 'dirichlet':
                        u1[0] = 0. 
                        u1[nInSeg+1] = 0.

                    #Neumann B.C 
                    if boundary == 'neumann':
                        u1[0] = u1[1]
                        u1[nInSeg+1] = u1[nInSeg]
                        
                    #ut = godunovstep1(ninseg+2,h,kcfl,u1,sc1,sc2)
                    #Booster Subroutine
                    ut = modules.godunovstep1(h,kcfl,u1[0:nInSeg+2])
                    u[row,colStart:colEnd+1] = ut[1:nInSeg+1]

                locX = locX + nSegs #Number of scanned segments

        def stepY(cols):
            locY = 0
            for col in cols:
                nSegs = nysegs[col]
                for seg in range(0,nSegs):
                    rowStart=ystart_seg[locY+seg]
                    rowEnd=yend_seg[locY+seg]
                    nInSeg=rowEnd-rowStart+1

                    #print('u0:', u0[1:nInSeg+1].shape) # (2,), (26,), (2,))
                    #print('u:',u[rowStart:rowEnd+1,col].shape) # (0,), (0,), (0,))
                    u0[1:nInSeg+1] = u[rowStart:rowEnd+1,col]
                    kap0[1:nInSeg+1] = w_L[rowStart:rowEnd+1,col]*(1-wt0) + w_H[rowStart:rowEnd+1,col]*wt0
                    kaph[1:nInSeg+1] = w_L[rowStart:rowEnd+1,col]*(1-wth) + w_H[rowStart:rowEnd+1,col]*wth

                    kap0[0] = 1 #??... to divide by
                    kap0[nInSeg+1] = 1 #?? ... to divide by
                    kaph[0] = 1  #?? ... to divide by 
                    kaph[nInSeg+1] = 1 ##?? ...to divide by

                    #Dirichlet B.C.
                    if boundary == 'dirichlet':
                        u0[0] = 0. 
                        u0[nInSeg+1] = 0.

                    #Neumann B.C 
                    if boundary == 'neumann':
                        u0[0] = u0[1]
                        u0[nInSeg+1] = u0[nInSeg]

                    #ut = godunovstep2(ninseg+2,h,kcfl,u0,sc1,sc2,kap0,kaph)
                    #Booster Subroutine
                    ut = modules.godunovstep2(h,kcfl,u0[0:nInSeg+2],kap0[0:nInSeg+2],kaph[0:nInSeg+2])

                    u[rowStart:rowEnd+1,col] = ut[1:nInSeg+1]

                locY = locY + nSegs

        def step1Y(cols):
            locY = 0
            for col in cols:
                nSegs = nysegs[col] #Number of segments 
                for seg in range(0,nSegs):
                    rowStart=ystart_seg[locY+seg] #From: segment starts
                    rowEnd=yend_seg[locY+seg] #To:segmend ends
                    nInSeg=rowEnd-rowStart+1

                    u0[1:nInSeg+1] = u[rowStart:rowEnd+1,col]

                    #Dirichlet b.c.
                    if boundary == 'dirichlet':
                        u0[0] = 0.
                        u0[nInSeg+1] = 0.
                    #Neumann B.C 
                    if boundary == 'neumann': 
                        u0[0] = u0[1]
                        u0[nInSeg+1] = u0[nInSeg]

                    #ut = godunovstep1(ninseg+2,h,kcfl,u0,sc1,sc2)
                    #Booster Subroutine
                    ut = modules.godunovstep1(h,kcfl,u0[0:nInSeg+2])
                    u[rowStart:rowEnd+1,col] = ut[1:nInSeg+1]

                locY = locY + nSegs

        def stepX(rows):
            locX = 0
            for row in rows:
                nSegs = nxsegs[row]
                for seg in range(0,nSegs):
                    colStart=xstart_seg[locX+seg]
                    colEnd=xend_seg[locX+seg]
                    nInSeg=colEnd-colStart+1

                    kap0[0] = 1.
                    kap0[nInSeg+1] = 1.
                    kaph[0] = 1.
                    kaph[nInSeg+1] = 1.

                    u1[1:nInSeg+1] = u[row,colStart:colEnd+1]
                    kap0[1:nInSeg+1] = w_L[row,colStart:colEnd+1]*(1-wt0) + w_H[row,colStart:colEnd+1]*wt0
                    #No interpolation
                    #kap0[1:ninseg+1] = w_L[i,jstart:jend+1]
                    kaph[1:nInSeg+1] = w_L[row,colStart:colEnd+1]*(1-wth) + w_H[row,colStart:colEnd+1]*wth
                    #No interpolation
                    #kaph[1:ninseg+1] = w_L[i,jstart:jend+1]

                    #Dirichlet b.c.
                    if boundary == 'dirichlet':
                        u1[0] = 0. 
                        u1[nInSeg+1] = 0.
                    #Neumann B.C 
                    if boundary == 'neumann':
                        u1[0] = u1[1]
                        u1[nInSeg+1] = u1[nInSeg]

                    #ut = godunovstep2(ninseg+2,h,kcfl,u1,sc1,sc2,kap0,kaph)
                    #Fortran Subroutine
                    ut = modules.godunovstep2(h,kcfl,u1[0:nInSeg+2],kap0[0:nInSeg+2],kaph[0:nInSeg+2])
                    u[row,colStart:colEnd+1] = ut[1:nInSeg+1]


                locX = locX + nSegs

        print("====="+self.data.mapsize+" simulation starting=====")
        #output = False
        #module = 'C'

        #Deciding which booster to use 
        if module == 'Fortran':
            if boundary =='dirichlet':
                import Fmodules as modules
                
            if boundary =='neumann':
                import FmodulesNeumann1 as modules

        elif module =='C':
            if boundary =='dirichlet':
                import Cmodules as modules
                
            if boundary =='neumann':
                import FmodulesNeumann1 as modules
         
        elif module =='Python':
            if boundary =='dirichlet':
                import Pymodules as modules
                
            if boundary =='neumann':
                import PymodulesNeumann1 as modules

        Ts = self.data.Ts #Time start
        Te = self.data.Te #Time end
        tsc = self.data.tsc #Time descaler
        h = self.data.h #Unitless time step
        kcfl = self.data.kcfl #kcfl
        u = self.data.u      
 
        NS = max(self.data.NY,self.data.NX) #find max size 
        u0 = zeros(NS)         
        u1 = zeros(NS) 
        kap0 = zeros(NS)
        kaph = zeros(NS)

        sigmoid = yppoints() #Interpolation points   

        # read the CC map lists and times=tepoch's for each frame: find first
        # Search for appropriate NPP maps:
        #      from start time Ts (in kya) to end time Te
        #loading carrying capacities from namelist
        yalist, masks, tepoch = get_cc_list(self.data.mapsize)

        kstart, kend = findIndex(tepoch,Ts, Te)
        
        # make sure there are at least 2 maps between Ts and Te
        if ((kend-kstart)<1) or ((tepoch[kstart]-tepoch[kend])<(Ts-Te)):
           print('Ts=%e, Te=%e, not space enough: kstart=%d, kend=%d\n'%(Ts,Te,kstart,kend))

        #      start and end maps found
        kmp = kstart # start counting maps here

        #Using Jed's map
        #if self.data.mapsize =='big':
        #    import netCDF4
        #    data = netCDF4.Dataset('/Users/Leo/Desktop/Maps_Leo/fep_test.nc','r')
        #    w_J = flipud(data.variables['FEP'][:].data)
        #    w_J = roll(w_J,-310,axis=1)
        #    w_J[w_J<0]=0
        #    w_L = w_J.copy()
        #    w_H = w_J.copy()
        #    t_L = tepoch[kmp]
        #    kmp = kmp + 1                  # increment map count
        #    t_H = tepoch[kmp]
        #else:
        #Loading and generating masks 
        m_L = templateMap(self.data.mapsize, masks, kmp) # template for the 1st map, Leo:too small values
        t_L = tepoch[kmp] 
        m_H = templateMap(self.data.mapsize, masks, kmp+1) # template for the 2nd map, Leo:too small values 
        m_LH = (m_L==1)|(m_H==1)
        #m_LH[m_LH==False]=0
        #m_LH[m_LH==True]=1

        w_L = kapepoch(m_LH,yalist,kmp, self.data.mapsize) # this is the first map            
        kmp = kmp + 1                  # increment map count

        w_H = kapepoch(m_LH, yalist, kmp, self.data.mapsize)
        t_H = tepoch[kmp]

        # fill in sub-threshold vals for w_L and w_H from m_LH template 
        w_L = fillins(w_L, m_L, m_LH, self.data.mapsize)
        w_H = fillins(w_H, m_H, m_LH, self.data.mapsize)
        
        #Smoothing data
        w_L = modules.smooth7(w_L,m_LH)
        w_H = modules.smooth7(w_L,m_LH)
        
        
        # Scan through map, construct segments for X and Y directions 
        #Python scanners
        nxsegs, xstart_seg, xend_seg = Xscan(m_LH)
        nysegs, ystart_seg, yend_seg = Yscan(m_LH)
        #Fortran scanners
        #nxsegs, xstart_seg, xend_seg = FXscan(m_LH)
        #nysegs, ystart_seg, yend_seg = FYscan(m_LH)

        #w_H = kapepoch(self.data.w_map,yalist,kmp, self.data.mapsize) # second map
        #t_H = tepoch[kmp]

        #Computing interpolation weights 
        s   = (t_L-Ts+tsc*h)/(t_L-t_H)  # first step interpolation parameter
        wt0 = 0.0
        # compute interpoated weight to get to step t=h
        ks  = floor(100*s)
        sd  = 100*s - ks               # residual sd = 100*s mod 1
        #wth = yp[0]*(1-sd) + yp[1]*sd  # first interpolant
        wth = sigmoid[ks]*(1-sd) + sigmoid[ks+1]*sd  # first interpolant
        t   = Ts                       # this is real time in kya

        # TGL alternates between 1 and 0 
        TGL = True
        imID = 0
        #TGL = False

        # BEGIN main loop
        # self.NT = 1
        rows = range(0,self.data.NY)
        cols = range(0,self.data.NX)
 
        start = timer()
        for it in range(0,self.data.NT):
            #%debug 0
            #print('iteration=',it) 
            if TGL: #First choice: half-step1 in X, and step 2 in Y 
                #half-step1 in X
                step1X(rows)
                
                #step2 in Y-Direction 
                stepY(cols)

                #half-step1 in X-Direction
                step1X(rows)

                TGL = False
                
            else:    
                ##Second choice: half-step1 in Y, and step 2 in X 
                #half-step1 in Y
                step1Y(cols)
                
                #Step 2 : X direction updates
                stepX(rows)
                
                # half step1 in Y direction
                step1Y(cols)
                
                TGL = True
                
            #if kmp > kend: 
            #    print 'break hit, leaving loop'
            #    break

            # increment t value, reset weights and maps if necessary
            t = t - tsc*h          # decrement t in kya
            #fprintf(' ordinary step: t_L=%e, t=%e, t_H=%e\n',t_L,t,t_H);
            if t > t_H:              # usual case, maps not exhausted
                wt0 = wth           # shift lower weight down
                s   = (t_L-t)/(t_L-t_H)  # interpolation parameter
                ks  = floor(100*s)
                sd  = 100*s - ks    # residual sd = 100*s mod 1
                wth = sigmoid[ks]*(1-sd) + sigmoid[ks+1]*sd  # first interpolant
            else:                   # reached upper map time 
                kmp = kmp +1                      # increment map count
                if kmp <= kend:               # kend is the last map
                    m_L = m_H
                    w_L = w_H                 # set lower map to prev. upper
                    t_L = t_H                 # set lower time to prev. upper
                    #if self.data.mapsize =='big':
                    #    w_H = w_J.copy()
                    #else:
                    #Leo: bloody error update m_H instead of w_H 
                    #w_H = templateMap(self.data.mapsize, masks, kmp)
                    m_H = templateMap(self.data.mapsize, masks, kmp)
                    m_LH = (m_L==1) | (m_H==1)
                    nxsegs, xstart_seg, xend_seg = Xscan(m_LH)
                    nysegs, ystart_seg, yend_seg = Yscan(m_LH)
                    #nxsegs, xstart_seg, xend_seg = FXscan(m_LH)
                    #nysegs, ystart_seg, yend_seg = FYscan(m_LH)

                    w_L = fillins(w_L, m_L, m_LH,self.data.mapsize)
                    #w_H = kapepoch(self.data.w_map, yalist, kmp, self.data.mapsize)  # read next map
                    w_H = kapepoch(m_LH, yalist, kmp, self.data.mapsize)
                    w_H = fillins(w_H, m_H, m_LH,self.data.mapsize)
                    t_H = tepoch[kmp]         # time frame of new map
                    
                    #Smoothing data
                    w_L = modules.smooth7(w_L,m_LH)
                    w_H = modules.smooth7(w_L,m_LH)
                    
                    wt0 = 0
                    s   = (t_L - t)/(t_L-t_H)  # interpolation parameter
                    ks  = floor(100*s)
                    sd  = 100*s - ks                    # mod1 interpolation 
                    wth = sigmoid[ks]*(1-sd) + sigmoid[ks+1]*sd  # first interpolant
                    
                    self.data.w_map = m_LH.copy()

            ##Monitoring data
            if (mod(it,self.data.NPLT)==0)or(it==self.data.NT):
                if t < 10:
                    tstr = '%4.2f'%t
                    print(tstr)
                    if output:
                        storeData(imID, tstr, self.data)
                    imID = imID+1

                else:
                    tstr = '%4.1f'%t
                    print(tstr)
                    if output:
                        storeData(imID, tstr, self.data)
                    imID = imID+1

        end = timer()
        print("\n====="+self.data.mapsize+" simulation completed=====")
        print('   Main Loop Time:', end-start)
        
        
class RunBcast(RunBehavior):
    def __init__(self, data):
        self.data = data
   
    def run(self, module = 'C', boundary = 'dirichlet', output = False):
        mpi = MPI.COMM_WORLD
        barrier = mpi.barrier
        
        def step1X(rowsLoc):
            locX = 0
            rowIn = 0 
            for row in rowsLoc:
                nSegs = nxsegs[rowIn] #Number of segments
                rowIn = rowIn+1
                for seg in range(0,nSegs):
                    colStart=xstart_seg[locX+seg] #From: segment starts
                    colEnd=xend_seg[locX+seg] #To:segmend ends
                    nInSeg=colEnd-colStart+1 #Number of inner segments

                    u1[1:nInSeg+1] = u[row,colStart:colEnd+1]

                    #Dirichlet B.C.
                    if boundary == 'dirichlet':
                        u1[0] = 0 # water cells 
                        u1[nInSeg+1] = 0 #Water cells

                    #Neumann B.C
                    if boundary == 'neumann':
                        u1[0] = u1[1]
                        u1[nInSeg+1] = u1[nInSeg]


                    #ut = godunovstep1(ninseg+2,h,kcfl,u1,sc1,sc2)
                    #Booster Subroutine
                    ut = modules.godunovstep1(h,kcfl,u1[0:nInSeg+2])
                    u[row,colStart:colEnd+1] = ut[1:nInSeg+1]

                locX = locX + nSegs #Number of scanned segments

        def stepY(colsLoc):    
            locY = 0
            colIn = 0 
            for col in colsLoc:
                nSegs = nysegs[colIn]
                colIn = colIn + 1 
                for seg in range(0,nSegs):
                    rowStart=ystart_seg[locY+seg]
                    rowEnd=yend_seg[locY+seg]
                    nInSeg=rowEnd-rowStart+1

                    #print('u0:', u0[1:nInSeg+1].shape) # (2,), (26,), (2,))
                    #print('u:',u[rowStart:rowEnd+1,col].shape) # (0,), (0,), (0,))
                    u0[1:nInSeg+1] = u[rowStart:rowEnd+1,col]
                    kap0[1:nInSeg+1] = w_L[rowStart:rowEnd+1,col]*(1-wt0) + w_H[rowStart:rowEnd+1,col]*wt0
                    kaph[1:nInSeg+1] = w_L[rowStart:rowEnd+1,col]*(1-wth) + w_H[rowStart:rowEnd+1,col]*wth

                    kap0[0] = 1 #??
                    kap0[nInSeg+1] = 1 #??
                    kaph[0] = 1  #??
                    kaph[nInSeg+1] = 1 ##??

                    #Dirichlet B.C.
                    if boundary == 'dirichlet':
                        u0[0] = 0 
                        u0[nInSeg+1] = 0

                    #Neumann B.C 
                    if boundary == 'neumann':
                        u0[0] = u0[1]
                        u0[nInSeg+1] = u0[nInSeg]

                    #ut = godunovstep2(ninseg+2,h,kcfl,u0,sc1,sc2,kap0,kaph)
                    #Booster Subroutine
                    ut = modules.godunovstep2(h,kcfl,u0[0:nInSeg+2],kap0[0:nInSeg+2],kaph[0:nInSeg+2])

                    u[rowStart:rowEnd+1,col] = ut[1:nInSeg+1]

                locY = locY + nSegs

        def step1Y(colsLoc):        
            locY = 0
            colIn = 0 
            for col in colsLoc:
                nSegs = nysegs[colIn] #Number of segments 
                colIn = colIn + 1
                for seg in range(0,nSegs):
                    rowStart=ystart_seg[locY+seg] #From: segment starts
                    rowEnd=yend_seg[locY+seg] #To:segmend ends
                    nInSeg=rowEnd-rowStart+1

                    u0[1:nInSeg+1] = u[rowStart:rowEnd+1,col]

                    #Dirichlet b.c.
                    if boundary == 'dirichlet':
                        u0[0] = 0
                        u0[nInSeg+1] = 0
                    #Neumann B.C 
                    if boundary == 'neumann':
                        u0[0] = u0[1]
                        u0[nInSeg+1] = u0[nInSeg]

                    #ut = godunovstep1(ninseg+2,h,kcfl,u0,sc1,sc2)
                    #Booster Subroutine
                    ut = modules.godunovstep1(h,kcfl,u0[0:nInSeg+2])
                    u[rowStart:rowEnd+1,col] = ut[1:nInSeg+1]

                locY = locY + nSegs

        def stepX(rowsLoc):
            locX = 0
            rowIn = 0 
            for row in rowsLoc:
                nSegs = nxsegs[rowIn]
                rowIn = rowIn + 1
                for seg in range(0,nSegs):
                    colStart=xstart_seg[locX+seg]
                    colEnd=xend_seg[locX+seg]
                    nInSeg=colEnd-colStart+1

                    kap0[0] = 1 
                    kap0[nInSeg+1] = 1
                    kaph[0] = 1
                    kaph[nInSeg+1] = 1

                    u1[1:nInSeg+1] = u[row,colStart:colEnd+1]
                    kap0[1:nInSeg+1] = w_L[row,colStart:colEnd+1]*(1-wt0) + w_H[row,colStart:colEnd+1]*wt0
                    #No interpolation
                    #kap0[1:ninseg+1] = w_L[i,jstart:jend+1]
                    kaph[1:nInSeg+1] = w_L[row,colStart:colEnd+1]*(1-wth) + w_H[row,colStart:colEnd+1]*wth
                    #No interpolation
                    #kaph[1:ninseg+1] = w_L[i,jstart:jend+1]

                    #Dirichlet b.c.
                    if boundary == 'dirichlet':
                        u1[0] = 0 
                        u1[nInSeg+1] = 0
                    #Neumann B.C 
                    if boundary == 'neumann':
                        u1[0] = u1[1]
                        u1[nInSeg+1] = u1[nInSeg]


                    #ut = godunovstep2(ninseg+2,h,kcfl,u1,sc1,sc2,kap0,kaph)
                    #Fortran Subroutine
                    ut = modules.godunovstep2(h,kcfl,u1[0:nInSeg+2],kap0[0:nInSeg+2],kaph[0:nInSeg+2])
                    u[row,colStart:colEnd+1] = ut[1:nInSeg+1]


                locX = locX + nSegs
                
        if mpi.rank == 0:
            print("====="+self.data.mapsize+" simulation starting=====")
        #output = False
        #module = 'C'

        #Deciding which booster to use 
        if module == 'Fortran':
            if boundary =='dirichlet':
                import Fmodules as modules
                
            if boundary =='neumann':
                import FmodulesNeumann1 as modules

        elif module =='C':
            if boundary =='dirichlet':
                import Cmodules as modules
                
            if boundary =='neumann':
                import FmodulesNeumann1 as modules
         
        elif module =='Python':
            if boundary =='dirichlet':
                import Pymodules as modules
                
            if boundary =='neumann':
                import PymodulesNeumann1 as modules
               


        Ts = self.data.Ts
        Te = self.data.Te
        tsc = self.data.tsc
        h = self.data.h
        kcfl = self.data.kcfl
        u = self.data.u      

        NS = max(self.data.NY,self.data.NX) #find max size 
        u0 = zeros(NS)         
        u1 = zeros(NS) 
        kap0 = zeros(NS)
        kaph = zeros(NS)

        rows = arange(self.data.NY)
        cols = arange(self.data.NX)
        #Local Rows
        #dRow = int(self.data.NY/mpi.size)
        #rowLoc = range(mpi.rank*dRow, (mpi.rank+1)*dRow)
        rowCat = array_split(rows,mpi.size)
        rowLoc = rowCat[mpi.rank]
        #Local Cols
        #dCol = int(self.data.NX/mpi.size)
        #colLoc = range(mpi.rank*dCol, (mpi.rank+1)*dCol)
        colCat = array_split(cols,mpi.size)
        colLoc = colCat[mpi.rank]

        sigmoid = yppoints() #Interpolation points   

        # read the CC map lists and times=tepoch's for each frame: find first
        # Search for appropriate NPP maps:
        #      from start time Ts (in kya) to end time Te
        #loading carrying capacities from namelist
        yalist, masks, tepoch = get_cc_list(self.data.mapsize)

        kstart, kend = findIndex(tepoch,Ts, Te)
        
        # make sure there are at least 2 maps between Ts and Te
        if ((kend-kstart)<1) or ((tepoch[kstart]-tepoch[kend])<(Ts-Te)):
           print('Ts=%e, Te=%e, not space enough: kstart=%d, kend=%d\n'%(Ts,Te,kstart,kend))

        #      start and end maps found
        kmp = kstart # start counting maps here

        #Using Jed's map
        #if self.data.mapsize =='big':
        #    import netCDF4
        #    data = netCDF4.Dataset('/Users/Leo/Desktop/Maps_Leo/fep_test.nc','r')
        #    w_J = flipud(data.variables['FEP'][:].data)
        #    w_J = roll(w_J,-310,axis=1)
        #    w_J[w_J<0]=0
        #    w_L = w_J.copy()
        #    w_H = w_J.copy()
        #    t_L = tepoch[kmp]
        #    kmp = kmp + 1                  # increment map count
        #    t_H = tepoch[kmp]
        #else:
        #Loading and generating masks 
        t_L = tepoch[kmp] 
        t_H = tepoch[kmp+1]
        
        #Compute m_LH
        #read m_LH
        m_LH = readTemplateMap(self.data.mapsize, masks,kmp)
        #Read carrying capacities
        #Read w_L, w_H
        w_L, w_H = readCC(self.data.mapsize, yalist, kmp)
        
        kmp = kmp + 1                  # increment map count

        # Scan through map, construct segments for X and Y directions 
        #Python scanners
        nxsegs, xstart_seg, xend_seg = Xscan(m_LH[rowLoc,:])
        nysegs, ystart_seg, yend_seg = Yscan(m_LH[:,colLoc])
        #Fortran scanners
        #nxsegs, xstart_seg, xend_seg = FXscan(m_LH)
        #nysegs, ystart_seg, yend_seg = FYscan(m_LH)

        #Computing interpolation weights 
        s   = (t_L-Ts+tsc*h)/(t_L-t_H)  # first step interpolation parameter
        wt0 = 0.0
        # compute interpoated weight to get to step t=h
        ks  = floor(100*s)
        sd  = 100*s - ks               # residual sd = 100*s mod 1
        #wth = yp[0]*(1-sd) + yp[1]*sd  # first interpolant
        wth = sigmoid[ks]*(1-sd) + sigmoid[ks+1]*sd  # first interpolant
        t   = Ts                       # this is real time in kya

        # TGL alternates between 1 and 0 
        TGL = True
        imID = 0
        #TGL = False

        # BEGIN main loop
        # self.NT = 1
        if mpi.rank == 0:
            start = timer()
            
        #for it in range(0,100):
        for it in range(0,self.data.NT):
            #%debug 0
            #print('iteration=',it) 
            if TGL: #First choice: half-step1 in X, and step 2 in Y 
                #Compute row chunk
                #half-step1 in X
                #step1X(rows)
                step1X(rowLoc)
                #Getting row chunks
                uLoc = u[rowLoc,:]#uLoc not being used  

                #Collecting row chunks
                uCat = mpi.gather(uLoc, root=0)
                if mpi.rank == 0:
                    u = concatenate(uCat,axis=0)
                    #plotting(u)

                #Broadcasting data
                u = mpi.bcast(u, root=0)

                #Compute col chunk
                #step2 in Y-Direction
                #stepY(cols)
                stepY(colLoc)

                #Reading columns chunks
                uLoc = u[:, colLoc]

                #Collecting col chunks 
                uCat = mpi.gather(uLoc, root=0)
                if mpi.rank == 0:
                    u = concatenate(uCat,axis=1)
                    #plotting(u)

                #Broadcasting data
                u = mpi.bcast(u, root=0)

                #Compute row chunks
                #half-step1 in X-Direction
                #step1X(rows)
                step1X(rowLoc)

                #Reading rows chunks
                uLoc = u[rowLoc,:]

                #Collecting row chunks
                uCat = mpi.gather(uLoc, root=0)
                if mpi.rank == 0:
                    u = concatenate(uCat,axis=0)
                    #plotting(u)

                #Broadcasting data
                u = mpi.bcast(u, root=0)

                TGL = False

                self.data.u = u.copy()

            else:
                ##Second choice: half-step1 in Y, and step 2 in X 

                #half-step1 in Y
                #step1Y(cols)
                step1Y(colLoc)

                #Reading columns chunks
                uLoc = u[:, colLoc]

                #Collecting col chunks 
                uCat = mpi.gather(uLoc, root=0)
                if mpi.rank == 0:
                    u = concatenate(uCat,axis=1)
                #Broadcasting data
                u = mpi.bcast(u, root=0)

                #Compute row chunks
                #Step 2 : X direction updates
                #stepX(rows)
                stepX(rowLoc)

                #Reading rows chunks
                uLoc = u[rowLoc,:]

                #Collecting row chunks
                uCat = mpi.gather(uLoc, root=0)
                if mpi.rank == 0:
                    u = concatenate(uCat,axis=0)

                #Broadcasting data
                u = mpi.bcast(u, root=0)

                #Compute col chunks 
                # half step1 in Y direction
                #step1Y(cols)
                step1Y(colLoc)

                #Reading columns chunks
                uLoc = u[:, colLoc]

                #Collecting col chunks 
                uCat = mpi.gather(uLoc, root=0)
                if mpi.rank == 0:
                    u = concatenate(uCat,axis=1)
                #Broadcasting data
                u = mpi.bcast(u, root=0)

                TGL = True

                self.data.u = u.copy()
            
            #if kmp > kend: 
            #    print 'break hit, leaving loop'
            #    break

            # increment t value, reset weights and maps if necessary
            t = t - tsc*h          # decrement t in kya
            #fprintf(' ordinary step: t_L=%e, t=%e, t_H=%e\n',t_L,t,t_H);
            if t > t_H:              # usual case, maps not exhausted
                wt0 = wth           # shift lower weight down
                s   = (t_L-t)/(t_L-t_H)  # interpolation parameter
                ks  = floor(100*s)
                sd  = 100*s - ks    # residual sd = 100*s mod 1
                wth = sigmoid[ks]*(1-sd) + sigmoid[ks+1]*sd  # first interpolant
            else:                   # reached upper map time 
                kmp = kmp +1                      # increment map count
                if kmp <= kend:               # kend is the last map
                    #read mask
                    m_LH = readTemplateMap(self.data.mapsize, masks,kmp-1)
                    
                    #Read carrying capacities
                    w_L, w_H = readCC(self.data.mapsize, yalist, kmp-1)
                    
                    t_L = t_H                 # set lower time to prev. upper
                    t_H = tepoch[kmp]         # time frame of new map
                    
                    nxsegs, xstart_seg, xend_seg = Xscan(m_LH[rowLoc,:])
                    nysegs, ystart_seg, yend_seg = Yscan(m_LH[:,colLoc])
                    #nxsegs, xstart_seg, xend_seg = FXscan(m_LH)
                    #nysegs, ystart_seg, yend_seg = FYscan(m_LH)

                    wt0 = 0
                    s   = (t_L - t)/(t_L-t_H)  # interpolation parameter
                    ks  = floor(100*s)
                    sd  = 100*s - ks                    # mod1 interpolation 
                    wth = sigmoid[ks]*(1-sd) + sigmoid[ks+1]*sd  # first interpolant
                    
                    self.data.w_map = m_LH.copy()


            ##Monitoring data
            if mpi.rank == 0:
                if (mod(it,self.data.NPLT)==0)or(it==self.data.NT):
                    if t < 10:
                        tstr = '%4.2f'%t
                        print(tstr)
                        if output:
                            storeData(imID, tstr, self.data)
                            
                        imID = imID+1

                    else:
                        tstr = '%4.1f'%t
                        print(tstr)
                        if output:
                            storeData(imID, tstr, self.data)
                            
                        imID = imID+1


        if mpi.rank == 0:                        
            end = timer()
            print("\n====="+self.data.mapsize+" simulation completed=====")
            print('   Main Loop Time:', end-start)

class RunScatter(RunBehavior):
    def __init__(self, data):
        self.data = data
        
    def run(self,module = 'C', boundary = 'dirichlet',output = False):        
        mpi = MPI.COMM_WORLD
        barrier = mpi.barrier

        def step1X(rowsLoc):
            locX = 0
            rowIn = 0 
            for row in rowsLoc:
                nSegs = nxsegs[rowIn] #Number of segments
                rowIn = rowIn+1
                for seg in range(0,nSegs):
                    colStart=xstart_seg[locX+seg] #From: segment starts
                    colEnd=xend_seg[locX+seg] #To:segmend ends
                    nInSeg=colEnd-colStart+1 #Number of inner segments

                    u1[1:nInSeg+1] = u[row,colStart:colEnd+1]

                    #Dirichlet B.C.
                    if boundary == 'dirichlet':
                        u1[0] = 0 # water cells 
                        u1[nInSeg+1] = 0 #Water cells

                    #Neumann B.C 
                    if boundary == 'neumann':
                        u1[0] = u1[1]
                        u1[nInSeg+1] = u1[nInSeg]


                    #ut = godunovstep1(ninseg+2,h,kcfl,u1,sc1,sc2)
                    #Booster Subroutine
                    ut = modules.godunovstep1(h,kcfl,u1[0:nInSeg+2])
                    u[row,colStart:colEnd+1] = ut[1:nInSeg+1]

                locX = locX + nSegs #Number of scanned segments

        def stepY(colsLoc):    
            locY = 0
            colIn = 0 
            for col in colsLoc:
                nSegs = nysegs[colIn]
                colIn = colIn + 1 
                for seg in range(0,nSegs):
                    rowStart=ystart_seg[locY+seg]
                    rowEnd=yend_seg[locY+seg]
                    nInSeg=rowEnd-rowStart+1

                    #print('u0:', u0[1:nInSeg+1].shape) # (2,), (26,), (2,))
                    #print('u:',u[rowStart:rowEnd+1,col].shape) # (0,), (0,), (0,))
                    u0[1:nInSeg+1] = u[rowStart:rowEnd+1,col]
                    kap0[1:nInSeg+1] = w_L[rowStart:rowEnd+1,col]*(1-wt0) + w_H[rowStart:rowEnd+1,col]*wt0
                    kaph[1:nInSeg+1] = w_L[rowStart:rowEnd+1,col]*(1-wth) + w_H[rowStart:rowEnd+1,col]*wth

                    kap0[0] = 1 #??
                    kap0[nInSeg+1] = 1 #??
                    kaph[0] = 1  #??
                    kaph[nInSeg+1] = 1 ##??

                    #Dirichlet B.C.
                    if boundary == 'dirichlet':
                        u0[0] = 0 
                        u0[nInSeg+1] = 0

                    #Neumann B.C 
                    if boundary == 'neumann':
                        u0[0] = u0[1]
                        u0[nInSeg+1] = u0[nInSeg]

                    #ut = godunovstep2(ninseg+2,h,kcfl,u0,sc1,sc2,kap0,kaph)
                    #Booster Subroutine
                    ut = modules.godunovstep2(h,kcfl,u0[0:nInSeg+2],kap0[0:nInSeg+2],kaph[0:nInSeg+2])

                    u[rowStart:rowEnd+1,col] = ut[1:nInSeg+1]

                locY = locY + nSegs

        def step1Y(colsLoc):        
            locY = 0
            colIn = 0 
            for col in colsLoc:
                nSegs = nysegs[colIn] #Number of segments 
                colIn = colIn + 1
                for seg in range(0,nSegs):
                    rowStart=ystart_seg[locY+seg] #From: segment starts
                    rowEnd=yend_seg[locY+seg] #To:segmend ends
                    nInSeg=rowEnd-rowStart+1

                    u0[1:nInSeg+1] = u[rowStart:rowEnd+1,col]

                    #Dirichlet b.c.
                    if boundary == 'dirichlet':
                        u0[0] = 0
                        u0[nInSeg+1] = 0
                    #Neumann B.C 
                    if boundary == 'neumann':
                        u0[0] = u0[1]
                        u0[nInSeg+1] = u0[nInSeg]

                    #ut = godunovstep1(ninseg+2,h,kcfl,u0,sc1,sc2)
                    #Booster Subroutine
                    ut = modules.godunovstep1(h,kcfl,u0[0:nInSeg+2])
                    u[rowStart:rowEnd+1,col] = ut[1:nInSeg+1]

                locY = locY + nSegs

        def stepX(rowsLoc):
            locX = 0
            rowIn = 0 
            for row in rowsLoc:
                nSegs = nxsegs[rowIn]
                rowIn = rowIn + 1
                for seg in range(0,nSegs):
                    colStart=xstart_seg[locX+seg]
                    colEnd=xend_seg[locX+seg]
                    nInSeg=colEnd-colStart+1

                    kap0[0] = 1 
                    kap0[nInSeg+1] = 1
                    kaph[0] = 1
                    kaph[nInSeg+1] = 1

                    u1[1:nInSeg+1] = u[row,colStart:colEnd+1]
                    kap0[1:nInSeg+1] = w_L[row,colStart:colEnd+1]*(1-wt0) + w_H[row,colStart:colEnd+1]*wt0
                    #No interpolation
                    #kap0[1:ninseg+1] = w_L[i,jstart:jend+1]
                    kaph[1:nInSeg+1] = w_L[row,colStart:colEnd+1]*(1-wth) + w_H[row,colStart:colEnd+1]*wth
                    #No interpolation
                    #kaph[1:ninseg+1] = w_L[i,jstart:jend+1]

                    #Dirichlet b.c.
                    if boundary == 'dirichlet':
                        u1[0] = 0 
                        u1[nInSeg+1] = 0
                    #Neumann B.C 
                    if boundary == 'neumann':
                        u1[0] = u1[1]
                        u1[nInSeg+1] = u1[nInSeg]


                    #ut = godunovstep2(ninseg+2,h,kcfl,u1,sc1,sc2,kap0,kaph)
                    #Fortran Subroutine
                    ut = modules.godunovstep2(h,kcfl,u1[0:nInSeg+2],kap0[0:nInSeg+2],kaph[0:nInSeg+2])
                    u[row,colStart:colEnd+1] = ut[1:nInSeg+1]


                locX = locX + nSegs

        if mpi.rank == 0:
            print("====="+self.data.mapsize+" simulation starting=====")
        #output = False
        #module = 'C'

        #Deciding which booster to use 
        if module == 'Fortran':
            if boundary =='dirichlet':
                import Fmodules as modules
                
            if boundary =='neumann':
                import FmodulesNeumann1 as modules

        elif module =='C':
            if boundary =='dirichlet':
                import Cmodules as modules
                
            if boundary =='neumann':
                import FmodulesNeumann1 as modules
         
        elif module =='Python':
            if boundary =='dirichlet':
                import Pymodules as modules
                
            if boundary =='neumann':
                import PymodulesNeumann1 as modules

        Ts = self.data.Ts
        Te = self.data.Te
        tsc = self.data.tsc
        h = self.data.h
        kcfl = self.data.kcfl
        u = self.data.u      

        NS = max(self.data.NY,self.data.NX) #find max size 
        u0 = zeros(NS)         
        u1 = zeros(NS) 
        kap0 = zeros(NS)
        kaph = zeros(NS)

        rows = arange(self.data.NY)
        cols = arange(self.data.NX)
        #Local Rows
        #dRow = int(self.data.NY/mpi.size)
        #rowLoc = range(mpi.rank*dRow, (mpi.rank+1)*dRow)
        rowCat = array_split(rows,mpi.size)
        rowLoc = rowCat[mpi.rank]
        #Local Cols
        #dCol = int(self.data.NX/mpi.size)
        #colLoc = range(mpi.rank*dCol, (mpi.rank+1)*dCol)
        colCat = array_split(cols,mpi.size)
        colLoc = colCat[mpi.rank]

        sigmoid = yppoints() #Interpolation points   

        # read the CC map lists and times=tepoch's for each frame: find first
        # Search for appropriate NPP maps:
        #      from start time Ts (in kya) to end time Te
        #loading carrying capacities from namelist
        yalist, masks, tepoch = get_cc_list(self.data.mapsize)

        kstart, kend = findIndex(tepoch,Ts, Te)

        # make sure there are at least 2 maps between Ts and Te
        if ((kend-kstart)<1) or ((tepoch[kstart]-tepoch[kend])<(Ts-Te)):
           print('Ts=%e, Te=%e, not space enough: kstart=%d, kend=%d\n'%(Ts,Te,kstart,kend))

        #      start and end maps found
        kmp = kstart # start counting maps here

        #Using Jed's map
        #if self.data.mapsize =='big':
        #    import netCDF4
        #    data = netCDF4.Dataset('/Users/Leo/Desktop/Maps_Leo/fep_test.nc','r')
        #    w_J = flipud(data.variables['FEP'][:].data)
        #    w_J = roll(w_J,-310,axis=1)
        #    w_J[w_J<0]=0
        #    w_L = w_J.copy()
        #    w_H = w_J.copy()
        #    t_L = tepoch[kmp]
        #    kmp = kmp + 1                  # increment map count
        #    t_H = tepoch[kmp]
        #else:
        #read m_LH
        m_LH = readTemplateMap(self.data.mapsize, masks,kmp)
        #Read carrying capacities
        #Read w_L, w_H
        w_L, w_H = readCC(self.data.mapsize, yalist, kmp)
                    
        #Loading and generating masks 
        #m_L = templateMap(self.data.mapsize, masks, kmp) # template for the 1st map, Leo:too small values
        t_L = tepoch[kmp] 
        t_H = tepoch[kmp+1]
        kmp = kmp + 1                  # increment map count

        # Scan through map, construct segments for X and Y directions 
        #Python scanners
        nxsegs, xstart_seg, xend_seg = Xscan(m_LH[rowLoc,:])
        nysegs, ystart_seg, yend_seg = Yscan(m_LH[:,colLoc])
        #Fortran scanners
        #nxsegs, xstart_seg, xend_seg = FXscan(m_LH)
        #nysegs, ystart_seg, yend_seg = FYscan(m_LH)

        #Computing interpolation weights 
        s   = (t_L-Ts+tsc*h)/(t_L-t_H)  # first step interpolation parameter
        wt0 = 0.0
        # compute interpoated weight to get to step t=h
        ks  = floor(100*s)
        sd  = 100*s - ks               # residual sd = 100*s mod 1
        #wth = yp[0]*(1-sd) + yp[1]*sd  # first interpolant
        wth = sigmoid[ks]*(1-sd) + sigmoid[ks+1]*sd  # first interpolant
        t   = Ts                       # this is real time in kya

        # TGL alternates between 1 and 0 
        TGL = True
        imID = 0
        #TGL = False

        # BEGIN main loop
        # self.NT = 1      
        if mpi.rank == 0:
            start = timer()
            
        #for it in range(0,100):
        for it in range(0,self.data.NT):
            #%debug 0
            #print('iteration=',it) 
            if TGL: #First choice: half-step1 in X, and step 2 in Y 
                #Split Rows
                if mpi.rank ==0:
                    uSplitRows = array_split(u,mpi.size,axis=0)
                else:
                    uSplitRows = None

                #Scattering data
                uSplitRows = mpi.scatter(uSplitRows, root=0)    
                u[rowLoc,:]=uSplitRows

                #half-step1 in X
                step1X(rowLoc)

                #Collecting rows
                uCat = mpi.gather(uSplitRows, root=0)
                #Splitting columns
                if mpi.rank == 0:
                    u = concatenate(uCat,axis=0)
                    uSplitCols = array_split(u,mpi.size,axis=1)
                else:
                    uSplitCols = None

                #Scattering data
                uSplitCols = mpi.scatter(uSplitCols, root=0)
                u[:,colLoc]=uSplitCols

                #step2 in Y-Direction
                stepY(colLoc)

                #Collecting cols
                uCat = mpi.gather(uSplitCols, root=0)

                #Splitting rows 
                if mpi.rank == 0:
                    u = concatenate(uCat,axis=1)
                    uSplitRows = array_split(u,mpi.size,axis=0)
                else:
                    uSplitRows = None

                #Scattering data
                uSplitRows = mpi.scatter(uSplitRows, root=0)
                u[rowLoc,:]=uSplitRows

                #half-step1 in X-Direction
                step1X(rowLoc)

                #Collecting rows
                uCat = mpi.gather(uSplitRows, root=0)
                #Splitting columns
                if mpi.rank == 0:
                    u = concatenate(uCat,axis=0)

                TGL = False

                self.data.u = u.copy()

            else:
                ##Second choice: half-step1 in Y, and step 2 in X 
                #Splitting columns
                if mpi.rank == 0:
                    uSplitCols = array_split(u,mpi.size,axis=1)
                else:
                    uSplitCols = None

                #Scattering data
                uSplitCols = mpi.scatter(uSplitCols, root=0)
                u[:,colLoc]=uSplitCols

                #half-step1 in Y
                step1Y(colLoc)

                #Collecting cols
                uCat = mpi.gather(uSplitCols, root=0)

                #Splitting rows 
                if mpi.rank == 0:
                    u = concatenate(uCat,axis=1)
                    uSplitRows = array_split(u,mpi.size,axis=0)
                else:
                    uSplitRows = None

                #Scattering data
                uSplitRows = mpi.scatter(uSplitRows, root=0)
                u[rowLoc,:]=uSplitRows

                #Step 2 : X direction updates
                stepX(rowLoc)

                #Collecting rows
                uCat = mpi.gather(uSplitRows, root=0)
                #Splitting columns
                if mpi.rank == 0:
                    u = concatenate(uCat,axis=0)
                    uSplitCols = array_split(u,mpi.size,axis=1)
                else:
                    uSplitCols = None

                #Scattering data
                uSplitCols = mpi.scatter(uSplitCols, root=0)
                u[:,colLoc]=uSplitCols

                # half step1 in Y direction
                step1Y(colLoc)

                #Collecting cols
                uCat = mpi.gather(uSplitCols, root=0)

                #Splitting rows 
                if mpi.rank == 0:
                    u = concatenate(uCat,axis=1)

                TGL = True

                self.data.u = u.copy()

            #if kmp > kend: 
            #    print 'break hit, leaving loop'
            #    break

            # increment t value, reset weights and maps if necessary
            t = t - tsc*h          # decrement t in kya
            #fprintf(' ordinary step: t_L=%e, t=%e, t_H=%e\n',t_L,t,t_H);
            if t > t_H:              # usual case, maps not exhausted
                wt0 = wth           # shift lower weight down
                s   = (t_L-t)/(t_L-t_H)  # interpolation parameter
                ks  = floor(100*s)
                sd  = 100*s - ks    # residual sd = 100*s mod 1
                wth = sigmoid[ks]*(1-sd) + sigmoid[ks+1]*sd  # first interpolant
            else:                   # reached upper map time 
                kmp = kmp +1                      # increment map count
                if kmp <= kend:               # kend is the last map
                    #read mask
                    m_LH = readTemplateMap(self.data.mapsize, masks,kmp-1)
                    #Read carrying capacities
                    #Read w_L, w_H
                    w_L, w_H = readCC(self.data.mapsize, yalist, kmp-1)
                    
                    t_L = t_H                 # set lower time to prev. upper
                    t_H = tepoch[kmp]         # time frame of new map
                    
                    nxsegs, xstart_seg, xend_seg = Xscan(m_LH[rowLoc,:])
                    nysegs, ystart_seg, yend_seg = Yscan(m_LH[:,colLoc])
                    #nxsegs, xstart_seg, xend_seg = FXscan(m_LH)
                    #nysegs, ystart_seg, yend_seg = FYscan(m_LH)

                    wt0 = 0
                    s   = (t_L - t)/(t_L-t_H)  # interpolation parameter
                    ks  = floor(100*s)
                    sd  = 100*s - ks                    # mod1 interpolation 
                    wth = sigmoid[ks]*(1-sd) + sigmoid[ks+1]*sd  # first interpolant

                    self.data.w_map = m_LH.copy()


            ##Monitoring data
            if mpi.rank == 0:
                if (mod(it,self.data.NPLT)==0)or(it==self.data.NT):
                    if t < 10:
                        tstr = '%4.2f'%t
                        print(tstr)
                        if output:
                            storeData(imID, tstr, self.data)
                            
                        imID = imID+1

                    else:
                        tstr = '%4.1f'%t
                        print(tstr)
                        if output:
                            storeData(imID, tstr, self.data)
                            
                        imID = imID+1

        if mpi.rank == 0:
            end = timer()
            print("\n====="+self.data.mapsize+" simulation completed=====")
            print('   Main Loop Time:', end-start)

class RunHDF5(RunBehavior):
    def __init__(self, data):
        self.data = data

    def run(self, module = 'C', boundary = 'dirichlet',output = False):
        
        sites=pd.read_pickle('ArcheologicalSites')
        empty = ones(10, dtype=bool)
        arrivalTime = zeros(10,dtype=float)
        
        def verify_arrival():
            for i in range(1,10): 
                lat_pix = sites.loc[i]['lat_pix']
                lon_pix = sites.loc[i]['lon_pix']
                meanKap=0.25*(w_L[lat_pix,lon_pix]+w_H[lat_pix,lon_pix])
                if (self.data.u[lat_pix,lon_pix]> meanKap) and empty[i]:
                    arrivalTime[i] = t
                    empty[i] = False
            
        
        def step1X(rows):
            locX = 0
            for row in rows:
                nSegs = nxsegs[row] #Number of segments 
                for seg in range(0,nSegs):
                    colStart=xstart_seg[locX+seg] #From: segment starts
                    colEnd=xend_seg[locX+seg] #To:segmend ends
                    nInSeg=colEnd-colStart+1 #Number of inner segments

                    u1[1:nInSeg+1] = u[row,colStart:colEnd+1]

                    #Dirichlet B.C.
                    if boundary == 'dirichlet':
                        u1[0] = 0 
                        u1[nInSeg+1] = 0

                    #Neumann B.C 
                    if boundary == 'neumann1':
                        u1[0] = u1[1]
                        u1[nInSeg+1] = u1[nInSeg]

                    #ut = godunovstep1(ninseg+2,h,kcfl,u1,sc1,sc2)
                    #Booster Subroutine
                    ut = modules.godunovstep1(h,kcfl,u1[0:nInSeg+2])
                    u[row,colStart:colEnd+1] = ut[1:nInSeg+1]

                locX = locX + nSegs #Number of scanned segments

        def stepY(cols):
            locY = 0
            for col in cols:
                nSegs = nysegs[col]
                for seg in range(0,nSegs):
                    rowStart=ystart_seg[locY+seg]
                    rowEnd=yend_seg[locY+seg]
                    nInSeg=rowEnd-rowStart+1

                    #print('u0:', u0[1:nInSeg+1].shape) # (2,), (26,), (2,))
                    #print('u:',u[rowStart:rowEnd+1,col].shape) # (0,), (0,), (0,))
                    u0[1:nInSeg+1] = u[rowStart:rowEnd+1,col]
                    kap0[1:nInSeg+1] = w_L[rowStart:rowEnd+1,col]*(1-wt0) + w_H[rowStart:rowEnd+1,col]*wt0
                    kaph[1:nInSeg+1] = w_L[rowStart:rowEnd+1,col]*(1-wth) + w_H[rowStart:rowEnd+1,col]*wth

                    kap0[0] = 1 #??
                    kap0[nInSeg+1] = 1 #??
                    kaph[0] = 1  #??
                    kaph[nInSeg+1] = 1 ##??

                    #Dirichlet B.C.
                    if boundary == 'dirichlet':
                        u0[0] = 0 
                        u0[nInSeg+1] = 0

                    #Neumann B.C 
                    if boundary == 'neumann1':
                        u0[0] = u0[1]
                        u0[nInSeg+1] = u0[nInSeg]

                    #ut = godunovstep2(ninseg+2,h,kcfl,u0,sc1,sc2,kap0,kaph)
                    #Booster Subroutine
                    ut = modules.godunovstep2(h,kcfl,u0[0:nInSeg+2],kap0[0:nInSeg+2],kaph[0:nInSeg+2])
                    
                    #Killing negative values, it can be avoided using the regularizer 
                    if(ut[1:nInSeg+1]<0).any():
                        print('Annihilation at col:%d, rows:[%d,%d]'%(col, rowStart, rowEnd))
                        ut[ut<0]=0. #Self annihilation when u<0

                    u[rowStart:rowEnd+1,col] = ut[1:nInSeg+1]

                locY = locY + nSegs

        def step1Y(cols):
            locY = 0
            for col in cols:
                nSegs = nysegs[col] #Number of segments 
                for seg in range(0,nSegs):
                    rowStart=ystart_seg[locY+seg] #From: segment starts
                    rowEnd=yend_seg[locY+seg] #To:segmend ends
                    nInSeg=rowEnd-rowStart+1

                    u0[1:nInSeg+1] = u[rowStart:rowEnd+1,col]

                    #Dirichlet b.c.
                    if boundary == 'dirichlet':
                        u0[0] = 0
                        u0[nInSeg+1] = 0
                    #Neumann B.C 
                    if boundary == 'neumann1':
                        u0[0] = u0[1]
                        u0[nInSeg+1] = u0[nInSeg]

                    #ut = godunovstep1(ninseg+2,h,kcfl,u0,sc1,sc2)
                    #Booster Subroutine
                    ut = modules.godunovstep1(h,kcfl,u0[0:nInSeg+2])
                    u[rowStart:rowEnd+1,col] = ut[1:nInSeg+1]

                locY = locY + nSegs

        def stepX(rows):
            locX = 0
            for row in rows:
                nSegs = nxsegs[row]
                for seg in range(0,nSegs):
                    colStart=xstart_seg[locX+seg]
                    colEnd=xend_seg[locX+seg]
                    nInSeg=colEnd-colStart+1

                    kap0[0] = 1 
                    kap0[nInSeg+1] = 1
                    kaph[0] = 1
                    kaph[nInSeg+1] = 1

                    u1[1:nInSeg+1] = u[row,colStart:colEnd+1]
                    kap0[1:nInSeg+1] = w_L[row,colStart:colEnd+1]*(1-wt0) + w_H[row,colStart:colEnd+1]*wt0
                    #No interpolation
                    #kap0[1:ninseg+1] = w_L[i,jstart:jend+1]
                    kaph[1:nInSeg+1] = w_L[row,colStart:colEnd+1]*(1-wth) + w_H[row,colStart:colEnd+1]*wth
                    #No interpolation
                    #kaph[1:ninseg+1] = w_L[i,jstart:jend+1]

                    #Dirichlet b.c.
                    if boundary == 'dirichlet':
                        u1[0] = 0 
                        u1[nInSeg+1] = 0
                    #Neumann B.C 
                    if boundary == 'neumann1':
                        u1[0] = u1[1]
                        u1[nInSeg+1] = u1[nInSeg]


                    #ut = godunovstep2(ninseg+2,h,kcfl,u1,sc1,sc2,kap0,kaph)
                    #Fortran Subroutine
                    ut = modules.godunovstep2(h,kcfl,u1[0:nInSeg+2],kap0[0:nInSeg+2],kaph[0:nInSeg+2])
                    
                    if(ut[1:nInSeg+1]<0).any():
                        print('Annihilation at cols:[%d,%d], row:%d'%(colStart, colEnd, row))
                        ut[ut<0]=0. #Self annihilation when u<0
                    
                    u[row,colStart:colEnd+1] = ut[1:nInSeg+1]


                locX = locX + nSegs

        print("====="+self.data.mapsize+" simulation starting=====")
        #output = False
        #module = 'C'

        #Deciding which booster to use 
        if module == 'Fortran':
            if boundary =='dirichlet':
                import Fmodules as modules
                
            if boundary =='neumann':
                import FmodulesNeumann1 as modules

        elif module =='C':
            if boundary =='dirichlet':
                import Cmodules as modules
                
            if boundary =='neumann1':
                import CmodulesNeumann1 as modules
                
            if boundary =='neumann2':
                import CmodulesNeumann3 as modules   
         
        elif module =='Python':
            if boundary =='dirichlet':
                import Pymodules as modules
                
            if boundary =='neumann1':
                import PymodulesNeumann1 as modules
                
            if boundary =='neumann2':
                import PymodulesNeumann3 as modules
                
                
        Ts = self.data.Ts #Time start
        Te = self.data.Te #Time end
        tsc = self.data.tsc #Time descaler
        h = self.data.h #Unitless time step
        kcfl = self.data.kcfl #kcfl
        u = self.data.u      
 
        NS = max(self.data.NY,self.data.NX) #find max size 
        u0 = zeros(NS)         
        u1 = zeros(NS) 
        kap0 = zeros(NS)
        kaph = zeros(NS)

        sigmoid = yppoints() #Interpolation points   

        # read the CC map lists and times=tepoch's for each frame: find first
        # Search for appropriate NPP maps:
        #      from start time Ts (in kya) to end time Te
        #loading carrying capacities from namelist
        yalist, masks, tepoch = get_cc_list(self.data.mapsize)
        kstart, kend = findIndex(tepoch,Ts, Te)
        
        # make sure there are at least 2 maps between Ts and Te
        if ((kend-kstart)<1) or ((tepoch[kstart]-tepoch[kend])<(Ts-Te)):
           print('Ts=%e, Te=%e, not space enough: kstart=%d, kend=%d\n'%(Ts,Te,kstart,kend))

        #      start and end maps found
        kmp = kstart # start counting maps here

        
        #Read masks 
        #Read m_L, t_L
        #Read m_H, t_H
        t_L = tepoch[kmp]
        t_H = tepoch[kmp+1]
        #Intermediate mask
        #Compute m_LH
        #read m_LH
        m_LH = readTemplateMap(self.data.mapsize, masks,kmp)

        #Read carrying capacities
        #Read w_L, w_H
        w_L, w_H = readCC(self.data.mapsize, yalist, kmp)

        kmp = kmp+1
        
        # Scan through map, construct segments for X and Y directions 
        #Python scanners
        nxsegs, xstart_seg, xend_seg = Xscan(m_LH)
        nysegs, ystart_seg, yend_seg = Yscan(m_LH)
        #Fortran scanners
        #nxsegs, xstart_seg, xend_seg = FXscan(m_LH)
        #nysegs, ystart_seg, yend_seg = FYscan(m_LH)

        #w_H = kapepoch(self.data.w_map,yalist,kmp, self.data.mapsize) # second map
        #t_H = tepoch[kmp]

        #Computing interpolation weights 
        s   = (t_L-Ts+tsc*h)/(t_L-t_H)  
        
        # first step interpolation weigth
        wt0 = 0.0
        # compute interpoated weight to get to step t=h
        ks  = floor(100*s)
        # residual sd = 100*s mod 1
        sd  = 100*s - ks                 
        # second interpolation weigth 
        #wth = yp[0]*(1-sd) + yp[1]*sd
        #Leo: correction for points near t_H
        if ks <100:
            wth = sigmoid[ks]*(1-sd) + sigmoid[ks+1]*sd
        else:
            wth = 1.

        # initial time in kya
        t   = Ts                       

        # TGL alternates between 1 and 0 
        TGL = True
        imID = 0
        #TGL = False

        # BEGIN main loop
        # self.NT = 1
        rows = range(0,self.data.NY)
        cols = range(0,self.data.NX)
 
        start = timer()
        for it in range(0,self.data.NT):
            #%debug 0
            #print('iteration=',it) 
            if TGL: #First choice: half-step1 in X, and step 2 in Y 
                #half-step1 in X
                step1X(rows)
                
                #step2 in Y-Direction 
                stepY(cols)

                #half-step1 in X-Direction
                step1X(rows)

                TGL = False
                
            else:    
                ##Second choice: half-step1 in Y, and step 2 in X 
                #half-step1 in Y
                step1Y(cols)
                
                #Step 2 : X direction updates
                stepX(rows)
                
                # half step1 in Y direction
                step1Y(cols)
                
                TGL = True
                
            
            verify_arrival()
            
            #if kmp > kend: 
            #    print 'break hit, leaving loop'
            #    break

            # increment t value, reset weights and maps if necessary
            
            # decrement t in kya
            t = t - tsc*h          
            #fprintf(' ordinary step: t_L=%e, t=%e, t_H=%e\n',t_L,t,t_H);
            # usual case, maps not exhausted
            # Time is decrementing from T_L to T_H 
            if t > t_H:
                #update weigth 
                wt0 = wth           # shift lower weight down
                s   = (t_L-t)/(t_L-t_H)  # interpolation parameter
                ks  = floor(100*s)
                sd  = 100*s - ks    # residual sd = 100*s mod 1
                wth = sigmoid[ks]*(1-sd) + sigmoid[ks+1]*sd  # first interpolant
            else:                   # reached upper map time 
                #Increment map counter
                kmp = kmp +1 # Leo:: review this increment map count Again???
                if kmp <= kend:               # kend is the last map
                    #m_L = m_H
                    w_L = w_H                 # set lower map to prev. upper
                    t_L = t_H                 # set lower time to prev. upper
                    #if self.data.mapsize =='big':
                    #    w_H = w_J.copy()
                    #else:
                    
                    #w_H = templateMap(self.data.mapsize, masks, kmp)
                    #m_LH = (m_L==1) | (m_H==1)
                    m_LH = readTemplateMap(self.data.mapsize, masks,kmp-1)
                    nxsegs, xstart_seg, xend_seg = Xscan(m_LH)
                    nysegs, ystart_seg, yend_seg = Yscan(m_LH)
                    #nxsegs, xstart_seg, xend_seg = FXscan(m_LH)
                    #nysegs, ystart_seg, yend_seg = FYscan(m_LH)

                    #w_L = fillins(w_L, m_L, m_LH)
                    #w_H = kapepoch(self.data.w_map, yalist, kmp, self.data.mapsize)  # read next map
                    #w_H = kapepoch(m_LH, yalist, kmp, self.data.mapsize)
                    #w_H = fillins(w_H, m_H, m_LH)
                    w_L, w_H = readCC(self.data.mapsize, yalist, kmp-1)
                    
                    t_H = tepoch[kmp]         # time frame of new map
                    #Resetting weights
                    #First interpolation weight 
                    wt0 = 0
                    # interpolation parameter 
                    s   = (t_L - t)/(t_L-t_H)  
                    ks  = floor(100*s)
                    sd  = 100*s - ks                    # mod1 interpolation 
                    wth = sigmoid[ks]*(1-sd) + sigmoid[ks+1]*sd  # first interpolant
                    
                    #Updating mask for plotting
                    self.data.w_map = m_LH.copy()

            ##Monitoring data
            if (mod(it,self.data.NPLT)==0)or(it==self.data.NT):
                if t < 10:
                    tstr = '%4.2f'%t
                    print(tstr)
                    if output:
                        storeData(imID, tstr, self.data)
                        
                    imID = imID+1

                else:
                    tstr = '%4.1f'%t
                    print(tstr)
                    if output:
                        storeData(imID, tstr, self.data)
                        
                    imID = imID+1

        end = timer()
        print("\n====="+self.data.mapsize+" simulation completed=====")
        print('   Main Loop Time:', end-start)
        pid=os.getpid()
        pickle.dump(arrivalTime, open('arrival-'+str(pid)+".p", "wb" ) )
        
class RunEmpty(RunBehavior):
    def __init(self):
        pass

    def run(self):
        pass
