\changetocdepth {2}
\select@language {english}
\contentsline {chapter}{Contents}{i}{section*.1}
\contentsline {chapter}{\chapternumberline {1}Introduction}{1}{chapter.1}
\contentsline {section}{\numberline {1.1}Objectives}{2}{section.1.1}
\contentsline {chapter}{\chapternumberline {2}Background}{3}{chapter.2}
\contentsline {section}{\numberline {2.1}Governing Equations}{4}{section.2.1}
\contentsline {section}{\numberline {2.2}Brownian Motion}{5}{section.2.2}
\contentsline {section}{\numberline {2.3}Langevin Equation}{8}{section.2.3}
\contentsline {section}{\numberline {2.4}The Birth and Death Process}{8}{section.2.4}
\contentsline {subsection}{\numberline {2.4.1}Death}{9}{subsection.2.4.1}
\contentsline {subsection}{\numberline {2.4.2}Birth}{9}{subsection.2.4.2}
\contentsline {subsection}{\numberline {2.4.3}The Master Equation}{10}{subsection.2.4.3}
\contentsline {subsection}{\numberline {2.4.4}Fokker-Plank Equation}{11}{subsection.2.4.4}
\contentsline {section}{\numberline {2.5}Generating Functions}{14}{section.2.5}
\contentsline {subsubsection}{Simple birth-and-death process}{14}{section*.2}
\contentsline {subsubsection}{Birth-and-death process with immigration}{15}{section*.3}
\contentsline {section}{\numberline {2.6}Gillespie Algorithm}{16}{section.2.6}
\contentsline {section}{\numberline {2.7}Analytical Solutions}{17}{section.2.7}
\contentsline {section}{\numberline {2.8}Numerical Schemes}{18}{section.2.8}
\contentsline {subsection}{\numberline {2.8.1}Operator Splitting}{19}{subsection.2.8.1}
\contentsline {subsubsection}{Godunov Splitting}{20}{section*.4}
\contentsline {subsubsection}{Strang Splitting}{21}{section*.5}
\contentsline {subsubsection}{General Operator Splitting}{22}{section*.6}
\contentsline {subsubsection}{Symplectic Integrators}{23}{section*.7}
\contentsline {subsubsection}{Fourier Solver}{23}{section*.8}
\contentsline {chapter}{\chapternumberline {3}Methods}{25}{chapter.3}
\contentsline {section}{\numberline {3.1}Godunov Solver 1D}{26}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Boundary conditions}{28}{subsection.3.1.1}
\contentsline {subsubsection}{Dirichlet}{28}{section*.9}
\contentsline {subsubsection}{Neumann}{29}{section*.10}
\contentsline {subsection}{\numberline {3.1.2}Tridiagonal Solvers}{34}{subsection.3.1.2}
\contentsline {section}{\numberline {3.2}Godunov Solver 2D}{35}{section.3.2}
\contentsline {subsection}{\numberline {3.2.1}Time Dependencies}{36}{subsection.3.2.1}
\contentsline {subsubsection}{Carrying capacities}{36}{section*.11}
\contentsline {subsubsection}{Sea-Level}{36}{section*.12}
\contentsline {subsection}{\numberline {3.2.2}Smoothing}{36}{subsection.3.2.2}
\contentsline {section}{\numberline {3.3}Parallelization}{37}{section.3.3}
\contentsline {section}{\numberline {3.4}Optimization}{37}{section.3.4}
\contentsline {chapter}{\chapternumberline {4}Results}{39}{chapter.4}
\contentsline {section}{\numberline {4.1}1D Solver}{39}{section.4.1}
\contentsline {section}{\numberline {4.2}Optimization}{39}{section.4.2}
\contentsline {section}{\numberline {4.3}2D Solver}{39}{section.4.3}
\contentsline {section}{\numberline {4.4}Stability and Oscillations}{39}{section.4.4}
\contentsline {subsection}{\numberline {4.4.1}HDF5 Data}{40}{subsection.4.4.1}
\contentsline {section}{\numberline {4.5}Parallelization}{40}{section.4.5}
\contentsline {chapter}{\chapternumberline {5}Conclusions}{45}{chapter.5}
\contentsline {chapter}{\chapternumberline {6}Appendix A: Algorithms}{46}{chapter.6}
\contentsline {chapter}{\chapternumberline {7}Appendix B: Analytical Solution}{51}{chapter.7}
\contentsline {chapter}{\chapternumberline {8}Appendix C: Time Line}{55}{chapter.8}
\contentsline {chapter}{References}{56}{section*.14}
