\section{Brownian Motion}

In 1827 the botanist Robert Brown discovered that pollen particles move erratically. \cite{Einstein1905} introduced the first mathematical treatment of erratic movement of the Brownian particles. He introduced a probabilistic description valid for an ensemble of Brownian particles \citep{Toral2014}. Introducing the probability density function, $\phi(\Delta)$, a Brownian particle travels a distance $\Delta$ within a time interval $\tau$. The assumptions about the function $\phi(\Delta)$ are: 

1) non-negativity; 

2) normalization, which means

\begin{equation}
\int_{-\infty}^{+\infty} \phi(\Delta) d\Delta = 1
\end{equation}; and

3) collisions occur with the same probability in any direction. This translates into a symmetry condition for $\phi(\Delta)$ as 
\begin{equation}
\phi(\Delta) = \phi(-\Delta).
\end{equation}  

%and $\phi$ only for very small values of $\Delta$ the condition 
%\begin{equation}
%\phi(\Delta) = \phi(-\Delta)
%\end{equation}

%Albert Einstein derived in his seminal paper the diffusion equation \citep{Einstein1905}

We can define the density of particles $u(x,t)$ such that $u(x,t)dx$ is the number of particles in the interval $(x,x+dx)$.

According to \cite{Einstein1905} the number of particles at $x$ at time $t+\tau$ equals the total number of particles that jumped to $x$ from $x+\Delta$ at time $t$,

\begin{equation}\label{eq:einstein}
u(x, t + \tau) dx= dx \int_{-\infty}^{+\infty} u(x+\Delta, t)\phi(\Delta)d \Delta. 
\end{equation}

For a very small $\tau$ we may write 
\begin{equation}\label{eq:taylorTau}
u(x, t + \tau) = u(x,t) + \tau \frac{\partial f}{\partial t }.
\end{equation}

We can now Taylor expand $u(x+\Delta,t)$ as
\begin{equation}\label{eq:taylorDelta}
u(x+\Delta, t) = u(x,t) + \Delta \frac{\partial u(x,t)}{\partial x} + \frac{\Delta^2}{2!} \frac{\partial^2 u(x,t)}{\partial x^2} \ldots 
\end{equation}

Replacing equations \ref{eq:taylorDelta} and \ref{eq:taylorTau} into \ref{eq:einstein} leads to
%u(x,t) + \tau \frac{\partial f}{\partial t} = dx \int_{\Delta = -\infty}^{+\infty}u(x+\Delta)\phi(\Delta) d\Delta \\
%u(x,t+\tau)dx = dx \int_{-\infty}^{\Delta=+\infty} u(x+\Delta) \phi(\Delta)d(\Delta) \\
\begin{align}
\begin{split}
u(x,t) + \frac{\partial u(x,t)}{\partial t} \tau = f \int_{-\infty}^{+\infty} \phi(\Delta) d\Delta \\
                                            + & \frac{\partial f}{\partial x} \int{\Delta \phi(\Delta)}d\Delta \\
                                              & + \frac{\partial^2 f}{\partial x^2}\int_{-\infty}^{+\infty} \frac{\Delta^2}{2!}\phi(\Delta)d\Delta \ldots, 
\end{split}
\end{align}

and we write the differential equation
\begin{equation}
\frac{\partial u(x,t)}{\partial t} = \frac{1}{\tau}\int_{-\infty}^{+\infty} \frac{\Delta^2}{2}\phi(\Delta) d\Delta \, \frac{\partial^2 f}{\partial x ^2}.
\end{equation}

Defining the diffusion coefficient

\begin{equation}
D:=\frac{1}{\tau} \int_{-\infty}^{+\infty} \frac{\Delta^2}{2}\phi(\Delta) d\Delta = \frac{<\Delta^2>}{2\tau},  
\end{equation}.

the diffusion equation reads

\begin{equation}
\frac{\partial u(x,t)}{\partial t} = D \nabla^2 u. 
\end{equation}

Given $n$ Particles at the origin at time $t=0$, the solution of the diffusion equation reads,

\begin{equation}
u(x,t) = \frac{n}{ (4 \pi D t)^{\frac{3}{2}}} e^{-\frac{x^2}{4 D t}}
\end{equation}, 

and it follows, 


\begin{equation}
\begin{split}
\langle x(t) \rangle = 0, \\
\langle x(t)^2 \rangle = 6 D t.
\end{split} 
\end{equation}


Following \cite{Murray} we can relate this result to Fickian diffusion. For classic Fickian diffusion, the flux of individuals $J$ is proportional to the gradient of the concentration, 

\begin{equation}
\begin{split} 
J \propto \frac{\partial u}{\partial x}, \\ 
J=-D\frac{\partial u}{\partial x}.
\end{split}
\end{equation}

From the general conservation equation, we can write 

\begin{equation}
\frac{\partial u}{\partial t} = - \frac{\partial J}{\partial x},  
\end{equation}

and if $D$ is constant it becomes

\begin{equation} \label{eq:diffusion}
\frac{\partial u}{\partial t} = D\frac{\partial^2 u}{\partial x^2}.
\end{equation}

As it was mentioned before, the equation \ref{eq:diffusion} describes the dispersal of a population in space.


\begin{comment}
Let us assume that to reach a point $x=m\Delta x$ after a time $t = n\Delta t$ the particle has moved $a$ steps to the right and b steps to the left \citep{Murray}. Then

\begin{equation*}
\begin{split}
m = a - b, \quad a + b = n \\
\Rightarrow a = \frac{n+m}{2}, \quad b=n-a 
\end{split}
\end{equation*}

The number of possible paths that a particle can reach a point $x=m\Delta x$ is 

\begin{equation*}
\frac{n!}{a!b!} = \frac{n!}{a!(n-a)!} = C_a^n,
\end{equation*}

where $C_a^n$ is the binomial coefficient defined by

\begin{equation*}
(x+y)^n = \sum_{a=0}^n C_a^n x^{n-a} y^a.
\end{equation*}

The total number of possible $n$-step paths is $2^n$ and the probability p(m,n) is 

\begin{equation}
p(m,n)=\frac{\text{favorable possibilities}}{\text{total possibilities}}=\frac{1}{2^n}\frac{n!}{a!(n-a)!},\quad a = \frac{n+m}{2},
\end{equation}
n+m is even and $p(m,n)$ is the binomial distribution.

Now let $n$ be large so that $n \pm  m$ is also large

\begin{equation}
n! = \Gamma (n+1) = \int_{0}^{\infty} e^{-t} t^n dt
\end{equation}  

\end{comment}
