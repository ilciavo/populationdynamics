\section{Generating Functions}

\cite{Bailey} proposed a sophisticated technique to study stochastic birth, death, and migration by using the generating moment function. To understand this technique we first define a the probability-generating function $P(s,t)$ for the probability distribution $p(n;t)$ as 

% to study stochastic birth, death and migration processes is the one proposed by \cite{Bailey} 


\begin{equation}
P(s,t) = \sum_{n=0}^{\infty} p(n;t)s^n,
\end{equation}

where $P(s,t)$ is a function of $t$ and the dummy variable $s$.

The probability generating function allows the determination of the moments of the random variable $n(t)$ \citep{Toral2014}. To recover the n-th moment we simply evaluate the n-th derivative of the generating function at $s=1$

%We can recover the probability distribution $p(x,t)$ by taking the first derivative with respect to $x$ and evaluating at $x=0$.

\begin{equation}
\langle n(t)^n \rangle= \sum_{n=0}^{\infty} n^n \,p(n;t) = \frac{\partial^n P(s,t)}{\partial s^n}\Big|_{s=1}. 
\end{equation}

We pass to the moment generating function $M(\theta,t)$ by putting $s = e^{\theta}$, giving 

\begin{equation}
M(\theta, t) = \sum_{n=0}^{\infty} p(n;t)e^{n\,\theta}.
\end{equation}

Similarly, the cumulant-generating function $K(\theta,t)$ is defined as 

\begin{equation}
K(\theta,t)=log M(\theta,t)
\end{equation}

\subsubsection{Simple birth-and-death process}

As described before one can write the probabilities of birth and death as  

\begin{equation}
\begin{split}
Pr(\text{birth}) = B \, n(t) \Delta t + \mathcal{O}(\Delta t), \\
Pr(\text{death}) = D \, n(t) \Delta t + \mathcal{O}(\Delta t).
\end{split}
\end{equation}

The partial differential equation for the moment-generating function can be written as 

\begin{equation}
\frac{\partial M}{\partial t } = \left \{ B (e^{\theta} -1) + D(e^{-\theta}-1) \right \} \frac{\partial M}{\partial \theta},
\end{equation}

with initial condition 
\begin{equation}
M(\theta, 0) = e^{a\theta}.
\end{equation}

\cite{Bailey1964} shows that the solution for the moment generating function reads

\begin{equation}
M(\theta, t) = \left ( \frac{D v(\theta, t) - 1}{B v(\theta,t)-1}\right ) ^a,
\end{equation}

where 

\begin{equation}
v(\theta, t) = \frac{(e^{\theta}-1)e^{(B - D)t}}{B e^{\theta} - D}.
\end{equation}

\subsubsection{Birth-and-death process with immigration}

We add now the effect of immigration to the aforementioned birth-and-death process. The effect of adding one individual to the system by effects of immigration is quantified by the parameter $V$, 

\begin{equation}
\begin{split}
Pr(\text{one unit gain}) = B \, n(t) \Delta t + V \Delta t + \mathcal{O}(\Delta t), \\
Pr(\text{one unit loss}) = D \, n(t) \Delta t + \mathcal{O}(\Delta t).
\end{split}
\end{equation}

If follows the partial differential equation for the moment-generating function,
  
\begin{equation}
\begin{split}
\frac{\partial M}{\partial t } &= (e^{\theta}-1)\left (B \frac{\partial}{\partial \theta} + V \right) M + D(e^{-\theta} -1) \frac{M}{\partial \theta}  \\ 
                               &= \left \{ B (e^{\theta} -1) + D(e^{-\theta}-1) \right \} \frac{\partial M}{\partial \theta} + V (e^{\theta}-1) M. 
\end{split}
\end{equation}

Now the solution found by \cite{Bailey1964} has the form  

\begin{equation}
M(\theta,t) = \frac{(B-D)^{V/B}\left\{ D(e^{(B-D)t}-1) - (D e^{(B-D)t}- B) e^{\theta} \right\}^a }{\left\{ (Be^{(B-D)t}-D)-B(e^{(B-D)t}-1)e^{\theta} \right \}^{a+V/B} }.
\end{equation}


For multiple dimensions the moment generating function is defined introducing dummy variables $\theta_i$,

\begin{equation}
M(\ldots \theta _i \ldots;t) = \mathbb{E}[exp \left( \sum_{i=-\infty}^{\infty} \theta_i \, X_i \right )] = \sum_{i=0}^{\infty} exp\left( \sum_{i=-\infty}^{\infty} \theta_i \, X_i \right) P(X_i;t)
\end{equation}

\cite{Bailey} went further and found a solution for the generating function for mean values of stochastic birth, death and migration. He uses the already mentioned machinery, Laplace transform, and modified Bessel functions to write a solution for the partial differential equation 

\begin{equation}
\frac{\partial M}{\partial t} = \sum_{i=-\infty}^{+\infty} \left\{ B(e^{\theta_i}-1) + D(e^{\theta_i}-1) + \frac{1}{2} V(e^{\theta_i+\theta_{i+i}-1)} +\frac{1}{2} V (e^{-\theta_i + \theta_{i-1}-1}) \right \} \frac{\partial M}{\partial \theta_i}.
\end{equation}

Finding this solution is not an easy task, and the reader may refer to \cite{Bailey} for an elaborated procedure on how to find the solution. The solution for the generating function for mean values is given by 

\begin{equation}
G(x,t) =  \sum_{i=-\infty}^{+\infty} a_i x^i     exp  \left[ \left\{ B-D-V + \frac{1}{2}  \left (x+\frac{1}{x} \right) \right\} t \right ].
\end{equation}

\begin{comment}

\begin{equation}\label{partialT}
\frac{\partial M}{\partial t} = \sum_{X_i=0}^{\infty} exp \left (\sum_{i=-\infty}^{\infty} \theta_i \, X_i \right ) \frac{\partial P(X_i;t)}{\partial t}
\end{equation}

\begin{equation} \label{partialTheta}
\frac{\partial M}{\partial \theta_i} = \sum_{X_i=0}^{\infty} X_i\,e^{\sum_{i=-\infty}^{\infty} \theta_i \, X_i} P(X_i;t)
\end{equation}

From the master equation \ref{eq:derivativeBasic} we can write  

\begin{equation}\label{masterEquation}
\frac{\partial P(X_i;t)}{\partial t}= -((X_i)B+(X_i)D)\, P(X_i;t)+ (X_i-1)B\, P(X_i-1;t)+ (X_i+1)D\,P(X_i+1;t) 
\end{equation}

Replacing \ref{partialT} into \ref{masterEquation}

\begin{align}
\begin{split}
\frac{\partial M}{\partial t} = \sum_{X_i=0}^{\infty} exp(\sum_{i=-\infty}^{\infty} \theta_i \, X_i)((-(X_i)B-(X_i)D)\, P(X_i;t) + &(X_i-1)B\, P(X_i-1;t) \\ 
                                                                                                                                &+ (X_i+1)D\,P(X_i+1;t)) 
\end{split}
\end{align}

Arranging terms

\begin{align} \label{expanded}
\begin{split}
\frac{\partial M}{\partial t} = -&B\sum_{X_i=0}^{\infty} exp(\sum_{i=-\infty}^{\infty} \theta_i \, X_i) X_i \, P(X_i;t) \\ 
                                &-D \sum_{X_i=0}^{\infty} exp(\sum_{i=-\infty}^{\infty} \theta_i \, X_i) X_i \, P(X_i;t)  \\
                                &+B\sum_{X_i=0}^{\infty} exp(\sum_{i=-\infty}^{\infty} \theta_i \, (X_i-1)) exp(\sum_{i=-\infty}^{\infty} \theta_i ) (X_i-1)\, P(X_i-1;t)  \\
                                &+D\sum_{X_i=0}^{\infty} exp(\sum_{i=-\infty}^{\infty} \theta_i \, (X_i+1)) exp(\sum_{i=-\infty}^{\infty} -\theta_i ) (X_i+1)\,P(X_i+1;t)
\end{split}
\end{align}

Finally replacing \ref{partialTheta} into \ref{expanded} 
\end{comment}
