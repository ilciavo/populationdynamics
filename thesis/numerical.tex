\section{Numerical Schemes}

In order to solve our partial differential equation numerically, we need to discretize it, and this will be covered in chapter \ref{ch:methods}. A key element of our implementation is the operator splitting method. Therefore we dedicate special attention to the derivation of this prominent technique. 
% and since this is one of the key elements of our work, we dedicate special attention to this prominent technique.
%\section{Reaction Diffusion Equation}

\subsection{Operator Splitting}

Operator Splitting methods were introduced last century, the fundamental concept behind this method is the divide-and-conquer strategy. Using this technique, we divide our problem into simple and solvable subsets of problems. It is essential to keep the right balance between simplicity and accuracy; therefore, special care must be taken with accumulated errors. In this section we describe the derivation of such methods. 

Given the following equation

\begin{equation}
\frac{dq}{dt} = A(q) + B(q),
\end{equation}

applying forward Euler leads to the poor operator adding scheme
%The simplest way to solve this differential equation is using the operator adding scheme 
\begin{equation}
\begin{split}
q_A^{n+1} = q^n + \Delta t A(q^n) \\
q_B^{n+1} = q^n + \Delta t B(q^n) \\
q^{n+1}_{A+B} = q^n + \Delta t A(q^n) + \Delta t B(q^n).
\end{split}
\end{equation}

Recently, \cite{numericalHydrodynamics} compare two well known splitting schemes, \cite{godunov1987} and \cite{strang1968}.

\cite{godunov1987} introduced the operator splitting scheme as
\begin{equation}
\begin{split}
q_A^{n,*} = q^n + \Delta t A(q^n) \\
q_B^{n+1} = q_A^{n,*} + \Delta t B(q_A^{n,*})
\end{split}
\end{equation} 

\cite{strang1968} and \cite{marchuk1968} introduced in parallel a more sophisticated splitting scheme

\begin{equation}
\begin{split}
q_{A/2}^{n,*} = q^n + \frac{\Delta t}{2} A(q^n) \\
q_{A/2+B}^{n,**} = q_{A/2}^{n,*} + \Delta t B(q_{A/2}^{n,*}) \\
q_{A+B}^{n+1} = q_{A/2+B}^{n,**} + \frac{\Delta t}{2} A(q_{A/2+B}^{n,**})
\end{split}
\end{equation}


\subsubsection{Godunov Splitting}

\cite{godunov1987} introduced the splitting schemes in the following manner. Given the equation 

\begin{align}
\begin{split}
\frac{\partial u }{\partial t} = \bm{A} u, \\ 
0 < t < T, \\
u|_{t=0} = u_0,
\end{split}
\end{align}

we can write the approximation

\begin{align}
\begin{split}
u(x,y,t_p+\tau) &= u(x, y, t_p) + \tau \frac{\partial u }{\partial t} + \mathcal{O} (\tau^2)\\
                &= u(x,y, t_p) + \tau \bm{A} u(x,y,t_p) + \mathcal{O}(\tau ^2) \\
                &= (\bm{I} + \tau \bm{A}) u(x,y, t_p) + \mathcal{O}(\tau ^2).   
\end{split}
\end{align}

Now let us assume that $A$ can be written as
%comes the splitting by writing 
\begin{equation}
\bm{A} = \bm{A}_1 + \bm{A}_2,
\end{equation}

hence it is possible to write  

\begin{equation}
\frac{\partial u }{\partial t} = \bm{A}_1 u + \bm{A}_2 u. 
\end {equation}

Introducing the variables $v(x,y,t_p) = u(x,y,t_p)$ and $w(x,y,t_p) = v(x,y,t_{p+1})$ such that 

\begin{equation}
\begin{split}
\frac{\partial v}{\partial t} = \bm{A}_1 v ,  \\
\frac{\partial w}{\partial t } = \bm{A}_2 w , \\
t_p\le t \le t_{p+1}. 
\end{split}
\end{equation}

Solving the equations 

\begin{align}
\begin{split}
w(x,y,t_{p+1})  &= (\bm{I}+ \tau \bm{A}_2) w(x,y, t_p) + \mathcal{O}(\tau ^2) \\
                &= (\bm{I}+ \tau \bm{A}_2) v(x,y,t_{p+1}) + \mathcal{O}(\tau ^2) \\
                &= (\bm{I}+ \tau \bm{A}_2) (\bm{I} + \tau \bm{A}_1)  v(x,y,t_p) + \mathcal{O} (\tau ^2) \\
                &= (\bm{I}+\tau (\bm{A}_1+\bm{A}_2)) v (x,y,t_p) + \mathcal{O} (\tau ^2) \\
                &= (\bm{I}+\tau \bm{A} ) v(x,y,t_p) + \mathcal{O}(\tau ^2) 
\end{split}
\end{align}

results in

\begin{equation}
w(x,y,t_{p+1}) = v (x,y,t_{p+1}) + \mathcal{O}(\tau^2), 
\end{equation}


which is our first order approximation. 

Now given the diffusion equation

\begin{equation}
\frac{\partial u}{\partial t} = \frac{\partial^2 u }{\partial x ^2} + \frac{\partial^2 u }{\partial y ^2}, 
\end{equation}

we can essentially divide this problem into two subproblems and define the diffusion matrices 

\begin{equation}
\begin{split}
\bm{A}_{xx} u_{m,n} = \frac{u_{m+1,n}-2 u_{m,n}+ u_{m-1,n}}{h^2}, \\
\bm{A}_{yy} u_{m,n} = \frac{u_{m,n+1}-2 u_{m,n}+ u_{m,n-1}}{h^2},
\end{split}
\end{equation}

which is the Alternating Direction Implicit method introduced by \cite{ADI}. 

\subsubsection{Strang Splitting}

Divide and conquer is a widely used technique to decrease the number of operations. This is the main idea behind operator splitting method which can be either used as dimensional or sequential splitting. Following \cite{strangMacNamara}, given the differential equation 

\begin{equation}
\frac{\partial u }{\partial t} = \bm{M} u = (\bm{A}+\bm{B})u,
\end{equation}

the solution will read 

\begin{equation}
u(h) = e ^{h(\bm{A}+\bm{B})} u(0).
\end{equation}

The term $e^{h(\bm{A}+\bm{B})}$ is not easy to compute and therefore we need an efficient way for computing this term. 

If we expand $e^{h(\bm{A}+\bm{B})}$, 

\begin{equation}
e^{h(\bm{A}+\bm{B})} = I + h(\bm{A}+\bm{B}) + \frac{1}{2} h^2 (\bm{A}+\bm{B})^2 \ldots,
\end{equation}

we need to compute the terms $e^{h\bm{A}}$ and $e^{h\bm{B}}$ separately. This separation leads to the following forms:
\begin{itemize}

\item In case $\bm{AB}-\bm{BA} = 0$ we can use the first order approximation \citep{golub},
\begin{equation}
e^{h(\bm{A}+\bm{B})} \approx e^{h\bm{A}} e^{h\bm{B}};
\end{equation}  

\item or if the latter fails, we can use the second order approximation \citep{marchuk1968,strang1968},  

\begin{equation}
e^{h(\bm{A}+\bm{B})} \approx e^{\frac{1}{2}hA} \, e^{h\bm{B}} \, e^{\frac{1}{2}h\bm{A}}.
\end{equation}

\end{itemize}

\subsubsection{General Operator Splitting}

\cite{numericalRecipes} generalizes the operator splitting method as a method of fractional steps. 

Given the following equation 

\begin{equation}
\frac{\partial u}{\partial t} = \mathcal{L} u,
\end{equation}

where $\mathcal{L}$ is an operator, we can divide it as a linear sum of $m$ pieces,

\begin{equation}
\mathcal{L} = \mathcal{L}_1 u + \mathcal{L}_3 u + \ldots \mathcal{L}_m u. 
\end{equation}

For each piece we know a differencing scheme for updating the variable $u$ from $n$ to $n+1$, and we can write these updates as 

\begin{equation}
\begin{split}
u^{n+1} = \mathcal{U}_1(u^n, \Delta t) \\
u^{n+1} = \mathcal{U}_2(u^n, \Delta t) \\
\ldots \\
u^{n+1} = \mathcal{U}_m(u^n, \Delta t)
\end{split}
\end{equation}

Now, one form of operator splitting would be to get from $n$ to $n+1$ doing:

\begin{equation}
\begin{split}
u^{n+(1/m)} = \mathcal{U}_1(u^n, \Delta t) \\
u^{n+(1/m)} = \mathcal{U}_2(u^n, \Delta t) \\
\ldots \\
u^{n+1} = \mathcal{U}_m(u^n, \Delta t)
\end{split}
\end{equation}

\subsubsection{Symplectic Integrators}

%Given the hamiltonian form 

%\begin{equation}
%H = T(p) + V(q)
%\end{equation}

Strikingly, \cite{yoshida1990} provides even a more generalized form of symplectic integrators\footnote{Symplectic integrators are essential to integrate Hamiltonian systems. They keep closed orbits for harmonic oscillators and preserve fundamental quantities, for example, energy, angular momentum, etc. A symplectic integrator conserves the area in phase space delimited by an ensemble of systems. For details of symplectic integrators, the reader can refer to \cite{harrier}.} of arbitrary order.
\begin{equation}
e^{\tau (\bm{A}+\bm{B})} = \prod_{i=1}^{k} e^{c_i \tau \bm{A}} e^{d_i \tau \bm{B}} + \mathcal{O}(\tau^{n+1}), 
\end{equation} 

where $c_i$ and $d_i$ are coefficients to be found.

If $n=1$, $c_1=d_1=1$,$k=1$, and we obtain the Godunov splitting scheme,

\begin{equation}
e^{\tau(\bm{A}+\bm{B})} = e^{\tau \bm{A}}e^{\tau \bm{B}} + \mathcal{O}(\tau^{n+1}),
\end{equation}

and if $n=2$, $c_1=c_2=\frac{1}{2}, d_1=1, d_2=0$, $k=0$, and we obtain the Strang-Marchuk splitting scheme,

\begin{equation}
e^{\tau(\bm{A}+\bm{B})} = e^{\frac{\tau \bm{A}}{2}}e^{\tau \bm{B}}e^{\frac{\tau \bm{A}}{2}} + \mathcal{O}(\tau^{n+1}).
\end{equation}

\subsubsection{Fourier Solver}

We now provide an example adapted from \cite{wikiwaves}. We will extend a simple Fourier solver using the Operator Splitting scheme. We recall the Fourier transform and inverse Fourier transform,

\begin{equation}
\mathcal{F}\left\{ u(x) \right\} = \hat{u}(k) = \int_{-\infty}^{+\infty}u(x)e^{-ikx}dx, 
\end{equation}

%and the inverse Fourier transform is given by 

\begin{equation}
u(x) = \mathcal{F}^{-1} \left\{ \hat{u}(k) \right\} = \frac{1}{2\pi} \int_{-\infty}^{+\infty}\hat{u}(k) e^{ikx} dx,
\end{equation}

and we also recall one of the most important identities, 

\begin{equation}
\int_{-\infty}^{+\infty} \frac{\partial u(x)}{\partial x} e^{-ikx} dx = ik \int _{-\infty} ^{+\infty} u(x) e^{-ikx} dx.
\end{equation}

Applying Fourier tranform to our reaction-diffusion equation

\begin{equation}
u_t(x,t) = u_{xx}(x,t) + u(x,t)(1-u(x,t))
u(0,x) = \phi(x)
\end{equation}

gives

\begin{equation}
\hat{u_t}(k,t) = (ik)^2 \hat{u}(k,t) + \mathcal{F}\left\{ u(x,t)(1-u(x,t)) \right\}.
\end{equation}.

Taking the inverse Fourier transform gives

\begin{equation}
u_t(x,t) = \mathcal{F} \left\{ (ik)^2 \hat{u}(k,t) \right\} + u(x,t) (1-u(x,t)).
\end{equation}.


Now we can apply the already mentioned operator splitting. On one hand, the Fourier transform in $x$ of the equation 

\begin{equation}
u_t = u_{xx}
\end{equation}

gives

\begin{equation}
\hat{u}_t = (ik)^2\hat{u}
\end{equation}.

Taking the inverse Fourier transform gives

\begin{equation}\label{eq:fourier}
\tilde{u}(x,t+\Delta t) = \mathcal{F}^{-1} \left\{e^{-k^2 \Delta t} \hat{u}(x,t) \right\} 
\end{equation}.

On the other hand, solving the equation using forward Euler

\begin{equation}
u_t = u \, (1-u)
\end{equation}

leads to

\begin{equation}
u(x,t+\Delta t) = u_0+u_0(1-u_0)\Delta t,
\end{equation}

and replacing our $\tilde{u}$ from equation \ref{eq:fourier} 

\begin{equation}
u(x,t+\Delta t) = \tilde{u}+\tilde{u}(1-\tilde{u})\Delta t.
\end{equation}

Our example represents the main idea of the operator splitting method. 

%\begin{equation}
%u(x,t+\Delta t) = e^{-u(1-u)\Delta t} \tilde{u}(x,t +\Delta t). 
%\end{equation}
