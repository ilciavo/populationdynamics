# README #

This README would normally document whatever steps are necessary to get your application up and running.



Let me know if you have any further question.


### What is this repository for? ###

* This repository contains my master thesis for Computational Science and Engineering. 
* It is a deterministic simulation of population dynamics using a Godunov solver of Fisher-Kolmogorov Equation in two dimensions 
* Version 0.1
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###


* The main file in parallel is called "Simulator2.ipynb" which launches python engines in parallel and collects the results from the cluster.

* For different resolutions and simulations, you can also look at "Simulation.py".

* I used Python notebooks (*.ipynb) to generate the outputs.

* The modules written in FORTRAN have the extension *.f90 (f2py is needed)


### Who do I talk to? ###

* Leonardo Echeverria: leonardo.echeverria@gmail.com
* Other community or team contact