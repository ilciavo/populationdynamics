from numpy import *
import scipy.signal

def godunovstep1(h,kcfl,u):
    # first compute Euler estimate
    #ut = (Amult(n,u))'
    #ut = k*ut 
    #uE = ut + h*(1-u).*u # commmon part of Euler est.
    n = size(u)
    kcfl4= kcfl/4

    #ut = Amult(n,u)
    ut = amultDirichlet(u)
    
    rhs = u[0:n]+ kcfl4*ut
    #solve using tridiag solver 
    #v = trisol0(n,kcfl,rhs,s1,s2)
    v = trisol0Dirichlet(kcfl, rhs)
    return v

def godunovstep2(h,k,u,kap0,kaph):
    #kap0 : carrying capacity low 
    #kaph : carrying capacity high
    # first compute Euler estimate
    #ut = (Amult(n,u))'
    #ut = k*ut 
    #uE = ut + h*(1-u).*u # commmon part of Euler est.
    #ut = Amult(n,u)
    n = size(u)
    ut = amultDirichlet(u)
    ut = k*ut
    #try:
    #right hand side part for part of Euler uses K(t)
    uC = u[0:n]/kap0[0:n]
    #except ZeroErrorDivision:
    #    print 'godunovstep2 divides by Zero kap0[0:n]', kap0[0:n]

    #try:
    #left hand side version uses K(t+h)
    uh = u[0:n]/kaph[0:n]
    #except ZeroErrorDivision:
    #print 'godunov2 divides by Zero kaph[0:n]', kap0[0:n]


    #common part of Euler estimate 
    uE = ut + h*(1-uC)*u[0:n]
    #Compute right hand side 
    ur = u[0:n]+0.5*uE #RHS
    uE = u[0:n]+uE
    
    #dA = zeros(n)
    dA = empty(n)
    dA[0] = 1 
    dA[n-1] = 1 #end points
    # interior diags. of LHS matrix:
    dA[1:n-1] = 1 + k - 0.5*h*(1-uh[1:n-1])
    
    #solve using tridiag solver 
    #v = trisol1(n,k,dA,ur,s1,s2)
    
    #Leo: no regularizer
    #v = trisol1Dirichlet(k,dA,ur)
    #Leo: saturation and avoiding negative values 
    v = minimum(maximum(trisol1Dirichlet(k,dA,ur),0.),kaph)
    
    return v

def amultDirichlet(u):
    # it performs tridiagonal matrix multiply, returns n elements of v
    # with v(1) = v(n) = 0, the others are u(i-1)+u(i+1)-2*u(i)
    #   v(1) = 0; v(n) = 0;
    #   v(2:n-1) = u(1:n-2)+u(3:n)-2*u(2:n-1);
    n = size(u)
    v = zeros(n)
    v[0] = 0 
    v[-1] = 0
    v[1:-1] = u[0:-2]+u[2:n]-2*u[1:-1]
    return v

def trisol0Dirichlet(k,b):
#  Tridiagonal system solver modeled on
#  G.E. Forsythe and C.B. Moler " Computer 
#  solutions to linear algebraic systems,"
#  Prentice-Hall publ., 1967
#  Tridiagonal system is Tx=b, where
#
#  T = [1     0   0    ....          0 ]
#      [-k/2 d(2) -k/2 ....          0 ]
#      [0    -k/2 d(3) -k/2  0 ....  0 ]
#      [0           ....             0 ]
#      [0              -k/2 d(n-1) -k/2]
#      [0                0    0      1 ]
# 
# and k = h/dx^2 is the CFL factor. Diagonals
# d = (1 d(2) d(3) ...  d(n-1) 1) are strong.
# s1 and s2 are work arrays.(Leo: s2 new diagonal, s1 new off-diagonal)  
# In the original F & M notation,
#
#  T = [d(1)  f(1)  0        ...             0 ]
#      [e(2)  d(2)  f(2)  0  ...             0 ]
#      [ 0          ....                     0 ]
#      [ 0                e(n-1) d(n-1) f(n-1) ]
#      [ 0                  0    e(n)   d(n)   ]
#
#  wpp 27 May 2012
# def trisol(n,k,d,b,s1,s2):
    n = size(b)
    s1 = zeros(n)
    s2 = zeros(n)
    x = zeros (n)
    mk4 = -k/4
    k2 = k/2
    s2[0] = 1
    s2[1] = 1 + k2
    s1[1] = mk4

    # Leo: Forward elimination
    for i in arange(2,n-1):
        s1[i] = mk4/s2[i-1]
        s2[i] = 1+k2 - mk4*s1[i]
        

    s1[n-1] = 0 
    s2[n-1] = 1
    
    x[0]=b[0]
    for i in arange(1,n):
        x[i] = b[i] - s1[i]*x[i-1]

    # Leo: backward substitution 
    #try:
    x[n-1] = x[n-1]/s2[n-1]
    ii = n-2
    #except ZeroDivisionError:
    #print 'Trysol0 dividing by Zero s2[n-1] n=',n
        
    for i in arange(0,n-2):
        #try: 
        x[ii] = (x[ii]-mk4*x[ii+1])/s2[ii]
        ii = ii - 1
        #except ZeroDivisionError:
        #print'Trisol0 dividing by Zero s2[ii] ii=',ii
        
    #x(1) = x(1)/s2(1);
    #try:
    x[0] = x[0]/s2[0]
    #except ZeroDivisionError:
        #print 'Trisol0 Error dividing by s2[0]==0'

    return x

def trisol1Dirichlet(k,d,b):
    n = size(d)
    # constant: mk2 (minus k divided by 2)
    #   mk2   = -k/2;
    #   s2(1) = 1;
    #   s2(2) = d(2);
    #   s1(2) = mk2;
    s1 = zeros(n)
    s2 = zeros(n)
    x = zeros(n)

    mk2 = -k/2
    s2[0] = 1
    s2[1] = d[1]
    s1[1] = mk2

    # Leo: Forward elimination
    #for i=3:n-1
    #  s1(i) = mk2/s2(i-1);
    #  s2(i) = d(i) - mk2*s1(i);
    #end
    for i in arange(2,n-1):
        s1[i] = mk2/s2[i-1]
        s2[i] = d[i] - mk2*s1[i]
        
    #s1(n) = 0; s2(n) = 1;
    s1[n-1] = 0
    s2[n-1] = 1
    
    #x(1) = b(1);
    #for i=2:n
    #    x(i) = b(i) - s1(i)*x(i-1);
    #end
    x[0]=b[0]
    for i in arange(1,n):
        x[i] = b[i] - s1[i]*x[i-1]

    # Leo: backward substitution 
    #x(n) = x(n)/s2(n); ii = n-1;
    #for i=1:n-2
    #   x(ii) = (x(ii) - mk2*x(ii+1))/s2(ii);
    #   ii    = ii - 1;
    #end
    x[n-1] = x[n-1]/s2[n-1]
    ii = n-2
    for i in arange(0,n-2):
        #try:
        x[ii] = (x[ii]-mk2*x[ii+1])/s2[ii]
        ii = ii - 1
        #except ZeroDivisionError:
        #    print 'Trisol1 dividing by Zero s2[ii] ii=',ii
        
    #x(1) = x(1)/s2(1);
    x[0] = x[0]/s2[0]
    return x

def smooth7(r_map,w_map):
    #Smooths points in NPP map by (2*L-1) x (2*L-1) quadratically
    #weighted window
    L=5
    #s_map = zeros((NX,NY))
    NX, NY = r_map.shape
    s_map = empty((NX,NY))
    
    for col in range(0,NX):
        for row in range(0,NY):
            if w_map[col,row]>0:      # only do windowing on land points
                wtot = 1.
                summ = r_map[col,row]

                #FIRST do points on cross 
                #Up/down on X
                for pix_col in range(1,L):
                    colpflg = True
                    colmflg = True
                    #isp1=i + ids - 1 # isp1 == 10 = i(8) + ids(3) -1
                    colp = col + pix_col - 1
                    #ism1=i - ids + 1 #Error ism1 == 10 
                    colm = col - pix_col + 1 

                    #print 'i,j',i,j
                    #print 'ids', ids
                    #print 'isp1',isp1

                    #if isp1>NX: 
                    if colp >= NX:
                        colpflg = False
                        
                    #if ism1<1:
                    if colm<0:  
                        colmflg = False
                        
                    w_col =(1.-(1.*pix_col/L)**2) # x weighting factor
                    #w_col =(1-(pix_col/L))**2 # x weighting factor


                    if colpflg and w_map[colp,row]>0:
                        wtot = wtot+w_col
                        summ = summ + w_col*r_map[colp,row]

                    if colmflg and w_map[colm,row]>0:
                        wtot = wtot+w_col;
                        summ = summ + w_col*r_map[colm,row]
    
                # Now left/right in Y-direction
                for pix_row in range(1,L):
                    #print 'js:', js
                    #jsp1=j + js - 1
                    #jsm1=j - js + 1 #Error j(9)-js(0) + 1 == 10
                    #rowp=row + pix_row -1
                    rowp=row + pix_row - 1 
                    rowm=row - pix_row + 1 
 
                    #if jsp1>=NY-1:
                    if rowp>=NY:
                        rowp=rowp-NY
                        
                    if rowm<0: 
                        rowm=rowm+NY
                            
                    w_row = (1.-(1.*pix_row/L)**2)  # y weighting factor
                    #w_row =(1-(pix_row/L))**2  # y weighting factor

                    if w_map[col,rowp]>0:
                        wtot = wtot+w_row
                        summ = summ + w_row*r_map[col,rowp]
                    #print 'i,j',i,j
                    #print 'jsm1:',jsm1
                    if w_map[col,rowm]>0: #Error: jsm1== ny, Error axis 1 size 5 
                        wtot = wtot+w_row
                        summ = summ + w_row*r_map[col,rowm]

                # END of points on cross
 
                #   All other points in square
                for pix_col in range(1,L):
                    colpflg = True; 
                    colmflg = True;
                    #isp1=i + ids - 1
                    #ism1=i - ids + 1
                    colp = col + pix_col - 1
                    colm = col - pix_col + 1 

                    #if isp1>NX:
                    if colp>=NX:
                        colpflg = False
                         
                    #if ism1<1:
                    if colp<0:
                        colmflg = False
                        
                    w_col =(1.-(1.*pix_col/L)**2);  # x weighting factor
                    #w_col =(1-(pix_col/L))**2;  # x weighting factor

                    for pix_row in range(1,L): 
                        #jsp1=j + js - 1
                        #jsm1=j - js + 1
                        rowp= row + pix_row - 1
                        rowm= row - pix_row + 1

                        #if jsp1>NY-1:
                        if rowp>=NY:
                            rowp=rowp-NY
                            
                        if rowm<0: 
                            rowm=rowm + NY
                            
                        w_row =(1.-(1.*pix_row/L)**2)  # y weighting factor
                        #w_row =(1-(pix_row/L))**2  # y weighting factor
                    
                        if colpflg:   # says isp1 <= NX
                            if w_map[colp,rowm]>0:
                                wt = w_col*w_row
                                wtot = wtot+wt;
                                summ = summ + wt*r_map[colp,rowm]
                    
                            if w_map[colp,rowp]>0:
                                wt = w_col*w_row
                                wtot = wtot+wt
                                summ = summ + wt*r_map[colp,rowp]

                        if colmflg:   # says ism1 >= 1
                            if w_map[colm,rowm]>0:
                                wt = w_col*w_row
                                wtot = wtot+wt
                                summ = summ + wt*r_map[colm,rowm]
                       
                            if w_map[colm,rowp]>0:
                                wt = w_col*w_row
                                wtot = wtot+wt;
                                summ = summ + wt*r_map[colm,rowp]
                       

                s_map[col,row] = summ/wtot # point is land, now averaged
            else:       # template=0
                s_map[col,row] = 0  # point is in water
            # end of center = landpoint if
        # end of j=1:NY loop
    #end of i=1:NX loop

    return s_map

def sciSmooth(r_map,mask):    
    # make some kind of kernel, there are many ways to do this...
    t = 1 - abs(linspace(-1, 1, 7))
    kernel = t.reshape(7, 1) * t.reshape(1, 7)
    kernel /= kernel.sum()   # kernel should sum to 1!  :) 
    
    # convolve 2d the kernel with each channel
    s_map = scipy.signal.convolve2d(r_map, kernel, mode='same')
    s_map = s_map*mask 
    
    return s_map
