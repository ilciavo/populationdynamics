function fisher
%
% finite difference scheme for 1-D Fisher/KPP eq. 
% wpp 27/05/2012

NX = 401; NT = 501; NTP = 21;
%NX = 801; NT = 101; NTP = 21;
a  = -40.0; b  = 40.0;
t0 = 0.0;   t1 = 40.0;

xm = linspace(a,b,NX);
ts = linspace(t0,t1,NT);
gs = zeros(NX,NT);
% scratch space, F & M's m (=s1),u (=s2) arrays
s1 = zeros(NX,1); s2 = zeros(NX,1);
m  = 0;
tstart=tic;
pd = pdepe(m,@hsfn,@hsic,@hsbc,xm,ts);
tend=toc(tstart);
fprintf(' 1Dpdepe time = %e\n',tend);
u0 = (hsic(xm))'; u = u0; h = ts(2)-ts(1); dx = xm(2)-xm(1);
kc = 0.5*h/(dx*dx);
fprintf(' h=%e, k=%e\n',h,kc); 
tstart=tic;
for i=1:NT
   gs(:,i) = godunovstep(NX,h,kc,u,s1,s2); % one step of Godunov
   u = gs(:,i);
end
nxm = (NX-1)/2+1;
tend=toc(tstart);
fprintf(' godunov time = %e\n',tend);
pt = pd(:,:,1); 
pltw = zeros(nxm,NTP);
pltg = zeros(nxm,NTP);
kk = 1; ntstep = (NT-1)/(NTP-1);
for k=1:NTP
  pltw(:,k) = pt(kk,nxm:NX);
  pltg(:,k) = gs(nxm:NX,kk);
  t(k) = ts(kk); kk=kk+ntstep;
end
xp = xm(nxm:NX);
for k=1:NTP
  tp(k) = t(k);
end
for k=1:NTP
   err = 0;
   for i=1:nxm
       err = err + (pltw(i,k)-pltg(i,k))*(pltw(i,k)-pltg(i,k));
   end
   errp(k) = sqrt(err/nxm);
   fprintf(' t=%e, err = %e\n',tp(k),errp(k));
   figure;
   plot(xp,pltw(:,k),'k-',xp,pltg(:,k),'r--','Linewidth',2);
   set(gca,'Fontsize',16);
   hleg=legend('pdepe','godunov');
   if t(k) < 15
      set(hleg,'Location','NorthEast');
   else
      set(hleg,'Location','SouthWest');
   end
   ylim([0 1.01]); 
   tstr = num2str(t(k));
   title([' wave front at t=',tstr],'Fontsize',16);
   xlabel('x','Fontsize',16);
   ylabel('u','Fontsize',16);
end
figure;
plot(tp,errp,'k-','Linewidth',2);
set(gca,'Fontsize',16);
title(' 2nd order BC rms err vs. t','Fontsize',16);
xlabel('t','Fontsize',16);
ylabel('err','Fontsize',16);

figure;
mesh(xm,ts,pt);
xlabel('x','Fontsize',16);
ylabel('t','Fontsize',16);
zlabel('u(t,x)','Fontsize',16);

function [c,f,s] = hsfn(x,t,u,ux)
c = 1;
f = ux/2;
s = u*(1-u);

function u0 = hsic(x)
[m,n]=size(x);
for i=1:n
   u0(i) = exp(-x(i)*x(i));
%  if abs(x(i)) < 1.0 
%     u0(i) = 1;
%  else
%     u0(i) = 0;
%  end
end

function [pa,qa,pb,qb] = hsbc(xa,ua,xb,ub,t)
pa = 0; qa = 1;
pb = 0; qb = 1;

function v = godunovstep(n,h,k,u,s1,s2);
% first compute Euler estimate
   ut = (Amult(n,u))';
   ut = k*ut;
   uE = ut + h*(1-u).*u;         % commmon part of Euler est.
% compute right hand side
   ur = u + 0.5*uE;              % RHS
   uE = u + uE;                  % Euler est.
% impose Neumann BCs on Euler estimate
   uE(1) = 4*uE(2)/3 - uE(3)/3;
   uE(n) = 4*uE(n-1)/3 - uE(n-2)/3;
% reset end points for explicit estimates for BCs
%  ur(1) = ur(1) - k*uE(1) + 0.5*k*uE(2) + 0.5*h*uE(1)*(1-uE(1)); 
   ur(1) = ur(1) + k*uE(1) - 0.5*k*uE(2) + 0.5*h*uE(1)*(1-uE(1)); 
%  ur(n) = ur(n) - k*uE(n) + 0.5*k*uE(n-1) + 0.5*h*uE(n)*(1-uE(n)); 
   ur(n) = ur(n) + k*uE(n) - 0.5*k*uE(n-1) + 0.5*h*uE(n)*(1-uE(n)); 
% set up left hand side matrix and solve implicit equations
   dA(1) = 1; dA(n) = 1;         % end points
% interior diags. of LHS matrix:
   dA(2:n-1) = 1 + k - 0.5*h*(1-uE(2:n-1));
% solve using Forsythe-Moler
   v  = trisol(n,k,dA,ur,s1,s2);

function x = trisol(n,k,d,b,s1,s2)
%  Tridiagonal system solver modeled on
%  G.E. Forsythe and C.B. Moler " Computer 
%  solutions to linear algebraic systems,"
%  Prentice-Hall publ., 1967
%  Tridiagonal system is Tx=b, where
%
%  T = [1  -k/2   0    ....          0 ]
%      [-k/2 d(2) -k/2 ....          0 ]
%      [0    -k/2 d(3) -k/2  0 ....  0 ]
%      [0           ....             0 ]
%      [0              -k/2 d(n-1) -k/2]
%      [0                0    -k/2   1 ]

% T = [-k/2     d(1)    -k/2 0        .... 0]
%     [0        -k/2    d(2)          .... 0]
%     [0        0                     .... 0]
%     [0                                   0]
%     [0                -k/2    d(n-1)  -k/2]

% 
% and k = 0.5*h/dx^2 is the CFL factor. Diagonals
% d = (1 d(2) d(3) ...  d(n-1) 1) are strong.
% s1 and s2 are work arrays. In the original 
% F & M notation,
%
%  T = [d(1)  f(1)  0        ...             0 ]
%      [e(2)  d(2)  f(2)  0  ...             0 ]
%      [ 0          ....                     0 ]
%      [ 0                e(n-1) d(n-1) f(n-1) ]
%      [ 0                  0    e(n)   d(n)   ]
%
%  wpp 27 May 2012
%
   mk2   = -k/2;
   s2(1) = 1;
   s2(2) = d(2);
   s1(2) = mk2;
%
   for i=3:n-1
      s1(i) = mk2/s2(i-1);
      s2(i) = d(i) - mk2*s1(i);
   end
   s1(n) = 0; s2(n) = 1;
%
   x(1) = b(1);
   for i=2:n
      x(i) = b(i) - s1(i)*x(i-1);
   end
%
   x(n) = x(n)/s2(n); ii = n-1;
   for i=1:n-2
       x(ii) = (x(ii) - mk2*x(ii+1))/s2(ii);
       ii    = ii - 1;
   end
   x(1) = x(1)/s2(1);

function v = Amult(n,u)
% does tridiagonal matrix multiply, returns n elements of v
   v(1) = -2*u(1) + u(2); v(n) = -2*u(n)+u(n-1);
   v(2:n-1) = u(1:n-2)+u(3:n)-2*u(2:n-1);
