function fisherKPP2D
%
% Fisher/KPP simulation on a world map, with sigmoid interpolation
% wpp 08/06/2015. This version is for Jody's 360 x 720 maps but
% should work with 720 x 1440 "exlarge". It also has the physical 
% scaling --> scale free changes from Sept. 2014 and Neumann BCs
% wpp 8/7/2015
% 
% these variables are for initial/final human population distributions
global orig_x orig_y EndDispersal_x EndDispersal_y
%
fprintf('\n INPUT Parameter summary: \n\n ');
% Kyb = 200.0;      % this is Simone's diffusion coeff.
Kyb = 208.0;      % this is Young and Bettinger's diffusion coeff.
                  % should be between 0.1 to 1000.0 km^2/yr.
if ~exist('Kyb')
   Kyb = input(' Enter diffusion coefficient ( 0.1 < Kyb < 1000 ): ')
end
fprintf('  Diffusion coeff: Kyb = %e (in km^2/yr) \n',Kyb)
% Ryb = 0.002;    % Simone's growth rate: 0.001 - 0.03 yr^{-1}
Ryb = 0.00167;    % Young & Bettinger's growth rate: 0.001 - 0.03 yr^{-1}
if ~exist('Ryb')
   Ryb = input(' Enter growth rate ( 0.001 < Ryb < 0.03 ): ')
end
fprintf('   Growth rate: Ryb = %e (in yr^{-1}) \n',Ryb)
% Earth measurement = circumference
Ecirc = 40074.78; % in km, cell size will be dx=dy=Ecirc/NY (km)
% start/stop simulation in kya, limits are
Ts  = 50.0;
if ~exist('Ts')
   Ts = input(' Enter start time ( 120 >= Ts > 1 kya ): ')
end
if Ts~=ceil(Ts)
   fprintf(' ERROR: Ts must be an integer no. of Kyears: Ts=%e\n',Ts);
end
Te  = 1.0;
if ~exist('Te')
   Te = input(' Enter end time ( 119 > Te >= 1 kya ): ')
end
if Te~=floor(Te)
   fprintf(' ERROR: Te must be an integer no. of Kyears: Te=%e\n',Te);
end
% END of Input parameters
%
%         Find start and end maps/templates:
%         ---------------------------------
%
[yalist, tplist, tepoch] = namelistLG;
kstart = 1;  while tepoch(kstart+1) >= Ts, kstart=kstart+1; end;
fprintf('   Start epoch Ts=%5.1f (kya), NPP map kstart=%d \n',Ts,kstart);
%
kend = 61; while tepoch(kend-1) <= Te, kend=kend-1; end;
fprintf('   End   epoch Te=%5.1f (kya), NPP map   kend=%d \n',Te,kend);
%
%      be sure there are at least 2 maps
if ((kend-kstart)<1)||((tepoch(kstart)-tepoch(kend))<(Ts-Te))
   fprintf('Ts=%e, Te=%e, not space enough: kstart=%d, kend=%d\n', ...
   Ts,Te,kstart,kend);
end
%         start and end maps/templates found
%
%              get first template
%              ------------------
kmp = kstart;
m_L = templatemap('large',tplist,kmp); % template for first map
[Grid_size_x, Grid_size_y] = size(m_L);
NX  = Grid_size_x; NY = Grid_size_y;
fprintf('   MAP SIZES:   NX=%d (N-S), NY=%d (E-W)\n', ...
Grid_size_x, Grid_size_y);
%
%       rescale units according to form eq. (3) in paper
%       distance SCALE FACTOR:
sfact = sqrt(0.5*Ryb/Kyb); % unit here is km^{-1}
sy = Ecirc/NY;
fprintf('   PIXEL SIZE: dx=dy= %4.1f (km)',sy);
dx = sfact*sy; % now unitless
fprintf(' = %5.1f (eq. (3) units) \n',dx);
dy = dx;       % square cells
%
%    now compute the step size and total number of timesteps
%    -------------------------------------------------------
%
t1 = 1.e+03*Ryb*(Ts-Te);   % total unitless time
h0 = dx*dx;                % CFL guess for h
m0 = ceil(1/h0);           % no. steps/(1 Ky)
h  = 1/m0;                 % actual step size such that m0*h=1;
NT = ceil(t1*m0);          % total number of timesteps
% Times are numbered by kiloyears (kyr or kya)
tsc = 1.e-3/Ryb;                  % Ryb is in yr^{-1}, not kyr^{-1}
%
xl = 1:NX;                        % points for plots in integers
xl = 1.e-4*(sy*xl- sy);           % in km
yl = 1:NY;
yl = 1.e-4*(sy*yl- sy);           % likewise for W-E direction
%
%         scratch space, F & M's m (=s1),u (=s2) arrays
%
NS   = max(NY,NX);
sc1  = zeros(NS,1); sc2 = zeros(NS,1);
kap0 = zeros(1,NS);
kaph = zeros(1,NS);
%
%           create sigmoid interpolation nodes
% 
yp  = yppoints;
% 
%          compute time step and CFL parameter
% 
kcfl = 0.5*h/(dx*dx);
fprintf('   For (%d x %d): h=%e, CFL=%e, \n',NX,NY,h,kcfl);
fprintf('      Tot. t-steps NT=%d, steps/kyr m0=%d \n',NT,m0); 
% 
% TGL alternates between 1 and 0 for alt. direction method
% 
TGL  = 1; in = 1;
t_L  = tepoch(kmp);
m_H  = templatemap('large',tplist,kmp+1); % template for 2nd map
% now create m_L, m_H combined mask: this is the template for
% all time steps in this t_L <= t < t_H epoch
%
m_LH = m_L | m_H;                % m_L, m_H are 0-1 templates
%
w_L  = kapepochLG(m_LH,yalist,kmp); % this is the first map
kmp  = kmp + 1;                  % increment map count
%
% scan through map, construct segments for X and Y-directions
% 
[nxsegs xstart_seg xend_seg] = Xscan(NX,NY,m_LH);
[nysegs ystart_seg yend_seg] = Yscan(NX,NY,m_LH);
% 
% fill in sub-threshold vals for w_L and w_H from m_LH template
w_L = fillins(w_L,m_L,m_LH);
w_H = kapepochLG(m_LH,yalist,kmp); % second map
w_H = fillins(w_H,m_H,m_LH);
t_H = tepoch(kmp);
s   = (t_L-Ts)/(t_L-t_H);       % t=Ts interpolation parameter
ks  = floor(100*s);
sd  = 100*s - ks;               % residual sd = 100*s mod 1
wt0 = yp(ks+1)*(1-sd) + yp(ks+2)*sd;  % first interpolant weight
% compute interpolated weight for t=Ts-tsc*h
s   = (t_L-Ts+tsc*h)/(t_L-t_H);
ks  = floor(100*s);
sd  = 100*s - ks;               % residual sd = 100*s mod 1
wth = yp(ks+1)*(1-sd) + yp(ks+2)*sd;  % first interpolant
%
% storage for solution (set zero)
%
u   = zeros(NX,NY);
%
% initialize distr. in Eritrea
%
u   = initu(u,m_LH,w_L,orig_x,orig_y);
u_log = m_LH-1+log(1+10*u);     % monotone version of u for plotting
%
% bigl = max(max(u_log));
% bigu = max(max(u));
% fprintf(' initial max(u) = %e, max(log(u))=%e\n',bigu,bigl);
cmin = -1; cmax = 2.4;          % -1 to log(1+10*u))
tstr = sprintf('%3.0f',Ts);
figure;
pcolor(yl,xl,u_log); shading flat;
set(gca,'FontSize',16);
caxis([cmin cmax]); colormap(jet); colorbar;
xlim([0 4]); 
ylim([0 2]); set(gca,'Ytick',[0 0.5 1 1.5 2]);
title([' init. data: log(1+10u(x,y)) at t=Ts=',tstr,' Kya'],'Fontsize',18);
xlabel(' W-E (x 10^{4} km) ','FontSize',18);
ylabel(' S-N (x 10^{4} km) ','FontSize',18);
ptflag = 0;                % this flag initiates plotting if > 0
%
%                start time stepping
%                -------------------
%
t    = Ts - tsc*h;         % this time is in Kyears
tbeg = tic;                % timer
for it=1:NT
   if TGL
% NY X direction updates for half-step1, Y direction for step 2
      locx = 0;
      for j=1:NY
          nsegs = nxsegs(j);
          for k=1:nsegs
              istart=xstart_seg(locx+k); iend=xend_seg(locx+k);
              ninseg=iend-istart+1;
              u0(1:ninseg) = u(istart:iend,j);
              ut = godunovstep1(ninseg,h,kcfl,u0,sc1,sc2);
              u(istart:iend,j) = ut(1:ninseg);
          end
          locx = locx + nsegs;
      end
% now do NX Y direction updates step2
      locy = 0;
      for i=1:NX
          nsegs = nysegs(i);
          for k=1:nsegs
              jstart=ystart_seg(locy+k); jend=yend_seg(locy+k);
              ninseg=jend-jstart+1;
              u1(1:ninseg) = u(i,jstart:jend)';
              kap0(1:ninseg) = w_L(i,jstart:jend)*(1-wt0) ...
                               + w_H(i,jstart:jend)*wt0;
              kaph(1:ninseg) = w_L(i,jstart:jend)*(1-wth) ...
                               + w_H(i,jstart:jend)*wth;
              ut = godunovstep2(ninseg,h,kcfl,u1,sc1,sc2,kap0,kaph);
              u(i,jstart:jend) = ut(1:ninseg);
          end
          locy = locy + nsegs;
      end
% and finally NY more X direction half step1s
      locx = 0;
      for j=1:NY
          nsegs = nxsegs(j);
          for k=1:nsegs
              istart=xstart_seg(locx+k); iend=xend_seg(locx+k);
              ninseg=iend-istart+1;
              u0(1:ninseg) = u(istart:iend,j);
              ut = godunovstep1(ninseg,h,kcfl,u0,sc1,sc2);
              u(istart:iend,j) = ut(1:ninseg);
          end
          locx = locx + nsegs;
      end
      TGL = 0; 
   else
% NX Y-direction updates for half-step1
      locy = 0;
      for i=1:NX
          nsegs = nysegs(i);
          for k=1:nsegs
              jstart=ystart_seg(locy+k); jend=yend_seg(locy+k);
              ninseg=jend-jstart+1;
              u1(1:ninseg) = u(i,jstart:jend)';
              ut = godunovstep1(ninseg,h,kcfl,u1,sc1,sc2);
              u(i,jstart:jend) = ut(1:ninseg)';
          end
          locy = locy + nsegs;
      end
% now do NY X-direction step2s
      locx = 0;
      for j=1:NY
          nsegs = nxsegs(j);
          for k=1:nsegs
              istart=xstart_seg(locx+k); iend=xend_seg(locx+k);
              ninseg=iend-istart+1;
              u0(1:ninseg) = u(istart:iend,j);
              kap0(1:ninseg) = w_L(istart:iend,j)*(1-wt0) ...
                               + w_H(istart:iend,j)*wt0;
              kaph(1:ninseg) = w_L(istart:iend,j)*(1-wth) ...
                               + w_H(istart:iend,j)*wth;
              ut = godunovstep2(ninseg,h,kcfl,u0,sc1,sc2,kap0,kaph);
              u(istart:iend,j) = ut(1:ninseg);
          end
          locx = locx + nsegs;
      end
% and finally, NX Y-direction half step1s
      locy = 0;
      for i=1:NX
          nsegs = nysegs(i);
          for k=1:nsegs
              jstart=ystart_seg(locy+k); jend=yend_seg(locy+k);
              ninseg=jend-jstart+1;
              u1(1:ninseg) = u(i,jstart:jend)';
              ut = godunovstep1(ninseg,h,kcfl,u1,sc1,sc2);
              u(i,jstart:jend) = ut(1:ninseg)';
          end
          locy = locy + nsegs;
      end
      TGL = 1;
% end TGL toggle between X and Y directions
   end
% increment t value, reset weights and maps if necessary
   t = t - tsc*h;              % count backwardsin K-years
   if (t > t_H)                % usual case, maps not exhausted
      if (wt0 <= 0.5) && (wth > 0.5)
         ptflag = 1;  % set ptflag when wth crosses 1/2
      else
         ptflag = 0;  % after that ptflag=0
      end
% 
      wt0 = wth;                         % shift lower weight down
      s   = (t_L-t)/(t_L-t_H);           % interpolation parameter
      ks  = floor(100*s);
      sd  = 100*s - ks;                  % residual sd = 100*s mod 1
      wth = yp(ks+1)*(1-sd) + yp(ks+2)*sd;  % interpolant
   else                                  % reached upper map time 
      kmp = kmp +1;                      % increment map count
      if kmp > kend                      % kmp > kend only if it=NT
         ptplot = 1;                     % plot last frame
      end
      if kmp <= kend;                    % kend is the last map
         m_L  = m_H;                     % set lower mask to prev. upper
         w_L  = w_H;                     % set lower map  to prev. upper
         t_L  = t_H;                     % set lower time to prev. upper
% new high mask
         m_H  = templatemap('large',tplist,kmp);   % new high mask
% master mask for next epoch
         m_LH = m_L | m_H;
%  re-segment new template
         [nxsegs xstart_seg xend_seg] = Xscan(NX,NY,m_LH);
         [nysegs ystart_seg yend_seg] = Yscan(NX,NY,m_LH);
         w_L  = fillins(w_L,m_L,m_LH);     % again,zeros
         w_H  = kapepochLG(m_LH,yalist,kmp);  % read next map
         w_H  = fillins(w_H,m_H,m_LH);     % zeros
         t_H  = tepoch(kmp);              % time frame of new map
         wt0  = 0;                        % w. new map, (1-wt)=1
         s    = (t_L-t)/(t_L-t_H);        % interpolation parameter
         ks   = floor(100*s);
         sd   = 100*s - ks;               % mod1 interpolation 
         wth  = yp(ks+1)*(1-sd) + yp(ks+2)*sd;  % interpolant wt.
      end
   end
   if ptflag || (it==NT)
      if t < 10
         tstr = sprintf('%3.1f',t);
      else
         tstr = sprintf('%3.0f',t);
      end
      u_log  = m_LH-1+log(1+10*u);
      figure;
      pcolor(yl,xl,u_log); shading flat;
      caxis([cmin cmax]); colormap(jet); colorbar;
      xlim([0 4]); 
      ylim([0 2]); set(gca,'Ytick',[0 0.5 1 1.5 2]);
      set(gca,'FontSize',16);
      caxis([cmin cmax]);
      title(['log(1+10u(x,y)) at t=',tstr,' Kya'],'Fontsize',18);
      xlabel(' W-E (x 10^{4} km) ','FontSize',18);
      ylabel(' S-N (x 10^{4} km) ','FontSize',18);
   end
end
%                   end time stepping
%
timeG=toc(tbeg);
fprintf('\n   Comp. Time (NX=%d, NY=%d, NT=%d) = %e secs.(incl. plots)\n',...
NX,NY,NT,timeG);

function wO = fillins(wI,mI,m_LH);
%  fills in zeros of wI with threshold values if m_LH=1 but mI=0
   thresh = 5.0e-7; kmax  = 172.2810; kth = kmax*thresh;
   wO = wI;          % fill output with all inputs
   wO(find(xor(mI,m_LH)))=kth; % set new values to kmax*thresh

function v = godunovstep2(n,h,k,u,s1,s2,kap0,kaph);
% first compute Euler estimate
   lambda = 1/h;
   reg = @(x) (tanh(x.^4)).^(1/4);  % regularizer for small CC
   ut = Amult(n,u);
   ut = k*ut;
% right hand side part for part of Euler uses K(t)
   uC = u(1:n)./kap0(1:n);
   uC = (1/h)*reg(h*uC); 
% left hand side version uses K(t+h)
   uh = u(1:n)./kaph(1:n);
   uh = (1/h)*reg(h*uh); 
% common part of Euler estimate
   uE = ut + h*(1-uC).*u(1:n);
% compute right hand side
   ur = u(1:n) + 0.5*uE;       % RHS
   uE = u(1:n) + uE;           % Euler est.
% impose Neumann BCs on Euler estimate
   uE(1) = 4*uE(2)/3 - u(3)/3;
   uE(n) = 4*uE(n-1)/3 - u(n-2)/3;
% explicit T.R. but regularized estimate for u(3) in RHS
   reL   = (1/h)*reg(h*u(3)/kap0(3));
   reH   = (1/h)*reg(h*uE(3)/kaph(3));
   uest  = u(3) + 0.5*k*(uE(2) - 2*uE(3) + uE(4)) ...
                + 0.5*k*(u(2) - 2*u(3) + u(4))    ...
                + 0.5*h*((1-reH)*uE(3)+(1-reL)*u(3));
   ur(1) = uest/3;
% and u(n-2) for RHS
   reL   = (1/h)*reg(h*u(n-2)/kap0(n-2));
   reH   = (1/h)*reg(h*uE(n-2)/kaph(n-2));
   uest  = u(n-2) + 0.5*k*(uE(n-3) - 2*uE(n-2) + uE(n-1)) ...
                  + 0.5*k*(u(n-3) - 2*u(n-2) + u(n-1))    ...
                  + 0.5*h*((1-reH)*uE(n-2)+(1-reL)*u(n-2));
   ur(n) = uest/3;
   dA(1) = 1; dA(n) = 1;            % end points
% interior diags. of LHS matrix:
   dA(2:n-1) = 1 + k - 0.5*h*(1-uh(2:n-1));
% solve tridiagonal system using Forsythe-Moler
   v  = trisol1(n,k,dA,ur,s1,s2);
% correction for v(1) and v(n) if desired
%  v(1) = 4*v(2)/3 - v(3)/3;
%  v(n) = 4*v(n-1)/3 - v(n-2)/3;

function x = trisol1(n,k,d,b,s1,s2)
%  Tridiagonal system solver modeled on
%  G.E. Forsythe and C.B. Moler " Computer 
%  solutions to linear algebraic systems,"
%  Prentice-Hall publ., 1967
%  Tridiagonal system is Tx=b, where
%
%  T = [d(1) -4/3   0    ....        0 ]
%      [-k/2 d(2) -k/2 ....          0 ]
%      [0    -k/2 d(3) -k/2  0 ....  0 ]
%      [0           ....             0 ]
%      [0              -k/2 d(n-1) -k/2]
%      [0                0  -4/3   d(n)]
% 
% and k = h/dx^2 is the CFL factor. Diagonals
% d = (1 d(2) d(3) ...  d(n-1) 1) are strong.
% s1 and s2 are work arrays. In the original 
% F & M notation,
%
%  T = [d(1)  f(1)  0        ...             0 ]
%      [e(2)  d(2)  f(2)  0  ...             0 ]
%      [ 0          ....                     0 ]
%      [ 0                e(n-1) d(n-1) f(n-1) ]
%      [ 0                  0    e(n)   d(n)   ]
%
%  s1(1) is not used. wpp 8 July 2015
%
   mk2   = -k/2;
   s2(1) = d(1);
   s1(2) = mk2/s2(1);
   s2(2) = d(2) + 4*s1(2)/3;
%
   for i=3:n-1
      s1(i) = mk2/s2(i-1);
      s2(i) = d(i) - mk2*s1(i);
   end
   s1(n) = -4/(3*s2(n-1)); s2(n) = d(n)-mk2*s1(n);
%
   x(1) = b(1);
   for i=2:n
      x(i) = b(i) - s1(i)*x(i-1);
   end
%
   x(n) = x(n)/s2(n); ii = n-1;
   for i=1:n-1
       x(ii) = (x(ii) - mk2*x(ii+1))/s2(ii);
       ii    = ii - 1;
   end

function v = godunovstep1(n,h,kcfl,u,s1,s2);
% first compute RHS for tridiagonal solver
   kcfl2  = kcfl/2;  % CFL number/2
   kcfl4  = kcfl/4;  % CFL number/4
   ut     = Amult(n,u);
   rhs    = u(1:n) + kcfl4*ut;
% compute an T.R. estimate for u(3) 
   uE     = u(3) + kcfl2*(u(2) - 2*u(3) + u(4));
   uest   = u(3) + kcfl4*(uE + u(3));
   rhs(1) = -uest/4;
% and u(n-2)
   uE     = u(n-2) + kcfl2*(u(n-3) - 2*u(n-2) + u(n-1));
   uest   = u(n-2) + kcfl4*(uE + u(n-2));
   rhs(n) = -uest/4;
% solve tridiagonal system using Forsythe-Moler
   v   = trisol0(n,kcfl,rhs,s1,s2);
% correction for v(1) and v(n) if desired
%  v(1) = 4*v(2)/3 - v(3)/3;
%  v(n) = 4*v(n-1)/3 - v(n-2)/3;

function x = trisol0(n,k,b,s1,s2)
%  Tridiagonal system solver modeled on
%  G.E. Forsythe and C.B. Moler " Computer 
%  solutions to linear algebraic systems,"
%  Prentice-Hall publ., 1967
%  Tridiagonal system is Tx=b, where
%
%  T = [1    -4/3     0   ....         0 ]
%      [-k/4 1+k/2  -k/4 ....          0 ]
%      [0    -k/4   1+k/2 -k/4  0 ...  0 ]
%      [0                ....          0 ]
%      [0              -k/4  1+k/2   -k/4]
%      [0                0    -4/3     1 ]
% 
% The original F & M notation can be seen in
% trisol( ). s1(1) is not used
%
%  wpp 8 July 2015
%
   mk4   = -k/4; k2=k/2;
   s2(1) = 1;
   s1(2) = mk4;
   s2(2) = 1 + k2 + 4*s1(2)/3;
%
   for i=3:n-1
      s1(i) = mk4/s2(i-1);
      s2(i) = 1 + k2 - mk4*s1(i);
   end
   s1(n) = -4/(3*s2(n-1)); s2(n) = 1-mk4*s1(n);
%
   x(1) = b(1);
   for i=2:n
      x(i) = b(i) - s1(i)*x(i-1);
   end
%
   x(n) = x(n)/s2(n); ii = n-1;
   for i=1:n-1
       x(ii) = (x(ii) - mk4*x(ii+1))/s2(ii);
       ii    = ii - 1;
   end

function v = Amult(n,u)
% does tridiagonal matrix multiply, returns n elements of v
% with v(1) = -2*u(1) + u(2), v(n) = -2*u(n) + u(n-1), 
% the others are u(i-1)+u(i+1)-2*u(i)
   v(1) = -2*u(1)+u(2); v(n) = -2*u(n)+u(n-1);
   v(2:n-1) = u(1:n-2)+u(3:n)-2*u(2:n-1);

function [nysegs ystart_seg yend_seg] = Yscan(NX,NY,m_LH)
% scan through X-direction, enumerate all Y-direction land segments
locy = 0;
for i = 1:NX
   nysegs(i) = 0; st = 0; k = 1;
   for j = 1:NY
       if m_LH(i,j)>0
          if st==0
             nysegs(i) = nysegs(i)+1;
             ystart_seg(locy+k) = j;
             st = 1;
          end
       else 
          if st>0
             yend_seg(locy+k) = j-1;
             k = k+1; 
             st = 0;
          end
       end
   end
   locy = locy + nysegs(i);
end 

function [nxsegs xstart_seg xend_seg] = Xscan(NX,NY,m_LH)
% scan through Y-direction, enumerate all X-direction land segments
locx = 0;
for j = 1:NY
    nxsegs(j) = 0; st = 0; k = 1;
    for i = 1:NX
        if m_LH(i,j)>0
           if st==0
              nxsegs(j) = nxsegs(j)+1;
              xstart_seg(locx+k) = i;
              st = 1;
           end
        else 
           if st>0
              xend_seg(locx+k) = i-1;
              k = k+1; 
              st = 0;
           end
        end
    end
    locx = locx + nxsegs(j);
end

 
function u = initu(u,m_LH,w_L,orig_x,orig_y)
% initialize to small normal distr. around orig_x, orig_y
    [NX, NY]=size(m_LH); nox = floor(NX/10); noy = floor(NY/10);
    nox2  = floor(nox/2); noy2 = floor(noy/2); 
%   r2max = nox2*nox2+noy2*noy2;
    r2max = nox*nox+noy*noy;
    for j=1:noy
        ry2 = (j-noy2)^2;
        for i=1:nox
           r2  = ry2 + (i-nox2)^2;
           if r2<r2max
%  Gaussian variant: if distr. is out of range (m_LH=0), set to zero
              u(i+orig_x-3,j+orig_y-3) = ...
                exp(-r2)*m_LH(i+orig_x-3,j+orig_y-3)* ...
                w_L(i+orig_x-3,j+orig_y-3);
           else
              u(i+orig_x-3,j+orig_y-3) = 0;
           end
        end
    end
