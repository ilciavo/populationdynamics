!Create Signature: f2pyA -m Fmodules -h  Fmodules.pyf Fmodules.f90 --overwrite-signature
!Compile:f2pyA -c   --fcompiler=gnu95 --verbose --opt='-O3' Fmodules.pyf Fmodules.f90 
!  G.E. Forsythe and C.B. Moler " Computer 
!  solutions to linear algebraic systems,"
!  Prentice-Hall publ., 1967
!  Tridiagonal system is Tx=b, where
!
!  T = [1     0   0    ....          0 ]
!      [-k/2 d(2) -k/2 ....          0 ]
!      [0    -k/2 d(3) -k/2  0 ....  0 ]
!      [0           ....             0 ]
!      [0              -k/2 d(n-1) -k/2]
!      [0                0    0      1 ]
! 
! and k = h/dx^2 is the CFL factor. Diagonals
! d = (1 d(2) d(3) ...  d(n-1) 1) are strong.
! s1 and s2 are work arrays.(Leo: s2 new diagonal, s1 new off-diagonal)  
! In the original F & M notation,
!
!  T = [d(1)  f(1)  0        ...             0 ]
!      [e(2)  d(2)  f(2)  0  ...             0 ]
!      [ 0          ....                     0 ]
!      [ 0                e(n-1) d(n-1) f(n-1) ]
!      [ 0                  0    e(n)   d(n)   ]

subroutine trisol(n, k, d, b, x)
  implicit none
  integer :: n, i, ii
  real(8) :: k, d(n), b(n), s1(n), s2(n), x(n), mk2

!f2py real(8), intent(out), dimension(n) :: x
!f2py real(8), intent(in), dimension(n) :: b
!f2py real(8), intent(in) :: k
!f2py integer, intent(in) :: n

  mk2 = -k/2
  s2(1) = 1
  s2(2) = d(2)
  s1(2) = mk2

!Forward Elimination 
  !nm1 = n-1
  do i = 3, n-1 
      s1(i) = mk2/s2(i-1)
      s2(i) = d(i)-mk2*s1(i)
  end do 

  s1(n) = 0
  s2(n) = 1 

  x(1) = b(1)

  do i=2, n
      x(i) = b(i)- s1(i)*x(i-1)
  end do 

!Backward substitution
  x(n) = x(n)/s2(n)
  ii = n-1

  do i=1, n-2
      x(ii) = (x(ii) - mk2*x(ii+1))/s2(ii)
      ii = ii-1
  end do 

  x(1) = x(1)/s2(1)

end subroutine trisol


subroutine trisol0(n, k, b, x)
  implicit none
  integer :: n, i, ii
  real(8) :: k, b(n), s1(n), s2(n), x(n), mk4, k2

!f2py real(8), intent(out), dimension(n) :: x
!f2py real(8), intent(in), dimension(n) :: b
!f2py real(8), intent(in) :: k
!f2py integer, intent(in) :: n

  mk4 = -k/4
  k2 = k/2
  s2(1) = 1
  s2(2) = 1 + k2
  s1(2) = mk4

!Forward Elimination 
  !nm1 = n-1
  do i = 3, n-1 
      s1(i) = mk4/s2(i-1)
      s2(i) = 1+k2-mk4*s1(i)
  end do 

  s1(n) = 0
  s2(n) = 1 

  x(1) = b(1)

  do i=2, n
      x(i) = b(i)- s1(i)*x(i-1)
  end do 

!Backward substitution
  x(n) = x(n)/s2(n)
  ii = n-1

  do i=1, n-2
      x(ii) = (x(ii) - mk4*x(ii+1))/s2(ii)
      ii = ii-1
  end do 

  x(1) = x(1)/s2(1)

end subroutine trisol0

subroutine trisol1(n, k, d, b, x)
  implicit none
  integer :: n, i, ii
  real(8) :: k, d(n), b(n), s1(n), s2(n), x(n), mk2


!f2py real(8), intent(out), dimension(n) :: x
!f2py real(8), intent(in), dimension(n) :: d
!f2py real(8), intent(in), dimension(n) :: b
!f2py real(8), intent(in) :: k
!f2py integer, intent(in) :: n

  mk2 = -k/2
  s2(1) = 1
  s2(2) = d(2)
  s1(2) = mk2

!Forward Elimination 
  !nm1 = n-1
  do i = 3, n-1 
      s1(i) = mk2/s2(i-1)
      s2(i) = d(i) - mk2*s1(i)
  end do 

  s1(n) = 0
  s2(n) = 1 

  x(1) = b(1)

  do i=2, n
      x(i) = b(i)- s1(i)*x(i-1)
  end do 

!Backward substitution
  x(n) = x(n)/s2(n)
  ii = n-1

  do i=1, n-2
      x(ii) = (x(ii) - mk2*x(ii+1))/s2(ii)
      ii = ii-1
  end do 

  x(1) = x(1)/s2(1)

end subroutine trisol1

subroutine amult(n, u, v)
  implicit none
  integer :: n, i
  real(8) :: u(n), v(n)

!f2py real(8), intent(out), dimension(n) :: v
!f2py real(8), intent(in), dimension(n) :: u
!f2py integer, intent(in) :: n

  v(1) = 0 
  v(n) = 0
  
!Forward Elimination 
  !v = u(1:n-2)+u(3:n)-2*u(2:n-1)
  do i = 2, n-1 
      v(i) = u(i-1)+u(i+1)-2*u(i)
  end do 

end subroutine amult

subroutine godunovstep(n, h, k, u, v)
  implicit none
  integer :: n, i
  real(8) :: h, k, u(n), v(n), ut(n), ue(n), ur(n), dA(n)

!f2py integer, intent(in) :: n
!f2py real(8), intent(in) :: h
!f2py real(8), intent(in) :: k
!f2py real(8), intent(in), dimension(n) :: u
!f2py real(8), intent(out), dimension(n) :: v

  call amult(n, u, ut)
  ut = k*ut
  ue = ut + h*(1-u)*u
  ur = u + 0.5*ue !RHS
  ue = u+ue
  dA(1) = 1
  dA(n) = 1
  
  do i = 2,n-1
      dA(i) = 1 + k - 0.5*h*(1-ue(i))
  end do 

  call trisol1(n,k,dA,ur,v)

end subroutine godunovstep


subroutine godunovstep2(n, h, k, u, kap0, kaph, v)
  implicit none
  integer :: n, i
  real(8) :: h, k, u(n), kap0(n), kaph(n), v(n), ut(n), uc(n), uh(n), ue(n), ur(n), dA(n)

!f2py integer, intent(in) :: n
!f2py real(8), intent(in) :: h
!f2py real(8), intent(in) :: k
!f2py real(8), intent(in), dimension(n) :: u
!f2py real(8), intent(in), dimension(n) :: kap0
!f2py real(8), intent(in), dimension(n) :: kaph
!f2py real(8), intent(out), dimension(n) :: v

  call amult(n, u, ut)
  ut = k*ut
  uc = u/kap0
  uh = u/kaph
  ue = ut + h*(1-uc)*u
  ur = u + 0.5*ue 
  ue = u+ue
  dA(1) = 1
  dA(n) = 1
  
  do i = 2,n-1
      dA(i) = 1 + k - 0.5*h*(1-uh(i))
  end do 

  call trisol1(n,k,dA,ur,v)
  
  v=max(min(v,0.),kaph)

end subroutine godunovstep2

subroutine godunovstep1(n, h, kcfl, u, v)
    implicit none
    integer :: n
    real(8) :: h, kcfl, kcfl4, u(n), v(n), ut(n), rhs(n)

    !f2py integer, intent(in) :: n
    !f2py real(8), intent(in) :: h
    !f2py real(8), intent(in) :: kcfl
    !f2py real(8), intent(in), dimension(n) :: u
    !f2py real(8), intent(out), dimension(n) :: v

    kcfl4 = kcfl/4
    call amult(n, u, ut)
    rhs = u + kcfl4*ut

    call trisol0(n,kcfl,rhs,v)

end subroutine godunovstep1


subroutine smooth7(cols, rows, r_map, w_map, s_map)
    implicit none
    logical :: colpflg, colmflg
    integer :: cols, rows, col, row, L, w_map(cols,rows), rowp, rowm, pix_row, pix_col, colp, colm
    real(8) :: r_map(cols,rows), s_map(cols,rows), wtot, w_col, w_row, summ

    !f2py integer, intent(in) :: cols
    !f2py integer, intent(in) :: rows
    !f2py real(8), intent(in), dimension(cols,rows) :: r_map
    !f2py integer, intent(in), dimension(cols,rows) :: w_map
    !f2py real(8), intent(out), dimension(cols,rows) :: s_map

    L = 5 !smoother uses 7 points 
  
    do row = 1, rows
        do col = 1, cols
            if (w_map(col,row)>0) then !Taking only land points
                wtot = 1. !Total Weight 
                summ = r_map(col,row) !summing values

                !scanning points on a cross 
                !Left/right on X
                do pix_col = 1, L-1
                    !flags to track land
                    colpflg = .TRUE. 
                    colmflg = .TRUE.
                    !column plus pixel smoother
                    colp = col + pix_col - 1
                    !column minus pixel smoother
                    colm = col - pix_col +1

                    if(colp>cols) then 
                        colpflg = .FALSE. ! flag out of the map
                    end if 
 
                    if(colm<1) then 
                        colmflg = .FALSE. !flag out of the map 
                    end if
 
                    !weighting factor in x 
                    w_col = (1.-(1.*pix_col/L)**2)
                    !w_col = (1-(pix_col/L))*(1-(pix_col/L))
 
                    if ( colpflg .and. (w_map(colp,row)>0)) then
                        !inside the map 
                        wtot = wtot+w_col ! updating total weight 
                        summ = summ + w_col*r_map(colp,row)   ! updating sum
                    end if
                    
                    if ( colmflg .and. (w_map(colm,row)>0)) then 
                        !inside the map 
                        wtot= wtot+w_col ! updating total weight 
                        summ = summ + w_col*r_map(colm,row)   ! updating sum
                    end if

                end do    

                !Bottom/Up in Y 
                do pix_row = 1, L-1
                    !flags to track land
                    !colpflg = .TRUE. 
                    !colmflg = .TRUE.
                    !column plus pixel smoother
                    rowp = row + pix_row - 1
                    !column minus pixel smoother
                    rowm = row - pix_row + 1

                    if(rowp>rows) then 
                        !colpflg = .FALSE. ! flag out of the map
                        rowp = rowp - rows 
                    end if 
 
                    if(rowm<1) then 
                        !colmflg = .FALSE. !flag out of the map 
                        rowm = rowm + rows 
                    end if
 
                    !weighting factor in y 
                    w_row =(1.-(1.*pix_row/L)**2)
                    !w_row = (1-(pix_row/L))*(1-(pix_row/L))
 
                    if (w_map(col,rowp)>0) then
                        !inside the map 
                        wtot = wtot+w_row ! updating total weight 
                        summ = summ + w_row*r_map(col,rowp)   ! updating sum
                    end if
                    
                    if (w_map(col,rowm)>0) then 
                        !inside the map 
                        wtot= wtot+w_row ! updating total weight 
                        summ = summ + w_row*r_map(col,rowm)   ! updating sum
                    end if

                end do !End of points on cross

                !Now smooth the square
                do pix_col = 1, L-1
                    colpflg = .TRUE.
                    colmflg = .TRUE.
                    colp = col + pix_col - 1
                    colm = col - pix_col + 1

                    if (colp > cols) then 
                        colpflg = .FALSE. 
                    end if
                
                    if (colm < 1) then
                        colmflg = .FALSE.
                    end if
                    
                    w_col = (1.-(1.*pix_col/L)**2)
                    !w_col = (1-pix_col/L)*(1-pix_col/L)
                    
                    do pix_row = 1, L-1
                        !rowpflg = .TRUE.
                        !rowmflg = .TRUE.
                        rowp = row + pix_row - 1
                        rowm = row - pix_row + 1

                        if (rowp > rows) then 
                            !colpflg = .FALSE.
                            rowp = rowp - rows 
                        end if
                
                        if (rowm < 1) then
                            !colmflg = .FALSE.
                            rowm = rowm + rows 
                        end if
                        
                        w_row =(1.-(1.*pix_row/L)**2)
                        !w_row = (1-(row/pix_row))*(1-(row/pix_row))
                        
                        if(colpflg) then ! inside the map from the right 
                            if (w_map(colp, rowm)>0) then
                                wtot = wtot + w_row*w_col
                                summ = summ + w_row*w_col*r_map(colp, rowm)
                            end if
                            
                            if (w_map(colp, rowp)>0) then
                                wtot = wtot + w_row*w_col
                                summ = summ + w_row*w_col*r_map(colp, rowp)
                            end if
                             
                        end if
  
                        if (colmflg) then !inside the map from the left
                            if (w_map(colm, rowm)>0) then
                                wtot = wtot + w_row*w_col
                                summ = summ + w_row*w_col*r_map(colm, rowm)
                            end if
                            
                            if (w_map(colm, rowp)>0) then
                                wtot = wtot + w_row*w_col
                                summ = summ + w_row*w_col*r_map(colm, rowp)
                            end if                             

                        end if 
                                             
                    end do 

                end do !Finish square scan  
            
                s_map(col, row) = summ/wtot !average of points in land
            
            else 
                s_map(col,row) = 0 ! pixel is in water

            end if 
            !s_map(col,row) = L*(r_map(col,row) + w_map(col,row))
        end do
    end do 

end subroutine smooth7

subroutine alloc_xscan(nx, ny, w_map, start_seg, end_seg)
    implicit none
    logical :: segmentStarted
    integer :: nx, ny, w_map(ny,nx), start_seg, end_seg, row, col

    !f2py integer, intent(in) :: nx
    !f2py integer, intent(in) :: ny
    !f2py integer, intent(in), dimension(nx,ny) :: w_map
    !f2py integer, intent(out) :: start_seg
    !f2py integer, intent(out) :: end_seg

    start_seg = 0
    end_seg = 0 

    do row = 1, ny
        segmentStarted = .FALSE. 
        do col = 1, nx
            if (w_map(row,col)>0) then 
                if (.NOT. segmentStarted) then 
                    segmentStarted = .TRUE.
                    start_seg = start_seg+1
                end if 
            else 
                if (segmentStarted) then
                    segmentStarted = .FALSE. 
                    end_seg = end_seg+1
                end if 
            end if 
        end do     
    end do

end subroutine alloc_xscan

subroutine xscan(nx, ny, w_map, size_start, size_end, nxsegs, start_seg, end_seg)
    implicit none
    logical :: segmentStarted
    integer :: nx, ny, w_map(ny,nx), row, col, segmentCounter, size_start, size_end, istart_seg, iend_seg
    integer :: nxsegs(ny), start_seg(size_start), end_seg(size_end)

    !f2py integer, intent(in) :: nx
    !f2py integer, intent(in) :: ny
    !f2py integer, intent(in), dimension(nx,ny) :: w_map
    !f2py integer, intent(in) :: size_start
    !f2py integer, intent(in) :: size_end
    !f2py integer, intent(out) :: nxsegs
    !f2py integer, intent(out) :: start_seg(size_start)
    !f2py integer, intent(out) :: end_seg(size_end)

    istart_seg = 0
    iend_seg = 0
 
    segmentCounter = 0

    do row = 1, ny
        nxsegs(row) = 0 
        segmentStarted = .FALSE. 
        do col = 1, nx
            if (w_map(row,col)>0) then 
                if (.NOT. segmentStarted) then 
                    nxsegs(row) = nxsegs(row) + 1 !Starting a segment
                    istart_seg = istart_seg + 1  
                    start_seg(istart_seg) = col - 1
                    segmentStarted = .TRUE.
                end if 
            else 
                if (segmentStarted) then
                    iend_seg = iend_seg + 1
                    end_seg(iend_seg)= col-2
                    segmentStarted = .FALSE. 
                end if 
            end if 
        end do     
        segmentCounter = segmentCounter + nxsegs(row) 
    end do
end subroutine xscan

subroutine alloc_yscan(nx, ny, w_map, start_seg, end_seg)
    implicit none
    logical :: segmentStarted
    integer :: nx, ny, w_map(ny,nx), start_seg, end_seg, row, col

    !f2py integer, intent(in) :: nx
    !f2py integer, intent(in) :: ny
    !f2py integer, intent(in), dimension(nx,ny) :: w_map
    !f2py integer, intent(out) :: start_seg
    !f2py integer, intent(out) :: end_seg

    start_seg = 0
    end_seg = 0 

    do col = 1, nx
        segmentStarted = .FALSE. 
        do row = 1, ny
            if (w_map(row,col)>0) then 
                if (.NOT. segmentStarted) then 
                    segmentStarted = .TRUE.
                    start_seg = start_seg+1
                end if 
            else 
                if (segmentStarted) then
                    segmentStarted = .FALSE. 
                    end_seg = end_seg+1
                end if 
            end if 
        end do     
    end do

end subroutine alloc_yscan

subroutine yscan(nx, ny, w_map, size_start, size_end, nysegs, start_seg, end_seg)
    implicit none
    logical :: segmentStarted
    integer :: nx, ny, w_map(ny,nx), row, col, size_start, size_end, istart_seg, iend_seg
    integer :: nysegs(nx), start_seg(size_start), end_seg(size_end)

    !f2py integer, intent(in) :: nx
    !f2py integer, intent(in) :: ny
    !f2py integer, intent(in), dimension(nx,ny) :: w_map
    !f2py integer, intent(in) :: size_start
    !f2py integer, intent(in) :: size_end
    !f2py integer, intent(out) :: nysegs
    !f2py integer, intent(out) :: start_seg(size_start)
    !f2py integer, intent(out) :: end_seg(size_end)

    istart_seg = 0
    iend_seg = 0

    do col = 1, nx
        nysegs(col) = 0 
        segmentStarted = .FALSE. 
        do row = 1, ny
            if (w_map(row,col)>0) then 
                if (.NOT. segmentStarted) then 
                    nysegs(col) = nysegs(col) + 1 !Starting a segment
                    istart_seg = istart_seg + 1  
                    start_seg(istart_seg) = row - 1
                    segmentStarted = .TRUE.
                end if 
            else 
                if (segmentStarted) then
                    iend_seg = iend_seg + 1
                    end_seg(iend_seg)= row -2
                    segmentStarted = .FALSE. 
                end if 
            end if 
        end do     
    end do

end subroutine yscan


subroutine test(nx, ny, r_map, s_map)
    implicit none
    integer :: nx, ny 
    real(8) :: r_map(nx,ny), s_map(nx,ny)
    !integer, allocatable :: a(:), b(:)

    !f2py integer, intent(in) :: nx
    !f2py integer, intent(in) :: ny
    !f2py real(8), intent(in), dimension(nx,ny) :: r_map
    !f2py real(8), intent(out), dimension(nx,ny) :: s_map

    s_map = r_map

    !s_map = r_map
    !allocate (a(nx))
    !a = [1,2,3]
    !call move_alloc(a,nxsegs)
    !print *, allocated(a), allocated(nxsegs) ! F, T
    !print *, nxsegs ![1, 2, 3]

end subroutine test


subroutine genfromtxt(filename, nx, ny, a)
    implicit none
    character(150):: filename
    real, dimension(ny,nx) :: a 
    integer :: row, col, ny, nx
    !use here intent(hide) instead
    !f2py character(120), intent(in) ::filename 
    !f2py integer, intent(in) :: nx
    !f2py integer, intent(in) :: ny
    !f2py real, intent(out), dimension(nx,ny) :: a
 
    !Opening file
    !filename="world_100x50.txt"
    open(5, file=filename)
 
    !read data again
    do row = 1, ny
        read(5,*) (a(row,col), col =1,nx) !reading line by line 
    end do
    close (5)


end subroutine genfromtxt


subroutine amult2(np2, u, v, dx)
    implicit none
    integer :: n, i, np2
    real(8) :: u(np2), v(np2), dx
!f2py real(8), intent(out), dimension(np2) :: v
!f2py real(8), intent(in), dimension(np2) :: u
!f2py real(8), intent(in) :: dx
!f2py integer, intent(in) :: np2

  n = np2-2

  v(1) = -dx*u(1)/2+dx*u(2)/2 
  v(n+2) = -dx*u(n+1)/2 + dx*u(n+2)/2
  
!Forward Elimination 
!v = u(1:n-2)+u(3:n)-2*u(2:n-1)
  do i = 2, n+1
      v(i) = u(i-1)+u(i+1)-2*u(i)
  end do 

end subroutine amult2


subroutine trisol2(np2, k, dx, d, b, x)
    implicit none
    integer :: n, i, ii, np2
    real(8) :: k, dx, d(np2), b(np2), cp(np2), dp(np2), x(np2), mk2, m2dx

!f2py real(8), intent(out), dimension(np2) :: x
!f2py real(8), intent(in), dimension(np2) :: d
!f2py real(8), intent(in), dimension(np2) :: b
!f2py real(8), intent(in) :: k
!f2py real(8), intent(in) :: dx
!f2py integer, intent(in) :: np2

  n = np2-2

  mk2 = -0.5*k
  m2dx = -0.5*dx

  cp(1) = -1.

  !Forward Elimination 
  !nm1 = n-1
  do i = 2, n+1
    cp(i) = mk2/(d(i)-mk2*cp(i-1))
  end do 

  dp(1) = b(1)/m2dx

  do i=2, n+1
      dp(i) = (b(i)-mk2*dp(i-1))/(d(i)-mk2*cp(i-1))
  end do 
  
 
  dp(n+2) = (b(n+2)-m2dx*dp(n+1))/(d(n+2)-m2dx*cp(n+1))
  
  x(n+2) = dp(n+2)

  !Backward substitution
  ii = n+1
  do i=1, n+1
      !print *, ii ![1, 2, 3]
      x(ii) = dp(ii) - cp(ii)*x(ii+1)
      ii = ii-1
  end do 

end subroutine trisol2

subroutine godunovstepneumann2nd(np2, h, k, dx, u, v)
    implicit none
    integer :: n, i, np2
    real(8) :: h, k, dx, u(np2), v(np2), ut(np2), ue(np2), ur(np2), dA(np2), sigmal, sigmar

!f2py integer, intent(in) :: np2
!f2py real(8), intent(in) :: h
!f2py real(8), intent(in) :: k
!f2py real(8), intent(in) :: dx
!f2py real(8), intent(in), dimension(np2) :: u
!f2py real(8), intent(out), dimension(np2) :: v

  n = np2-2

  !Fluxes
  sigmal = 0.
  sigmar = 0.

  call amult2(np2, u, ut, dx)
  ut = k*ut
  ue = ut + h*(1.-u)*u
  ur = u + 0.5*ue !RHS
  ue = u+ue

  ur(1) = sigmal
  ur(np2) = sigmar
 
  dA(1) = -0.5*dx
  dA(np2) = 0.5*dx
  
  do i = 2,np2-1
      dA(i) = 1. + k - 0.5*h*(1.-ue(i))
      !print *, dA(i)
  end do 

  call trisol2(np2,k,dx,dA,ur,v)

end subroutine godunovstepneumann2nd

subroutine amult3(n, u, v)
  implicit none
  integer :: n, i
  real(8) :: u(n), v(n)

!f2py real(8), intent(out), dimension(n) :: v
!f2py real(8), intent(in), dimension(n) :: u
!f2py integer, intent(in) :: n

  v(1) = -2.*u(1)+u(2)
  v(n) = u(n-1)-2*u(n)
  
!Forward Elimination 
  !v = u(1:n-2)+u(3:n)-2*u(2:n-1)
  do i = 2, n-1 
      v(i) = u(i-1)+u(i+1)-2*u(i)
  end do 

end subroutine amult3

subroutine trisol3(n, k, d, b, x)
  implicit none
  integer :: n, i, ii
  real(8) :: k, d(n), b(n), s1(n), s2(n), x(n), mk2

!f2py real(8), intent(out), dimension(n) :: x
!f2py real(8), intent(in), dimension(n) :: b
!f2py real(8), intent(in) :: k
!f2py integer, intent(in) :: n

  mk2 = -k/2
  s2(1) = d(1)
  s1(2) = mk2/s2(1)
  s2(2) = d(2)+4.*s1(2)/3.
  !s1(3) = mk2/s2(1)
  !s2(2) = d(2)+4.*s1(3)/3.

!Forward Elimination 
  !nm1 = n-1
  do i = 3, n-1 
      s1(i) = mk2/s2(i-1)
      s2(i) = d(i)-mk2*s1(i)
  end do 

!Neumann
  s1(n) = -4./(3*s2(n-1))
  s2(n) = d(n)-mk2*s1(n) 

  x(1) = b(1)
  do i=2, n
      x(i) = b(i)- s1(i)*x(i-1)
  end do 

!Backward substitution
  x(n) = x(n)/s2(n)
  ii = n-1

  do i=1, n-2
      x(ii) = (x(ii) - mk2*x(ii+1))/s2(ii)
      ii = ii-1
  end do 

  !x(1) = x(1)/s2(1)

end subroutine trisol3

subroutine godunovstepneumann3rd(n, h, k, u, v)
  implicit none
  integer :: n, i
  real(8) :: h, k, u(n), v(n), ut(n), ue(n), ur(n), dA(n), uest

!f2py integer, intent(in) :: n
!f2py real(8), intent(in) :: h
!f2py real(8), intent(in) :: k
!f2py real(8), intent(in), dimension(n) :: u
!f2py real(8), intent(out), dimension(n) :: v

  call amult3(n, u, ut)
  ut = k*ut
  ue = ut + h*(1.-u)*u
  ur = u + 0.5*ue !RHS
  ue = u+ue
 
  !impose neumann on Euler estimate 
  ue(1) = 4.*ue(2)/3.-ue(3)/3.
  ue(n)=4.*ue(n-1)/3.-ue(n-2)/3.

  uest = u(3)+0.5*k*(ue(2)-2.*ue(3)+ue(4))+0.5*k*(u(2)-2.*u(3)+u(4))+0.5*h*((1-ue(3))*ue(3)+(1-u(3))*u(3))
  ur(1) = -uest/3

  uest = u(n-2)+0.5*k*(ue(n-3)-2.*ue(n-2)+ue(n-1))+0.5*k*(u(n-3)-2.*u(n-2)+u(n-1))+0.5*h*((1-ue(n-2))*ue(n-2)+(1-u(n-2))*u(n-2))
  ur(n) = -uest/3

  dA(1) = 1.
  dA(n) = 1.
  
  do i = 2,n-1
      dA(i) = 1 + k - 0.5*h*(1-ue(i))
      !print *, dA(i)
  end do 

  call trisol3(n,k,dA,ur,v)

  !impose Neumann on output 
  v(1)=4.*v(2)/3.-v(3)/3
  v(n)=4*v(n-1)/3.-v(n-2)/3

end subroutine godunovstepneumann3rd
