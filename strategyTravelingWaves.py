# Using strategy pattern from book Head First Design Patterns and example from
# http://stackoverflow.com/questions/963965/how-is-this-strategy-pattern
# -written-in-python-the-sample-in
# folding: zM, za, zR
from toolsWaves import *
from abc import ABCMeta, abstractmethod

################################################################################
# Data interface and concrete implementation classes
################################################################################

class Data:
    def __init__(self):
        pass

class SmallData(Data):
    def __init__(self, w_map, u, h, kcfl, NT, NPLT , NX, NY, Ts, Te, tsc, mapsize):
        self.w_map = w_map
        self.u = u
        self.h = h 
        self.kcfl = kcfl
        self.NT = NT
        self.NPLT = NPLT
        self.NX = NX
        self.NY = NY 
        self.Ts = Ts
        self.Te = Te
        self.tsc = tsc
        self.mapsize = mapsize

class BigData(Data):
    pass 

class LargeData(Data):
    pass

################################################################################
# Abstract Simulation class and concrete Simulation type classes.
################################################################################

class Simulation:
    def __init__(self, run, plot):
        self._run_behavior = run
        self._plot_behavior = plot

    def run(self):
        return self._run_behavior.run()

    def plot(self):
        return self._plot_behavior.plot() 

class SmallSimulation(Simulation):
    def __init__(self):
        mapsize = 'small'
        print("=====Initializing "+mapsize+" simulation=====")

        # default INPUT parameters:
        NT = 2001
        #NT =101
        NPLT = 40
        #self.NPLT=40
        Kyb = 208.0      # this is Young and Bettinger's diffusion coeff.
                          # should be between 0.1 to 1000.0 km^2/yr.
        Ryb = 0.00167    # Young & Bettinger's growth rate: 0.001 - 0.03 yr^{-1}
        # Earth measurement = circumference
        Ecirc = 40074.78 # in km, cell size will be dx=dy = Ediam/(NX-1) (km)
        #start/stop simulation in kya
        Ts  = 50.0    
        Te  = 1.0

        print(' INPUT Parameter summary: ')
        print('   Number of time steps: NT=%d '% NT)
        print('   Plotting interval: NPLT=%d '% NPLT)
        print('   Diffusion coeff: Kyb = %e (in km^2/yr)'% Kyb)
        print('   Growth rate: Ryb = %e (in yr^{-1})'% Ryb)
        # END of Input parameters


        w_map=createWrlMap(mapsize)
        NY, NX = w_map.shape
        print(' MAP SIZE: NX=Grid_size_x=%d, NY=Grid_size_y=%d '%(NX, NY))

        # Now rescale units according to form eq. (3) in paper
        # distance SCALE FACTOR:
        sfact = sqrt(0.5*Ryb/Kyb); # unit here is km^{-1}
        real_dx = Ecirc/(NX-1)
        dx = sfact*real_dx # % now unitless
        dy = dx;    # square cells
        x1  = 0.5*Ecirc*sfact
        x0  = - x1   # W-E direction
        y1  = 0.5*x1
        y0  = -y1       # S-N direction
        t1  = 1.e+3*Ryb*(Ts-Te) # unitless time as in eq. (3)
        h   = t1/NT            # unitless time step, as in eq. (3)
        # eq (3) scaled time relative to NPP frames (by kya), NPP
        # frames are numbered by kiloyears (kyr or kya)
        tsc = 1.e-3/Ryb                  # Ryb is in yr^{-1}, not kyr^{-1}

        print('   CELL size: dx=dy = %5.1f (km)= %5.1f (eq. (3) units)' %(real_dx,dx))
        print('   BOX: X x Y = [%5.1f, %5.1f] x [%5.1f, %5.1f]' %(x0,x1,y0,y1))
        u = zeros((NY, NX)) 

        orig_y = (24-1)
        orig_x = (17-1)

        #Function to be written 
        initu(u,w_map, orig_x, orig_y, dx, dy)
        
        # compute time step and CFL parameter
        kcfl = 0.5*h/(dx*dx)
        print('   eq. (3) step size h=%e, and CFL number=%e \n'%(h,kcfl))

        self.data = SmallData(w_map, u, h, kcfl, NT, NPLT, NX, NY, Ts, Te, tsc, mapsize)

        Simulation.__init__(self, RunSingleLayer(self.data), Plot(self.data))

class LargeSimulation(Simulation):
    def __init__(self):
        mapsize = 'large'
        print("=====Initializing "+mapsize+" simulation=====")

        # default INPUT parameters:
        #NT = 2001
        #NT =101
        NPLT = 40
        #self.NPLT=40
        Kyb = 208.0      # this is Young and Bettinger's diffusion coeff.
                          # should be between 0.1 to 1000.0 km^2/yr.
        Ryb = 0.00167    # Young & Bettinger's growth rate: 0.001 - 0.03 yr^{-1}
        # Earth measurement = circumference
        Ecirc = 40074.78 # in km, cell size will be dx=dy = Ediam/(NX-1) (km)
        #start/stop simulation in kya
        Ts  = 50.0
        #Ts = 5.0 #Leo: Test to match new mapsim19    
        Te  = 1.0

        # END of Input parameters

        # Find start map/template:
        yalist, masks, tepoch = get_cc_list(mapsize)

        kstart = 0                
        while tepoch[kstart+1] >= Ts:
            kstart=kstart+1

        # Get first template 
        kmp = kstart
        w_L = templateMap(mapsize, masks, kmp) #Template for the first map 
        NY, NX = w_L.shape
        #NX, NY = w_L.shape


        # Now rescale units according to form eq. (3) in paper
        # distance SCALE FACTOR:
        sfact = sqrt(0.5*Ryb/Kyb); # unit here is km^{-1}
        #real_dx = Ecirc/(NX-1)
        #dx = sfact*real_dx # % now unitless
        #dy = dx;    # square cells

        #Leo: newmap19 new part of the code 
        sy = Ecirc/(NY-1)
        dx = sfact*sy
        dy = dx 
        
        #x1  = 0.5*Ecirc*sfact
        #x0  = - x1   # W-E direction
        #y1  = 0.5*x1
        #y0  = -y1       # S-N direction
        
        #Leo: mapsim19 now compute the step size and total number of timesteps

        t1  = 1.e+3*Ryb*(Ts-Te) # unitless time as in eq. (3)
        #h0 = dx*dx              # CFL guess for h
        #Leo: improve accuracy
        h0 = 0.05*dx*dx
        m0 = ceil(1/h0)         #no.teps/(1 Ky)
        h = 1/m0                #step size such that m0*h == 1 
        NT = int(ceil(t1*m0))        #total number of time steps, casting since ceil returns a float   
        #h   = t1/NT            # unitless time step, as in eq. (3)
        # eq (3) scaled time relative to NPP frames (by kya), NPP
        # frames are numbered by kiloyears (kyr or kya)
        tsc = 1.e-3/Ryb                  # Ryb is in yr^{-1}, not kyr^{-1}

        #xl = range(0,NX) #points for plots on integers
        #xl = 1.e-4*(real_dx*array(xl)-real_dx)
        #yl = range(0,NY)
        #yl = 1.e-4*(real_dx*array(yl)-real_dx)

        print(' INPUT Parameter summary: ')
        print('   Number of time steps: NT=%d '% NT) 
        print('   Plotting interval: NPLT=%d '% NPLT)
        print('   Diffusion coeff: Kyb = %e (in km^2/yr)'% Kyb)
        print('   Growth rate: Ryb = %e (in yr^{-1})'% Ryb)
        print(' MAP SIZE: NX=%d (E-W), NY=%d (N-S)'%(NX, NY))
        #print('   CELL size: dx=dy = %5.1f (km)= %5.1f (eq. (3) units)' %(real_dx,dx)
        print('   CELL size: dx=dy = %5.1f (km)= %5.1f (eq. (3) units)' %(sy,dy))
        #print('   BOX: X x Y = [%5.1f, %5.1f] x [%5.1f, %5.1f]' %(x0,x1,y0,y1))

        u = zeros((NY, NX)) 

        orig_y = (24-1)
        orig_x = (17-1)
        # Leo: fix this for 720x360 map, Actually it doesn't really matter the exact position
        #orig_x=185
        #orig_y=80

        #Starting population somewhere in Africa
        initu(u,w_L, orig_x, orig_y, dx, dy)
        
        # compute time step and CFL parameter
        kcfl = 0.5*h/(dx*dx)
        print('   eq. (3) step size h=%e, and CFL number=%e \n'%(h,kcfl))

        self.data = SmallData(w_L, u, h, kcfl, NT, NPLT, NX, NY, Ts, Te, tsc, mapsize)

        Simulation.__init__(self, RunMultiLayer(self.data), Plot(self.data))
        #Simulation.__init__(self, RunHDF5(self.data), Plot(self.data))

 
class HugeSimulation(Simulation):
    def __init__(self):
        mapsize = 'huge'
        print("=====Initializing "+mapsize+" simulation=====")


        # default INPUT parameters:
        #NT = 2001
        #NT =101
        NPLT = 40
        #self.NPLT=40
        Kyb = 208.0      # this is Young and Bettinger's diffusion coeff.
                          # should be between 0.1 to 1000.0 km^2/yr.
        Ryb = 0.00167    # Young & Bettinger's growth rate: 0.001 - 0.03 yr^{-1}
        # Earth measurement = circumference
        Ecirc = 40074.78 # in km, cell size will be dx=dy = Ediam/(NX-1) (km)
        #start/stop simulation in kya
        Ts  = 50.0
        #Ts = 5.0 #Leo: Test to match new mapsim19    
        Te  = 1.0

        # END of Input parameters

        # Find start map/template:
        yalist, masks, tepoch = get_cc_list(mapsize)

        kstart = 0                
        while tepoch[kstart+1] >= Ts:
            kstart=kstart+1

        # Get first template 
        kmp = kstart
        w_L = templateMap(mapsize, masks, kmp) #Template for the first map 
        NY, NX = w_L.shape
        #NX, NY = w_L.shape


        # Now rescale units according to form eq. (3) in paper
        # distance SCALE FACTOR:
        sfact = sqrt(0.5*Ryb/Kyb); # unit here is km^{-1}
        #real_dx = Ecirc/(NX-1)
        #dx = sfact*real_dx # % now unitless
        #dy = dx;    # square cells

        #Leo: newmap19 new part of the code 
        sy = Ecirc/(NY-1)
        dx = sfact*sy
        dy = dx 
        
        #x1  = 0.5*Ecirc*sfact
        #x0  = - x1   # W-E direction
        #y1  = 0.5*x1
        #y0  = -y1       # S-N direction
        
        #Leo: mapsim19 now compute the step size and total number of timesteps

        t1  = 1.e+3*Ryb*(Ts-Te) # unitless time as in eq. (3)
        #h0 = dx*dx              # CFL guess for h
        #Leo:improve accuracy
        h0 = 0.05*dx*dx
        m0 = ceil(1/h0)         #no.teps/(1 Ky)
        h = 1/m0                #step size such that m0*h == 1 
        NT = int(ceil(t1*m0))        #total number of time steps, casting since ceil returns a float   
        #h   = t1/NT            # unitless time step, as in eq. (3)
        # eq (3) scaled time relative to NPP frames (by kya), NPP
        # frames are numbered by kiloyears (kyr or kya)
        tsc = 1.e-3/Ryb                  # Ryb is in yr^{-1}, not kyr^{-1}

        #xl = range(0,NX) #points for plots on integers
        #xl = 1.e-4*(real_dx*array(xl)-real_dx)
        #yl = range(0,NY)
        #yl = 1.e-4*(real_dx*array(yl)-real_dx)

        print(' INPUT Parameter summary: ')
        print('   Number of time steps: NT=%d '% NT) 
        print('   Plotting interval: NPLT=%d '% NPLT)
        print('   Diffusion coeff: Kyb = %e (in km^2/yr)'% Kyb)
        print('   Growth rate: Ryb = %e (in yr^{-1})'% Ryb)
        print(' MAP SIZE: NX=%d (E-W), NY=%d (N-S)'%(NX, NY))
        #print('   CELL size: dx=dy = %5.1f (km)= %5.1f (eq. (3) units)' %(real_dx,dx))
        print('   CELL size: dx=dy = %5.1f (km)= %5.1f (eq. (3) units)' %(sy,dy))
        #print('   BOX: X x Y = [%5.1f, %5.1f] x [%5.1f, %5.1f]' %(x0,x1,y0,y1))

        u = zeros((NY, NX)) 

        orig_y = (24-1)
        orig_x = (17-1)
        # Leo: fix this for 720x360 map, Actually it doesn't really matter the exact position
        #orig_x=185
        #orig_y=80

        #Starting population somewhere in Africa
        initu(u,w_L, orig_x, orig_y, dx, dy)
        
        # compute time step and CFL parameter
        kcfl = 0.5*h/(dx*dx)
        print('   eq. (3) step size h=%e, and CFL number=%e \n'%(h,kcfl))

        self.data = SmallData(w_L, u, h, kcfl, NT, NPLT, NX, NY, Ts, Te, tsc, mapsize)

        Simulation.__init__(self, RunMultiLayer(self.data), Plot(self.data))



class BigSimulation(Simulation):
    def __init__(self):
        Simulation.__init__(self, RunEmpty(), Plot())
        #Simulation.__init__(self)
        print("=====Initializing Big Simulation=====")


################################################################################
# Run behavior interface and behavior implementation classes.
################################################################################

class RunBehavior:
    __metaclass__ = ABCMeta
    @abstractmethod 
    def run(self):
        pass

class RunEmpty(RunBehavior):
    def __init(self):
        pass

    def run(self):
        pass

class RunSingleLayer(RunBehavior):
    def __init__(self, data):
        self.data = data

    def run(self):
        print("=====Small Simulation Starting=====")
        output = False
        module = 'Fortran'
        #Deciding which booster to use 
        if module == 'Fortran':
            import Fmodules as modules

        elif module =='C':
            import Cmodules as modules
         
        elif module =='Python':
            import Pymodules as modules 

        #Python scanners 
        nxsegs, xstart_seg, xend_seg = Xscan(self.data.w_map)
        nysegs, ystart_seg, yend_seg = Yscan(self.data.w_map)
        #Fortran scanners 
        #nxsegs, xstart_seg, xend_seg = FXscan(self.data.w_map)
        #nysegs, ystart_seg, yend_seg = FYscan(self.data.w_map)


        NS = max(self.data.NY,self.data.NX)
        u0 = zeros(NS) #find max size 
        u1 = zeros(NS) #find max size
        kap0 = zeros(NS)
        kaph = zeros(NS)
        Ts = self.data.Ts
        Te = self.data.Te
        tsc = self.data.tsc
        h = self.data.h
        kcfl = self.data.kcfl
        u = self.data.u      
        mapsize = self.data.mapsize

        yp = yppoints() #Interpolation points    

        # read the CC map lists and times=tepoch's for each frame: find first
        # Search for appropriate NPP maps:
        #      from start time Ts (in kya) to end time Te
        #loading carrying capacities from namelist
        yalist, tepoch = get_cc_list(mapsize)


        #kstart = 1  
        kstart = 0
        while tepoch[kstart+1] >= Ts:
            kstart=kstart+1

        print('   Start time Ts=%5.1f (kya), NPP frame kstart=%d\n'%(Ts,kstart))

        kend = 61 #Leo: Verify this, it's probably 60
        while tepoch[kend-1] <= Te: 
            kend=kend-1

        print('   End time   Te=%5.1f (kya), NPP frame kend=%d\n'%(Te,kend))

        # make sure there are at least 2 maps between Ts and Te
        if ((kend-kstart)<1) or ((tepoch[kstart]-tepoch[kend])<(Ts-Te)):
           print('Ts=%e, Te=%e, not space enough: kstart=%d, kend=%d\n'%(Ts,Te,kstart,kend))

        #      start and end maps found
        kmp = kstart # start counting maps here

        #Using Jed's map
        #if self.data.mapsize =='big':
        #    import netCDF4
        #    data = netCDF4.Dataset('/Users/Leo/Desktop/Maps_Leo/fep_test.nc','r')
        #    w_J = flipud(data.variables['FEP'][:].data)
        #    w_J = roll(w_J,-310,axis=1)
        #    w_J[w_J<0]=0
        #    w_L = w_J.copy()
        #    w_H = w_J.copy()
        #    t_L = tepoch[kmp]
        #    kmp = kmp + 1                  # increment map count
        #    t_H = tepoch[kmp]
        #else:
        w_L = kapepoch(self.data.w_map, yalist, kmp, mapsize) # this is the first map
        t_L = tepoch[kmp]
        kmp = kmp + 1                  # increment map count
        w_H = kapepoch(self.data.w_map,yalist,kmp, mapsize) # second map
        t_H = tepoch[kmp]

        s   = (t_L-Ts+tsc*h)/(t_L-t_H)  # first step interpolation parameter
        wt0 = 0.0
        # compute interpoated weight to get to step t=h
        ks  = floor(100*s)
        sd  = 100*s - ks               # residual sd = 100*s mod 1
        wth = yp[0]*(1-sd) + yp[1]*sd  # first interpolant
        t   = Ts                       # this is real time in kya

        # TGL alternates between 1 and 0 
        TGL = True
        imID = 0
        #TGL = False

        # BEGIN main loop
        # self.NT = 1
        start = timer()
        for it in range(0,self.data.NT):
            if TGL: #First choice: half-step1 in Y, and step 2 in X 
                locy = 0
                #half-step1 in Y
                for j in range(0,self.data.NX):
                    nsegs = nysegs[j] #Number of segments 
                    for k in range(0,nsegs):
                        istart=ystart_seg[locy+k] #From: segment starts
                        iend=yend_seg[locy+k] #To:segmend ends
                        ninseg=iend-istart+1 #number of cells inside the segment

                        u0[1:ninseg+1] = u[istart:iend+1,j]

                        #Dirichlet b.c.
                        u0[0] = 0
                        u0[ninseg+1] = 0
                        #Neumann b.c.
                        #u0[0] = u0[1]
                        #u0[ninseg+1] = u0[ninseg]

                        #ut = godunovstep1(ninseg+2,h,kcfl,u0,sc1,sc2)
                        #Booster Subroutine


                        ut = modules.godunovstep1(h,kcfl,u0[0:ninseg+2])

                        #Update u
                        #Leo: Check this 
                        #u[istart:iend,j] = ut[2:ninseg+1]
                        u[istart:iend+1,j] = ut[1:ninseg+1]

                        
                    locy = locy + nsegs
                        
                #Step 2 : X direction updates
                locx = 0
                for i in range(0,self.data.NY):
                    nsegs = nxsegs[i]
                    for k in range(0,nsegs):
                        jstart=xstart_seg[locx+k]
                        jend=xend_seg[locx+k]
                        ninseg=jend-jstart+1

                        u1[1:ninseg+1] = u[i,jstart:jend+1]
                        kap0[0] = 1 
                        kap0[ninseg+1] = 1
                        kaph[0] = 1
                        kaph[ninseg+1] = 1
                        kap0[1:ninseg+1] = w_L[i,jstart:jend+1]*(1-wt0) + w_H[i,jstart:jend+1]*wt0
                        #No interpolation
                        #kap0[1:ninseg+1] = w_L[i,jstart:jend+1]                        
                        kaph[1:ninseg+1] = w_L[i,jstart:jend+1]*(1-wth) + w_H[i,jstart:jend+1]*wth
                        #No interpolation
                        #kaph[1:ninseg+1] = w_L[i,jstart:jend+1]


                        #Dirichlet b.c.
                        u1[0] = 0 
                        u1[ninseg+1] = 0
                        #Neumann b.c.
                        #u1[0] = u1[1]
                        #u1[ninseg+1] = u1[ninseg]
                        
                        #ut = godunovstep2(ninseg+2,h,kcfl,u1,sc1,sc2,kap0,kaph)
                        #Booster Subroutine
                        ut = modules.godunovstep2(h,kcfl,u1[0:ninseg+2],kap0[0:ninseg+2],kaph[0:ninseg+2])
                        #Leo: Check this
                        #u[i,jstart:jend] = ut[2:ninseg+1]
                        u[i,jstart:jend+1] = ut[1:ninseg+1]

                  
                    locx = locx + nsegs
                        
                # half step1 in Y direction
                locy = 0
                for j in range(0,self.data.NX):
                    nsegs = nysegs[j]
                    for k in range(0,nsegs):
                        istart=ystart_seg[locy+k]
                        iend=yend_seg[locy+k]
                        ninseg=iend-istart+1

                        u0[1:ninseg+1] = u[istart:iend+1,j]

                        #Dirichlet b.c.
                        u0[0] = 0
                        u0[ninseg+1] = 0
                        #Neumann b.c.
                        #u0[0] = u0[1]
                        #u0[ninseg+1] = u0[ninseg]

                        #ut = godunovstep1(ninseg+2,h,kcfl,u0,sc1,sc2)
                        #Booster Subroutine
                        ut = modules.godunovstep1(h,kcfl,u0[0:ninseg+2])                        
                        u[istart:iend+1,j] = ut[1:ninseg+1]

                  
                    locy = locy + nsegs

                TGL = False
            else:
                ##Second choice: half-step1 in X, and step 2 in Y 
                locx = 0
                #half-step1 in X
                for i in range(0,self.data.NY):
                    nsegs = nxsegs[i] #Number of segments 
                    for k in range(0,nsegs):
                        jstart=xstart_seg[locx+k] #From: segment starts
                        jend=xend_seg[locx+k] #To:segmend ends
                        ninseg=jend-jstart+1
                        u1[1:ninseg+1] = u[i,jstart:jend+1]

                        #Dirichlet b.c.
                        u1[0] = 0 
                        u1[ninseg+1] = 0
                        #Neumann b.c.
                        #u1[0] = u1[1]
                        #u1[ninseg+1] = u1[ninseg]

                        #ut = godunovstep1(ninseg+2,h,kcfl,u1,sc1,sc2)
                        #Fortran Subroutine
                        ut = modules.godunovstep1(h,kcfl,u1[0:ninseg+2])
                        u[i,jstart:jend+1] = ut[1:ninseg+1]

                        
                    locx = locx + nsegs
                #step2 in Y-Direction 
                locy = 0
                for j in range(0,self.data.NX):
                    nsegs = nysegs[j]
                    for k in range(0,nsegs):
                        istart=ystart_seg[locy+k]
                        iend=yend_seg[locy+k]
                        ninseg=iend-istart+1

                        u0[1:ninseg+1] = u[istart:iend+1,j]
                        kap0[0] = 1 
                        kap0[ninseg+1] = 1
                        kaph[0] = 1 
                        kaph[ninseg+1] = 1

                        kap0[1:ninseg+1] = w_L[istart:iend+1,j]*(1-wt0) + w_H[istart:iend+1,j]*wt0
                        kaph[1:ninseg+1] = w_L[istart:iend+1,j]*(1-wth) + w_H[istart:iend+1,j]*wth

                        #Dirichlet b.c.
                        u0[0] = 0 
                        u0[ninseg+1] = 0
                        #Neumann b.c.
                        #u0[0] = u0[1]
                        #u0[ninseg+1] = u0[ninseg]

                        #ut = godunovstep2(ninseg+2,h,kcfl,u0,sc1,sc2,kap0,kaph)
                        #Booster Subroutine
                        ut = modules.godunovstep2(h,kcfl,u0[0:ninseg+2],kap0[0:ninseg+2],kaph[0:ninseg+2])

                        u[istart:iend+1,j] = ut[1:ninseg+1]
                  
                    locy = locy + nsegs
                #half-step1 in X-Direction
                locx = 0
                for i in range(0,self.data.NY):
                    nsegs = nxsegs[i]
                    for k in range(0,nsegs):
                        jstart=xstart_seg[locx+k]
                        jend=xend_seg[locx+k]
                        ninseg=jend-jstart+1
                        u1[1:ninseg+1] = u[i,jstart:jend+1]

                        #Dirichlet b.c.
                        u1[0] = 0
                        u1[ninseg+1] = 0
                        #Neumann b.c.
                        #u1[0] = u1[1]
                        #u1[ninseg+1] = u1[ninseg]

                        #ut = godunovstep1(ninseg+2,h,kcfl,u1,sc1,sc2)
                        #Booster Subroutine
                        ut = modules.godunovstep1(h,kcfl,u1[0:ninseg+2])
                        u[i,jstart:jend+1] = ut[1:ninseg+1]

                  
                    locx = locx + nsegs
                TGL = True
                
            if kmp > kend: 
                print('break hit, leaving loop')
                break

            # increment t value, reset weights and maps if necessary
            t = t - tsc*h          # decrement t in kya
            #fprintf(' ordinary step: t_L=%e, t=%e, t_H=%e\n',t_L,t,t_H);
            if t > t_H:              # usual case, maps not exhausted
                wt0 = wth           # shift lower weight down
                s   = (t_L-t)/(t_L-t_H)  # interpolation parameter
                ks  = floor(100*s)
                sd  = 100*s - ks    # residual sd = 100*s mod 1
                wth = yp[ks]*(1-sd) + yp[ks+1]*sd  # first interpolant
            else:                   # reached upper map time 
                kmp = kmp +1                      # increment map count
                if kmp <= kend:               # kend is the last map
                    w_L = w_H                 # set lower map to prev. upper
                    t_L = t_H                 # set lower time to prev. upper
                    #if self.data.mapsize =='big':
                    #    w_H = w_J.copy()
                    #else:
                    w_H = kapepoch(self.data.w_map,yalist,kmp, self.data.mapsize)  # read next map
                    t_H = tepoch[kmp]         # time frame of new map

                    wt0 = 0
                    s   = (t_L - t)/(t_L-t_H)  # interpolation parameter
                    ks  = floor(100*s)
                    sd  = 100*s - ks                    # mod1 interpolation 
                    wth = yp[ks]*(1-sd) + yp[ks+1]*sd  # first interpolant

            ##Storing Data 
            if (mod(it,self.data.NPLT)==0)or(it==self.data.NT):
                if t < 10:
                    from  matplotlib import pyplot 
                    import matplotlib.cm as cm
                    tstr = '   %4.2f'%t
                    print(tstr)
                    if output:
                        u_plot = self.data.w_map-1+log(1+10*self.u)
                        fig = pyplot.figure(figsize=(30,20))
                        pyplot.subplot(111)
                        pyplot.title('Time='+tstr+'kY')
                        im = pyplot.imshow(u_plot, cmap = cm.RdBu, vmin= -1, vmax = 2)
                        #pyplot.clim(0,1)
                        cb = pyplot.colorbar()
                        #cb.set_clim(-1, 3.0)
                        fig.savefig(str(imID)+'-'+tstr+'.png',dpi = 100)
                        pyplot.close()
                        imID= imID+1

                else:
                    from  matplotlib import pyplot 
                    import matplotlib.cm as cm
                    tstr = '   %4.1f'%t
                    print(tstr)
                    if output:
                        u_plot = self.data.w_map-1+log(1+10*self.u)
                        fig = pyplot.figure(figsize=(30,20))
                        pyplot.subplot(111)
                        pyplot.title('Time='+tstr+'kY')
                        im = pyplot.imshow(u_plot, cmap = cm.RdBu, vmin= -1, vmax = 2)
                        #pyplot.clim(0,1)
                        cb = pyplot.colorbar()
                        #cb.set_clim(-1, 3.0)
                        fig.savefig(str(imID)+'-'+tstr+'.png',dpi = 100)
                        pyplot.close()
                        imID = imID+1

        end = timer()

        print("=====Small simulation completed=====")
        print('   Main Loop Time:', end-start)


class RunMultiLayer(RunBehavior):
    def __init__(self, data):
        self.data = data

    def run(self):
        print("====="+self.data.mapsize+" Simulation Starting=====")
        output = False
        module = 'Fortran'

        #Deciding which booster to use 
        if module == 'Fortran':
            import Fmodules as modules

        elif module =='C':
            import Cmodules as modules
         
        elif module =='Python':
            import Pymodules as modules 

        Ts = self.data.Ts
        Te = self.data.Te
        tsc = self.data.tsc
        h = self.data.h
        kcfl = self.data.kcfl
        u = self.data.u      
 
        NS = max(self.data.NY,self.data.NX) #find max size 
        u0 = zeros(NS)         
        u1 = zeros(NS) 
        kap0 = zeros(NS)
        kaph = zeros(NS)

        yp = yppoints() #Interpolation points   

        # read the CC map lists and times=tepoch's for each frame: find first
        # Search for appropriate NPP maps:
        #      from start time Ts (in kya) to end time Te
        #loading carrying capacities from namelist
        yalist, masks, tepoch = get_cc_list(self.data.mapsize)

        #kstart = 1  
        kstart = 0
        while tepoch[kstart+1] >= Ts:
            kstart=kstart+1

        print('   Start time Ts=%5.1f (kya), NPP frame kstart=%d\n'%(Ts,kstart))

        kend = 61 #Leo: Verify this, it's probably 60
        while tepoch[kend-1] <= Te: 
            kend=kend-1

        print('   End time   Te=%5.1f (kya), NPP frame kend=%d\n'%(Te,kend))

        # make sure there are at least 2 maps between Ts and Te
        if ((kend-kstart)<1) or ((tepoch[kstart]-tepoch[kend])<(Ts-Te)):
           print('Ts=%e, Te=%e, not space enough: kstart=%d, kend=%d\n'%(Ts,Te,kstart,kend))

        #      start and end maps found
        kmp = kstart # start counting maps here

        #Using Jed's map
        #if self.data.mapsize =='big':
        #    import netCDF4
        #    data = netCDF4.Dataset('/Users/Leo/Desktop/Maps_Leo/fep_test.nc','r')
        #    w_J = flipud(data.variables['FEP'][:].data)
        #    w_J = roll(w_J,-310,axis=1)
        #    w_J[w_J<0]=0
        #    w_L = w_J.copy()
        #    w_H = w_J.copy()
        #    t_L = tepoch[kmp]
        #    kmp = kmp + 1                  # increment map count
        #    t_H = tepoch[kmp]
        #else:
        #Loading and generating masks 
        m_L = templateMap(self.data.mapsize, masks, kmp) # template for the 1st map, Leo:too small values
        t_L = tepoch[kmp] 
        m_H = templateMap(self.data.mapsize, masks, kmp+1) # template for the 2nd map, Leo:too small values 
        m_LH = (m_L==1)|(m_H==1)
        #m_LH[m_LH==False]=0
        #m_LH[m_LH==True]=1

        w_L = kapepoch(m_LH,yalist,kmp, self.data.mapsize) # this is the first map            
        kmp = kmp + 1                  # increment map count
        # fill in sub-threshold vals for w_L and w_H from m_LH template 
        w_L = fillins(w_L, m_L, m_LH)

        w_H = kapepoch(m_LH, yalist, kmp, self.data.mapsize)
        t_H = tepoch[kmp]

        # Scan through map, construct segments for X and Y directions 
        #Python scanners
        nxsegs, xstart_seg, xend_seg = Xscan(m_LH)
        nysegs, ystart_seg, yend_seg = Yscan(m_LH)
        #Fortran scanners
        #nxsegs, xstart_seg, xend_seg = FXscan(m_LH)
        #nysegs, ystart_seg, yend_seg = FYscan(m_LH)

        #w_H = kapepoch(self.data.w_map,yalist,kmp, self.data.mapsize) # second map
        #t_H = tepoch[kmp]

        #Computing interpolation weights 
        s   = (t_L-Ts+tsc*h)/(t_L-t_H)  # first step interpolation parameter
        wt0 = 0.0
        # compute interpoated weight to get to step t=h
        ks  = floor(100*s)
        sd  = 100*s - ks               # residual sd = 100*s mod 1
        #wth = yp[0]*(1-sd) + yp[1]*sd  # first interpolant
        wth = yp[ks]*(1-sd) + yp[ks+1]*sd  # first interpolant
        t   = Ts                       # this is real time in kya

        # TGL alternates between 1 and 0 
        TGL = True
        imID = 0
        #TGL = False

        # BEGIN main loop
        # self.NT = 1
 
        start = timer()
        for it in range(0,self.data.NT):
            #%debug 0
            #print('iteration=',it) 
            if TGL: #First choice: half-step1 in X, and step 2 in Y 
                locX = 0
                #half-step1 in X
                for row in range(0,self.data.NY):
                    nSegs = nxsegs[row] #Number of segments 
                    for k in range(0,nSegs):
                        colStart=xstart_seg[locX+k] #From: segment starts
                        colEnd=xend_seg[locX+k] #To:segmend ends
                        nInSeg=colEnd-colStart+1 #Number of inner segments
                        
                        u1[1:nInSeg+1] = u[row,colStart:colEnd+1]

                        #Dirichlet B.C.
                        u1[0] = 0 
                        u1[nInSeg+1] = 0

                        #Neumann B.C 
                        #u1[0] = u1[1]
                        #u1[nInSeg+1] = u1[nInSeg]

                        
                        #ut = godunovstep1(ninseg+2,h,kcfl,u1,sc1,sc2)
                        #Booster Subroutine
                        ut = modules.godunovstep1(h,kcfl,u1[0:nInSeg+2])
                        u[row,colStart:colEnd+1] = ut[1:nInSeg+1]

                    locX = locX + nSegs #Number of scanned segments
                #step2 in Y-Direction 
                locY = 0
                for col in range(0,self.data.NX):
                    nSegs = nysegs[col]
                    for k in range(0,nSegs):
                        rowStart=ystart_seg[locY+k]
                        rowEnd=yend_seg[locY+k]
                        nInSeg=rowEnd-rowStart+1

                        #print('u0:', u0[1:nInSeg+1].shape) # (2,), (26,), (2,))
                        #print('u:',u[rowStart:rowEnd+1,col].shape) # (0,), (0,), (0,))
                        u0[1:nInSeg+1] = u[rowStart:rowEnd+1,col]
                        kap0[1:nInSeg+1] = w_L[rowStart:rowEnd+1,col]*(1-wt0) + w_H[rowStart:rowEnd+1,col]*wt0
                        kaph[1:nInSeg+1] = w_L[rowStart:rowEnd+1,col]*(1-wth) + w_H[rowStart:rowEnd+1,col]*wth

                        kap0[0] = 1 #??
                        kap0[nInSeg+1] = 1 #??
                        kaph[0] = 1  #??
                        kaph[nInSeg+1] = 1 ##??

                        #Dirichlet B.C.
                        u0[0] = 0 
                        u0[nInSeg+1] = 0

                        #Neumann B.C 
                        #u0[0] = u0[1]
                        #u0[nInSeg+1] = u0[nInSeg]

                        #ut = godunovstep2(ninseg+2,h,kcfl,u0,sc1,sc2,kap0,kaph)
                        #Booster Subroutine
                        ut = modules.godunovstep2(h,kcfl,u0[0:nInSeg+2],kap0[0:nInSeg+2],kaph[0:nInSeg+2])

                        u[rowStart:rowEnd+1,col] = ut[1:nInSeg+1]

                    locY = locY + nSegs
                #half-step1 in X-Direction
                locX = 0
                for row in range(0,self.data.NY):
                    nSegs = nxsegs[row]
                    for k in range(0,nSegs):
                        colStart=xstart_seg[locX+k]
                        colEnd=xend_seg[locX+k]
                        nInSeg=colEnd-colStart+1
                        u1[1:nInSeg+1] = u[row,colStart:colEnd+1]

                        #Dirichlet b.c.
                        #u1[0] = 0
                        #u1[nInSeg+1] = 0

                        #Neumann B.C 
                        u1[0] = u1[1]
                        u1[nInSeg+1] = u1[nInSeg]

                        #ut = godunovstep1(ninseg+2,h,kcfl,u1,sc1,sc2)
                        #Booster Subroutine
                        ut = modules.godunovstep1(h,kcfl,u1[0:nInSeg+2])
                        u[row,colStart:colEnd+1] = ut[1:nInSeg+1]


                    locX = locX + nSegs
                TGL = True
                

            else:    
                ##Second choice: half-step1 in Y, and step 2 in X 
                locY = 0
                #half-step1 in Y
                for col in range(0,self.data.NX):
                    nSegs = nysegs[j] #Number of segments 
                    for k in range(0,nsegs):
                        rowStart=ystart_seg[locY+k] #From: segment starts
                        rowEnd=yend_seg[locY+k] #To:segmend ends
                        ninseg=rowEnd-rowStart+1

                        u0[1:nInSeg+1] = u[rowStart:rowEnd+1,col]

                        #Dirichlet b.c.
                        #u0[0] = 0
                        #u0[ninseg+1] = 0
                        #Neumann B.C 
                        u0[0] = u0[1]
                        u0[nInSeg+1] = u0[nInSeg]

                        #ut = godunovstep1(ninseg+2,h,kcfl,u0,sc1,sc2)
                        #Booster Subroutine
                        ut = modules.godunovstep1(h,kcfl,u0[0:nInSeg+2])
                        u[rowStart:rowEnd+1,col] = ut[1:nInSeg+1]

                    locY = locY + nSegs

                #Step 2 : X direction updates
                locx = 0
                for row in range(0,self.data.NY):
                    nsegs = nxsegs[i]
                    for k in range(0,nsegs):
                        colStart=xstart_seg[locx+k]
                        colEnd=xend_seg[locx+k]
                        nInSeg=colEnd-colStart+1

                        kap0[0] = 1 
                        kap0[nInSeg+1] = 1
                        kaph[0] = 1
                        kaph[nInSeg+1] = 1

                        u1[1:nInSeg+1] = u[row,colStart:colEnd+1]
                        kap0[1:nInSeg+1] = w_L[row,colStart:colEnd+1]*(1-wt0) + w_H[row,colStart:colEnd+1]*wt0
                        #No interpolation
                        #kap0[1:ninseg+1] = w_L[i,jstart:jend+1]
                        kaph[1:nInSeg+1] = w_L[row,colStart:colEnd+1]*(1-wth) + w_H[row,colStart:colEnd+1]*wth
                        #No interpolation
                        #kaph[1:ninseg+1] = w_L[i,jstart:jend+1]

                        #Dirichlet b.c.
                        u1[0] = 0 
                        u1[nInSeg+1] = 0
                        #Neumann B.C 
                        #u1[0] = u1[1]
                        #u1[nInSeg+1] = u1[nInSeg]


                        #ut = godunovstep2(ninseg+2,h,kcfl,u1,sc1,sc2,kap0,kaph)
                        #Fortran Subroutine
                        ut = modules.godunovstep2(h,kcfl,u1[0:nInSeg+2],kap0[0:nInSeg+2],kaph[0:nInSeg+2])
                        u[row,colStart:colEnd+1] = ut[1:nInSeg+1]


                    locX = locX + nSegs

                # half step1 in Y direction
                locy = 0
                for col in range(0,self.data.NX):
                    nsegs = nysegs[j]
                    for k in range(0,nsegs):
                        rowStart=ystart_seg[locY+k]
                        rowEnd=yend_seg[locY+k]
                        nInSeg=rowEnd-rowStart+1

                        u0[1:nInSeg+1] = u[rowStart:rowEnd+1,col]

                        #Dirichlet b.c.
                        u0[0] = 0
                        u0[nInSeg+1] = 0
                        #Neumann B.C 
                        #u0[0] = u0[1]
                        #u0[nInSeg+1] = u0[nInSeg]

                        #ut = godunovstep1(ninseg+2,h,kcfl,u0,sc1,sc2)
                        #Booster Subroutine
                        ut = modules.godunovstep1(h,kcfl,u0[0:nInSeg+2])                        
                        u[rowStart:rowEnd+1,col] = ut[1:nInSeg+1]


                    locY = locY + nSegs

                TGL = False
                
            #if kmp > kend: 
            #    print 'break hit, leaving loop'
            #    break

            # increment t value, reset weights and maps if necessary
            t = t - tsc*h          # decrement t in kya
            #fprintf(' ordinary step: t_L=%e, t=%e, t_H=%e\n',t_L,t,t_H);
            if t > t_H:              # usual case, maps not exhausted
                wt0 = wth           # shift lower weight down
                s   = (t_L-t)/(t_L-t_H)  # interpolation parameter
                ks  = floor(100*s)
                sd  = 100*s - ks    # residual sd = 100*s mod 1
                wth = yp[ks]*(1-sd) + yp[ks+1]*sd  # first interpolant
            else:                   # reached upper map time 
                kmp = kmp +1                      # increment map count
                if kmp <= kend:               # kend is the last map
                    m_L = m_H
                    w_L = w_H                 # set lower map to prev. upper
                    t_L = t_H                 # set lower time to prev. upper
                    #if self.data.mapsize =='big':
                    #    w_H = w_J.copy()
                    #else:
                    w_H = templateMap(self.data.mapsize, masks, kmp)
                    m_LH = (m_L==1) | (m_H==1)
                    nxsegs, xstart_seg, xend_seg = Xscan(m_LH)
                    nysegs, ystart_seg, yend_seg = Yscan(m_LH)
                    #nxsegs, xstart_seg, xend_seg = FXscan(m_LH)
                    #nysegs, ystart_seg, yend_seg = FYscan(m_LH)

                    w_L = fillins(w_L, m_L, m_LH)
                    #w_H = kapepoch(self.data.w_map, yalist, kmp, self.data.mapsize)  # read next map
                    w_H = kapepoch(m_LH, yalist, kmp, self.data.mapsize)
                    w_H = fillins(w_H, m_H, m_LH)
                    t_H = tepoch[kmp]         # time frame of new map
                    wt0 = 0
                    s   = (t_L - t)/(t_L-t_H)  # interpolation parameter
                    ks  = floor(100*s)
                    sd  = 100*s - ks                    # mod1 interpolation 
                    wth = yp[ks]*(1-sd) + yp[ks+1]*sd  # first interpolant

            ##Storing Data 
            if (mod(it,self.data.NPLT)==0)or(it==self.data.NT):
                if t < 10:
                    from  matplotlib import pyplot 
                    import matplotlib.cm as cm
                    tstr = '   %4.2f'%t
                    print(tstr)
                    if output:
                        u_plot = self.data.w_map-1+log(1+10*self.u)
                        fig = pyplot.figure(figsize=(30,20))
                        pyplot.subplot(111)
                        pyplot.title('Time='+tstr+'kY')
                        im = pyplot.imshow(u_plot, cmap = cm.RdBu, vmin= -1, vmax = 2)
                        #pyplot.clim(0,1)
                        cb = pyplot.colorbar()
                        #cb.set_clim(-1, 3.0)
                        fig.savefig(str(imID)+'-'+tstr+'.png',dpi = 100)
                        pyplot.close()
                        imID= imID+1

                else:
                    from  matplotlib import pyplot 
                    import matplotlib.cm as cm
                    tstr = '   %4.1f'%t
                    print(tstr)
                    if output:
                        u_plot = self.data.w_map-1+log(1+10*self.u)
                        fig = pyplot.figure(figsize=(30,20))
                        pyplot.subplot(111)
                        pyplot.title('Time='+tstr+'kY')
                        im = pyplot.imshow(u_plot, cmap = cm.RdBu, vmin= -1, vmax = 2)
                        #pyplot.clim(0,1)
                        cb = pyplot.colorbar()
                        #cb.set_clim(-1, 3.0)
                        fig.savefig(str(imID)+'-'+tstr+'.png',dpi = 100)
                        pyplot.close()
                        imID = imID+1

        end = timer()
        print("====="+self.data.mapsize+" simulation completed=====")
        print('   Main Loop Time:', end-start)

class RunHDF5(RunBehavior):
    def __init__(self, data):
        self.data = data

    def run(self):
        print("====="+self.data.mapsize+" Simulation Starting=====")
        output = False
        module = 'C'

        #Deciding which booster to use 
        if module == 'Fortran':
            import Fmodules as modules

        elif module =='C':
            import Cmodules as modules
         
        elif module =='Python':
            import Pymodules as modules 

        Ts = self.data.Ts
        Te = self.data.Te
        tsc = self.data.tsc
        h = self.data.h
        kcfl = self.data.kcfl
        u = self.data.u      
 
        NS = max(self.data.NY,self.data.NX) #find max size 
        u0 = zeros(NS)         
        u1 = zeros(NS) 
        kap0 = zeros(NS)
        kaph = zeros(NS)

        yp = yppoints() #Interpolation points   

        # read the CC map lists and times=tepoch's for each frame: find first
        # Search for appropriate NPP maps:
        #      from start time Ts (in kya) to end time Te
        #loading carrying capacities from namelist
        yalist, masks, tepoch = get_cc_list(self.data.mapsize)

        #kstart = 1  
        kstart = 0
        while tepoch[kstart+1] >= Ts:
            kstart=kstart+1

        print('   Start time Ts=%5.1f (kya), NPP frame kstart=%d\n'%(Ts,kstart) )

        kend = 61 #Leo: Verify this, it's probably 60
        while tepoch[kend-1] <= Te: 
            kend=kend-1

        print('   End time   Te=%5.1f (kya), NPP frame kend=%d\n'%(Te,kend))

        # make sure there are at least 2 maps between Ts and Te
        if ((kend-kstart)<1) or ((tepoch[kstart]-tepoch[kend])<(Ts-Te)):
           print('Ts=%e, Te=%e, not space enough: kstart=%d, kend=%d\n'%(Ts,Te,kstart,kend))

        #      start and end maps found
        kmp = kstart # start counting maps here

        #Using Jed's map
        #if self.data.mapsize =='big':
        #    import netCDF4
        #    data = netCDF4.Dataset('/Users/Leo/Desktop/Maps_Leo/fep_test.nc','r')
        #    w_J = flipud(data.variables['FEP'][:].data)
        #    w_J = roll(w_J,-310,axis=1)
        #    w_J[w_J<0]=0
        #    w_L = w_J.copy()
        #    w_H = w_J.copy()
        #    t_L = tepoch[kmp]
        #    kmp = kmp + 1                  # increment map count
        #    t_H = tepoch[kmp]
        #else:
        #Loading and generating masks
        #Loading precomputed masks and carrying capacities
        m_LH = readTemplateMap(self.data.mapsize, masks, kmp)
        w_L, w_H = readCC(yalist, kmp, self.data.mapsize)
 
        #m_L = templateMap(self.data.mapsize, masks, kmp) # template for the 1st map, Leo:too small values
        t_L = tepoch[kmp] 
        #m_H = templateMap(self.data.mapsize, masks, kmp+1) # template for the 2nd map, Leo:too small values 
        #m_LH = (m_L==1)|(m_H==1)
        #m_LH[m_LH==False]=0
        #m_LH[m_LH==True]=1

        #w_L = kapepoch(m_LH,yalist,kmp, self.data.mapsize) # this is the first map            
        kmp = kmp + 1                  # increment map count
        # fill in sub-threshold vals for w_L and w_H from m_LH template 
        #w_L = fillins(w_L, m_L, m_LH)

        #w_H = kapepoch(m_LH, yalist, kmp, self.data.mapsize)
        t_H = tepoch[kmp]

        # Scan through map, construct segments for X and Y directions 
        #Python scanners
        nxsegs, xstart_seg, xend_seg = Xscan(m_LH)
        nysegs, ystart_seg, yend_seg = Yscan(m_LH)
        #Fortran scanners
        #nxsegs, xstart_seg, xend_seg = FXscan(m_LH)
        #nysegs, ystart_seg, yend_seg = FYscan(m_LH)

        #w_H = kapepoch(self.data.w_map,yalist,kmp, self.data.mapsize) # second map
        #t_H = tepoch[kmp]

        #Computing interpolation weights 
        s   = (t_L-Ts+tsc*h)/(t_L-t_H)  # first step interpolation parameter
        wt0 = 0.0
        # compute interpoated weight to get to step t=h
        ks  = floor(100*s)
        sd  = 100*s - ks               # residual sd = 100*s mod 1
        #wth = yp[0]*(1-sd) + yp[1]*sd  # first interpolant
        wth = yp[ks]*(1-sd) + yp[ks+1]*sd  # first interpolant
        t   = Ts                       # this is real time in kya

        # TGL alternates between 1 and 0 
        TGL = True
        imID = 0
        #TGL = False

        # BEGIN main loop
        # self.NT = 1
 
        start = timer()
        for it in range(0,self.data.NT):
            #%debug 0
            #print 'iteration=',it 
            if TGL: #First choice: half-step1 in X, and step 2 in Y 
                locX = 0
                #half-step1 in X
                for row in range(0,self.data.NY):
                    nSegs = nxsegs[row] #Number of segments 
                    for k in range(0,nSegs):
                        colStart=xstart_seg[locX+k] #From: segment starts
                        colEnd=xend_seg[locX+k] #To:segmend ends
                        nInSeg=colEnd-colStart+1 #Number of inner segments
                        
                        u1[1:nInSeg+1] = u[row,colStart:colEnd+1]

                        #Dirichlet B.C.
                        u1[0] = 0 
                        u1[nInSeg+1] = 0

                        #Neumann B.C 
                        #u1[0] = u1[1]
                        #u1[nInSeg+1] = u1[nInSeg]

                        
                        #ut = godunovstep1(ninseg+2,h,kcfl,u1,sc1,sc2)
                        #Booster Subroutine
                        ut = modules.godunovstep1(h,kcfl,u1[0:nInSeg+2])
                        u[row,colStart:colEnd+1] = ut[1:nInSeg+1]

                    locX = locX + nSegs #Number of scanned segments
                #step2 in Y-Direction 
                locY = 0
                for col in range(0,self.data.NX):
                    nSegs = nysegs[col]
                    for k in range(0,nSegs):
                        rowStart=ystart_seg[locY+k]
                        rowEnd=yend_seg[locY+k]
                        nInSeg=rowEnd-rowStart+1

                        #print 'u0:', u0[1:nInSeg+1].shape # (2,), (26,), (2,)
                        #print 'u:',u[rowStart:rowEnd+1,col].shape # (0,), (0,), (0,)
                        u0[1:nInSeg+1] = u[rowStart:rowEnd+1,col]
                        kap0[1:nInSeg+1] = w_L[rowStart:rowEnd+1,col]*(1-wt0) + w_H[rowStart:rowEnd+1,col]*wt0
                        kaph[1:nInSeg+1] = w_L[rowStart:rowEnd+1,col]*(1-wth) + w_H[rowStart:rowEnd+1,col]*wth

                        kap0[0] = 1 #??
                        kap0[nInSeg+1] = 1 #??
                        kaph[0] = 1  #??
                        kaph[nInSeg+1] = 1 ##??

                        #Dirichlet B.C.
                        u0[0] = 0 
                        u0[nInSeg+1] = 0

                        #Neumann B.C 
                        #u0[0] = u0[1]
                        #u0[nInSeg+1] = u0[nInSeg]

                        #ut = godunovstep2(ninseg+2,h,kcfl,u0,sc1,sc2,kap0,kaph)
                        #Booster Subroutine
                        ut = modules.godunovstep2(h,kcfl,u0[0:nInSeg+2],kap0[0:nInSeg+2],kaph[0:nInSeg+2])

                        u[rowStart:rowEnd+1,col] = ut[1:nInSeg+1]

                    locY = locY + nSegs
                #half-step1 in X-Direction
                locX = 0
                for row in range(0,self.data.NY):
                    nSegs = nxsegs[row]
                    for k in range(0,nSegs):
                        colStart=xstart_seg[locX+k]
                        colEnd=xend_seg[locX+k]
                        nInSeg=colEnd-colStart+1
                        u1[1:nInSeg+1] = u[row,colStart:colEnd+1]

                        #Dirichlet b.c.
                        #u1[0] = 0
                        #u1[nInSeg+1] = 0

                        #Neumann B.C 
                        u1[0] = u1[1]
                        u1[nInSeg+1] = u1[nInSeg]

                        #ut = godunovstep1(ninseg+2,h,kcfl,u1,sc1,sc2)
                        #Booster Subroutine
                        ut = modules.godunovstep1(h,kcfl,u1[0:nInSeg+2])
                        u[row,colStart:colEnd+1] = ut[1:nInSeg+1]


                    locX = locX + nSegs
                TGL = True
                

            else:    
                ##Second choice: half-step1 in Y, and step 2 in X 
                locY = 0
                #half-step1 in Y
                for col in range(0,self.data.NX):
                    nSegs = nysegs[j] #Number of segments 
                    for k in range(0,nsegs):
                        rowStart=ystart_seg[locY+k] #From: segment starts
                        rowEnd=yend_seg[locY+k] #To:segmend ends
                        ninseg=rowEnd-rowStart+1

                        u0[1:nInSeg+1] = u[rowStart:rowEnd+1,col]

                        #Dirichlet b.c.
                        #u0[0] = 0
                        #u0[ninseg+1] = 0
                        #Neumann B.C 
                        u0[0] = u0[1]
                        u0[nInSeg+1] = u0[nInSeg]

                        #ut = godunovstep1(ninseg+2,h,kcfl,u0,sc1,sc2)
                        #Booster Subroutine
                        ut = modules.godunovstep1(h,kcfl,u0[0:nInSeg+2])
                        u[rowStart:rowEnd+1,col] = ut[1:nInSeg+1]

                    locY = locY + nSegs

                #Step 2 : X direction updates
                locx = 0
                for row in range(0,self.data.NY):
                    nsegs = nxsegs[i]
                    for k in range(0,nsegs):
                        colStart=xstart_seg[locx+k]
                        colEnd=xend_seg[locx+k]
                        nInSeg=colEnd-colStart+1

                        kap0[0] = 1 
                        kap0[nInSeg+1] = 1
                        kaph[0] = 1
                        kaph[nInSeg+1] = 1

                        u1[1:nInSeg+1] = u[row,colStart:colEnd+1]
                        kap0[1:nInSeg+1] = w_L[row,colStart:colEnd+1]*(1-wt0) + w_H[row,colStart:colEnd+1]*wt0
                        #No interpolation
                        #kap0[1:ninseg+1] = w_L[i,jstart:jend+1]
                        kaph[1:nInSeg+1] = w_L[row,colStart:colEnd+1]*(1-wth) + w_H[row,colStart:colEnd+1]*wth
                        #No interpolation
                        #kaph[1:ninseg+1] = w_L[i,jstart:jend+1]

                        #Dirichlet b.c.
                        u1[0] = 0 
                        u1[nInSeg+1] = 0
                        #Neumann B.C 
                        #u1[0] = u1[1]
                        #u1[nInSeg+1] = u1[nInSeg]


                        #ut = godunovstep2(ninseg+2,h,kcfl,u1,sc1,sc2,kap0,kaph)
                        #Fortran Subroutine
                        ut = modules.godunovstep2(h,kcfl,u1[0:nInSeg+2],kap0[0:nInSeg+2],kaph[0:nInSeg+2])
                        u[row,colStart:colEnd+1] = ut[1:nInSeg+1]


                    locX = locX + nSegs

                # half step1 in Y direction
                locy = 0
                for col in range(0,self.data.NX):
                    nsegs = nysegs[j]
                    for k in range(0,nsegs):
                        rowStart=ystart_seg[locY+k]
                        rowEnd=yend_seg[locY+k]
                        nInSeg=rowEnd-rowStart+1

                        u0[1:nInSeg+1] = u[rowStart:rowEnd+1,col]

                        #Dirichlet b.c.
                        u0[0] = 0
                        u0[nInSeg+1] = 0
                        #Neumann B.C 
                        #u0[0] = u0[1]
                        #u0[nInSeg+1] = u0[nInSeg]

                        #ut = godunovstep1(ninseg+2,h,kcfl,u0,sc1,sc2)
                        #Booster Subroutine
                        ut = modules.godunovstep1(h,kcfl,u0[0:nInSeg+2])                        
                        u[rowStart:rowEnd+1,col] = ut[1:nInSeg+1]


                    locY = locY + nSegs

                TGL = False
                
            #if kmp > kend: 
            #    print 'break hit, leaving loop'
            #    break

            # increment t value, reset weights and maps if necessary
            t = t - tsc*h          # decrement t in kya
            #fprintf(' ordinary step: t_L=%e, t=%e, t_H=%e\n',t_L,t,t_H);
            if t > t_H:              # usual case, maps not exhausted
                wt0 = wth           # shift lower weight down
                s   = (t_L-t)/(t_L-t_H)  # interpolation parameter
                ks  = floor(100*s)
                sd  = 100*s - ks    # residual sd = 100*s mod 1
                wth = yp[ks]*(1-sd) + yp[ks+1]*sd  # first interpolant
            else:                   # reached upper map time 
                kmp = kmp +1                      # increment map count
                #if kmp <= kend:               # kend is the last map
                if kmp < kend:               # kend is the last map
                    #m_L = m_H
                    #w_L = w_H                 # set lower map to prev. upper
                    t_L = t_H                 # set lower time to prev. upper
                    #if self.data.mapsize =='big':
                    #    w_H = w_J.copy()
                    #else:
                    #w_H = templateMap(self.data.mapsize, masks, kmp)
                    #m_LH = (m_L==1) | (m_H==1)
                    m_LH = readTemplateMap(self.data.mapsize, masks, kmp)
                    w_L, w_H = readCC(yalist, kmp, self.data.mapsize)

                    nxsegs, xstart_seg, xend_seg = Xscan(m_LH)
                    nysegs, ystart_seg, yend_seg = Yscan(m_LH)
                    #nxsegs, xstart_seg, xend_seg = FXscan(m_LH)
                    #nysegs, ystart_seg, yend_seg = FYscan(m_LH)

                    #w_L = fillins(w_L, m_L, m_LH)
                    #w_H = kapepoch(self.data.w_map, yalist, kmp, self.data.mapsize)  # read next map
                    #w_H = kapepoch(m_LH, yalist, kmp, self.data.mapsize)
                    #w_H = fillins(w_H, m_H, m_LH)
                    t_H = tepoch[kmp]         # time frame of new map
                    wt0 = 0
                    s   = (t_L - t)/(t_L-t_H)  # interpolation parameter
                    ks  = floor(100*s)
                    sd  = 100*s - ks                    # mod1 interpolation 
                    wth = yp[ks]*(1-sd) + yp[ks+1]*sd  # first interpolant

            ##Storing Data 
            if (mod(it,self.data.NPLT)==0)or(it==self.data.NT):
                if t < 10:
                    from  matplotlib import pyplot 
                    import matplotlib.cm as cm
                    tstr = '   %4.2f'%t
                    print(tstr)
                    if output:
                        u_plot = self.data.w_map-1+log(1+10*self.u)
                        fig = pyplot.figure(figsize=(30,20))
                        pyplot.subplot(111)
                        pyplot.title('Time='+tstr+'kY')
                        im = pyplot.imshow(u_plot, cmap = cm.RdBu, vmin= -1, vmax = 2)
                        #pyplot.clim(0,1)
                        cb = pyplot.colorbar()
                        #cb.set_clim(-1, 3.0)
                        fig.savefig(str(imID)+'-'+tstr+'.png',dpi = 100)
                        pyplot.close()
                        imID= imID+1

                else:
                    from  matplotlib import pyplot 
                    import matplotlib.cm as cm
                    tstr = '   %4.1f'%t
                    print(tstr)
                    if output:
                        u_plot = self.data.w_map-1+log(1+10*self.u)
                        fig = pyplot.figure(figsize=(30,20))
                        pyplot.subplot(111)
                        pyplot.title('Time='+tstr+'kY')
                        im = pyplot.imshow(u_plot, cmap = cm.RdBu, vmin= -1, vmax = 2)
                        #pyplot.clim(0,1)
                        cb = pyplot.colorbar()
                        #cb.set_clim(-1, 3.0)
                        fig.savefig(str(imID)+'-'+tstr+'.png',dpi = 100)
                        pyplot.close()
                        imID = imID+1

        end = timer()
        print("====="+self.data.mapsize+" simulation completed=====")
        print('   Main Loop Time:', end-start)

################################################################################
# Plot behavior interface and behavior implementation classes.
################################################################################

class PlotBehavior:
    __metaclass__ = ABCMeta
    @abstractmethod 
    def plot(self):
        pass

class Plot(PlotBehavior):
    def __init__(self, data):
        self.data = data        
    def plot(self):
        print("=====Plotting Results=====")
        from  matplotlib import pyplot 
        import matplotlib.cm as cm
        fig = pyplot.figure(figsize=(30,20))
        #pyplot.subplot(211)
        #im = pyplot.imshow(self.w_map, cmap = cm.RdBu)
        #cb = pyplot.colorbar()
        #cb.set_clim(0.0,1.0)

        #pyplot.subplot(212)
        #im = pyplot.imshow(self.w_map2, cmap = cm.RdBu)
        #cb = pyplot.colorbar()
        #cb.set_clim(0.0,1.0)

        u_plot = self.data.w_map-1+log(1+10*self.data.u)
        fig = pyplot.figure(figsize=(30,20))
        pyplot.subplot(111)
        pyplot.title('Title')
        im = pyplot.imshow(u_plot, cmap = cm.RdBu_r)
        cb = pyplot.colorbar()


################################################################################
# Test Code.
################################################################################

if __name__ == '__main__':
    smallSimulation = SmallSimulation()
    largeSimulation = LargeSimulation()
    hugeSimulation = HugeSimulation()
    #bigSimulation = BigSimulation()

    smallSimulation.run()
    largeSimulation.run()
    #bigSimulation.run()
    hugeSimulation.run()


    smallSimulation.plot()
    largeSimulation.plot()
    #bigSimulation.plot()
    hugeSimulation.plot()


