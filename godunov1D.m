function fisher
%
% finite difference scheme for 1-D Fisher/KPP eq. 
% wpp 27/05/2012
%
NX = 401*4; NT = 101; NTP = 21;
a  = -40.0; b  = 40.0;
t0 = 0.0;   t1 = 20.0;
xm = linspace(a,b,NX);
ts = linspace(t0,t1,NT);
gs = zeros(NX,NT);
% scratch space, F & M's m (=s1),u (=s2) arrays
s1 = zeros(NX,1); s2 = zeros(NX,1);
m  = 0;
tstart=tic;
pd = pdepe(m,@hsfn,@hsic,@hsbc,xm,ts);
tend=toc(tstart);
fprintf(' 1Dpdepe time = %e\n',tend);
u0 = (hsic(xm))'; u = u0; h = ts(2)-ts(1); dx = xm(2)-xm(1);
kc = 0.5*h/(dx*dx); 
tstart=tic;
for i=1:NT
   gs(:,i) = godunovstep(NX,h,kc,u,s1,s2); % one step of Godunov
   u = gs(:,i);
end
tend=toc(tstart);
fprintf(' godunov time = %e\n',tend);
pt = pd(:,:,1); nxm = (NX-1)/2+1;
pltw = zeros(nxm,NTP);
pltg = zeros(nxm,NTP);
kk = 1; ntstep = (NT-1)/(NTP-1); 
for k=1:NTP
  pltw(:,k) = pt(kk,nxm:NX);
  pltg(:,k) = gs(nxm:NX,kk);
  t(k) = ts(kk); kk=kk+ntstep;
end
xp = xm(nxm:NX);
% now compute characteristic 
% use point where u crosses 1/2 of maximum
for k=1:NTP
  tp(k) = t(k);
  cross = 1000.0; 
  half = 0.5*pltw(1,k);
  for j=1:nxm
    crosstst = abs(pltw(j,k)-half);
    if abs(crosstst)<cross
       cross = abs(crosstst); xC = xp(j); jC=j;
    end
  end
% fprintf(' k=%d, jC=%d, xC=%e, cross=%e\n',k,jC,xC,cross);
  xhalf(k) = xC; 
end
for k=1:NTP
   figure;
   plot(xp,pltw(:,k),'k-',xp,pltg(:,k),'r--','Linewidth',2);
   set(gca,'Fontsize',16);
   legend('pdepe','godunov');
   ylim([0 1.01]); 
   tstr = num2str(t(k));
   title([' wave front at t=',tstr],'Fontsize',16);
   xlabel('x','Fontsize',16);
   ylabel('u','Fontsize',16);
end
figure;
mesh(xm,ts,pt);
xlabel('x','Fontsize',16);
ylabel('t','Fontsize',16);
zlabel('u(t,x)','Fontsize',16);
% plot characteristic
figure;
plot(tp,xhalf); 
title('x_{1/2} vs t','Fontsize',16);
xlabel('t','Fontsize',16);
ylabel('x_{1/2}(t)','Fontsize',16);

function [c,f,s] = hsfn(x,t,u,ux)
c = 1;
f = ux/2;
s = u*(1-u);

function u0 = hsic(x)
[m,n]=size(x);
for i=1:n
%  u0(i) = exp(-x(i)*x(i));
   if abs(x(i)) < 1.0 
      u0(i) = 1;
   else
      u0(i) = 0;
   end
end

function [pa,qa,pb,qb] = hsbc(xa,ua,xb,ub,t)
pa = ua; qa = 0;
pb = ub; qb = 0;

function v = godunovstep(n,h,k,u,s1,s2);
% first compute Euler estimate
   ut = (Amult(n,u))'; 
   ut = k*ut;
   uE = ut + h*(1-u).*u;         % commmon part of Euler est.
% compute right hand side
   ur = u + 0.5*uE;              % RHS
   uE = u + uE;                  % Euler est.
   dA(1) = 1; dA(n) = 1;            % end points
% interior diags. of LHS matrix:
   dA(2:n-1) = 1 + k - 0.5*h*(1-uE(2:n-1));
% solve using Forsythe-Moler
   v  = trisol(n,k,dA,ur,s1,s2);

function x = trisol(n,k,d,b,s1,s2)
%  Tridiagonal system solver modeled on
%  G.E. Forsythe and C.B. Moler " Computer 
%  solutions to linear algebraic systems,"
%  Prentice-Hall publ., 1967
%  Tridiagonal system is Tx=b, where
%
%  T = [1  -k/2   0    ....          0 ]
%      [-k/2 d(2) -k/2 ....          0 ]
%      [0    -k/2 d(3) -k/2  0 ....  0 ]
%      [0           ....             0 ]
%      [0              -k/2 d(n-1) -k/2]
%      [0                0    -k/2   1 ]
% 
% and k = h/dx^2 is the CFL factor. Diagonals
% d = (1 d(2) d(3) ...  d(n-1) 1) are strong.
% s1 and s2 are work arrays.(Leo: s2 new diagonal, s1 new off-diagonal)  
% In the original F & M notation,
%
%  T = [d(1)  f(1)  0        ...             0 ]
%      [e(2)  d(2)  f(2)  0  ...             0 ]
%      [ 0          ....                     0 ]
%      [ 0                e(n-1) d(n-1) f(n-1) ]
%      [ 0                  0    e(n)   d(n)   ]
%
%  wpp 27 May 2012

% constant: mk2 (minus k divided by 2)
   mk2   = -k/2;
   s2(1) = 1;
   s2(2) = d(2);
   s1(2) = mk2;
% Leo: Forward elimination
   for i=3:n-1
      s1(i) = mk2/s2(i-1);
      s2(i) = d(i) - mk2*s1(i);
   end
   s1(n) = 0; s2(n) = 1;
% 
   x(1) = b(1);
   for i=2:n
      x(i) = b(i) - s1(i)*x(i-1);
   end
% Leo: backward substitution 
   x(n) = x(n)/s2(n); ii = n-1;
   for i=1:n-2
       x(ii) = (x(ii) - mk2*x(ii+1))/s2(ii);
       ii    = ii - 1;
   end
   x(1) = x(1)/s2(1);

function v = Amult(n,u)
% does tridiagonal matrix multiply, returns n elements of v
% with v(1) = v(n) = 0, the others are u(i-1)+u(i+1)-2*u(i)
   v(1) = 0; v(n) = 0;
   v(2:n-1) = u(1:n-2)+u(3:n)-2*u(2:n-1);
