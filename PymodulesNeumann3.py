from numpy import *
import scipy.signal

#Functions for Neumann boundary conditions

def amult3Neumann(u):
    # d^2u/dx^2=1/(dx**2)*Au
    # it performs tridiagonal matrix multiply, returns n elements of v
    # with v(1) = v(n) = , the others are u(i-1)+u(i+1)-2*u(i)
    #   v(1) = 0; v(n) = ;
    #   v(2:n-1) = u(1:n-2)+u(3:n)-2*u(2:n-1);

    #Wes
    #v = zeros(n)
    #Wes:Dirichlet
    #v[0] = 0; v[-1] = 0;
    #Wes:Neumann
    #v[0] = -2*u[0]+ u[1]
    #v[-1] = -2*u[-1]+u[-2]

    #Leo: same as wesley
    n = size(u)
    v = zeros(n)
    v[0] = - 2. * u[0] + u[1]
    v[-1] = u[n-2] - 2. * u[n-1]
    #v[0] = -dx*u[0]/2+dx*u[1]/2 # Sigma =0
    #v[-1] =  -dx*u[-2]/2+dx*u[-1]/2 #Sigma=0 
    #v[0] = -u[0]/(2*dx)+u[1]/(2*dx) #Sigma=0
    #v[-1] =  -u[-2]/(2*dx)+u[-1]/(2*dx) #Sigma=0 
    
    v[1:-1] = u[0:-2]-2*u[1:-1]+u[2:]
    return v

def trisol3Neumann(k,d,b):
    #[1 -4/3 .....               ]
    #[-k/2 d1 -k2....            ]
    #[          ...-k/2 dn-2 -k/2]
    # constant: mk2 (minus k divided by 2)
    #   mk2   = -k/2;
    #   s2(1) = 1;
    #   s2(2) = d(2);
    #   s1(2) = mk2;
    n = size(d)
    x = zeros (n)
    s1 = zeros(n)
    s2 = zeros(n)
    mk2 = -k/2

    
    #Neumann
    s2[0] = d[0] #s2_i=d_i-(c_i)*s1[i] = 1 
    s1[1] = mk2/s2[0] # c_i/s2[i-1] 
    s2[1] = d[1]+4.*s1[1]/3. #d_1-(a1*s1_1)
    #s1[2] = mk2/s2[0]
    #s2[1] = d[1]+4.*s1[2]/3.

    #print('S2',0,s2[0])
    #print('S1',1,s1[1])
    #print('S2',1,s2[1])
    #s2[0] = -1/(2*dx)



    # Leo: Forward elimination
    #for i=3:n-1
    #  s1(i) = mk2/s2(i-1);
    #  s2(i) = d(i) - mk2*s1(i);
    #end
    for i in arange(2,n-1):
        s1[i] = mk2/s2[i-1]; #a[i]/s2[i-1]
        s2[i] = d[i] - mk2*s1[i] #d[i]-c[i]*s1[i]
        #print('S1',i,s1[i])
        #print('S2',i,s1[i])
    
    #Neumann
    #s1(n) = 0; s2(n) = 1;
    s1[n-1] = -4./(3*s2[n-2]) #a[n-1]/s2[n-1-1]
    s2[n-1] = d[n-1]-mk2*s1[n-1]; #d[n-1] - c[n-1]*s1[n-1]
    
    #x(1) = b(1);
    #for i=2:n
    #    x(i) = b(i) - s1(i)*x(i-1);
    #end
    x[0]=b[0] #x[0] = b[0]
    for i in arange(1,n):
        x[i] = b[i] - s1[i]*x[i-1] #x[i] = b[i] - s1[i] *x[i-1]

    # Leo: backward substitution 
    #x(n) = x(n)/s2(n); ii = n-1;
    #for i=1:n-2
    #   x(ii) = (x(ii) - mk2*x(ii+1))/s2(ii);
    #   ii    = ii - 1;
    #end
    x[n-1] = x[n-1]/s2[n-1] #x[n-1] / s2[n-1]
    ii = n-2
    for i in arange(0,n-2):
        x[ii] = (x[ii]-mk2*x[ii+1])/s2[ii]; #x[i]=(x[i]-c[i]*x[i+1])/s2[i]
        ii = ii - 1;
    
    #x(1) = x(1)/s2(1);
    #x[0] = x[0]/s2[0]; #s2[0]=1
    return x;

def godunovstepNeumann3rd(h,k,u):
    # first compute Euler estimate
    #ut = (Amult(n,u))'
    #ut = k*ut
    #uE = ut + h*(1-u).*u # commmon part of Euler est.
    n = size(u)
    ut = amult3Neumann(u)
    ut = k*ut
    uE = ut + h*(1-u)*u
    
    # compute right hand side
    #ur = u + 0.5*uE #RHS
    #uE = u + uE #Euler est.
    #dA(1) = 1; dA(n) = 1 #end points
    ur = u + 0.5*uE #RHS
    uE = u + uE #Euler estimate
    #print('uE', uE)
    
    #Leo: Code from Wesley 3rd approach 
    # impose Neumann BCs on Euler estimate
    uE[0] = 4.*uE[1]/3. - uE[2]/3.
    uE[-1] = 4.*uE[-2]/3. - uE[-3]/3.
    #print('uE[0]',uE[0])
    #print('uE[-1]',uE[-1])
    
    #reset end points for explicit estimates for BCs
    #ur(1) = ur(1) - k*uE(1) + 0.5*k*uE(2) + 0.5*h*uE(1)*(1-uE(1)); 
    #Estimate using trapezoidal rule for u[2] eq.5 
    #u[2] + 1/2(A*uE[2]+ A*u[2]) + 1/2*(f(uE[2])+u[2])
    uest = u[2]+0.5*k*(uE[1]-2*uE[2]+uE[3]) \
            +0.5*k*(u[1]-2*u[2]+u[3]) \
            +0.5*h*((1-uE[2])*uE[2]+(1-u[2])*u[2]) #error here 
    ur[0] = -uest/3
    
    #print(ur[0])
    
    uest = u[n-3]+0.5*k*(uE[n-4]-2*uE[n-3]+uE[n-2]) \
            +0.5*k*(u[n-4]-2*u[n-3]+u[n-2]) \
            +0.5*h*((1-uE[n-3])*uE[n-3]+(1-u[n-3])*u[n-3]) #Leo: error here be careful
    
    ur[n-1] = -uest/3.
    
    #print(ur[n-1])
    
    #ur[0] = ur[0] + k*uE[0] - 0.5*k*uE[1] + 0.5*h*uE[0]*(1-uE[0]); 
    #ur(n) = ur(n) - k*uE(n) + 0.5*k*uE(n-1) + 0.5*h*uE(n)*(1-uE(n)); 
    #ur[-1] = ur[-1] + k*uE[-1] - 0.5*k*uE[-2] + 0.5*h*uE[-1]*(1-uE[-1]); 

    #Leo: Impose second aproach
    #ur[-1] = u [1] 
    
    dA = zeros(n)
    dA[0] = 1 
    dA[n-1] = 1 #end points

    # interior diags. of LHS matrix:
    #dA(2:n-1) = 1 + k - 0.5*h*(1-uE(2:n-1));
    #dA[1:n-2] = 1 + k - 0.5*h*(1-uE[1:n-2])
    dA[1:n-1] = 1 + k - 0.5*h*(1-uE[1:n-1]) #normal stuff for inner values

    #print('dA:',dA)
   
    
    # solve using Forsythe-Moler
    #v  = trisol(n,k,dA,ur,s1,s2);
    v = trisol3Neumann(k,dA,ur)
    #v = modules.trisol(k,dA,ur)
    #v = Cmodules.trisol(k,dA,ur)
    #reset v[0] and v[n-1]
    #Imposed b.c. on the output 
    v[0]=4.*v[1]/3.-v[2]/3.
    v[n-1]=4.*v[n-2]/3.-v[n-3]/3.
    return v

def amultNeumann(u): 
    n = size(u)
    v = zeros(n)
    v[0] = - 2. * u[0]+ u[1]  
    v[-1] = u[n-2] - 2. * u[n-1]
    v[1:-1] = u[0:-2] - 2 * u[1:-1] + u[2:n]
    return v

def trisol0Neumann(k,b):
#  Tridiagonal system solver modeled on
#  G.E. Forsythe and C.B. Moler " Computer 
#  solutions to linear algebraic systems,"
#  Prentice-Hall publ., 1967
#  Tridiagonal system is Tx=b, where
#
#  T = [1        -4/3   0    ....     0 ]
#      [-k/4 1+k/2 -k/4 ....          0 ]
#      [0    -k/4 1+k/2 -k/4  0 ....  0 ]
#      [0           ....              0 ]
#      [0               -k/4 1+k/2  -k/4]
#      [0                 0    -4/3   1 ]
# 
# and k = h/dx^2 is the CFL factor. Diagonals
# d = (1 d(2) d(3) ...  d(n-1) 1) are strong.
# s1 and s2 are work arrays.(Leo: s2 new diagonal, s1 new off-diagonal)  
# In the original F & M notation,
#
#  T = [d(1)  f(1)  0        ...             0 ]
#      [e(2)  d(2)  f(2)  0  ...             0 ]
#      [ 0          ....                     0 ]
#      [ 0                e(n-1) d(n-1) f(n-1) ]
#      [ 0                  0    e(n)   d(n)   ]
#
#  wpp 27 May 2012
# def trisol(n,k,d,b,s1,s2):
    #First choice
    #a = -3/2h
    #b = 2/h
    #c = -1/2h
    #mca = -c/a
    mca = -1./3.
    #ba = b/a 
    ba = -4./3.
    #mba = -b/a
    mba = 4./3.
    """
    #Second Choice
    #a = -1/h
    #b = 3/2h
    #c = -1/2h
    #mca = -c/a
    mca = -1./2.
    #ba = b/a 
    ba = -3./2.
    #mba = -b/a
    mba = 3./2.
    """

    n = size(b)
    s1 = zeros(n)
    s2 = zeros(n)
    x = zeros (n)
    #Neumann boundary conditions
    mk4 = -k/4
    k2 = k/2
    s2[0] = 1. #s2_i=d_i-(c_i)*s1[i] = 1 
    s1[1] = mk4  # c_i/s2[i-1] = (-k/4)/1 
    s2[1] = 1. + k2 + mba*s1[1] #d[1]-(a1*s1_1) = d[1]-(-4/3 * s1_1)
 
    # Leo: Forward elimination
    for i in arange(2,n-1):
        s1[i] = mk4 / s2[i-1] #a[i]/s2[i-1]
        s2[i] = 1 + k2 - mk4 * s1[i] #d[i]-c[i]*s1[i]


    s1[n-1] = ba / s2[n-2]      #a[n-1]/s2[n-1-1]
    s2[n-1] = 1. - mk4 * s1[n-1] #d[n-1] - c[n-1]*s1[n-1]

    
    x[0]=b[0] #x[0] = b[0]

    for i in arange(1,n):
        x[i] = b[i] - s1[i] * x[i-1] #x[i] = b[i] - s1[i] *x[i-1]

    # Leo: backward substitution 
    #try:
    x[n-1] = x[n-1]/s2[n-1] #x[n-1] / s2[n-1]
    #if (x[n-1]<0):
    #     x[n-1]=0.
    #    raise ValueError('x less than 0 ')

    #if (abs(x[n-1])<1e-4)<0:
    #    x[n-1] = 0

    ii = n-2
    #except ZeroDivisionError:
    #print 'Trysol0 dividing by Zero s2[n-1] n=',n
        
    for i in arange(0,n-2):
        #try: 
        x[ii] = ( x[ii] - mk4 * x[ii+1] ) / s2[ii]  #x[i]=(x[i]-c[i]*x[i+1])/s2[i]
        ii = ii - 1
        #if (abs(x[ii])<1e-4):
        #    x[ii]=0.

        #if (x[ii]<0):
        #    x[ii]=0.
            #raise ValueError('x less than 0 ')

        #except ZeroDivisionError:
        #print'Trisol0 dividing by Zero s2[ii] ii=',ii

    #Wesley: newline
    x[0] = (x[0] + mba*x[1])/s2[0]
        
    #x(1) = x(1)/s2(1);
    #try:
    #x[0] = x[0]/s2[0]
    #except ZeroDivisionError:
        #print 'Trisol0 Error dividing by s2[0]==0'
    #Leo: 05.06.2015 Colosal error here
    #Use when necessary
    #if x[1]< u[2]/4: x[0]<0
    #if x[n-2] < u[n-3]/4: x[n-1] < 0   
    #x[0]= mba * x[1] + mca * x[2] # this are water cells, sometimes we get negative values
    #x[n-1]= mba * x[n-2] + mca * x[n-3] # these are water cells, but sometimes we get negative values 

    return x

def trisol1Neumann(k,d,b):
    n = size(d)

    #[1 b/a                  ...]
    #[-k/2 d[1] -k/2         ...]
    #[                          ]
    #[          -k/2 d[n-2] -k/2]
    #[                  b/a    1]
    # constant: mk2 (minus k divided by 2)
    #   mk2   = -k/2;
    #   s2(1) = 1;
    #   s2(2) = d(2);
    #   s1(2) = mk2;
    #a = -3/h
    #b = 2/h
    #c = -1/2h
    mca = -1./3.
    #ba = b/a 
    ba = -4./3.
    #mba = -b/a
    mba = 4./3.
    """
    #Second Choice
    #a = -1/h
    #b = 3/2h
    #c = -1/2h
    #mca = -c/a
    mca = -1./2.
    #ba = b/a 
    ba = -3./2.
    #mba = -b/a
    mba = 3./2.
    """
    s1 = zeros(n)
    s2 = zeros(n)
    x = zeros(n)

    mk2 = -k / 2
    s2[0] = d[0] #s2_i=d_i-(c_i)*s1[i] = 1 
    s1[1] = mk2 / s2[0] # c_i/s2[i-1] = (-k/2)/1 
    s2[1] = d[1] + mba * s1[1] #d[1]-(a1*s1_1) = d[1]-(-4/3 * s1_1)
        

    # Leo: Forward elimination
    #for i=3:n-1
    #  s1(i) = mk2/s2(i-1);
    #  s2(i) = d(i) - mk2*s1(i);
    #end
    for i in arange(2,n-1):
        s1[i] = mk2 / s2[i-1] #a[i]/s2[i-1]
        s2[i] = d[i] - mk2 * s1[i] #d[i]-c[i]*s1[i]
        
    #s1(n) = 0; s2(n) = 1;
    s1[n-1] = ba / s2[n-2] #a[n-1]/s2[n-1-1]
    s2[n-1] = d[n-1] - mk2 * s1[n-1] #d[n-1] - c[n-1]*s1[n-1]
    
    #x(1) = b(1);
    #for i=2:n
    #    x(i) = b(i) - s1(i)*x(i-1);
    #end
    x[0]=b[0]
    for i in arange(1,n):
        x[i] = b[i] - s1[i]*x[i-1] #x[i] = b[i] - s1[i] *x[i-1]

    # Leo: backward substitution 
    #x(n) = x(n)/s2(n); ii = n-1;
    #for i=1:n-2
    #   x(ii) = (x(ii) - mk2*x(ii+1))/s2(ii);
    #   ii    = ii - 1;
    #end
    x[n-1] = x[n-1]/s2[n-1] #x[n-1] / s2[n-1]
    #if (abs(x[n-1])<0):
    #if (x[n-1]<0):
    #    x[n-1] = 0.

    ii = n-2
    for i in arange(0,n-2):
        #try:
        x[ii] = ( x[ii] - mk2 * x[ii+1] ) / s2[ii] #x[i]=(x[i]-c[i]*x[i+1])/s2[i]
        ii = ii - 1
        #if (abs(x[ii])<1e-4):
        #    x[ii]=0.0

        #if (x[ii]<0):
        #    x[ii]=0.
            #raise ValueError('x less than 0 ')
        #except ZeroDivisionError:
        #    print 'Trisol1 dividing by Zero s2[ii] ii=',ii
    
    #Wesley new line
    x[0] = (x[0] + mba*x[1])/s2[0]
        
    #x(1) = x(1)/s2(1);
    #x[0] = x[0]/s2[0]

    #Leo Colosal error here
    #use when necessary  
    #x[0] = mba * x[1] + mca * x[2]
    #x[n-1] = mba * x[n-2] + mca * x[n-3] #Leo: error!!!!

    return x    

def godunovstep1(h,kcfl,uw):
    # first compute Euler estimate
    #ut = (Amult(n,u))'
    #ut = k*ut 
    #uE = ut + h*(1-u).*u # commmon part of Euler est.
    #u'(x0) = a U0 + b U1 + c U2
    #a = -3/2h
    #b = 2/h
    #c = -1/2h
    #mca=-c/a
    mca = -1./3.
    #ba = b/a 
    ba = -4./3.
    #mba = -b/a
    mba = 4./3.
    """
    #Second Choice
    #a = -1/h
    #b = 3/2h
    #c = -1/2h
    #mca = -c/a
    mca = -1./2.
    #ba = b/a 
    ba = -3./2.
    #mba = -b/a
    mba = 3./2.
    """
    u = uw[1:-1] #getting only land points
    v = zeros_like(uw)
    n = size(u)

    kcfl4= kcfl/4.
    kcfl2= kcfl/2.

    if n>3:
        #ut = Amult(n,u)
        ut = amultNeumann(u) #Leo: getting negative value!!!
    
        rhs = u[0:n]+ kcfl4*ut

        #Euler estimate for u[2]
        #uE = u + kcfl2*ut 
        #if n>3: 
            #uE = u[2] + kcfl2*(u[1]-2*u[2]+u[3]) #index 3 out of bounds
            #Wesley new lines:
        uE = (1. - kcfl ) * u[2] + kcfl2 * ( u[1] + u[3] )
        uE = max(uE,0.)
        uest = ( 1 + kcfl4 ) * u[2] + kcfl4 * uE
        
        #Leo: to be defined
        #if n == 3:
        #    uE = u[2] + kcfl2*(u[1]-2*u[2])

        #Wesley
        #uest = u[2] + kcfl4*(uE+u[2]) #Eq 8a ... where is this from??
        #uest = uE
        #uest = u + kcfl4*amultNeumann(uE+u) 

        #TEST: uest = u[2] + kcfl2*(uE+u[2]) #still negative 

        #Wes writes:
        #rhs[0] = -uest/4. #WHY?????

        #Leo: correction
        #-1/3 = -c/a 
        #Before: 
        #rhs[0] = mca*uest
        rhs[0] = mca*uest

        #print(rhs[0])

        #Leo: using euler estimate 
        #rhs[0] = mca*uE
        #rhs[0] = mca * uest[2]        
        #if (rhs[n-2]<0):
        #    raise ValueError('rhs less than 0 ')
    
        #Euler estimate for u[n-2]
        #Leo: error for one single pixel
        #if n>3:
            #uE = u[n-3] + kcfl2*(u[n-4] -2.*u[n-3]+ u[n-2])
            
        uE = (1.-kcfl)*u[n-3] + kcfl2*(u[n-4] + u[n-2])
        uE = max(uE,0.)
        
        #Leo:modifying
        #if n == 3 : 
        #    uE = u[n-3] + kcfl2*(-2.*u[n-3]+ u[n-2])

        #uest = u[n-3] + kcfl4*(uE+u[n-3]) #Where is this coming from ????
        #uest = uE
        #TEST: uest = 0.5*(uE+u[n-3]) 
        #TEST: uest = u[n-3] + kcfl2*(uE+u[n-3])
        #Wesley new line
        uest = (1.+kcfl4)*u[n-3] + kcfl4*uE 
        
        #Wesley
        #rhs[n-1] = -uest/4. #WHY???????
        #Leo: correction 
        #-1/3 = -c/a
        #Before: 
        #rhs[n-1] = mca*uest
        rhs[n-1] = mca*uest

        #print(rhs[n-1])

        #Leo Test
        #rhs[n-1] = mca*uE
        #rhs[n-1] = mca * uest[n-3]



        #if (rhs[n-1]<0):
        #    raise ValueError('rhs less than 0 ')
 

        #solve using tridiag solver 
        #v = trisol0(n,kcfl,rhs,s1,s2)
        v[1:-1] = maximum(trisol0Neumann(kcfl, rhs),0.)
        
    if n == 3:
        ur1 = (1.-kcfl2)*u[1] + kcfl4*(u[0]+u[2])
        v[1] = max(ur1,0.)
        v[2] = v[1]
        v[3] = v[1]
  
    if n == 2:
        #Scalar euler stimate ... both stimates are equal 
        #ur = zeros(2) 
        #ur[0] = u[0] + kcfl4 * ( 2. * u[0] -u[1] )  
        #ur[1] = u[1] + kcfl4 * ( -u[0] + 2 * u[1] )
        #Leo: correction matrix A = [-2 1; 1 -2]
        #ur[0] = u[0] + kcfl4 * ( -2. * u[0] +u[1] )  
        #ur[1] = u[1] + kcfl4 * ( u[0] - 2 * u[1] )
        
        ur0 = ( 1 - kcfl2 ) * u[0] + kcfl4 * u[1]  
        ur1 = ( 1 - kcfl2 ) * u[1] + kcfl4 * u[0]

        #building the matrix  
        #a = 1. - kcfl2
        #b = kcfl4 
        #c = 1. - kcfl2
        a = 1 + kcfl2
        b = -kcfl4
        
        #explicit solution of the system         
        #because of neumann boundary conditions both updates are the same
        #be careful here, we have a water cell at v[0] and v[3] 
        #v[1] = ( c * ur[0] - b * ur[1] ) / ( a * c - b * b )
        v[1] = ( a * ur0 - b * ur1 ) / ( a * a - b * b )
        #if v[1]<0:
        #    v[1] = 0.
        #    #raise ValueError('v less than 0 ')

        v[2] = v[1]

    if n == 1: 
        v[1] = max(u[0],0.)
        #Scalar value for one single pixel 
        #v[1] = u * (1.+kcfl4)/(1.-kcfl4)
        #if v[1]<0:
        #    raise ValueError('v less than 0 ') 
    
    #else: 
    #    v[1:-1] = ( u + h / 2. * ( 1. - u / kap0 ) * u ) / ( 1. + k - 0.5 * h * ( 1 - u / kaph ) )
 
    #return v


    #else:
    #    v[1:-1] = u/(1.+kcfl2)

    return v

def godunovstep2(h,k,uw,kap0w,kaphw):
    #kap0 : carrying capacity low 
    #kaph : carrying capacity high
    # first compute Euler estimate
    #ut = (Amult(n,u))'
    #ut = k*ut 
    #uE = ut + h*(1-u).*u # commmon part of Euler est.
    #ut = Amult(n,u)
    #Wes: include regularizer tanh(x.^4).^(1/4)
    #Weights  for boundary conditions 
    #u'(x0) =  a*U0 + b*U1 + c*U2  = sigma
    #mca = -c/a
    #a = -3/h
    #b = 2/h
    #c = -1/2h
    mca = -1./3.
    #ba = b/a 
    ba = -4./3.
    #mba = -b/a
    mba = 4./3.
    """
    #Second Choice
    #a = -1/h
    #b = 3/2h
    #c = -1/2h
    #mca = -c/a
    mca = -1./2.
    #ba = b/a 
    ba = -3./2.
    #mba = -b/a
    mba = 3./2.
    """
    u = uw[1:-1] 
    kap0 = kap0w[1:-1]
    kaph = kaphw[1:-1]

    n = size(u)
    v = zeros_like(uw)


    if n>3:
        ut = amultNeumann(u)

        ut = k * ut
        #try:
        #right hand side part for part of Euler uses K(t)
        uC = u[0:n] / kap0[0:n]
        #except ZeroErrorDivision:
        #    print 'godunovstep2 divides by Zero kap0[0:n]', kap0[0:n]

        #try:
        #left hand side version uses K(t+h)
        #Leo: mistake here??? we need to use uE 
        #uh = u[0:n] / kaph[0:n]
        #except ZeroErrorDivision:
        #print 'godunov2 divides by Zero kaph[0:n]', kap0[0:n]


        #common part of Euler estimate 
        uE = ut + h * (1. - uC ) * u[0:n]
        #Compute right hand side 
        ur = u[0:n] + 0.5 * uE #RHS
        uE = u[0:n] + uE #Euler estimate
    
        #Now comes Neumann over Euler estimate 
        #4/3= -b/a
        #-1/3= -c/a
        uE[0] = max(mba * uE[1]+ mca * u[2],0.) #avoiding negative values
        uE[0] = min(uE[0], kaph[0]) #saturation
        
        uE[n-1] =  max(mba * uE[n-2] + mca * u[n-3],0.) #avoiding negative values
        uE[n-1] = min(uE[n-1], kaph[n-1]) #saturation

        #Wesley: new lines
        reL = u[2] / kap0[2]
        reH = uE[2] /kaph[2]
        uest = (1-k)*u[2] + 0.5 * k * (uE[1] - 2*uE[2] + uE[3]) \
                            + 0.5 * k * (uE[1]+uE[3]) \
                             + 0.5 * h * ((1.-reH)*uE[2]+(1.-reL)*u[2])
        uest = min(uest,kaph[0])
        
        #Leo: using uE for uh 
        #uh = uE[0:n] / kaph[0:n]

        #Explitic T.R (not regularized estimate for u[2]) in RHS
        #reL = (1./h)*reg(
        #reH = (1./h)*reg(

        #kap = 0.5*(kap0[2] + kaph[2])
        #Leo: fixing bug for one single pixel
        #if n>3:
        #    uest = u[2] + 0.5 * k * ( uE[1] - 2. * uE[2] + uE[3] ) \
        #                + 0.5 * k * ( u[1] - 2. * u[2] + u[3] ) \
        #                + 0.5 * h * ( ( 1. - uE[2] / kap ) * uE[2] + ( 1. - u[2] / kap ) * u[2] ) #error here
        #if n ==3: 
        #    uest =  u[2] + 0.5 * k * ( uE[1] - 2. * uE[2] ) \
        #                 + 0.5 * k * ( u[1] - 2. * u[2] ) \
        #                 + 0.5 * h * ( ( 1. - uE[2] / kap ) * uE[2] + ( 1 - u[2] / kap ) * u[2] ) #error here

        #mca=-1/3 = -c/a
        ur[0] = mca * uest #wesley rites uest/3
   

        #Wesley: new lines
        reL = u[n-3] / kap0[n-3]
        reH = uE[n-3] / kaph[n-3]
        uest = (1-k)*u[n-3] + 0.5 * k * (uE[n-4] - 2*uE[n-3] + uE[n-2]) \
                            + 0.5 * k * (uE[n-4]+uE[n-2]) \
                             + 0.5 * h * ((1.-reH)*uE[n-3]+(1.-reL)*u[n-3])
                
        uest = min(uest,kaph[n-3])
        

        #print(ur[0])
        #not regularized with tanh
        #We take the average kap

        #kap = 0.5 * ( kap0[n-3] + kaph[n-3])
        #if n>3:
        #    uest = u[n-3]   + 0.5 * k * ( uE[n-4] - 2. * uE[n-3] + uE[n-2] ) \
        #                    + 0.5 * k *( u[n-4] - 2. * u[n-3] + u[n-2] ) \
        #                    + 0.5 * h * ( ( 1. - uE[n-3] / kap ) * uE[n-3] + ( 1. - u[n-3] / kap ) * u[n-3] ) #Leo: error here be careful
        #if n==3:
        #    uest = u[n-3]   + 0.5 * k * ( -2. * uE[n-3] + uE[n-2] ) \
        #                    + 0.5 * k * ( -2. * u[n-3] + u[n-2] ) \
        #                    + 0.5 * h * ( ( 1 - uE[n-3] /kap ) * uE[n-3] + (1. - u[n-3] / kap ) * u[n-3] ) #Leo: error here be careful

        #mca = -1/3 = -c/a
        ur[n-1] = mca * uest #wesley writes uest/3
 
        #dA = zeros(n)
        dA = empty(n)
        dA[0] = 1 
        dA[n-1] = 1 #end points
        
        #Wesley: new line
        uh = uE[0:n]/kaph[0:n]
        
        # interior diags. of LHS matrix:
        dA[1:n-1] = 1 + k - 0.5 * h * ( 1. - uh[1:n-1] )
 
        #solve using tridiag solver 
        #v = trisol1(n,k,dA,ur,s1,s2)

        #using regularizer
        v[1:-1] = minimum(maximum(trisol1Neumann(k,dA,ur),0.),kaph[0:n])
        
    if n == 3:
        #Wes: new lines
        reL = u[1] / kap0[1]
        uC = k * ( u[0] - 2 * u[1] + u[2]) + h *( 1 - reL ) * u[1]
        uE = max(u[1] + uC, 0)
        uE = min(uE, kaph[1])
        reE = uE / kaph[1]
        S2 = 1 - 0.5*h*(1-reE)
        rHS2 = u[1] + 0.5 * uC
        v[2] = max(rHS2/S2, 0.)
        kmax = min(kaph[0], kaph[1]) #Really max?
        kmax = min(kmax, kaph[2])
        v[2] = min(v[2], kmax)
        v[1] = v[2]
        v[3] = v[2]
        
    if n == 2:
        #Scalar euler stimate ... both stimates are equal 
        #ur = zeros(2) 
        #ue = u[0] + k * ( 2. * u[0]- u[1] ) + h * u[0] / kap0[0] * ( 1. - u[0] / kap0[0])
        #ur[0] = u[0] + k / 2. * ( 2. * u[0] -u[1] ) + h / (2. * kap0[0]) * ( 1. - u[0] / kap0[0] ) * u[0] 
        #ur[1] = u[1] + k / 2. * ( -u[0] + 2 * u[1] ) + h / (2. * kap0[1]) * ( 1. - u[1] / kap0[1] ) * u[1]
        #Leo: correction of matrix A
        #ue = u[0] + k * ( -2. * u[0] + u[1] ) + h * u[0] / kap0[0] * ( 1. - u[0] / kap0[0])
        #ur[0] = u[0] + k / 2. * ( -2. * u[0] + u[1] ) + h / (2. * kap0[0]) * ( 1. - u[0] / kap0[0] ) * u[0] 
        #ur[1] = u[1] + k / 2. * ( u[0] - 2 * u[1] ) + h / (2. * kap0[1]) * ( 1. - u[1] / kap0[1] ) * u[1]
        
        #Wesley new lines
        reL1 = u[0] / kap0[0]
        reL2 = u[1] / kap0[1]
        uE0 = u[0] + k * ( u[1] - 2 * u[0] ) + h * ( 1. - reL1 )*u[0]
        uE0 = max(uE0,0)
        uE0 = min(uE0, kaph[0])
        
        uE1 = u[1] + k * ( u[0] - 2 * u[1] ) + h * ( 1. - reL1 )*u[1]
        uE1 = max(uE1,0)
        uE1 = min(uE1, kaph[1])
        
        reE0 = uE0 / kap0[0]
        reE1 = uE1 / kap0[1]
        r1 = ( 1 - k ) * u[0] + 0.5 * k * u[1] + 0.5 * h * ( 1 - reL1 ) * u[0]
        r2 = ( 1 - k ) * u[1] + 0.5 * k * u[0] + 0.5 * h * ( 1 - reL1 ) * u[1]

        #building the matrix  
        #a = 1. - k - ( 1. - ue / kaph[0] ) * h  / (2. * kaph[0])
        #b = k/2. 
        #c = 1. - k - ( 1. - ue / kaph[1] ) * h / ( 2. * kaph[1] )
        
        a = 1 + k - 0.5 * h * (1 - reE0)
        b = - 0.5 * k
        c = 1 + k - 0.5 * h * (1 - reE1)
        
        #explicit solution of the system         
        #because of neumann boundary conditions both updates are the same
        #be careful here, we have a water cell at v[0] and v[3] 
        #v[1] = ( c * ur[0] - b * ur[1] ) / ( a * c - b * b )
        #v[2] = v[1]
        
        #Wesley: new lines
        v[1] = (a * r1 - b * r2) / ( a * c - b * b)
        v[2] = (r2 - b * v[1]) / c
        v[1] = min(v[1], kaph[0])
        v[2] = min(v[2], kaph[1])
        kmax = min(kaph[0], kaph[1])
        v[1] = max(0.5*(v[1]+v[2]),0.) 
        v[1] = min(v[1], kmax)
        v[2] = v[1]

    if n == 1: 
        #Scalar value for one single pixel 
        #uE = u + h * ( 1. - u/kap0 ) * u  
        #v[1:-1] = (1. + (1. - u/kap0) * h / ( 2. * kaph ) ) / (1. - ( 1 - uE / kaph ) * h / ( 2. * kaph ))
        #Wesley: new lines
        reL = u[0] / kap0[0]
        uE = (1+h*(1-reL))*u[0]
        uE = max(uE,0.)
        uE = min(uE, kaph[0])
        reE = uE/kaph[0]
        v[0] = ( 1 + 0.5 * h * ( 1. - reL ) ) * u[0] / ( 1 - 0.5 * h * (1-reE))
        v[0] = max(v[0],0)
        v[0] = min(v[1], kaph[0])
    
    #else: 
    #    v[1:-1] = ( u + h / 2. * ( 1. - u / kap0 ) * u ) / ( 1. + k - 0.5 * h * ( 1 - u / kaph ) )
 
    return v


def smooth7(r_map,w_map):
    #Smooths points in NPP map by (2*L-1) x (2*L-1) quadratically
    #weighted window
    L=5
    #s_map = zeros((NX,NY))
    NX, NY = r_map.shape
    s_map = empty((NX,NY))
    
    for col in range(0,NX):
        for row in range(0,NY):
            if w_map[col,row]>0:      # only do windowing on land points
                wtot = 1.
                summ = r_map[col,row]

                #FIRST do points on cross 
                #Up/down on X
                for pix_col in range(1,L):
                    colpflg = True
                    colmflg = True
                    #isp1=i + ids - 1 # isp1 == 10 = i(8) + ids(3) -1
                    colp = col + pix_col - 1
                    #ism1=i - ids + 1 #Error ism1 == 10 
                    colm = col - pix_col + 1 

                    #print 'i,j',i,j
                    #print 'ids', ids
                    #print 'isp1',isp1

                    #if isp1>NX: 
                    if colp >= NX:
                        colpflg = False
                        
                    #if ism1<1:
                    if colm<0:  
                        colmflg = False
                        
                    w_col =(1.-(1.*pix_col/L)**2) # x weighting factor
                    #w_col =(1-(pix_col/L))**2 # x weighting factor


                    if colpflg and w_map[colp,row]>0:
                        wtot = wtot+w_col
                        summ = summ + w_col*r_map[colp,row]

                    if colmflg and w_map[colm,row]>0:
                        wtot = wtot+w_col;
                        summ = summ + w_col*r_map[colm,row]
    
                # Now left/right in Y-direction
                for pix_row in range(1,L):
                    #print 'js:', js
                    #jsp1=j + js - 1
                    #jsm1=j - js + 1 #Error j(9)-js(0) + 1 == 10
                    #rowp=row + pix_row -1
                    rowp=row + pix_row - 1 
                    rowm=row - pix_row + 1 
 
                    #if jsp1>=NY-1:
                    if rowp>=NY:
                        rowp=rowp-NY
                        
                    if rowm<0: 
                        rowm=rowm+NY
                            
                    w_row = (1.-(1.*pix_row/L)**2)  # y weighting factor
                    #w_row =(1-(pix_row/L))**2  # y weighting factor

                    if w_map[col,rowp]>0:
                        wtot = wtot+w_row
                        summ = summ + w_row*r_map[col,rowp]
                    #print 'i,j',i,j
                    #print 'jsm1:',jsm1
                    if w_map[col,rowm]>0: #Error: jsm1== ny, Error axis 1 size 5 
                        wtot = wtot+w_row
                        summ = summ + w_row*r_map[col,rowm]

                # END of points on cross
 
                #   All other points in square
                for pix_col in range(1,L):
                    colpflg = True; 
                    colmflg = True;
                    #isp1=i + ids - 1
                    #ism1=i - ids + 1
                    colp = col + pix_col - 1
                    colm = col - pix_col + 1 

                    #if isp1>NX:
                    if colp>=NX:
                        colpflg = False
                         
                    #if ism1<1:
                    if colp<0:
                        colmflg = False
                        
                    w_col =(1.-(1.*pix_col/L)**2);  # x weighting factor
                    #w_col =(1-(pix_col/L))**2;  # x weighting factor

                    for pix_row in range(1,L): 
                        #jsp1=j + js - 1
                        #jsm1=j - js + 1
                        rowp= row + pix_row - 1
                        rowm= row - pix_row + 1

                        #if jsp1>NY-1:
                        if rowp>=NY:
                            rowp=rowp-NY
                            
                        if rowm<0: 
                            rowm=rowm + NY
                            
                        w_row =(1.-(1.*pix_row/L)**2)  # y weighting factor
                        #w_row =(1-(pix_row/L))**2  # y weighting factor
                    
                        if colpflg:   # says isp1 <= NX
                            if w_map[colp,rowm]>0:
                                wt = w_col*w_row
                                wtot = wtot+wt;
                                summ = summ + wt*r_map[colp,rowm]
                    
                            if w_map[colp,rowp]>0:
                                wt = w_col*w_row
                                wtot = wtot+wt
                                summ = summ + wt*r_map[colp,rowp]

                        if colmflg:   # says ism1 >= 1
                            if w_map[colm,rowm]>0:
                                wt = w_col*w_row
                                wtot = wtot+wt
                                summ = summ + wt*r_map[colm,rowm]
                       
                            if w_map[colm,rowp]>0:
                                wt = w_col*w_row
                                wtot = wtot+wt;
                                summ = summ + wt*r_map[colm,rowp]
                       

                s_map[col,row] = summ/wtot # point is land, now averaged
            else:       # template=0
                s_map[col,row] = 0  # point is in water
            # end of center = landpoint if
        # end of j=1:NY loop
    #end of i=1:NX loop

    return s_map

def sciSmooth(r_map,mask):    
    # make some kind of kernel, there are many ways to do this...
    t = 1 - abs(linspace(-1, 1, 7))
    kernel = t.reshape(7, 1) * t.reshape(1, 7)
    kernel /= kernel.sum()   # kernel should sum to 1!  :) 
    
    # convolve 2d the kernel with each channel
    s_map = scipy.signal.convolve2d(r_map, kernel, mode='same')
    s_map = s_map*mask 
    
    return s_map
