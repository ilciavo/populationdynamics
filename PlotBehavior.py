################################################################################
# Plot behavior interface and behavior implementation classes.
################################################################################

from abc import ABCMeta, abstractmethod
from numpy import *
from  matplotlib import pyplot 
import matplotlib.cm as cm
from mpl_toolkits.axes_grid1 import make_axes_locatable
pyplot.style.use(['ggplot'])
        
class PlotBehavior:
    __metaclass__ = ABCMeta
    @abstractmethod 
    def plot(self):
        pass

class Plot(PlotBehavior):
    def __init__(self, data):
        self.data = data        
    def plot(self):
        print("=====Plotting Results=====")

        #fig = pyplot.figure(figsize=(30,20))
        #pyplot.subplot(211)
        #im = pyplot.imshow(self.w_map, cmap = cm.RdBu)
        #cb = pyplot.colorbar()
        #cb.set_clim(0.0,1.0)

        #pyplot.subplot(212)
        #im = pyplot.imshow(self.w_map2, cmap = cm.RdBu)
        #cb = pyplot.colorbar()
        #cb.set_clim(0.0,1.0)

        
        #u_plot = self.data.w_map-1+log(1+10*self.data.u)
        #fig = pyplot.figure(figsize=(30,20))
        #pyplot.title('$mask-1+log(1+10\,u)$ \n Ts='+str(self.data.Ts), fontsize=20)
        #im = pyplot.imshow(u_plot, cmap = cm.RdBu_r)
        
        #ax = pyplot.gca()
        #divider = make_axes_locatable(ax)
        #cax = divider.append_axes("right", size="10%", pad=0.05)
        #cb = pyplot.colorbar()
        
        pyplot.figure(figsize=(30,20))
        ax = pyplot.gca()
        pyplot.title('$mask-1+log(1+10\,u)$ \n Ts='+str(self.data.Ts), fontsize=20)
        im = ax.imshow(self.data.w_map-1+log(1+10*self.data.u), cmap = cm.RdBu_r)
        divider = make_axes_locatable(ax)
        cax = divider.append_axes("right", size="1%", pad=0.05)
        pyplot.colorbar(im, cax=cax)

