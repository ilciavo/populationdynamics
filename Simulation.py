from toolsWaves import *
from RunBehavior import *
from PlotBehavior import *
from DataInterface import * 
from mpi4py import MPI

################################################################################
# Abstract Simulation class and concrete Simulation type classes.
################################################################################

class Simulation:
    def __init__(self, Run, Plot):
        self._run_behavior = Run
        self._plot_behavior = Plot

    def run(self, *args):
        return self._run_behavior.run(*args)

    def plot(self):
        return self._plot_behavior.plot() 

class SmallSimulation(Simulation):
    def __init__(self):
        mapsize = 'small'
        print("=====Initializing "+mapsize+" simulation=====")

        # default INPUT parameters:
        NT = 2001
        #NT =101
        NPLT = 40
        #self.NPLT=40
        Kyb = 208.0      # this is Young and Bettinger's diffusion coeff.
                          # should be between 0.1 to 1000.0 km^2/yr.
        Ryb = 0.00167    # Young & Bettinger's growth rate: 0.001 - 0.03 yr^{-1}
        # Earth measurement = circumference
        Ecirc = 40074.78 # in km, cell size will be dx=dy = Ediam/(NX-1) (km)
        #start/stop simulation in kya
        Ts  = 50.0    
        Te  = 1.0

 
        # END of Input parameters


        w_map=createWrlMap(mapsize)
        NY, NX = w_map.shape
       
        # Now rescale units according to form eq. (3) in paper
        # distance SCALE FACTOR:
        sfact = sqrt(0.5*Ryb/Kyb); # unit here is km^{-1}
        real_dx = Ecirc/(NX-1)
        dx = sfact*real_dx # % now unitless
        dy = dx;    # square cells
        x1  = 0.5*Ecirc*sfact
        x0  = - x1   # W-E direction
        y1  = 0.5*x1
        y0  = -y1       # S-N direction
        simTime  = 1.e+3*Ryb*(Ts-Te) # unitless time as in eq. (3)
        h   = simTime/NT            # unitless time step, as in eq. (3)
        # eq (3) scaled time relative to NPP frames (by kya), NPP
        # frames are numbered by kiloyears (kyr or kya)
        tsc = 1.e-3/Ryb                  # Ryb is in yr^{-1}, not kyr^{-1}

        u = zeros((NY, NX)) 

        orig_y = (24-1)
        orig_x = (17-1)

        #Function to be written 
        initu(u,w_map, orig_x, orig_y, dx, dy)
        
        # compute time step and CFL parameter
        kcfl = 0.5*h/(dx*dx)
        
        print(' INPUT Parameter summary: ')
        print('   Number of time steps: NT=%d '% NT)
        print('   Plotting interval: NPLT=%d '% NPLT)
        print('   Diffusion coeff: Kyb = %e (in km^2/yr)'% Kyb)
        print('   Growth rate: Ryb = %e (in yr^{-1})'% Ryb)
        print('   MAP SIZE: NX=Grid_size_x=%d, NY=Grid_size_y=%d '%(NX, NY))
        print('   CELL size: dx=dy = %5.1f (km)= %5.1f (eq. (3) units)' %(real_dx,dx))
        print('   BOX: X x Y = [%5.1f, %5.1f] x [%5.1f, %5.1f]' %(x0,x1,y0,y1))
        print('   eq. (3) step size h=%e, and CFL number=%e \n'%(h,kcfl))

        self.data = SmallData(w_map, u, h, kcfl, NT, NPLT, NX, NY, Ts, Te, tsc, mapsize)

        Simulation.__init__(self, RunSingleLayer(self.data), Plot(self.data))

#Change this to Serial Simulation
class SerialSimulation(Simulation):
    #def __init__(self):
    def __init__(self, mapsize='large', Ts = 50., Te=1.0, Ryb = 0.00167, Kyb = 208.0, inRun=RunHDF5):
        #mapsize = 'large'
        print("=====Initializing "+mapsize+" simulation=====")

        # default INPUT parameters:
        #NT = 2001
        #NT =101
        NPLT = 40
        #self.NPLT=40
        #Kyb = 208.0*2      # this is Young and Bettinger's diffusion coeff.
                          # should be between 0.1 to 1000.0 km^2/yr.
        #Ryb = 0.00167    # Young & Bettinger's growth rate: 0.001 - 0.03 yr^{-1}
        # Earth measurement = circumference
        Ecirc = 40074.78 # in km, cell size will be dx=dy = Ediam/(NX-1) (km)
        #start/stop simulation in kya
        #Ts = 70.0
        #Ts  = 50.0
        #Ts = 5.0 #Leo: Test to match new mapsim19    
        #Te  = 1.0
        
        # END of Input parameters

        # Find start map/template:
        yalist, masks, tepoch = get_cc_list(mapsize)
        
        kstart, kend = findIndex(tepoch, Ts, Te) 

        # Get first template 
        kmp = kstart
        w_L = templateMap(mapsize, masks, kmp) #Template for the first map 
        NY, NX = w_L.shape
        #NX, NY = w_L.shape


        # Now rescale units according to form eq. (3) in paper
        # distance SCALE FACTOR:
        sfact = sqrt(0.5*Ryb/Kyb); # unit here is km^{-1}
        #real_dx = Ecirc/(NX-1)
        #dx = sfact*real_dx # % now unitless
        #dy = dx;    # square cells

        #Leo: newmap19 new part of the code 
        sy = Ecirc/(NY-1)
        dx = sfact*sy
        dy = dx 
        
        #x1  = 0.5*Ecirc*sfact
        #x0  = - x1   # W-E direction
        #y1  = 0.5*x1
        #y0  = -y1       # S-N direction
        
        #Leo: mapsim19 now compute the step size and total number of timesteps
        #Total unitless simulation time
        #as in eq(3)
        simTime  = 1.e+3*Ryb*(Ts-Te)
        # Initial CFL guess for h
        #Wes: h0=dx*dx
        #Leo: h0= 0.5*dx*dx
        h0 = dx*dx              
        m0 = ceil(1/h0)         #no.teps/(1 Ky)
        h = 1/m0                #step size such that m0*h == 1 
        NT = int(ceil(simTime*m0))        #total number of time steps, casting since ceil returns a float   
        #h   = simTime/NT            # unitless time step, as in eq. (3)
        # eq (3) scaled time relative to NPP frames (by kya), NPP
        # frames are numbered by kiloyears (kyr or kya)
        tsc = 1.e-3/Ryb                  # Ryb is in yr^{-1}, not kyr^{-1}

        #xl = range(0,NX) #points for plots on integers
        #xl = 1.e-4*(real_dx*array(xl)-real_dx)
        #yl = range(0,NY)
        #yl = 1.e-4*(real_dx*array(yl)-real_dx)

        u = zeros((NY, NX)) 

        orig_y = (24-1)
        orig_x = (17-1)
        # Leo: fix this for 720x360 map, Actually it doesn't really matter the exact position
        #orig_x=185
        #orig_y=80

        #Starting population somewhere in Africa
        initu(u,w_L, orig_x, orig_y, dx, dy)
        
        # compute time step and CFL parameter
        kcfl = 0.5*h/(dx*dx)
        
        print(' INPUT Parameter summary: ')
        print('   Number of time steps: NT=%d '% NT) 
        print('   Plotting interval: NPLT=%d '% NPLT)
        print('   Diffusion coeff: Kyb = %e (in km^2/yr)'% Kyb)
        print('   Growth rate: Ryb = %e (in yr^{-1})'% Ryb)
        print('   MAP SIZE: NX=%d (E-W), NY=%d (N-S)'%(NX, NY))
        #print('   CELL size: dx=dy = %5.1f (km)= %5.1f (eq. (3) units)' %(real_dx,dx)
        print('   CELL size: dx=dy = %5.1f (km)= %5.1f (eq. (3) units)' %(sy,dy))
        #print('   BOX: X x Y = [%5.1f, %5.1f] x [%5.1f, %5.1f]' %(x0,x1,y0,y1))
        print('   eq. (3) step size h=%e, and CFL number=%e \n'%(h,kcfl))
        print('   Start time Ts=%5.1f (kya), NPP frame kstart=%d\n'%(Ts,kstart))
        print('   End time   Te=%5.1f (kya), NPP frame kend=%d\n'%(Te,kend))

        self.data = SmallData(w_L, u, h, kcfl, NT, NPLT, NX, NY, Ts, Te, tsc, mapsize)
        #Simulation.__init__(self, RunMultiLayer(self.data), Plot(self.data))
        #Simulation.__init__(self, RunHDF5(self.data), Plot(self.data))
        Simulation.__init__(self, inRun(self.data), Plot(self.data))
        
class ParallelSimulation(Simulation):
    def __init__(self, mapsize, Ts = 50., Ryb = 0.00167, Kyb = 208.0):
        #mapsize = 'large'
        mpi = MPI.COMM_WORLD
        if mpi.rank == 0: 
            print("=====Initializing parallel "+mapsize+" simulation=====")

        # default INPUT parameters:
        #NT = 2001
        #NT =101
        NPLT = 40
        #self.NPLT=40
        #Kyb = 208.0      # this is Young and Bettinger's diffusion coeff.
                          # should be between 0.1 to 1000.0 km^2/yr.
        #Ryb = 0.00167    # Young & Bettinger's growth rate: 0.001 - 0.03 yr^{-1}
        # Earth measurement = circumference
        Ecirc = 40074.78 # in km, cell size will be dx=dy = Ediam/(NX-1) (km)
        #start/stop simulation in kya
        #Ts = 60.0
        #Ts  = 50.0
        #Ts = 5.0 #Leo: Test to match new mapsim19    
        Te  = 1.0

        # END of Input parameters

        # Find start map/template:
        yalist, masks, tepoch = get_cc_list(mapsize)

        kstart, kend = findIndex(tepoch, Ts, Te)

        # Get first template 
        kmp = kstart
        w_L = templateMap(mapsize, masks, kmp) #Template for the first map 
        NY, NX = w_L.shape
        #NX, NY = w_L.shape


        # Now rescale units according to form eq. (3) in paper
        # distance SCALE FACTOR:
        sfact = sqrt(0.5*Ryb/Kyb); # unit here is km^{-1}
        #real_dx = Ecirc/(NX-1)
        #dx = sfact*real_dx # % now unitless
        #dy = dx;    # square cells

        #Leo: newmap19 new part of the code 
        sy = Ecirc/(NY-1)
        dx = sfact*sy
        dy = dx 
        
        #x1  = 0.5*Ecirc*sfact
        #x0  = - x1   # W-E direction
        #y1  = 0.5*x1
        #y0  = -y1       # S-N direction
        
        #Leo: mapsim19 now compute the step size and total number of timesteps

        simTime  = 1.e+3*Ryb*(Ts-Te) # unitless time as in eq. (3)
        #Wes: h0=dx*dx
        #Leo: h0=0.5*dx*dx
        h0 = dx*dx              # CFL guess for h
        m0 = ceil(1/h0)         #no.teps/(1 Ky)
        h = 1/m0                #step size such that m0*h == 1 
        NT = int(ceil(simTime*m0))        #total number of time steps, casting since ceil returns a float   
        #h   = simTime/NT            # unitless time step, as in eq. (3)
        # eq (3) scaled time relative to NPP frames (by kya), NPP
        # frames are numbered by kiloyears (kyr or kya)
        tsc = 1.e-3/Ryb                  # Ryb is in yr^{-1}, not kyr^{-1}

        #xl = range(0,NX) #points for plots on integers
        #xl = 1.e-4*(real_dx*array(xl)-real_dx)
        #yl = range(0,NY)
        #yl = 1.e-4*(real_dx*array(yl)-real_dx)

        u = zeros((NY, NX)) 

        orig_y = (24-1)
        orig_x = (17-1)
        # Leo: fix this for 720x360 map, Actually it doesn't really matter the exact position
        #orig_x=185
        #orig_y=80

        #Starting population somewhere in Africa
        initu(u,w_L, orig_x, orig_y, dx, dy)
        
        # compute time step and CFL parameter
        kcfl = 0.5*h/(dx*dx)

        #Storing data 
        self.data = SmallData(w_L, u, h, kcfl, NT, NPLT, NX, NY, Ts, Te, tsc, mapsize)
        
        if mpi.rank == 0: 
            print(' INPUT Parameter summary: ')
            print('   Number of time steps: NT=%d '% NT) 
            print('   Plotting interval: NPLT=%d '% NPLT)
            print('   Diffusion coeff: Kyb = %e (in km^2/yr)'% Kyb)
            print('   Growth rate: Ryb = %e (in yr^{-1})'% Ryb)
            print('   MAP SIZE: NX=%d (E-W), NY=%d (N-S)'%(NX, NY))
            #print('   CELL size: dx=dy = %5.1f (km)= %5.1f (eq. (3) units)' %(real_dx,dx))
            print('   CELL size: dx=dy = %5.1f (km)= %5.1f (eq. (3) units)' %(sy,dy))
            #print('   BOX: X x Y = [%5.1f, %5.1f] x [%5.1f, %5.1f]' %(x0,x1,y0,y1))
            print('   eq. (3) step size h=%e, and CFL number=%e \n'%(h,kcfl))
            print('   Start time Ts=%5.1f (kya), NPP frame kstart=%d\n'%(Ts,kstart))
            print('   End time   Te=%5.1f (kya), NPP frame kend=%d\n'%(Te,kend))

class BcastSimulation(ParallelSimulation):
    def __init__(self, mapsize):
        ParallelSimulation.__init__(self, mapsize)
        
        Simulation.__init__(self, RunBcast(self.data), Plot(self.data))

class ScatterSimulation(ParallelSimulation):
    def __init__(self,mapsize):
        ParallelSimulation.__init__(self, mapsize)
        
        Simulation.__init__(self, RunScatter(self.data), Plot(self.data))
        
class BigSimulation(Simulation):
    def __init__(self):
        Simulation.__init__(self, RunEmpty(), Plot())
        #Simulation.__init__(self)
        print("=====Initializing big Simulation=====")




