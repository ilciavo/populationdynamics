//Compile with :f2pyA -c --verbose --opt="O2"  CmodulesNeumann.pyf CmodulesNeumann.c
/*
!  G.E. Forsythe and C.B. Moler " Computer 
!  solutions to linear algebraic systems,"
!  Prentice-Hall publ., 1967
!  Tridiagonal system is Tx=b, where
!
!  T = [1     0   0    ....          0 ]
!      [-k/2 d(2) -k/2 ....          0 ]
!      [0    -k/2 d(3) -k/2  0 ....  0 ]
!      [0           ....             0 ]
!      [0              -k/2 d(n-1) -k/2]
!      [0                0    0      1 ]
! 
! and k = h/dx^2 is the CFL factor. Diagonals
! d = (1 d(2) d(3) ...  d(n-1) 1) are strong.
! s1 and s2 are work arrays.(Leo: s2 new diagonal, s1 new off-diagonal)  
! In the original F & M notation,
!
!  T = [d(1)  f(1)  0        ...             0 ]
!      [e(2)  d(2)  f(2)  0  ...             0 ]
!      [ 0          ....                     0 ]
!      [ 0                e(n-1) d(n-1) f(n-1) ]
!      [ 0                  0    e(n)   d(n)   ]
*/
#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>


void smooth7(int nx, int ny, double* r_map, int * w_map, double* s_map)
{
    double L = 5;
    double wtot, summ, w_col, w_row;
    bool colpflg, colmflg;
    int col, row, colp, colm, pix_col, pix_row, rowp, rowm; 

    for (col = 0; col<ny; ++col)
    {
        for(row = 0; row <nx; ++ row)
        {
            if (w_map[col+row*ny]>0)
            { //Taking only land points
            //Leo Test
            //s_map[row+col*nx] = 1.;
            
                wtot = 1.; //Total Weight 
                summ = r_map[col+row*ny]; //summing values
    
                //scanning points on a cross 
                //Left/right on X
                for (pix_col = 1; pix_col<L; ++pix_col)
                {
                    //flags to track land
                    colpflg = true; 
                    colmflg = true;
                    //column plus pixel smoother
                    colp = col + pix_col - 1;
                    //column minus pixel smoother
                    colm = col - pix_col +1;

                    if(colp>=nx) colpflg = false; // flag out of the map
 
                    if(colm<0) colmflg = false; // flag out of the map 
 
                    //weighting factor in x 
                    w_col = (1.-(1.*pix_col/L)*(1.*pix_col/L));
 
                    if ( colpflg && (w_map[colp+row*ny]>0))
                    {
                        //inside the map 
                        wtot = wtot+w_col; // updating total weight 
                        summ = summ + w_col*r_map[colp+row*ny]; // updating sum
                    }
                    
                    if ( colmflg && (w_map[colm+row*ny]>0))
                    {
                        //inside the map 
                        wtot= wtot+w_col; // updating total weight 
                        summ = summ + w_col*r_map[colm+row*ny];   // updating sum
                    }

                }  

                //Bottom/Up in Y 
                for (pix_row = 1; pix_row<L; ++pix_row)
                {
                    //flags to track land
                    //colpflg = true 
                    //colmflg = true
                    //column plus pixel smoother
                    rowp = row + pix_row - 1;
                    //column minus pixel smoother
                    rowm = row - pix_row + 1;

                    if(rowp>=ny)
                    {
                        //colpflg = .FALSE. ! flag out of the map
                        rowp = rowp - ny; 
                    }
 
                    if(rowm<0)
                    { 
                        //colmflg = .FALSE. !flag out of the map 
                        rowm = rowm + ny; 
                    }
 
                    //weighting factor in x 
                    w_row =(1.-(1.*pix_row/L)*(1.*pix_row/L));
 
                    if (w_map[col+rowm*ny]>0)
                    {
                        //inside the map 
                        wtot = wtot+w_row; // updating total weight 
                        summ = summ + w_row*r_map[col+rowp*ny];   //updating sum
                    }
                    
                    if (w_map[col+rowm*ny]>0)
                    { 
                        //inside the map 
                        wtot= wtot+w_row; // updating total weight 
                        summ = summ + w_row*r_map[col+rowm*ny];   // updating sum
                    }

                } //End of points on cross
                
                //Now smooth the square
                for(pix_col = 1; pix_col<L; ++pix_col)
                {
                    colpflg = true;
                    colmflg = true;
                    colp = col + pix_col - 1;
                    colm = col - pix_col + 1;

                    if (colp >= nx) colpflg = false; 
                
                    if (colm < 0) colmflg = false;
                    
                    w_col = (1.-(1.*pix_col/L)*(1.*pix_col/L));
                    
                    for (pix_row = 1;pix_row<L;++pix_row)
                    {
                        //rowpflg = .TRUE.
                        //rowmflg = .TRUE.
                        rowp = row + pix_row - 1;
                        rowm = row - pix_row + 1;

                        if (rowp >= ny)
                        {
                            //colpflg = .FALSE.
                            rowp = rowp - ny; 
                        }
                
                        if (rowm < 0)
                        {
                            //colmflg = .FALSE.
                            rowm = rowm + ny; 
                        }
                        
                        w_row =(1.-(1.*pix_row/L)*(1.*pix_row/L));
                        
                        if(colpflg)
                        { //inside the map from the right 
                            if (w_map[colp+rowm*ny]>0)
                            { 
                                wtot = wtot + w_row*w_col;
                                summ = summ + w_row*w_col*r_map[colp +rowm*ny];
                            }
                            
                            if (w_map[colp+rowp*ny]>0)
                            {
                                wtot = wtot + w_row*w_col;
                                summ = summ + w_row*w_col*r_map[colp+rowp*ny];
                            }
                             
                        }
  
                        if (colmflg) 
                        { //inside the map from the left
                            if (w_map[colm+rowm*ny]>0)
                            {
                                wtot = wtot + w_row*w_col;
                                summ = summ + w_row*w_col*r_map[colm+rowm*ny];
                            }
                            
                            if (w_map[colm+rowp*ny]>0) 
                            {
                                wtot = wtot + w_row*w_col;
                                summ = summ + w_row*w_col*r_map[colm+rowp*ny];
                            }                             

                        } 
                                             
                    } 

                }//Finish square scan  
            
                s_map[col*nx+row] = summ/wtot; //average of points in land
            
            }
            else
            {
                s_map[row+col*nx] = 0.; // pixel is in water

            } 
            //s_map(col,row) = L*(r_map(col,row) + w_map(col,row))
        }
    }

}

///------------NEUMANN SOLVERS------------////
void amult2neumann(int np2, double* u, double *v , double dx)
{
    int i = 0;
    int n = np2-2;

    v[0] = -dx*u[0]/2+ dx*u[1]/2;
    v[n+1] = -dx*u[n]/2+dx*u[n+1]/2;
    //printf("u[%i]=%g\n", n, u[n]); 
    //printf("u[%i]=%g\n", n+1, u[n+1]); 
    for (i=1; i<n+1; ++i){
        v[i] = u[i-1]-2*u[i]+u[i+1];
    }

}

void trisol2neumann(int np2, double k, double dx, double* d,double* b,double *x)
{
    int i;
    int n = np2-2;

    //*(x) = *(d);
    double *cp = malloc(sizeof(double)*(np2));
    double *dp = malloc(sizeof(double)*(np2));

    double mk2 = -0.5*k;
    double m2dx = -0.5*dx;

    //cp[0] = (1/(2*dx))(-1/(2*dx))
    cp[0]=-1.;

    //Leo: forward elimination
    for (i=1; i<n+1; ++i){
        cp[i] = mk2/(d[i]-mk2*cp[i-1]);
    }

    dp[0] = b[0]/m2dx;
    
    for (i=1; i<n+1; ++i){
        dp[i] = (b[i]-mk2*dp[i-1])/(d[i]-mk2*cp[i-1]);
    }
    
    dp[n+1] = (b[n+1]-m2dx*dp[n])/(d[n+1]-m2dx*cp[n]);
    
    x[n+1] = dp[n+1];
    
    //Backward substitution
    for(i=n; i>-1; --i){
        //printf("[%i]\n", i); 
        x[i] = dp[i] - cp[i]*x[i+1];
    }
    

    free(cp);
    free(dp);
}

void godunovstepneumann2nd(int np2, double h, double k, double dx, double* u, double* v)
{
    int i;
    int n = np2-2;

    double sigmaL=0., sigmaR=0.; //Boundary flux 
    //double ut[n], uE[n], ur[n], dA[n];
    double *ut = malloc(sizeof(double)*(np2));
    double *uE = malloc(sizeof(double)*(np2));
    double *ur = malloc(sizeof(double)*(np2));
    double *dA = malloc(sizeof(double)*(np2));
    
    amult2neumann(np2, u, ut, dx);

    for (i = 0; i<np2; ++i){
    ut[i] = k * ut[i]; 
    uE[i] = ut[i] + h*(1-u[i])*u[i]; 
    //ur = u + 0.5*uE;
    ur[i] = u[i] + 0.5*uE[i]; //RHS
    //uE = k*ut + h*(1-u)*u;
    uE[i] = u[i] + uE[i]; //Euler estimate
    }

    //Compute right hand side 

    //Leo: impose second approach 
    //Neumann b.c
    ur[0] = sigmaL;
    ur[n+1] = sigmaR;
    

    dA[0] = -0.5*dx;
    dA[n+1] = 0.5*dx;


    for (i = 1; i<n+1; ++i){
        dA[i] = 1. + k - 0.5*h*(1.-uE[i]);
        //printf("dA[%i]=%g\n", i, dA[i]); 
        
    }

    trisol2neumann(np2, k, dx, dA, ur, v);
    free(ut);
    free(uE);
    free(ur);
    free(dA);
}

void amult3neumann(int n, double* u, double *v)
{
    int i = 0;
    v[0] = -2.*u[0]+ u[1];
    v[n-1] = u[n-2]-2.*u[n-1];
    for (i=1; i<n-1; ++i){
        v[i] = u[i-1]+u[i+1]-2*u[i];
    }

}

void trisol3neumann(int n, double k, double* d,double* b,double *x)
{
    int i, ii;
    //Performance counters
    //int muls=0, adds=0;
    //*(x) = *(d);
    //double s1[n];
    //double s2[n];
    //double *s1 = (double*)malloc(sizeof(double)*n);
    double *s1 = malloc(sizeof(double)*n);
    double *s2 = malloc(sizeof(double)*n);

    double mk2 = -k/2;

    //Neumann
    s2[0]=d[0];
    s1[1]=mk2/s2[0];
    s2[1]=d[1]+4.*s1[1]/3; 
    //s1[2]=mk2/s2[0];
    //s2[1]=d[1]+4.*s1[2]/3; //s1[2]???
    //printf("S2[%i]=%g\n", 0, s2[0]);
    //printf("S1[%i]=%g\n", 1, s1[1]); 
    //printf("S2[%i]=%g\n", 1, s2[1]); 

    //muls += 2; 
    //Forward elimination
    for (i=2; i<n-1; ++i){
        s1[i]=mk2/s2[i-1];
        s2[i]=d[i] - mk2*s1[i];

        //muls+=2;
        //++adds;
        //printf("S1[%i]=%g\n", i, s1[i]);
        //printf("S2[%i]=%g\n", i, s2[i]); 
    }

    //Neumann
    s1[n-1]=-4./(3.*s2[n-2]);
    s2[n-1]=d[n-1]-mk2*s1[n-1];

    //*(x) = *(b);
    x[0] = b[0];
    for (i=1; i<n; ++i){
        //*(x+i)=*(b+i)-s1[i]*(*(x+i-1));
        x[i]=b[i]-s1[i]*(x[i-1]);
        //++muls;
        //++adds;
    }

    //Backward substitution
    x[n-1]= x[n-1]/s2[n-1];
    ii = n-2;
    //++muls;

    for(i=0; i<n-2; ++i){
        x[ii]=(x[ii] - mk2*x[ii+1])/s2[ii];
        ii = ii-1;
        //muls+=3;
        //++adds ;
    }
    //x[0]=x[0]/s2[0];
    //++muls;
    //printf("muls=%i\n", muls);
    //printf("adds=%i\n", adds);     
    free(s1);
    free(s2);     
}


void godunovstepneumann3rd(int n, double h, double k, double* u, double* v)
{
    int i;
    double uest;
    //double ut[n], uE[n], ur[n], dA[n];
    double *ut = malloc(sizeof(double)*n);
    double *uE = malloc(sizeof(double)*n);
    double *ur = malloc(sizeof(double)*n);
    double *dA = malloc(sizeof(double)*n);
    

    //dA[0] = 1;
    //dA[n-1] = 1;

    amult3neumann(n, u, ut);
    for (i =0; i<n; ++i){
        //uE = k*ut + h*(1-u)*u;
        uE[i] = k*ut[i] + h*(1-u[i])*u[i];
        //ur = u + 0.5*uE;
        ur[i] = u[i]+ 0.5*uE[i];
        uE[i] = u[i] + uE[i];
        //printf("ut[%i]=%g\n", i, ut[i]);
        //printf("uE[%i]=%g\n", i, uE[i]);
        //printf("ur[%i]=%g\n", i, ur[i]); 

    }
    
    //impose Neumann on euler estimate 
    uE[n-1] = 4.*uE[n-2]/3. - (uE[n-3]/3.);
    uE[0] = (4.*uE[1]/3.) - (uE[2]/3.); //something veerry weird happening here... wrong results 

    //printf("uE[%i]=%g\n", 0, uE[0]);
    //printf("uE[%i]=%g\n", n-1, uE[n-1]);
    //printf("ur[%i]=%g\n", i, ur[i]); 


    uest= u[2] + 0.5*k*(uE[1]-2.*uE[2]+uE[3])+ 0.5*k*(u[1]-2.*u[2]+u[3]) + 0.5*h*((1.-uE[2])*uE[2]+(1.-u[2])*u[2]);
    ur[0]=-uest/3;

    //printf("ur[%i]=%g\n", 0, ur[0]);


    uest= u[n-3] + 0.5*k*(uE[n-4]-2*uE[n-3]+uE[n-2])+ 0.5*k*(u[n-4]-2*u[n-3]+u[n-2]) + 0.5*h*((1-uE[n-3])*uE[n-3]+(1-u[n-3])*u[n-3]);
    ur[n-1]=-uest/3;    

    //printf("ur[%i]=%g\n", n-1, ur[n-1]);


    dA[0] = 1.;
    dA[n-1] = 1.;

    for (i = 1; i<n-1; ++i){
        dA[i] = 1 + k - 0.5*h*(1-uE[i]); //possible error here 
        //printf("dA[%i]=%g\n", i, dA[i]); 
        
    }

    trisol3neumann(n, k, dA, ur, v);

    //impose Neumann on output
    v[0] = 4.*v[1]/3.-v[2]/3.;
    v[n-1] = 4.*v[n-2]/3. - v[n-3]/3;

    free(ut);
    free(uE);
    free(ur);
    free(dA);
}

void amultneumann(int n, double* u, double *v)
{
    int i = 0;
    //int muls=0, adds=0;
    v[0] = -2*u[0]+u[1];
    v[n-1] = -2*u[n-1]+u[n-1];
    for (i=1; i<n-1; ++i){
        v[i] = u[i-1]+u[i+1]-2*u[i];
        //++muls;
        //adds+=2;
    }
    //printf("muls=%i\n", muls);
    //printf("adds=%i\n", adds);
}


void trisol0neumann(int n, double k, double* b,double *x)
{
    int i, ii;
    //*(x) = *(d);
    //double s1[n];
    //double s2[n];
    double *s1 = malloc(sizeof(double)*n);
    double *s2 = malloc(sizeof(double)*n);

    double mk4 = -k/4;
    double k2 = k/2;
    s2[0]=1;
    s1[1]=mk4;
    s2[1]=1+k2 + 4.*s1[1]/3.;

    //muls+=3
    //++adds;
    //Forward elimination
    for (i=2; i<n-1; ++i){
        s1[i]=mk4/s2[i-1];
        s2[i]= 1+ k2 - mk4 *s1[i];
        //muls+=2;
        
        //printf("S1[%i]=%g\n", i, s1[i]);
        //printf("S2[%i]=%g\n", i, s2[i]); 
    }
    s1[n-1]=-4./(3.*s2[n-2]);
    s2[n-1]=1-mk4*s1[n-1];

    //*(x) = *(b);
    x[0] = b[0];
    for (i=1; i<n; ++i){
        //*(x+i)=*(b+i)-s1[i]*(*(x+i-1));
        x[i]=b[i]-s1[i]*(x[i-1]);

    
    }

    //Backward substitution
    x[n-1]= x[n-1]/s2[n-1];
    ii = n-2;

    for(i=0; i<n-2; ++i){
        x[ii]=(x[ii] - mk4*x[ii+1])/s2[ii];
        ii = ii-1;
    }
    //x[0]=x[0]/s2[0];

    x[0] = 4.*x[1]/3. - x[2];
    x[n-1] = 4.*x[n-1]/3. - x[n-2]/3.;

    free(s1);
    free(s2);
}


void trisol1neumann(int n, double k, double* d,double* b,double *x)
{
    int i, ii;
    //*(x) = *(d);
    //double s1[n];
    //double s2[n];
    double *s1 = malloc(sizeof(double)*n);
    double *s2 = malloc(sizeof(double)*n);

    double mk2 = -k/2;

    s2[0]=d[0];
    s1[1]=mk2/s2[0];
    s2[1]=d[1]+4.*s1[1]/3.;

    //Forward elimination
    for (i=2; i<n-1; ++i){
        s1[i]=mk2/s2[i-1];
        s2[i]=d[i] - mk2*s1[i];
        //printf("S1[%i]=%g\n", i, s1[i]);
        //printf("S2[%i]=%g\n", i, s2[i]); 
    }
    s1[n-1]=-4./(3.*s2[n-2]);
    s2[n-1]=d[n-1]-mk2*s1[n-1];

    //*(x) = *(b);
    x[0] = b[0];
    for (i=1; i<n; ++i){
        //*(x+i)=*(b+i)-s1[i]*(*(x+i-1));
        x[i]=b[i]-s1[i]*(x[i-1]);
    
    }

    //Backward substitution
    x[n-1]= x[n-1]/s2[n-1];
    ii = n-2;

    for(i=0; i<n-2; ++i){
        x[ii]=(x[ii] - mk2*x[ii+1])/s2[ii];
        ii = ii-1;
    }
    //x[0]=x[0]/s2[0];
    x[0] = 4.*x[1]/3. - x[2];
    x[n-1] = 4.*x[n-1]/3.-x[n-2]/3.;

    free(s1);
    free(s2);
}


void godunovstep1(int n, double h, double kcfl, double* u, double* v)
{
    int i;
    //double ut[n], rhs[n];
    double *ut = malloc(sizeof(double)*n);
    double *rhs = malloc(sizeof(double)*n);
    
    double kcfl4 = kcfl / 4;

    double kcfl2 = kcfl / 2;

    double ue, uest;

    amultneumann(n, u, ut);

    for (i =0; i<n; ++i){
        rhs[i] = u[i] + kcfl4 * ut[i];
        //printf("ut[%i]=%g\n", i, ut[i]); 
        //printf("rhs[%i]=%g\n", i, rhs[i]); 
    }

    if (n>3){
        ue = u[2] + kcfl2 * ( u[1] - 2 * u[2] + u[3] ); 
    }
    else{
        ue = u[2] + kcfl2 * ( u[1] - 2 * u[2] );
    }

    uest = u[2] + kcfl4*(ue+u[2]);

    rhs[0] = -uest/3;

    if (n>3){
        ue = u[n-3] + kcfl2 * ( u[n-4] - 2 * u[n-3] + u[n-2] ); 
    }
    else{
        ue = u[n-3] + kcfl2 * ( - 2*u[n-3] + u[n-2] );
    }

    uest = u[n-3] + kcfl4 * ( ue + u[n-3] );

    rhs[n-1] = - uest / 3; 

    trisol0neumann(n, kcfl, rhs, v);
    //trisol(n, kcfl, rhs, rhs, v);
    free(ut);
    free(rhs);

}


void godunovstep2(int n, double h, double k, double* u, double* kap0, double* kaph, double* v)
{
    int i;
    //double ut[n], uE[n], uC[n], uH[n], ur[n], dA[n];
    double *ut = malloc(sizeof(double)*n);
    double *uE = malloc(sizeof(double)*n);
    double *uC = malloc(sizeof(double)*n);
    double *uH = malloc(sizeof(double)*n);
    double *ur = malloc(sizeof(double)*n);
    double *dA = malloc(sizeof(double)*n);
    double kap,uest;

    amultneumann(n, u, ut);

    for (i =0; i<n; ++i){
        //uE = k*ut + h*(1-u)*u;
        uC[i] = u[i] / kap0[i];
        uH[i] = u[i] / kaph[i];
        uE[i] = k * ut[i] + h * ( 1 - uC[i] ) * u[i];
        //ur = u + 0.5*uE;
        ur[i] = u[i] + 0.5 * uE[i];
        uE[i] = u[i] + uE[i];
        //printf("ut[%i]=%g\n", i, ut[i]);
        //printf("uE[%i]=%g\n", i, uE[i]);
        //printf("ur[%i]=%g\n", i, ur[i]); 

    }
    //Euler estimate for Neumann 
    uE[0] = 4. * uE[1] / 3. - u[2] / 3.;

    uE[n-1] = 4. * uE[n-2] / 3. - u[n-3] / 3.;

    kap = 0.5 * ( kap0[2] + kaph[2] );

    if(n>3){
        uest = u[2] + 0.5 * k * ( uE[1] - 2 * uE[2] + uE[3] )
                     + 0.5 * k * ( u[1] - 2 * u[2] + u[3] )
                      + 0.5 * h * ( (1-uE[2] / kap) * uE[2] + (1 - u[2] / kap ) * u[2]); //#error here
    }
    else{
        uest =  u[2] + 0.5 * k * ( uE[1] - 2 * uE[2] )
                      + 0.5 * k * ( u[1] - 2 * u[2] )
                       + 0.5 * h * ( ( 1 - uE[2] / kap ) * uE[2] + ( 1 - u[2] / kap ) * u[2] );
    }

    ur[0] = -uest/3; //wes writes uest/3

    kap = 0.5*(kap0[n-3] + kaph[n-3]);
 
    if (n > 3){
        uest = u[n-3] + 0.5 * k * ( uE[n-4] - 2 * uE[n-3] + uE[n-2] )
                        + 0.5 * k * ( u[n-4] - 2 * u[n-3] + u[n-2] )
                          + 0.5 * h * ( ( 1 - uE[n-3] /kap ) * uE[n-3] + ( 1 - u[n-3] /kap ) * u[n-3] );
    //#Leo: error here be careful
    }
    else {
        uest = u[n-3] + 0.5 * k * ( -2 * uE[n-3] + uE[n-2] )
                       + 0.5 * k * ( -2 * u[n-3] + u[n-2] )
                        + 0.5 * h * ( ( 1 - uE[n-3] / kap ) * uE[n-3] + ( 1 - u[n-3] / kap ) * u[n-3]);
    }

    ur[n-1] = - uest / 3; //wes writes uest/3

    dA[0] = 1;
    dA[n-1] = 1;
    for (i = 1; i<n-1; ++i){
        dA[i] = 1 + k - 0.5 * h * ( 1 - uH[i] );
        //printf("dA[%i]=%g\n", i, dA[i]);         
    }

    trisol1neumann(n, k, dA, ur, v);

    free(ut);
    free(uE);
    free(uC);
    free(uH);
    free(ur);
    free(dA);
}


/////--------------test------------------------

void test(int nx, int ny, double* r_map, double* s_map)
{
    int col, row;
    for (col =0; col<ny; ++col){
        for (row = 0; row<nx; ++row ){
            //s_map[row,col] = r_map[row,col];
            //r_map is read in column mayor order: Fortran 
            //s_map is stored in row mayor order: C
            s_map[row+col*nx] = r_map[col+row*ny];

        }
    }

}

int  main(){
return 0; 
}
