def ParseBoolean (b):
    # ...
    if len(b) < 1:
        raise ValueError ('Cannot parse empty string into boolean.')
    b = b[0].lower()
    if b == 't' or b == 'y' or b == '1':
        return True
    if b == 'f' or b == 'n' or b == '0':
        return False
    raise ValueError ('Cannot parse string into boolean.')

import sys

try:
    accelerator = sys.argv[1]
    output = ParseBoolean(sys.argv[2])
except IndexError:
    print('Usage:', sys.argv[0], 'accelerator output'); sys.exit(1)

from TravelingWaves2D import *
sim = Simulation('small')
#sim.plotMaps()

import cProfile, pstats

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

pr = cProfile.Profile()
pr.enable()
# ... do something ...
#start = timer()
sim.run(output=output, module=accelerator)
#end = timer()
#pr.disable()
#s = StringIO.StringIO()
#sortby = 'cumulative'
#ps = pstats.Stats(pr, stream=s).sort_stats(sortby)
#ps.print_stats()
print('Modules from '+accelerator)
#print(s.getvalue())

