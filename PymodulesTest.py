from numpy import *
def godunovstep1(h,kcfl,u):
    # first compute Euler estimate
    #ut = (Amult(n,u))'
    #ut = k*ut 
    #uE = ut + h*(1-u).*u # commmon part of Euler est.
    n = size(u)
    kcfl4= kcfl/4

    #ut = Amult(n,u)
    ut = amult(u)
    
    rhs = u[0:n]+ kcfl4*ut
    #solve using tridiag solver 
    #v = trisol0(n,kcfl,rhs,s1,s2)
    v = trisol0(kcfl, rhs)
    return v

def godunovstep2(h,k,u,kap0,kaph):
    #kap0 : carrying capacity low 
    #kaph : carrying capacity high
    # first compute Euler estimate
    #ut = (Amult(n,u))'
    #ut = k*ut 
    #uE = ut + h*(1-u).*u # commmon part of Euler est.
    #ut = Amult(n,u)
    n = size(u)
    ut = amult(u)
    ut = k*ut
    #try:
    #right hand side part for part of Euler uses K(t)
    uC = u[0:n]/kap0[0:n]
    #except ZeroErrorDivision:
    #    print 'godunovstep2 divides by Zero kap0[0:n]', kap0[0:n]

    #try:
    #left hand side version uses K(t+h)
    uh = u[0:n]/kaph[0:n]
    #except ZeroErrorDivision:
    #print 'godunov2 divides by Zero kaph[0:n]', kap0[0:n]


    #common part of Euler estimate 
    uE = ut + h*(1-uC)*u[0:n]
    #Compute right hand side 
    ur = u[0:n]+0.5*uE #RHS
    uE = u[0:n]+uE
    
    #dA = zeros(n)
    dA = empty(n)
    dA[0] = 1 
    dA[n-1] = 1 #end points
    # interior diags. of LHS matrix:
    dA[1:n-1] = 1 + k - 0.5*h*(1-uh[1:n-1])
    
    #solve using tridiag solver 
    #v = trisol1(n,k,dA,ur,s1,s2)
    v = trisol1(k,dA,ur)
    
    return v

def amult(u):
    # it performs tridiagonal matrix multiply, returns n elements of v
    # with v(1) = v(n) = 0, the others are u(i-1)+u(i+1)-2*u(i)
    #   v(1) = 0; v(n) = 0;
    #   v(2:n-1) = u(1:n-2)+u(3:n)-2*u(2:n-1);
    n = size(u)
    v = zeros(n)
    v[0] = 0 
    v[-1] = 0
    v[1:-1] = u[0:-2]+u[2:n]-2*u[1:-1]
    return v

def trisol0(k,b):
    #  Tridiagonal system solver modeled on
#  G.E. Forsythe and C.B. Moler " Computer 
#  solutions to linear algebraic systems,"
#  Prentice-Hall publ., 1967
#  Tridiagonal system is Tx=b, where
#
#  T = [1     0   0    ....          0 ]
#      [-k/2 d(2) -k/2 ....          0 ]
#      [0    -k/2 d(3) -k/2  0 ....  0 ]
#      [0           ....             0 ]
#      [0              -k/2 d(n-1) -k/2]
#      [0                0    0      1 ]
# 
# and k = h/dx^2 is the CFL factor. Diagonals
# d = (1 d(2) d(3) ...  d(n-1) 1) are strong.
# s1 and s2 are work arrays.(Leo: s2 new diagonal, s1 new off-diagonal)  
# In the original F & M notation,
#
#  T = [d(1)  f(1)  0        ...             0 ]
#      [e(2)  d(2)  f(2)  0  ...             0 ]
#      [ 0          ....                     0 ]
#      [ 0                e(n-1) d(n-1) f(n-1) ]
#      [ 0                  0    e(n)   d(n)   ]
#
#  wpp 27 May 2012
# def trisol(n,k,d,b,s1,s2):
    n = size(b)
    s1 = zeros(n)
    s2 = zeros(n)
    x = zeros (n)
    mk4 = -k/4
    k2 = k/2
    s2[0] = 1
    s2[1] = 1 + k2
    s1[1] = mk4

    # Leo: Forward elimination
    for i in arange(2,n-1):
        s1[i] = mk4/s2[i-1]
        s2[i] = 1+k2 - mk4*s1[i]
        

    s1[n-1] = 0 
    s2[n-1] = 1
    
    x[0]=b[0]
    for i in arange(1,n):
        x[i] = b[i] - s1[i]*x[i-1]

    # Leo: backward substitution 
    #try:
    x[n-1] = x[n-1]/s2[n-1]
    ii = n-2
    #except ZeroDivisionError:
    #print 'Trysol0 dividing by Zero s2[n-1] n=',n
        
    for i in arange(0,n-2):
        #try: 
        x[ii] = (x[ii]-mk4*x[ii+1])/s2[ii]
        ii = ii - 1
        #except ZeroDivisionError:
        #print'Trisol0 dividing by Zero s2[ii] ii=',ii
        
    #x(1) = x(1)/s2(1);
    #try:
    x[0] = x[0]/s2[0]
    #except ZeroDivisionError:
        #print 'Trisol0 Error dividing by s2[0]==0'

    return x

def trisol1(k,d,b):
    n = size(d)
    # constant: mk2 (minus k divided by 2)
    #   mk2   = -k/2;
    #   s2(1) = 1;
    #   s2(2) = d(2);
    #   s1(2) = mk2;
    s1 = zeros(n)
    s2 = zeros(n)
    x = zeros(n)

    mk2 = -k/2
    s2[0] = 1
    s2[1] = d[1]
    s1[1] = mk2

    # Leo: Forward elimination
    #for i=3:n-1
    #  s1(i) = mk2/s2(i-1);
    #  s2(i) = d(i) - mk2*s1(i);
    #end
    for i in arange(2,n-1):
        s1[i] = mk2/s2[i-1]
        s2[i] = d[i] - mk2*s1[i]
        
    #s1(n) = 0; s2(n) = 1;
    s1[n-1] = 0
    s2[n-1] = 1
    
    #x(1) = b(1);
    #for i=2:n
    #    x(i) = b(i) - s1(i)*x(i-1);
    #end
    x[0]=b[0]
    for i in arange(1,n):
        x[i] = b[i] - s1[i]*x[i-1]

    # Leo: backward substitution 
    #x(n) = x(n)/s2(n); ii = n-1;
    #for i=1:n-2
    #   x(ii) = (x(ii) - mk2*x(ii+1))/s2(ii);
    #   ii    = ii - 1;
    #end
    x[n-1] = x[n-1]/s2[n-1]
    ii = n-2
    for i in arange(0,n-2):
        #try:
        x[ii] = (x[ii]-mk2*x[ii+1])/s2[ii]
        ii = ii - 1
        #except ZeroDivisionError:
        #    print 'Trisol1 dividing by Zero s2[ii] ii=',ii
        
    #x(1) = x(1)/s2(1);
    x[0] = x[0]/s2[0]
    return x

def smooth7(r_map,w_map):
    #Smooths points in NPP map by (2*L-1) x (2*L-1) quadratically
    #weighted window
    L=5
    #s_map = zeros((NX,NY))
    rows, cols = r_map.shape
    s_map = empty((rows,cols))

    for row in range(0,rows):
        for col in range(0,cols):
            if w_map[row,col]>0:      # only do windowing on land points
                wtot = 1.
                summ = r_map[row,col]

                #FIRST do points on cross 
                #Left/Right in X
                for pix_col in range(1,L):
                    colpflg = True #inside map 
                    colmflg = True #inside map 
                    #colp = col + pixel 
                    #isp1=i + ids - 1 # isp1 == 10 = i(8) + ids(3) -1
                    colp = col + pix_col - 1
                    #colm = col + pixel  
                    #ism1=i - ids + 1 #Error ism1 == 10 
                    colm = col - pix_col + 1 

                    #print 'i,j',i,j
                    #print 'ids', ids
                    #print 'isp1',isp1

                    #if isp1>NX: 
                    if colp >= cols:
                        colpflg = False #outside map 
                        
                    #if ism1<1:
                    if colm<0:  
                        colmflg = False #outside map 
                        
                    w_col =(1.-(1.*pix_col/L)**2) # x weighting factor
                    #w_col =(1-(pix_col/L))**2 # x weighting factor

                    #Inside map and mask 
                    if colpflg and w_map[row,colp]>0:
                        wtot = wtot+w_col #accumulating weights
                        summ = summ + w_col*r_map[row,colp] #averaging pixel

                    #Inside map and mask 
                    if colmflg and w_map[row,colm]>0:
                        wtot = wtot+w_col #accumulating weights
                        summ = summ + w_col*r_map[row,colm] #averaging pixel
    
                # Now Bottom/Up in Y-direction
                for pix_row in range(1,L):
                    #print 'js:', js
                    #jsp1=j + js - 1
                    #jsm1=j - js + 1 #Error j(9)-js(0) + 1 == 10
                    #rowp=row + pix_row -1
                    #rowp = row + pixel 
                    rowp=row + pix_row - 1
                    #colm = row - pixel                      
                    rowm=row - pix_row + 1 
 
                    #if jsp1>=NY-1:
                    if rowp>=rows:
                        rowp=rowp-rows # Periodic Leo: this should be for cols  
                        
                    if rowm<0: 
                        rowm=rowm+rows # Perioc Leo: this should be for cols 
                            
                    w_row = (1.-(1.*pix_row/L)**2)  # y weighting factor
                    #w_row =(1-(pix_row/L))**2  # y weighting factor

                    #Inside map and mask 
                    if w_map[rowp,col]>0:
                        wtot = wtot+w_row #accumulating weights
                        summ = summ + w_row*r_map[rowp,col] #averaging pixel 
                    #print 'i,j',i,j
                    #print 'jsm1:',jsm1
                    #Inside map and mask 
                    if w_map[rowm,col]>0: #Error: jsm1== ny, Error axis 1 size 5 
                        wtot = wtot+w_row #Accumulating weights
                        summ = summ + w_row*r_map[rowm,col] #averaging pixel

                # END of points on cross
 
                #   All other points in square [-3,-2,-1,0,1,2,3]
                for pix_col in range(1,L):
                    colpflg = True; 
                    colmflg = True;
                    #isp1=i + ids - 1
                    #ism1=i - ids + 1
                    #colp = col + [0,1,2,3]
                    colp = col + pix_col - 1
                    #colm = col - [0,1,2,3]
                    colm = col - pix_col + 1 

                    #Non Periodic pixels 
                    #Leo: This should be for rows
                    #if isp1>NX:
                    if colp>=cols:
                        colpflg = False
                         
                    #if ism1<1:
                    if colp<0:
                        colmflg = False
                        
                    w_col =(1.-(1.*pix_col/L)**2);  # x weighting factor
                    #w_col =(1-(pix_col/L))**2;  # x weighting factor

                    for pix_row in range(1,L): 
                        #jsp1=j + js - 1
                        #jsm1=j - js + 1
                        #rowp = row + [0,1,2,3]
                        rowp= row + pix_row - 1
                        #rowm = row - [0,1,2,3]
                        rowm= row - pix_row + 1

                        #Periodic 
                        #Leo: this should be for cols
                        #if jsp1>NY-1:
                        if rowp>=rows:
                            rowp=rowp-rows
                            
                        if rowm<0: 
                            rowm=rowm + rows
                            
                        w_row =(1.-(1.*pix_row/L)**2)  # y weighting factor
                        #w_row =(1-(pix_row/L))**2  # y weighting factor
                    
                        if colpflg:   # says isp1 <= NX
                            #Inside map and mask 
                            if w_map[rowm, colp]>0:
                                wt = w_col*w_row #product of weights rowm, colp 
                                wtot = wtot+wt; #summing weights
                                summ = summ + wt*r_map[rowm, colp] #averaging pixel 
                    
                            if w_map[rowp,colp]>0:
                                wt = w_col*w_row #product of weithts rowp, colp 
                                wtot = wtot+wt
                                summ = summ + wt*r_map[rowp,colp] #averaging pixel 

                        if colmflg:   # says ism1 >= 1
                            if w_map[rowm,colm]>0: 
                                wt = w_col*w_row #product of weights rowm, colm
                                wtot = wtot+wt # summing weights
                                summ = summ + wt*r_map[rowm,colm] #averaging pixel 
                       
                            if w_map[rowp,colm]>0: 
                                wt = w_col*w_row #product of weights rowp, colm
                                wtot = wtot+wt; #summing weights
                                summ = summ + wt*r_map[rowp, colm] # averaging pixel 
                       

                s_map[row,col] = summ/wtot # point is land, now averaged
            else:       # template=0
                s_map[row,col] = 0  # point is in water
            # end of center = landpoint if
        # end of j=1:NY loop
    #end of i=1:NX loop

    return s_map

def smooth7New(r_map,w_map):
    #Smooths points in NPP map by (2*L-1) x (2*L-1) quadratically
    #weighted window
    L=5
    #s_map = zeros((NX,NY))
    rows, cols = r_map.shape
    s_map = empty((rows,cols))

    for row in range(0,rows):
        for col in range(0,cols):
            if w_map[row,col]>0:      # only do windowing on land points
                wtot = 1.
                summ = r_map[row,col]

                #FIRST do points on cross 
                #Left/Right in X
                for pix_col in range(1,L):
                    #colp = col + pixel 
                    colp = col + pix_col - 1
                    #colm = col + pixel  
                    colm = col - pix_col + 1 

                    if colp>=cols:
                        colp=colp-cols # Periodic Leo: this should be for cols  
                        
                    if colm<0: 
                        colm=colm+cols # Perioc Leo: this should be for cols 
                    

                    w_col =(1.-(1.*pix_col/L)**2) # x weighting factor

                    #Inside map and mask 
                    if w_map[row,colp]>0:
                        wtot = wtot+w_col #accumulating weights
                        summ = summ + w_col*r_map[row,colp] #averaging pixel

                    #Inside map and mask 
                    if w_map[row,colm]>0:
                        wtot = wtot+w_col #accumulating weights
                        summ = summ + w_col*r_map[row,colm] #averaging pixel
    
                # Now Bottom/Up in Y-direction
                for pix_row in range(1,L):
                    
                    rowpflg = True #inside map 
                    rowmflg = True #inside map
 
                    #rowp = row + pixel 
                    rowp=row + pix_row - 1
                    #colm = row - pixel                      
                    rowm=row - pix_row + 1 

                    #if isp1>NX: 
                    if rowp >= rows:
                        rowpflg = False #outside map 
                        
                    #if ism1<1:
                    if rowm<0:  
                        rowmflg = False #outside map                     
                            
                    w_row = (1.-(1.*pix_row/L)**2)  # y weighting factor

                    #Inside map and mask 
                    if rowpflg and w_map[rowp,col]>0:
                        wtot = wtot+w_row #accumulating weights
                        summ = summ + w_row*r_map[rowp,col] #averaging pixel 
                    #Inside map and mask 
                    if rowmflg and w_map[rowm,col]>0: #Error: jsm1== ny, Error axis 1 size 5 
                        wtot = wtot+w_row #Accumulating weights
                        summ = summ + w_row*r_map[rowm,col] #averaging pixel

                # END of points on cross
 
                #   All other points in square [-3,-2,-1,0,1,2,3]
                for pix_col in range(1,L):
                    #colp = col + [0,1,2,3]
                    colp = col + pix_col - 1
                    #colm = col - [0,1,2,3]
                    colm = col - pix_col + 1 

                    #Non Periodic pixels 
                    #Leo: This should be for rows
                    #if isp1>NX:
                    if colp>=cols:
                        colp=colp-cols 
                        # Periodic Leo: this should be for cols                          
                        #colpflg = False
                         
                    #if ism1<1:
                    if colp<0:
                        colm=colm+cols 
                        # Perioc Leo: this should be for cols 
                        #colmflg = False
                        
                    w_col =(1.-(1.*pix_col/L)**2);  # x weighting factor
                    #w_col =(1-(pix_col/L))**2;  # x weighting factor

                    for pix_row in range(1,L):
                        rowpflg = True; 
                        rowmflg = True;
 
                        #rowp = row + [0,1,2,3]
                        rowp= row + pix_row - 1
                        #rowm = row - [0,1,2,3]
                        rowm= row - pix_row + 1

                        #Periodic 
                        #Leo: this should be for cols
                        #if jsp1>NY-1:
                        if rowp>=cols:
                            rowpflg = False
                            #colp=colp-cols
                            
                        if rowm<0: 
                            rowpflg = False
                            #colm=colm + cols
                            
                        w_row =(1.-(1.*pix_row/L)**2)  # y weighting factor
                        #w_row =(1-(pix_row/L))**2  # y weighting factor
                    
                        if rowpflg:   # says isp1 <= NX
                            #Inside map and mask 
                            if w_map[rowp, colm]>0:
                                wt = w_col*w_row #product of weights rowm, colp 
                                wtot = wtot+wt; #summing weights
                                summ = summ + wt*r_map[rowp, colm] #averaging pixel 
                    
                            if w_map[rowp,colp]>0:
                                wt = w_col*w_row #product of weithts rowp, colp 
                                wtot = wtot+wt
                                summ = summ + wt*r_map[rowp,colp] #averaging pixel 

                        if rowmflg:   # says ism1 >= 1
                            if w_map[rowm,colm]>0: 
                                wt = w_col*w_row #product of weights rowm, colm
                                wtot = wtot+wt # summing weights
                                summ = summ + wt*r_map[rowm,colm] #averaging pixel 
                       
                            if w_map[rowm,colp]>0: 
                                wt = w_col*w_row #product of weights rowp, colm
                                wtot = wtot+wt; #summing weights
                                summ = summ + wt*r_map[rowm, colp] # averaging pixel 
                       

                s_map[row,col] = summ/wtot # point is land, now averaged
            else:       # template=0
                s_map[row,col] = 0  # point is in water
            # end of center = landpoint if
        # end of j=1:NY loop
    #end of i=1:NX loop

    return s_map
