!Create Signature: f2pyA -m CmodulesNeumann1 -h  CmodulesNeumann1.pyf CmodulesNeumann1.f90 --overwrite-signature

subroutine trisol0neumann(np2, k, b, x)
!f2py intent(c) trisol0neumann
  implicit none
  integer :: np2, i, ii
  real(8) :: k, b(np2), x(np2)

!f2py real(8), intent(out), dimension(n) :: x
!f2py real(8), intent(c), dimension(n) :: b
!f2py real(8), intent(c) :: k
!f2py integer, intent(c) :: np2

end subroutine trisol0neumann

subroutine trisol1neumann(np2, k, d, b, x)
!f2py intent(c) trisol1neumann
  implicit none
  integer :: np2
  real(8) :: k, d(np2), b(np2), x(np2)

!f2py real(8), intent(out), dimension(np2) :: x
!f2py real(8), intent(c), dimension(np2) :: d
!f2py real(8), intent(c), dimension(np2) :: b
!f2py real(8), intent(c) :: k
!f2py integer, intent(c) :: np2

end subroutine trisol1neumann

subroutine godunovstep1(np2, h, kcfl, u, v)
!f2py intent(c) godunovstep1
  implicit none
  integer :: np2
  real(8) :: h, kcfl, u(np2), v(np2)

!f2py integer, intent(c) :: np2
!f2py real(8), intent(c) :: h
!f2py real(8), intent(c) :: kcfl
!f2py real(8), intent(c), dimension(n) :: u
!f2py real(8), intent(out), dimension(n) :: v

end subroutine godunovstep1

subroutine godunovstep2(np2, h, k, u, kap0, kaph, v)
!f2py intent(c) godunovstep2
  implicit none
  integer :: np2
  real(8) :: h, k, u(np2), kap0(np2), kaph(np2), v(np2)

!f2py integer, intent(c) :: np2
!f2py real(8), intent(c) :: h
!f2py real(8), intent(c) :: k
!f2py real(8), intent(c), dimension(np2) :: u
!f2py real(8), intent(c), dimension(np2) :: kap0
!f2py real(8), intent(c), dimension(np2) :: kaph
!f2py real(8), intent(out), dimension(np2) :: v

end subroutine godunovstep2

subroutine test(nx, ny, r_map, s_map)
!f2py intent(c) test

  implicit none
  integer :: nx, ny 
  real(8) :: r_map(nx,ny), s_map(nx,ny)

!f2py integer, intent(c) :: nx
!f2py integer, intent(c) :: ny
!f2py real(8), intent(c), dimension(nx,ny) :: r_map
!f2py real(8), intent(out), dimension(nx,ny) :: s_map

  s_map = r_map

end subroutine test

subroutine smooth7(nx, ny, r_map, w_map, s_map)
!f2py intent(c) smooth7
  implicit none
  logical :: colpflg, colmflg
  integer :: nx, ny, col, row, L, w_map(nx,ny), rowp, rowm, pix_row, pix_col, colp, colm
  real(8) :: r_map(nx,ny), s_map(nx,ny), wtot, w_col, w_row, summ

!f2py integer, intent(c) :: nx
!f2py integer, intent(c) :: ny
!f2py real(8), intent(c), dimension(nx,ny) :: r_map
!f2py integer, intent(c), dimension(nx,ny) :: w_map
!f2py real(8), intent(out), dimension(nx,ny) :: s_map


end subroutine smooth7

subroutine godunovstepneumann2nd(np2, h, k, dx, u, v)
!f2py intent(c) godunovstepneumann2nd
  implicit none
  integer :: np2
  real(8) :: h, k, dx, u(np2), v(np2)

!f2py integer, intent(c) :: np2
!f2py real(8), intent(c) :: h
!f2py real(8), intent(c) :: k
!f2py real(8), intent(c) :: dx
!f2py real(8), intent(c), dimension(np2) :: u
!f2py real(8), intent(out), dimension(np2) :: v

end subroutine godunovstepneumann2nd

subroutine trisol2neumann(np2, k, dx, d, b, x)
!f2py intent(c) trisol2neumann
  implicit none
  integer :: np2
  real(8) :: k, dx, d(np2), b(np2), x(np2), mk2


!f2py real(8), intent(out), dimension(np2) :: x
!f2py real(8), intent(c), dimension(np2) :: d
!f2py real(8), intent(c), dimension(np2) :: b
!f2py real(8), intent(c) :: k
!f2py real(8), intent(c) :: dx
!f2py integer, intent(c) :: np2

end subroutine trisol2neumann

subroutine amultneumann(np2, u, v)
!f2py intent(c) amultneumann
  implicit none
  integer :: np2
  real(8) :: u(np2), v(np2)

!f2py real(8), intent(out), dimension(np2) :: v
!f2py real(8), intent(c), dimension(np2) :: u
!f2py integer, intent(c) :: np2

end subroutine amultneumann


subroutine amult2neumann(np2, u, v, dx)
!f2py intent(c) amult2neumann
  implicit none
  integer :: np2
  real(8) :: u(np2), v(np2), dx

!f2py real(8), intent(out), dimension(np2) :: v
!f2py real(8), intent(c), dimension(np2) :: u
!f2py integer, intent(c) :: np2
!f2py real(8), intent(c) :: dx

end subroutine amult2neumann

subroutine amult3neumann(n, u, v)
!f2py intent(c) amult3neumann
  implicit none
  integer :: n
  real(8) :: u(n), v(n)

!f2py real(8), intent(out), dimension(n) :: v
!f2py real(8), intent(c), dimension(n) :: u
!f2py integer, intent(c) :: n

end subroutine amult3neumann

subroutine trisol3neumann(n, k, d, b, x)
!f2py intent(c) trisol3neumann
  implicit none
  integer :: n
  real(8) :: k, d(n), b(n), s1(n), s2(n), x(n), mk2, k2

!f2py real(8), intent(out), dimension(n) :: x
!f2py real(8), intent(c), dimension(n) :: d
!f2py real(8), intent(c), dimension(n) :: b
!f2py real(8), intent(c) :: k
!f2py integer, intent(c) :: n

end subroutine trisol3neumann

subroutine godunovstepneumann3rd(n, h, k, u, v)
!f2py intent(c) godunovstepneumann3rd
  implicit none
  integer :: n
  real(8) :: h, k, u(n), v(n)

!f2py integer, intent(c) :: n
!f2py real(8), intent(c) :: h
!f2py real(8), intent(c) :: k
!f2py real(8), intent(c), dimension(n) :: u
!f2py real(8), intent(out), dimension(n) :: v

end subroutine godunovstepneumann3rd

