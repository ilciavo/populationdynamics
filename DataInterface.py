################################################################################
# Data interface and concrete implementation classes
################################################################################

class Data:
    def __init__(self):
        pass

class SmallData(Data):
    def __init__(self, w_map, u, h, kcfl, NT, NPLT , NX, NY, Ts, Te, tsc, mapsize):
        self.w_map = w_map
        self.u = u
        self.h = h 
        self.kcfl = kcfl
        self.NT = NT
        self.NPLT = NPLT
        self.NX = NX
        self.NY = NY 
        self.Ts = Ts
        self.Te = Te
        self.tsc = tsc
        self.mapsize = mapsize

class BigData(Data):
    pass 

class LargeData(Data):
    pass



