from numpy import *
import scipy.signal

#Functions for Neumann boundary conditions
def amult2Neumann(u,dx):
    # d^2u/dx^2=1/(dx**2)*Au
    # it performs tridiagonal matrix multiply, returns n elements of v
    # with v(1) = v(n) = , the others are u(i-1)+u(i+1)-2*u(i)
    #   v(1) = 0; v(n) = ;
    #   v(2:n-1) = u(1:n-2)+u(3:n)-2*u(2:n-1);

    #Wes
    #v = zeros(n)
    #Wes:Dirichlet
    #v[0] = 0; v[-1] = 0;
    #Wes:Neumann
    #v[0] = -2*u[0]+ u[1]
    #v[-1] = -2*u[-1]+u[-2]

    #Leo: using second approach by LeVeque using 2 ghost cells  
    np2 = size(u)
    n = np2-2
    v = zeros(n+2)
    v[0] = -dx*u[0]/2+dx*u[1]/2 #Sigma= 0
    v[-1] =  -dx*u[-2]/2+dx*u[-1]/2 #Sigma= 0 
    #v[0] = -u[0]/(2*dx)+u[1]/(2*dx) #Sigma = 0
    #v[-1] =  -u[-2]/(2*dx)+u[-1]/(2*dx) #Sigma = 0 
    
    v[1:-1] = u[0:-2]-2*u[1:-1]+u[2:]
    return v

#  Tridiagonal system solver modeled on
#  Using Thomas algorithm 
#  Tridiagonal system is Tx=b, where
#     [-1/(2dx) 1/(2dx)     0         .... 0]
# T = [-k/2     d(1)    -k/2 0        .... 0]
#     [0        -k/2    d(2)          .... 0]
#     [0        0                     .... 0]
#     [0                                   0]
#     [0                -k/2    d(n-1)  -k/2]
#     [0                        -1/(2dx) 1/2dx]

# and k = h/dx^2 is the CFL factor. Diagonals
# d = (1 d(2) d(3) ...  d(n-1) 1) are strong.
# cp and dp are working arrays.
# In the original F & M notation,
#
#  T = [d(1)  f(1)  0        ...             0 ]
#      [e(2)  d(2)  f(2)  0  ...             0 ]
#      [ 0          ....                     0 ]
#      [ 0                e(n-1) d(n-1) f(n-1) ]
#      [ 0                  0    e(n)   d(n)   ]
#
#  wpp 27 May 2012
def trisol2Neumann(k,dx,d,b):
    np2 = size(d)
    cp = zeros(np2)
    dp = zeros(np2)
    n = np2-2
    # constant: mk2 (minus k divided by 2)
    x = zeros (n+2)
    mk2 = -k/2
    #m2dx = -1/(2*dx)
    m2dx = -dx/2
    
    #cp[0] = 0.
    #cp[0]=(1/(2*dx))/(-1/(2*dx))
    cp[0] = -1.
    
    # Leo: Forward elimination
    for i in range(1,n+1): #from 1 to n
        #print(d[i])
        #print(cp[i-1])
        cp[i]=mk2/(d[i]-mk2*cp[i-1])
        #print(cp[i])
                
    dp[0]=b[0]/(m2dx)
    
    for i in range(1,n+1):
        #print(dp[i-1])
        dp[i]=(b[i]-mk2*dp[i-1])/(d[i]-mk2*cp[i-1])

    dp[n+1]=(b[n+1]-m2dx*dp[n])/(d[n+1]-m2dx*cp[n])
    x[n+1]=dp[n+1]
    
    # Leo: backward substitution 
    for i in range(n,-1,-1):
        #print(i)
        #print(i)
        #print(dp[i])
        #print(cp[i])
        #print(x[i+1])
        x[i]=dp[i]-cp[i]*x[i+1]
    
    return x;

def godunovstepNeumann2nd(h,k,dx,u):
    np2 = size(u)
    
    n = np2-2
    # first compute Euler estimate
    #ut = (Amult(n,u))'
    #ut = k*ut
    #uE = ut + h*(1-u).*u # commmon part of Euler est.
    ut = amult2Neumann(u,dx) #returns n+2 vector
    ut = k*ut 
    
    uE = ut + h*(1-u)*u
    
    # compute right hand side
    #ur = u + 0.5*uE #RHS
    #uE = u + uE #Euler est.
    #dA(1) = 1; dA(n) = 1 #end points
    ur = u + 0.5*uE #RHS
    uE = u + uE #Euler estimate
    
    #Leo: Impose second aproach
    #ur[-1] = u [1] 
    #Neumann bc
    sigma = 0.
    #sigma = 1.
    ur[0]=sigma 
    ur[-1]=-sigma
    
    #Neumann boundary conditions
    dA = zeros(n+2)
    dA[0] = -dx/2
    dA[n+1] = dx/2 #end points
    #dA[0] = -1/(2*dx) 
    #dA[n+1] = 1/(2*dx) #end points

    # interior diags. of LHS matrix:
    #dA(2:n-1) = 1 + k - 0.5*h*(1-uE(2:n-1));
    #dA[1:n-2] = 1 + k - 0.5*h*(1-uE[1:n-2])
    dA[1:-1] = 1 + k - 0.5*h*(1-uE[1:-1])
    
    #print(dA)
   
    # solve using Forsythe-Moler
    #v  = trisol(n,k,dA,ur,s1,s2);
    v = trisol2Neumann(k,dx,dA,ur)
    #v = modules.trisol(k,dA,ur)
    #v = Cmodules.trisol(k,dA,ur)
    return v

def godunovstep1Neumann2nd(h,k,kcfl,u, dx):

    return v

def godunovstep2Neumann2nd():
    
    return v

def amult3Neumann(u):
    # d^2u/dx^2=1/(dx**2)*Au
    # it performs tridiagonal matrix multiply, returns n elements of v
    # with v(1) = v(n) = , the others are u(i-1)+u(i+1)-2*u(i)
    #   v(1) = 0; v(n) = ;
    #   v(2:n-1) = u(1:n-2)+u(3:n)-2*u(2:n-1);

    #Wes
    #v = zeros(n)
    #Wes:Dirichlet
    #v[0] = 0; v[-1] = 0;
    #Wes:Neumann
    #v[0] = -2*u[0]+ u[1]
    #v[-1] = -2*u[-1]+u[-2]

    #Leo: same as wesley
    n = size(u)
    v = zeros(n)
    v[0] = -2.*u[0]+u[1]
    v[-1] = u[n-2]-2.*u[n-1]
    #v[0] = -dx*u[0]/2+dx*u[1]/2 # Sigma =0
    #v[-1] =  -dx*u[-2]/2+dx*u[-1]/2 #Sigma=0 
    #v[0] = -u[0]/(2*dx)+u[1]/(2*dx) #Sigma=0
    #v[-1] =  -u[-2]/(2*dx)+u[-1]/(2*dx) #Sigma=0 
    
    v[1:-1] = u[0:-2]-2*u[1:-1]+u[2:]
    return v

def trisol3Neumann(k,d,b):
    # constant: mk2 (minus k divided by 2)
    #   mk2   = -k/2;
    #   s2(1) = 1;
    #   s2(2) = d(2);
    #   s1(2) = mk2;
    n = size(d)
    x = zeros (n)
    s1 = zeros(n)
    s2 = zeros(n)
    mk2 = -k/2

    
    #Neumann
    s2[0] = d[0]
    s1[1] = mk2/s2[0]
    s2[1] = d[1]+4.*s1[1]/3.
    #s1[2] = mk2/s2[0]
    #s2[1] = d[1]+4.*s1[2]/3.

    #print('S2',0,s2[0])
    #print('S1',1,s1[1])
    #print('S2',1,s2[1])
    #s2[0] = -1/(2*dx)



    # Leo: Forward elimination
    #for i=3:n-1
    #  s1(i) = mk2/s2(i-1);
    #  s2(i) = d(i) - mk2*s1(i);
    #end
    for i in arange(2,n-1):
        s1[i] = mk2/s2[i-1];
        s2[i] = d[i] - mk2*s1[i]
        #print('S1',i,s1[i])
        #print('S2',i,s1[i])
    
    #Neumann
    #s1(n) = 0; s2(n) = 1;
    s1[n-1] = -4./(3*s2[n-2])
    s2[n-1] = d[n-1]-mk2*s1[n-1];
    
    #x(1) = b(1);
    #for i=2:n
    #    x(i) = b(i) - s1(i)*x(i-1);
    #end
    x[0]=b[0]
    for i in arange(1,n):
        x[i] = b[i] - s1[i]*x[i-1]

    # Leo: backward substitution 
    #x(n) = x(n)/s2(n); ii = n-1;
    #for i=1:n-2
    #   x(ii) = (x(ii) - mk2*x(ii+1))/s2(ii);
    #   ii    = ii - 1;
    #end
    x[n-1] = x[n-1]/s2[n-1]
    ii = n-2
    for i in arange(0,n-2):
        x[ii] = (x[ii]-mk2*x[ii+1])/s2[ii];
        ii = ii - 1;
    
    #x(1) = x(1)/s2(1);
    #x[0] = x[0]/s2[0];
    return x;

def godunovstepNeumann3rd(h,k,u):
    # first compute Euler estimate
    #ut = (Amult(n,u))'
    #ut = k*ut
    #uE = ut + h*(1-u).*u # commmon part of Euler est.
    n = size(u)
    ut = amult3Neumann(u)
    ut = k*ut
    uE = ut + h*(1-u)*u
    
    # compute right hand side
    #ur = u + 0.5*uE #RHS
    #uE = u + uE #Euler est.
    #dA(1) = 1; dA(n) = 1 #end points
    ur = u + 0.5*uE #RHS
    uE = u + uE #Euler estimate
    #print('uE', uE)
    
    #Leo: Code from Wesley 3rd approach 
    # impose Neumann BCs on Euler estimate
    uE[0] = 4.*uE[1]/3. - uE[2]/3.
    uE[-1] = 4.*uE[-2]/3. - uE[-3]/3.
    #print('uE[0]',uE[0])
    #print('uE[-1]',uE[-1])
    
    #reset end points for explicit estimates for BCs
    #ur(1) = ur(1) - k*uE(1) + 0.5*k*uE(2) + 0.5*h*uE(1)*(1-uE(1)); 
    #Estimate using trapezoidal rule for u[2] eq.5 
    #u[2] + 1/2(A*uE[2]+ A*u[2]) + 1/2*(f(uE[2])+u[2])
    uest = u[2]+0.5*k*(uE[1]-2*uE[2]+uE[3]) \
            +0.5*k*(u[1]-2*u[2]+u[3]) \
            +0.5*h*((1-uE[2])*uE[2]+(1-u[2])*u[2]) #error here 
    ur[0] = -uest/3
    
    #print(ur[0])
    
    uest = u[n-3]+0.5*k*(uE[n-4]-2*uE[n-3]+uE[n-2]) \
            +0.5*k*(u[n-4]-2*u[n-3]+u[n-2]) \
            +0.5*h*((1-uE[n-3])*uE[n-3]+(1-u[n-3])*u[n-3]) #Leo: error here be careful
    
    ur[n-1] = -uest/3.
    
    #print(ur[n-1])
    
    #ur[0] = ur[0] + k*uE[0] - 0.5*k*uE[1] + 0.5*h*uE[0]*(1-uE[0]); 
    #ur(n) = ur(n) - k*uE(n) + 0.5*k*uE(n-1) + 0.5*h*uE(n)*(1-uE(n)); 
    #ur[-1] = ur[-1] + k*uE[-1] - 0.5*k*uE[-2] + 0.5*h*uE[-1]*(1-uE[-1]); 

    #Leo: Impose second aproach
    #ur[-1] = u [1] 
    
    dA = zeros(n)
    dA[0] = 1 
    dA[n-1] = 1 #end points

    # interior diags. of LHS matrix:
    #dA(2:n-1) = 1 + k - 0.5*h*(1-uE(2:n-1));
    #dA[1:n-2] = 1 + k - 0.5*h*(1-uE[1:n-2])
    dA[1:n-1] = 1 + k - 0.5*h*(1-uE[1:n-1]) #normal stuff for inner values

    #print('dA:',dA)
   
    
    # solve using Forsythe-Moler
    #v  = trisol(n,k,dA,ur,s1,s2);
    v = trisol3Neumann(k,dA,ur)
    #v = modules.trisol(k,dA,ur)
    #v = Cmodules.trisol(k,dA,ur)
    #reset v[0] and v[n-1]
    #Imposed b.c. on the output 
    v[0]=4.*v[1]/3.-v[2]/3.
    v[n-1]=4.*v[n-2]/3.-v[n-3]/3.
    return v

def amultNeumann(u):
    n = size(u)
    v = zeros(n)
    v[0] = -2.*u[0]+ u[1] 
    v[-1] = -2.*u[n-1] + u[n-2]
    v[1:-1] = u[0:-2]+u[2:n]-2*u[1:-1]
    return v

def trisol0Neumann(k,b):
#  Tridiagonal system solver modeled on
#  G.E. Forsythe and C.B. Moler " Computer 
#  solutions to linear algebraic systems,"
#  Prentice-Hall publ., 1967
#  Tridiagonal system is Tx=b, where
#
#  T = [1        -4/3   0    ....     0 ]
#      [-k/4 1+k/2 -k/4 ....          0 ]
#      [0    -k/4 1+k/2 -k/4  0 ....  0 ]
#      [0           ....              0 ]
#      [0               -k/4 1+k/2  -k/4]
#      [0                 0    -4/3   1 ]
# 
# and k = h/dx^2 is the CFL factor. Diagonals
# d = (1 d(2) d(3) ...  d(n-1) 1) are strong.
# s1 and s2 are work arrays.(Leo: s2 new diagonal, s1 new off-diagonal)  
# In the original F & M notation,
#
#  T = [d(1)  f(1)  0        ...             0 ]
#      [e(2)  d(2)  f(2)  0  ...             0 ]
#      [ 0          ....                     0 ]
#      [ 0                e(n-1) d(n-1) f(n-1) ]
#      [ 0                  0    e(n)   d(n)   ]
#
#  wpp 27 May 2012
# def trisol(n,k,d,b,s1,s2):
    n = size(b)
    s1 = zeros(n)
    s2 = zeros(n)
    x = zeros (n)
    #Neumann boundary conditions
    mk4 = -k/4
    k2 = k/2
    s2[0] = 1.
    s1[1] = mk4
    s2[1] = 1. + k2 + 4.*s1[1]/3.
    
    # Leo: Forward elimination
    for i in arange(2,n-1):
        s1[i] = mk4/s2[i-1]
        s2[i] = 1+k2 - mk4*s1[i]
        

    s1[n-1] = -4./(3.*s2[n-2]) 
    s2[n-1] = 1-mk4*s1[n-1]
    
    x[0]=b[0]
    for i in arange(1,n):
        x[i] = b[i] - s1[i]*x[i-1]

    # Leo: backward substitution 
    #try:
    x[n-1] = x[n-1]/s2[n-1]
    ii = n-2
    #except ZeroDivisionError:
    #print 'Trysol0 dividing by Zero s2[n-1] n=',n
        
    for i in arange(0,n-2):
        #try: 
        x[ii] = (x[ii]-mk4*x[ii+1])/s2[ii]
        ii = ii - 1
        #if (x[ii]<0):
        #    raise ValueError('x less than 0 ')

        #except ZeroDivisionError:
        #print'Trisol0 dividing by Zero s2[ii] ii=',ii
        
    #x(1) = x(1)/s2(1);
    #try:
    #x[0] = x[0]/s2[0]
    #except ZeroDivisionError:
        #print 'Trisol0 Error dividing by s2[0]==0'
    x[0]=4*x[1]/3 - x[2] # this are water cells, sometimes we get negative values
    x[n-1]=4*x[n-1]/3-x[n-2]/3 # these are water cells, but sometimes we get negative values 

    return x

def trisol1Neumann(k,d,b):
    n = size(d)
    # constant: mk2 (minus k divided by 2)
    #   mk2   = -k/2;
    #   s2(1) = 1;
    #   s2(2) = d(2);
    #   s1(2) = mk2;
    s1 = zeros(n)
    s2 = zeros(n)
    x = zeros(n)

    mk2 = -k/2
    s2[0] = d[0]
    s1[1] = mk2/s2[0]
    s2[1] = d[1] + 4.*s1[1]/3. 

    # Leo: Forward elimination
    #for i=3:n-1
    #  s1(i) = mk2/s2(i-1);
    #  s2(i) = d(i) - mk2*s1(i);
    #end
    for i in arange(2,n-1):
        s1[i] = mk2/s2[i-1]
        s2[i] = d[i] - mk2*s1[i]
        
    #s1(n) = 0; s2(n) = 1;
    s1[n-1] = -4./(3.*s2[n-2])
    s2[n-1] = d[n-1] - mk2*s1[n-1]
    
    #x(1) = b(1);
    #for i=2:n
    #    x(i) = b(i) - s1(i)*x(i-1);
    #end
    x[0]=b[0]
    for i in arange(1,n):
        x[i] = b[i] - s1[i]*x[i-1]

    # Leo: backward substitution 
    #x(n) = x(n)/s2(n); ii = n-1;
    #for i=1:n-2
    #   x(ii) = (x(ii) - mk2*x(ii+1))/s2(ii);
    #   ii    = ii - 1;
    #end
    x[n-1] = x[n-1]/s2[n-1]
    ii = n-2
    for i in arange(0,n-2):
        #try:
        x[ii] = (x[ii]-mk2*x[ii+1])/s2[ii]
        ii = ii - 1
        #if (x[ii]<0):
        #    raise ValueError('x less than 0 ')
        #except ZeroDivisionError:
        #    print 'Trisol1 dividing by Zero s2[ii] ii=',ii
        
    #x(1) = x(1)/s2(1);
    #x[0] = x[0]/s2[0]

    x[0]=4*x[1]/3 - x[2]
    x[n-1]=4*x[n-1]/3-x[n-2]/3

    return x    

def godunovstep1(h,kcfl,u):
    # first compute Euler estimate
    #ut = (Amult(n,u))'
    #ut = k*ut 
    #uE = ut + h*(1-u).*u # commmon part of Euler est.
    n = size(u)
    kcfl4= kcfl/4.
    kcfl2= kcfl/2.
    #ut = Amult(n,u)
    ut = amultNeumann(u)
    
    rhs = u[0:n]+ kcfl4*ut
    
    #Euler estimate for u[2]
    if n>3: 
        uE = u[2] + kcfl2*(u[1]-2*u[2]+u[3]) #index 3 out of bounds 
    else:
        uE = u[2] + kcfl2*(u[1]-2*u[2])

    uest = u[2] + kcfl4*(uE+u[2]) #Eq 8a
    #Wes writes:
    #rhs[0] = -uest/4.

    #Leo: correction 
    rhs[0] = -uest/3

    #if (rhs[n-2]<0):
    #    raise ValueError('rhs less than 0 ')
    
    #Euler estimate for u[n-2]
    #Leo: error for one single pixel
    if n>3:
        uE = u[n-3] + kcfl2*(u[n-4] -2.*u[n-3]+ u[n-2])
    else: 
        uE = u[n-3] + kcfl2*(-2.*u[n-3]+ u[n-2])

    uest = u[n-3] + kcfl4*(uE+u[n-3])

    #rhs[n-1] = -uest/4.
    #Leo: correction 
    rhs[n-1] = -uest/3.

    #if (rhs[n-1]<0):
    #    raise ValueError('rhs less than 0 ')
 

    #solve using tridiag solver 
    #v = trisol0(n,kcfl,rhs,s1,s2)
    v = trisol0Neumann(kcfl, rhs)

    return v

def godunovstep2(h,k,u,kap0,kaph):
    #kap0 : carrying capacity low 
    #kaph : carrying capacity high
    # first compute Euler estimate
    #ut = (Amult(n,u))'
    #ut = k*ut 
    #uE = ut + h*(1-u).*u # commmon part of Euler est.
    #ut = Amult(n,u)
    #Wes: include regularizer tanh(x.^4).^(1/4)
    n = size(u)
    ut = amultNeumann(u)
    ut = k*ut
    #try:
    #right hand side part for part of Euler uses K(t)
    uC = u[0:n]/kap0[0:n]
    #except ZeroErrorDivision:
    #    print 'godunovstep2 divides by Zero kap0[0:n]', kap0[0:n]

    #try:
    #left hand side version uses K(t+h)
    uh = u[0:n]/kaph[0:n]
    #except ZeroErrorDivision:
    #print 'godunov2 divides by Zero kaph[0:n]', kap0[0:n]


    #common part of Euler estimate 
    uE = ut + h*(1.-uC)*u[0:n]
    #Compute right hand side 
    ur = u[0:n]+0.5*uE #RHS
    uE = u[0:n]+uE #Euler estimate
    
    #Now comes Neumann over Euler estimate 
    uE[0] = 4.*uE[1]/3. - u[2]/3.
    uE[n-1] =  4.*uE[n-2]/3. - u[n-3]/3.
   
    #Explitic T.R (not regularized estimate for u[2]) in RHS
    #reL = (1./h)*reg(
    #reH = (1./h)*reg(

    kap = 0.5*(kap0[2] + kaph[2])
    #Leo: fixing bug for one single pixel
    if n>3:
        uest = u[2]+0.5*k*(uE[1]-2*uE[2]+uE[3]) \
                +0.5*k*(u[1]-2*u[2]+u[3]) \
                +0.5*h*((1-uE[2]/kap)*uE[2]+(1-u[2]/kap)*u[2]) #error here
    else: 
        uest =  u[2]+0.5*k*(uE[1]-2*uE[2]) \
                +0.5*k*(u[1]-2*u[2]) \
                +0.5*h*((1-uE[2]/kap)*uE[2]+(1-u[2]/kap)*u[2]) #error here

    ur[0] = -uest/3. #wesley rites uest/3
    
    #print(ur[0])
    #not regularized with tanh
    #We take the average kap
    kap = 0.5*(kap0[n-3] + kaph[n-3])
    if n>3:
        uest = u[n-3]+0.5*k*(uE[n-4]-2*uE[n-3]+uE[n-2]) \
                +0.5*k*(u[n-4]-2*u[n-3]+u[n-2]) \
                +0.5*h*((1-uE[n-3]/kap)*uE[n-3]+(1-u[n-3]/kap)*u[n-3]) #Leo: error here be careful
    else:
        uest = u[n-3]+0.5*k*(-2*uE[n-3]+uE[n-2]) \
                +0.5*k*(-2*u[n-3]+u[n-2]) \
                +0.5*h*((1-uE[n-3]/kap)*uE[n-3]+(1-u[n-3]/kap)*u[n-3]) #Leo: error here be careful

    ur[n-1] = -uest/3. #wesley writes uest/3
 
    #dA = zeros(n)
    dA = empty(n)
    dA[0] = 1 
    dA[n-1] = 1 #end points
    # interior diags. of LHS matrix:
    dA[1:n-1] = 1 + k - 0.5*h*(1-uh[1:n-1])
 
    #solve using tridiag solver 
    #v = trisol1(n,k,dA,ur,s1,s2)
    v = trisol1Neumann(k,dA,ur)
 
    return v


def smooth7(r_map,w_map):
    #Smooths points in NPP map by (2*L-1) x (2*L-1) quadratically
    #weighted window
    L=5
    #s_map = zeros((NX,NY))
    NX, NY = r_map.shape
    s_map = empty((NX,NY))
    
    for col in range(0,NX):
        for row in range(0,NY):
            if w_map[col,row]>0:      # only do windowing on land points
                wtot = 1.
                summ = r_map[col,row]

                #FIRST do points on cross 
                #Up/down on X
                for pix_col in range(1,L):
                    colpflg = True
                    colmflg = True
                    #isp1=i + ids - 1 # isp1 == 10 = i(8) + ids(3) -1
                    colp = col + pix_col - 1
                    #ism1=i - ids + 1 #Error ism1 == 10 
                    colm = col - pix_col + 1 

                    #print 'i,j',i,j
                    #print 'ids', ids
                    #print 'isp1',isp1

                    #if isp1>NX: 
                    if colp >= NX:
                        colpflg = False
                        
                    #if ism1<1:
                    if colm<0:  
                        colmflg = False
                        
                    w_col =(1.-(1.*pix_col/L)**2) # x weighting factor
                    #w_col =(1-(pix_col/L))**2 # x weighting factor


                    if colpflg and w_map[colp,row]>0:
                        wtot = wtot+w_col
                        summ = summ + w_col*r_map[colp,row]

                    if colmflg and w_map[colm,row]>0:
                        wtot = wtot+w_col;
                        summ = summ + w_col*r_map[colm,row]
    
                # Now left/right in Y-direction
                for pix_row in range(1,L):
                    #print 'js:', js
                    #jsp1=j + js - 1
                    #jsm1=j - js + 1 #Error j(9)-js(0) + 1 == 10
                    #rowp=row + pix_row -1
                    rowp=row + pix_row - 1 
                    rowm=row - pix_row + 1 
 
                    #if jsp1>=NY-1:
                    if rowp>=NY:
                        rowp=rowp-NY
                        
                    if rowm<0: 
                        rowm=rowm+NY
                            
                    w_row = (1.-(1.*pix_row/L)**2)  # y weighting factor
                    #w_row =(1-(pix_row/L))**2  # y weighting factor

                    if w_map[col,rowp]>0:
                        wtot = wtot+w_row
                        summ = summ + w_row*r_map[col,rowp]
                    #print 'i,j',i,j
                    #print 'jsm1:',jsm1
                    if w_map[col,rowm]>0: #Error: jsm1== ny, Error axis 1 size 5 
                        wtot = wtot+w_row
                        summ = summ + w_row*r_map[col,rowm]

                # END of points on cross
 
                #   All other points in square
                for pix_col in range(1,L):
                    colpflg = True; 
                    colmflg = True;
                    #isp1=i + ids - 1
                    #ism1=i - ids + 1
                    colp = col + pix_col - 1
                    colm = col - pix_col + 1 

                    #if isp1>NX:
                    if colp>=NX:
                        colpflg = False
                         
                    #if ism1<1:
                    if colp<0:
                        colmflg = False
                        
                    w_col =(1.-(1.*pix_col/L)**2);  # x weighting factor
                    #w_col =(1-(pix_col/L))**2;  # x weighting factor

                    for pix_row in range(1,L): 
                        #jsp1=j + js - 1
                        #jsm1=j - js + 1
                        rowp= row + pix_row - 1
                        rowm= row - pix_row + 1

                        #if jsp1>NY-1:
                        if rowp>=NY:
                            rowp=rowp-NY
                            
                        if rowm<0: 
                            rowm=rowm + NY
                            
                        w_row =(1.-(1.*pix_row/L)**2)  # y weighting factor
                        #w_row =(1-(pix_row/L))**2  # y weighting factor
                    
                        if colpflg:   # says isp1 <= NX
                            if w_map[colp,rowm]>0:
                                wt = w_col*w_row
                                wtot = wtot+wt;
                                summ = summ + wt*r_map[colp,rowm]
                    
                            if w_map[colp,rowp]>0:
                                wt = w_col*w_row
                                wtot = wtot+wt
                                summ = summ + wt*r_map[colp,rowp]

                        if colmflg:   # says ism1 >= 1
                            if w_map[colm,rowm]>0:
                                wt = w_col*w_row
                                wtot = wtot+wt
                                summ = summ + wt*r_map[colm,rowm]
                       
                            if w_map[colm,rowp]>0:
                                wt = w_col*w_row
                                wtot = wtot+wt;
                                summ = summ + wt*r_map[colm,rowp]
                       

                s_map[col,row] = summ/wtot # point is land, now averaged
            else:       # template=0
                s_map[col,row] = 0  # point is in water
            # end of center = landpoint if
        # end of j=1:NY loop
    #end of i=1:NX loop

    return s_map

def sciSmooth(r_map,mask):    
    # make some kind of kernel, there are many ways to do this...
    t = 1 - abs(linspace(-1, 1, 7))
    kernel = t.reshape(7, 1) * t.reshape(1, 7)
    kernel /= kernel.sum()   # kernel should sum to 1!  :) 
    
    # convolve 2d the kernel with each channel
    s_map = scipy.signal.convolve2d(r_map, kernel, mode='same')
    s_map = s_map*mask 
    
    return s_map
