from numpy import *
import scipy.signal

#Functions for Neumann boundary conditions
def amult2Neumann(u):
    # d^2u/dx^2=1/(dx**2)*Au
    # it performs tridiagonal matrix multiply, returns n elements of v
    # with v(1) = v(n) = , the others are u(i-1)+u(i+1)-2*u(i)
    #   v(1) = 0; v(n) = ;
    #   v(2:n-1) = u(1:n-2)+u(3:n)-2*u(2:n-1);

    #Wes
    #v = zeros(n)
    #Wes:Dirichlet
    #v[0] = 0; v[-1] = 0;
    #Wes:Neumann
    #v[0] = -2*u[0]+ u[1]
    #v[-1] = -2*u[-1]+u[-2]

    #Leo: using second approach by LeVeque using 2 ghost cells on water   
    np2 = size(u)
    n = np2-2
    v = zeros(n+2)
    v[0] = -u[0]/2.+u[1]/2. #Sigma= 0
    #v[-1] =  -u[-2]/2 + u[-1]/2 #Sigma= 0 
    v[-1] =  u[-2]/2. - u[-1]/2. #Sigma= 0 
    #v[0] = -u[0]/(2*dx)+u[1]/(2*dx) #Sigma = 0
    #v[-1] =  -u[-2]/(2*dx)+u[-1]/(2*dx) #Sigma = 0 
    
    v[1:-1] = u[0:-2]-2.*u[1:-1]+u[2:]
    return v

def amultNeumann(u):
    np2 = size(u)
    n = np2-2
    v = zeros(n+2)
    v[0] = -u[0]/2.+ u[1]/2. 
    #v[-1] = -u[n-2]/2. + u[n-1]/2.
    v[-1] = - u[np2-2]/2. + u[np2-1]/2. #Leo: VEEEERY CAREFULL HERE!!!! 
    v[1:-1] = u[0:-2]-2.*u[1:-1]+u[2:]
    return v


#  Tridiagonal system solver modeled on
#  Using Thomas algorithm 
#  Tridiagonal system is Tx=b, where
#     [-1/2 1/2     0         .... 0]
# T = [-k/2     d(1)    -k/2 0        .... 0]
#     [0        -k/2    d(2)          .... 0]
#     [0        0                     .... 0]
#     [0                                   0]
#     [0                -k/2    d(n-1)  -k/2]
#     [0                        -1/2     1/2]

# and k = h/dx^2 is the CFL factor. Diagonals
# d = (1 d(2) d(3) ...  d(n-1) 1) are strong.
# cp and dp are working arrays.
# In the original F & M notation,
#
#  T = [d(1)  c(1)  0        ...             0 ]
#      [a(2)  d(2)  c(2)  0  ...             0 ]
#      [ 0          ....                     0 ]
#      [ 0                a(n-1) d(n-1) c(n-1) ]
#      [ 0                  0    a(n)   d(n)   ]
#
#  wpp 27 May 2012
def trisol2Neumann(k,d,b):
    np2 = size(d)
    cp = zeros(np2)
    dp = zeros(np2)
    n = np2-2
    # constant: mk2 (minus k divided by 2)
    x = zeros (n+2)
    mk2 = -k/2
    #m2dx = -1/(2*dx)
    m2dx = -1/2.
    #m2dx = -1/(2*dx)
    p2dx = 1/2.
    
    d[0] = m2dx
    d[n+1] = p2dx
    
    #cp[0] = 0.
    #ci'=c1/b1
    #cp[0]=(1/(2*dx))/(-1/(2*dx)) : 
    cp[0] = -1.

 
    # Leo: Forward elimination
    #from 1 to n ci'=ci/(bi-ai*c'[i-1])
    for i in range(1,n+1): 
        #print(d[i])
        #print(cp[i-1])
        cp[i]= mk2 / ( d[i] - mk2 * cp[i-1] )
        #print(cp[i])

    #d1' = b1/d1
    dp[0]=b[0]/(m2dx)

    #di' = (bi-ai*d'[i-1])/(di-ai*c'[i-1]) 
    for i in range(1,n+1): 
        #print(dp[i-1])
        dp[i] = ( b[i] - mk2 * dp[i-1] ) / ( d[i] - mk2 * cp[i-1] )

    dp[n+1]=( b[n+1] - m2dx * dp[n] ) / ( d[n+1] - m2dx * cp[n] )
    
    # Leo: backward substitution
    # xn = dn'
    x[n+1]=dp[n+1]

    # xi = di' - ci'*x[i+1]
    for i in range(n,-1,-1):
        #print(i)
        #print(i)
        #print(dp[i])
        #print(cp[i])
        #print(x[i+1])
        x[i]= dp[i] - cp[i] * x[i+1]
    
    return x;


def trisol0Neumann(k,b):
#  Tridiagonal system solver modeled on
#  G.E. Forsythe and C.B. Moler " Computer 
#  solutions to linear algebraic systems,"
#  Prentice-Hall publ., 1967
#  Tridiagonal system is Tx=b, where
#
#  T = [-1/2    1/2   0    ....     0 ]
#      [-k/4 1+k/2 -k/4 ....          0 ]
#      [0    -k/4 1+k/2 -k/4  0 ....  0 ]
#      [0           ....              0 ]
#      [0               -k/4 1+k/2  -k/4]
#      [0                 0    -1/2   1/2]
# 
# and k = h/dx^2 is the CFL factor. Diagonals
# d = (1 d(2) d(3) ...  d(n-1) 1) are strong.
# s1 and s2 are work arrays.(Leo: s2 new diagonal, s1 new off-diagonal)  
# In the original F & M notation,
#
#  T = [d(1)  c(1)  0        ...             0 ]
#      [a(2)  d(2)  c(2)  0  ...             0 ]
#      [ 0          ....                     0 ]
#      [ 0                a(n-1) d(n-1) c(n-1) ]
#      [ 0                  0    a(n)   d(n)   ]
#
#  wpp 27 May 2012
# def trisol(n,k,d,b,s1,s2):

    np2 = size(b)
    cp = zeros(np2)
    dp = zeros(np2)
    d = zeros(np2)
    n = np2-2
    # constant: mk2 (minus k divided by 2)
    x = zeros (n+2)

    #mk2 = -k/2
    k2 = k / 2.
    mk4 = - k / 4.

    #m2dx = -1/(2*dx)
    m2dx = - 1 / 2.
    p2dx = 1 / 2.
    
    #cp[0] = 0.
    #cp[0]=(1/(2*dx))/(-1/(2*dx))
    #ci'=c1/d1
    cp[0] = - 1.

    d[0] = m2dx
    d[n+1] = p2dx
    
    # Leo: Forward elimination
    # Leo: Forward elimination
    #from 1 to n ci'=ci/(di-ai*c'[i-1])
    for i in range(1,n+1): #from 1 to n
        #print(d[i])
        #print(cp[i-1])
        #cp[i]=mk2/(d[i]-mk2*cp[i-1])
        d[i] = 1. + k2
        cp[i]= mk4 / ( d[i] - mk4 * cp[i-1] )
        #print(cp[i])
       
    #d1' = b1/d1     
    dp[0]= b[0] / ( m2dx )

    #di' = (bi-ai*d'[i-1])/(di-ai*c'[i-1])     
    for i in range(1,n+1):
        #print(dp[i-1])
        dp[i]=( b[i] - mk4 * dp[i-1] ) / ( d[i] - mk4 * cp[i-1] )

    dp[n+1] = ( b[n+1] - m2dx * dp[n] ) / ( d[n+1] - m2dx * cp[n] )
    x[n+1] = dp[n+1]
    
    # Leo: backward substitution 
    for i in range(n,-1,-1):
        #print(i)
        #print(i)
        #print(dp[i])
        #print(cp[i])
        #print(x[i+1])
        x[i] = dp[i] - cp[i] * x[i+1]
        #if (x[i]<0):
        #    #x[ii]=0.
        #    raise ValueError('x less than 0 ')

    return x

def trisol1Neumann(k,d,b):
    np2 = size(d)
    cp = zeros(np2)
    dp = zeros(np2)
    n = np2-2
    # constant: mk2 (minus k divided by 2)
    x = zeros (n+2)
    mk2 = -k/2
    #m2dx = -1/(2*dx)
    m2dx = -1/2.
    p2dx = 1/2.
   
    d[0] = m2dx
    d[n+1] = p2dx
 
    #cp[0] = 0.
    #cp[0]=(1/(2*dx))/(-1/(2*dx))
    cp[0] = -1.    

    # Leo: Forward elimination
    for i in range(1,n+1): #from 1 to n
        #print(d[i])
        #print(cp[i-1])
        cp[i]= mk2 / ( d[i] - mk2 * cp[i-1] )
        #print(cp[i])
                
    dp[0]= b[0] / ( m2dx )
    
    for i in range(1,n+1):
        #print(dp[i-1])
        dp[i]=( b[i] - mk2 * dp[i-1] ) / ( d[i] - mk2 * cp[i-1] )

    dp[n+1] = ( b[n+1] - m2dx * dp[n] ) / ( d[n+1] - m2dx * cp[n] )
    x[n+1] = dp[n+1]
    
    # Leo: backward substitution 
    for i in range(n,-1,-1):
        #print(i)
        #print(i)
        #print(dp[i])
        #print(cp[i])
        #print(x[i+1])
        x[i]= dp[i] - cp[i] * x[i+1]
        #if (x[i]<0):
        #    #x[ii]=0.
        #    raise ValueError('x less than 0 ')
    
    return x;

def godunovstepNeumann2nd(h,k,u):
    np2 = size(u)
    
    n = np2-2
    # first compute Euler estimate
    #ut = (Amult(n,u))'
    #ut = k*ut
    #uE = ut + h*(1-u).*u # commmon part of Euler est.
    ut = amult2Neumann(u) #returns n+2 vector
    ut = k*ut 
    
    uE = ut + h*(1-u)*u
    
    # compute right hand side
    #ur = u + 0.5*uE #RHS
    #uE = u + uE #Euler est.
    #dA(1) = 1; dA(n) = 1 #end points
    ur = u + 0.5*uE #RHS
    uE = u + uE #Euler estimate
    
    #Leo: Impose second aproach
    #ur[-1] = u [1] 
    #Neumann bc
    sigma = 0.
    #sigma = 1.
    ur[0]=sigma 
    ur[-1]=sigma
    
    #Neumann boundary conditions
    dA = zeros(n+2)
    dA[0] = -0.5
    dA[n+1] = 0.5 #end points
    #dA[0] = -1/(2*dx) 
    #dA[n+1] = 1/(2*dx) #end points

    # interior diags. of LHS matrix:
    #dA(2:n-1) = 1 + k - 0.5*h*(1-uE(2:n-1));
    #dA[1:n-2] = 1 + k - 0.5*h*(1-uE[1:n-2])
    dA[1:-1] = 1 + k - 0.5*h*(1-uE[1:-1])
    
    #print(dA)
   
    # solve using Forsythe-Moler
    #v  = trisol(n,k,dA,ur,s1,s2);
    v = trisol2Neumann(k,dA,ur)
    #v = modules.trisol(k,dA,ur)
    #v = Cmodules.trisol(k,dA,ur)
    return v

def godunovstep1(h,kcfl,u):

    np2 = size(u)
    
    n = np2-2
    kcfl4 = kcfl/4.
    kcfl2 = kcfl/2.
    # first compute Euler estimate
    #ut = (Amult(n,u))'
    #ut = k*ut
    #uE = ut + h*(1-u).*u # commmon part of Euler est.
    ut = amultNeumann(u) #returns n+2 vector
    ut = kcfl4*ut 
    
    # compute right hand side
    #ur = u + 0.5*uE #RHS
    #uE = u + uE #Euler est.
    #dA(1) = 1; dA(n) = 1 #end points
    ur = u + ut #RHS
    
    #Leo: Impose second aproach
    #ur[-1] = u [1] 
    #Neumann bc
    sigma = 0.
    #sigma = 1.
    ur[0]=sigma 
    ur[-1]=sigma
    
    # solve using Forsythe-Moler
    #v  = trisol(n,k,dA,ur,s1,s2);
    v = trisol0Neumann(kcfl,ur)
    #v = modules.trisol(k,dA,ur)
    #v = Cmodules.trisol(k,dA,ur)
    return v

def godunovstep2(h,k,u,kap0,kaph):
    #kap0 : carrying capacity low 
    #kaph : carrying capacity high

    np2 = size(u)
    
    n = np2-2
    # first compute Euler estimate
    #ut = (Amult(n,u))'
    #ut = k*ut
    #uE = ut + h*(1-u).*u # commmon part of Euler est.
    ut = amultNeumann(u) #returns n+2 vector
    ut = k*ut 
    
    #right hand side part for part of Euler uses K(t)
    uC = u/kap0
    #left hand side version uses K(t+h)
    #Leo error here 
    #uh = u/kaph
   
 
    uE = ut + h*(1-uC)*u
    
    # compute right hand side
    #ur = u + 0.5*uE #RHS
    #uE = u + uE #Euler est.
    #dA(1) = 1; dA(n) = 1 #end points
    ur = u + 0.5*uE #RHS
    uE = u + uE #Euler estimate
    
    #Leo: we use uE to get uh 
    uh = uE/kaph
    
    #Leo: Impose second aproach
    #ur[-1] = u [1] 
    #Neumann bc
    sigma = 0.
    #sigma = 1.
    ur[0]=sigma 
    ur[-1]=sigma
    
    #Neumann boundary conditions
    dA = zeros(n+2)
    dA[0] = -0.5
    dA[n+1] = 0.5 #end points
    #dA[0] = -1/(2*dx) 
    #dA[n+1] = 1/(2*dx) #end points

    # interior diags. of LHS matrix:
    #dA(2:n-1) = 1 + k - 0.5*h*(1-uE(2:n-1));
    #dA[1:n-2] = 1 + k - 0.5*h*(1-uE[1:n-2])
    dA[1:-1] = 1 + k - 0.5 * h * ( 1 - uh[1:-1] )
    
    #print(dA)
   
    # solve using Forsythe-Moler
    #v  = trisol(n,k,dA,ur,s1,s2);
    
    #Leo: No saturation 
    #v = trisol1Neumann(k,dA,ur)
    #Leo: Saturation and avoid negative densities 
    v = minimum(maximum(trisol1Neumann(k,dA,ur),0.),kaph)
    
    #v = modules.trisol(k,dA,ur)
    #v = Cmodules.trisol(k,dA,ur)
    return v 

def smooth7(r_map,w_map):
    #Smooths points in NPP map by (2*L-1) x (2*L-1) quadratically
    #weighted window
    L=5
    #s_map = zeros((NX,NY))
    NX, NY = r_map.shape
    s_map = empty((NX,NY))
    
    for col in range(0,NX):
        for row in range(0,NY):
            if w_map[col,row]>0:      # only do windowing on land points
                wtot = 1.
                summ = r_map[col,row]

                #FIRST do points on cross 
                #Up/down on X
                for pix_col in range(1,L):
                    colpflg = True
                    colmflg = True
                    #isp1=i + ids - 1 # isp1 == 10 = i(8) + ids(3) -1
                    colp = col + pix_col - 1
                    #ism1=i - ids + 1 #Error ism1 == 10 
                    colm = col - pix_col + 1 

                    #print 'i,j',i,j
                    #print 'ids', ids
                    #print 'isp1',isp1

                    #if isp1>NX: 
                    if colp >= NX:
                        colpflg = False
                        
                    #if ism1<1:
                    if colm<0:  
                        colmflg = False
                        
                    w_col =(1.-(1.*pix_col/L)**2) # x weighting factor
                    #w_col =(1-(pix_col/L))**2 # x weighting factor


                    if colpflg and w_map[colp,row]>0:
                        wtot = wtot+w_col
                        summ = summ + w_col*r_map[colp,row]

                    if colmflg and w_map[colm,row]>0:
                        wtot = wtot+w_col;
                        summ = summ + w_col*r_map[colm,row]
    
                # Now left/right in Y-direction
                for pix_row in range(1,L):
                    #print 'js:', js
                    #jsp1=j + js - 1
                    #jsm1=j - js + 1 #Error j(9)-js(0) + 1 == 10
                    #rowp=row + pix_row -1
                    rowp=row + pix_row - 1 
                    rowm=row - pix_row + 1 
 
                    #if jsp1>=NY-1:
                    if rowp>=NY:
                        rowp=rowp-NY
                        
                    if rowm<0: 
                        rowm=rowm+NY
                            
                    w_row = (1.-(1.*pix_row/L)**2)  # y weighting factor
                    #w_row =(1-(pix_row/L))**2  # y weighting factor

                    if w_map[col,rowp]>0:
                        wtot = wtot+w_row
                        summ = summ + w_row*r_map[col,rowp]
                    #print 'i,j',i,j
                    #print 'jsm1:',jsm1
                    if w_map[col,rowm]>0: #Error: jsm1== ny, Error axis 1 size 5 
                        wtot = wtot+w_row
                        summ = summ + w_row*r_map[col,rowm]

                # END of points on cross
 
                #   All other points in square
                for pix_col in range(1,L):
                    colpflg = True; 
                    colmflg = True;
                    #isp1=i + ids - 1
                    #ism1=i - ids + 1
                    colp = col + pix_col - 1
                    colm = col - pix_col + 1 

                    #if isp1>NX:
                    if colp>=NX:
                        colpflg = False
                         
                    #if ism1<1:
                    if colp<0:
                        colmflg = False
                        
                    w_col =(1.-(1.*pix_col/L)**2);  # x weighting factor
                    #w_col =(1-(pix_col/L))**2;  # x weighting factor

                    for pix_row in range(1,L): 
                        #jsp1=j + js - 1
                        #jsm1=j - js + 1
                        rowp= row + pix_row - 1
                        rowm= row - pix_row + 1

                        #if jsp1>NY-1:
                        if rowp>=NY:
                            rowp=rowp-NY
                            
                        if rowm<0: 
                            rowm=rowm + NY
                            
                        w_row =(1.-(1.*pix_row/L)**2)  # y weighting factor
                        #w_row =(1-(pix_row/L))**2  # y weighting factor
                    
                        if colpflg:   # says isp1 <= NX
                            if w_map[colp,rowm]>0:
                                wt = w_col*w_row
                                wtot = wtot+wt;
                                summ = summ + wt*r_map[colp,rowm]
                    
                            if w_map[colp,rowp]>0:
                                wt = w_col*w_row
                                wtot = wtot+wt
                                summ = summ + wt*r_map[colp,rowp]

                        if colmflg:   # says ism1 >= 1
                            if w_map[colm,rowm]>0:
                                wt = w_col*w_row
                                wtot = wtot+wt
                                summ = summ + wt*r_map[colm,rowm]
                       
                            if w_map[colm,rowp]>0:
                                wt = w_col*w_row
                                wtot = wtot+wt;
                                summ = summ + wt*r_map[colm,rowp]
                       

                s_map[col,row] = summ/wtot # point is land, now averaged
            else:       # template=0
                s_map[col,row] = 0  # point is in water
            # end of center = landpoint if
        # end of j=1:NY loop
    #end of i=1:NX loop

    return s_map

def sciSmooth(r_map,mask):    
    # make some kind of kernel, there are many ways to do this...
    t = 1 - abs(linspace(-1, 1, 7))
    kernel = t.reshape(7, 1) * t.reshape(1, 7)
    kernel /= kernel.sum()   # kernel should sum to 1!  :) 
    
    # convolve 2d the kernel with each channel
    s_map = scipy.signal.convolve2d(r_map, kernel, mode='same')
    s_map = s_map*mask 
    
    return s_map
