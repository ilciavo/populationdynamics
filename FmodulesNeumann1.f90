!Create Signature: f2pyA -m FmodulesNeumann1 -h  FmodulesNeumann1.pyf FmodulesNeumann1.f90 --overwrite-signature
!Compile:f2pyA -c   --fcompiler=gnu95 --verbose --opt='-O3' FmodulesNeumann1.pyf FmodulesNeumann1.f90 

subroutine amultneumann(np2, u, v)
  implicit none
  integer :: np2, i, n 
  real(8) :: u(np2), v(np2)
!f2py real(8), intent(out), dimension(np2) :: v
!f2py real(8), intent(in), dimension(np2) :: u
!f2py integer, intent(in) :: np2
  n = np2-2
  v(1) = -0.5*u(1) + 0.5*u(2) 
  v(np2) = 0.5*u(np2-1) - 0.5*u(np2) 
  
  !v[1:-1] = u[0:-2]-2.*u[1:-1]+u[2:]
  do i = 2, np2-1 
      v(i) = u(i-1)-2*u(i)+u(i+1)
  end do 

end subroutine amultneumann

!Tridiagonal solver using Thomas algorithm
! T = [-1/2    1/2   0    ....     0 ]
!      [-k/4 1+k/2 -k/4 ....          0 ]
!      [0    -k/4 1+k/2 -k/4  0 ....  0 ]
!      [0           ....              0 ]
!      [0               -k/4 1+k/2  -k/4]
!      [0                 0    -1/2   1/2]
subroutine trisol0neumann(np2, k, b, x)
    implicit none
    integer :: n, i, ii, np2
    real(8) :: k, d(np2), b(np2), cp(np2), dp(np2), x(np2), mk4, k2, m2dx, p2dx

!f2py real(8), intent(out), dimension(np2) :: x
!f2py real(8), intent(in), dimension(np2) :: d
!f2py real(8), intent(in), dimension(np2) :: b
!f2py real(8), intent(in) :: k
!f2py integer, intent(in) :: np2

    n = np2-2

    !constants 
    k2 = 0.5 * k 
    mk4 = -0.25 * k
   
    !m2dx = -1/(2*dx) 
    m2dx = -0.5
    p2dx = 0.5

    !ci' = c1/d1 = (1/2)/(-1/2) = -1
    cp(1) = -1.


    !diagonal: d1 = -1/2, d[np2] = 1/2 
    d(1) = m2dx
    d(np2) = p2dx

    !Forward Elimination 
    !from 1 to n ci'=ci/(di-ai*c'[i-1])
    do i = 2, n+1
        d(i) = 1. + k2
        cp(i) = mk4 / ( d(i) - mk4 * cp(i-1) )
    end do 

    !d1' = b1/d1     
    dp(1) = b(1)/m2dx

    do i=2, n+1
        dp(i) = ( b(i) - mk4 * dp(i-1) ) / ( d(i) - mk4 * cp(i-1) )
    end do 
  
 
    dp(n+2) = ( b(n+2) - m2dx * dp(n+1) ) / ( d(n+2) - m2dx * cp(n+1) )
  
    x(n+2) = dp(n+2)

    !Backward substitution
    ii = n+1
    do i=1, n+1
        !print *, ii ![1, 2, 3]
        x(ii) = dp(ii) - cp(ii)*x(ii+1)
        ii = ii-1
    end do 

end subroutine trisol0neumann

subroutine trisol1neumann(np2, k, d, b, x)

    implicit none
    integer :: n, i, ii, np2
    real(8) :: k, d(np2), b(np2), cp(np2), dp(np2), x(np2), mk2, m2dx, p2dx

!f2py real(8), intent(out), dimension(np2) :: x
!f2py real(8), intent(in), dimension(np2) :: d
!f2py real(8), intent(in), dimension(np2) :: b
!f2py real(8), intent(in) :: k
!f2py integer, intent(in) :: np2

    n = np2-2

    !constants 
    mk2 = -0.5*k

    !m2dx = -dx/2
    m2dx = -0.5
    p2dx = 0.5

    !cp[0]=(1/(2*dx))/(-1/(2*dx))
    cp(1) = -1.

    !Forward Elimination 
    !nm1 = n-1
    do i = 2, n+1
        cp(i) = mk2 / ( d(i) - mk2 * cp(i-1) )
    end do 

    dp(1) = b(1) / m2dx

    do i=2, n+1
        dp(i) = ( b(i) - mk2 * dp(i-1) ) / ( d(i) - mk2 * cp(i-1) )
    end do 
  
 
    dp(n+2) = ( b(n+2) - m2dx * dp(n+1) ) / ( d(n+2) - m2dx * cp(n+1) )
  
    x(n+2) = dp(n+2)

    !Backward substitution
    ii = n+1
    do i=1, n+1
        !print *, ii ![1, 2, 3]
        x(ii) = dp(ii) - cp(ii) * x(ii+1)
        ii = ii-1
    end do 

end subroutine trisol1neumann

subroutine godunovstep1(np2, h, kcfl, u, v)

    implicit none
    integer :: n, np2
    real(8) :: h, kcfl, u(np2), v(np2), ut(np2), ur(np2), sigmal, sigmar, kcfl4, kcfl2

!f2py integer, intent(in) :: np2
!f2py real(8), intent(in) :: h
!f2py real(8), intent(in) :: kcfl
!f2py real(8), intent(in), dimension(np2) :: u
!f2py real(8), intent(out), dimension(np2) :: v

  n = np2-2
  !Constants 
  kcfl4 = kcfl/4.
  kcfl2 = kcfl/2. 

  !Fluxes
  sigmal = 0.
  sigmar = 0.

  !Compute Euler estimate
  call amultneumann(np2, u, ut)
  ur = u + kcfl4*ut !RHS

  ur(1) = sigmal
  ur(np2) = sigmar
 
  call trisol0neumann(np2,kcfl,ur,v)

end subroutine godunovstep1

subroutine godunovstep2(np2, h, k, u, kap0, kaph, v)

    implicit none
    integer :: n, i, np2
    real(8) :: h, k, u(np2), kap0(np2), kaph(np2), v(np2), ut(np2), ue(np2), ur(np2), dA(np2), sigmal, sigmar, uc(np2), uh(np2)

!f2py integer, intent(in) :: np2
!f2py real(8), intent(in) :: h
!f2py real(8), intent(in) :: k
!f2py real(8), intent(in), dimension(np2) :: kap0
!f2py real(8), intent(in), dimension(np2) :: kaph
!f2py real(8), intent(in), dimension(np2) :: u
!f2py real(8), intent(out), dimension(np2) :: v

    n = np2-2

    !Fluxes
    sigmal = 0.
    sigmar = 0.

    !Euler Estimate 
    call amultneumann(np2, u, ut)
    ut = k*ut

    uc = u/kap0

    ue = ut + h*(1.-uc)*u
    ur = u + 0.5*ue !RHS
    ue = u+ue

    uh = ue/kaph

    ur(1) = sigmal
    ur(np2) = sigmar
 
    dA(1) = -0.5
    dA(np2) = 0.5
  
    do i = 2,np2-1
        dA(i) = 1. + k - 0.5 * h * ( 1. - uh(i) )
        !print *, dA(i)
    end do 

  call trisol1neumann(np2,k,dA,ur,v)
  
  v=max(min(v,0.),kaph)

end subroutine godunovstep2


subroutine smooth7(cols, rows, r_map, w_map, s_map)
    implicit none
    logical :: colpflg, colmflg
    integer :: cols, rows, col, row, L, w_map(cols,rows), rowp, rowm, pix_row, pix_col, colp, colm
    real(8) :: r_map(cols,rows), s_map(cols,rows), wtot, w_col, w_row, summ

    !f2py integer, intent(in) :: cols
    !f2py integer, intent(in) :: rows
    !f2py real(8), intent(in), dimension(cols,rows) :: r_map
    !f2py integer, intent(in), dimension(cols,rows) :: w_map
    !f2py real(8), intent(out), dimension(cols,rows) :: s_map

    L = 5 !smoother uses 7 points 
  
    do row = 1, rows
        do col = 1, cols
            if (w_map(col,row)>0) then !Taking only land points
                wtot = 1. !Total Weight 
                summ = r_map(col,row) !summing values

                !scanning points on a cross 
                !Left/right on X
                do pix_col = 1, L-1
                    !flags to track land
                    colpflg = .TRUE. 
                    colmflg = .TRUE.
                    !column plus pixel smoother
                    colp = col + pix_col - 1
                    !column minus pixel smoother
                    colm = col - pix_col +1

                    if(colp>cols) then 
                        colpflg = .FALSE. ! flag out of the map
                    end if 
 
                    if(colm<1) then 
                        colmflg = .FALSE. !flag out of the map 
                    end if
 
                    !weighting factor in x 
                    w_col = (1.-(1.*pix_col/L)**2)
                    !w_col = (1-(pix_col/L))*(1-(pix_col/L))
 
                    if ( colpflg .and. (w_map(colp,row)>0)) then
                        !inside the map 
                        wtot = wtot+w_col ! updating total weight 
                        summ = summ + w_col*r_map(colp,row)   ! updating sum
                    end if
                    
                    if ( colmflg .and. (w_map(colm,row)>0)) then 
                        !inside the map 
                        wtot= wtot+w_col ! updating total weight 
                        summ = summ + w_col*r_map(colm,row)   ! updating sum
                    end if

                end do    

                !Bottom/Up in Y 
                do pix_row = 1, L-1
                    !flags to track land
                    !colpflg = .TRUE. 
                    !colmflg = .TRUE.
                    !column plus pixel smoother
                    rowp = row + pix_row - 1
                    !column minus pixel smoother
                    rowm = row - pix_row + 1

                    if(rowp>rows) then 
                        !colpflg = .FALSE. ! flag out of the map
                        rowp = rowp - rows 
                    end if 
 
                    if(rowm<1) then 
                        !colmflg = .FALSE. !flag out of the map 
                        rowm = rowm + rows 
                    end if
 
                    !weighting factor in y 
                    w_row =(1.-(1.*pix_row/L)**2)
                    !w_row = (1-(pix_row/L))*(1-(pix_row/L))
 
                    if (w_map(col,rowp)>0) then
                        !inside the map 
                        wtot = wtot+w_row ! updating total weight 
                        summ = summ + w_row*r_map(col,rowp)   ! updating sum
                    end if
                    
                    if (w_map(col,rowm)>0) then 
                        !inside the map 
                        wtot= wtot+w_row ! updating total weight 
                        summ = summ + w_row*r_map(col,rowm)   ! updating sum
                    end if

                end do !End of points on cross

                !Now smooth the square
                do pix_col = 1, L-1
                    colpflg = .TRUE.
                    colmflg = .TRUE.
                    colp = col + pix_col - 1
                    colm = col - pix_col + 1

                    if (colp > cols) then 
                        colpflg = .FALSE. 
                    end if
                
                    if (colm < 1) then
                        colmflg = .FALSE.
                    end if
                    
                    w_col = (1.-(1.*pix_col/L)**2)
                    !w_col = (1-pix_col/L)*(1-pix_col/L)
                    
                    do pix_row = 1, L-1
                        !rowpflg = .TRUE.
                        !rowmflg = .TRUE.
                        rowp = row + pix_row - 1
                        rowm = row - pix_row + 1

                        if (rowp > rows) then 
                            !colpflg = .FALSE.
                            rowp = rowp - rows 
                        end if
                
                        if (rowm < 1) then
                            !colmflg = .FALSE.
                            rowm = rowm + rows 
                        end if
                        
                        w_row =(1.-(1.*pix_row/L)**2)
                        !w_row = (1-(row/pix_row))*(1-(row/pix_row))
                        
                        if(colpflg) then ! inside the map from the right 
                            if (w_map(colp, rowm)>0) then
                                wtot = wtot + w_row*w_col
                                summ = summ + w_row*w_col*r_map(colp, rowm)
                            end if
                            
                            if (w_map(colp, rowp)>0) then
                                wtot = wtot + w_row*w_col
                                summ = summ + w_row*w_col*r_map(colp, rowp)
                            end if
                             
                        end if
  
                        if (colmflg) then !inside the map from the left
                            if (w_map(colm, rowm)>0) then
                                wtot = wtot + w_row*w_col
                                summ = summ + w_row*w_col*r_map(colm, rowm)
                            end if
                            
                            if (w_map(colm, rowp)>0) then
                                wtot = wtot + w_row*w_col
                                summ = summ + w_row*w_col*r_map(colm, rowp)
                            end if                             

                        end if 
                                             
                    end do 

                end do !Finish square scan  
            
                s_map(col, row) = summ/wtot !average of points in land
            
            else 
                s_map(col,row) = 0 ! pixel is in water

            end if 
            !s_map(col,row) = L*(r_map(col,row) + w_map(col,row))
        end do
    end do 

end subroutine smooth7

subroutine test(nx, ny, r_map, s_map)
    implicit none
    integer :: nx, ny 
    real(8) :: r_map(nx,ny), s_map(nx,ny)
    !integer, allocatable :: a(:), b(:)

    !f2py integer, intent(in) :: nx
    !f2py integer, intent(in) :: ny
    !f2py real(8), intent(in), dimension(nx,ny) :: r_map
    !f2py real(8), intent(out), dimension(nx,ny) :: s_map

    s_map = r_map

    !s_map = r_map
    !allocate (a(nx))
    !a = [1,2,3]
    !call move_alloc(a,nxsegs)
    !print *, allocated(a), allocated(nxsegs) ! F, T
    !print *, nxsegs ![1, 2, 3]

end subroutine test

subroutine amult2neumann(np2, u, v, dx)
    implicit none
    integer :: n, i, np2
    real(8) :: u(np2), v(np2), dx
!f2py real(8), intent(out), dimension(np2) :: v
!f2py real(8), intent(in), dimension(np2) :: u
!f2py real(8), intent(in) :: dx
!f2py integer, intent(in) :: np2

  n = np2-2

  v(1) = -dx*u(1)/2+dx*u(2)/2 
  v(n+2) = -dx*u(n+1)/2 + dx*u(n+2)/2
  
!Forward Elimination 
!v = u(1:n-2)+u(3:n)-2*u(2:n-1)
  do i = 2, n+1
      v(i) = u(i-1)+u(i+1)-2*u(i)
  end do 

end subroutine amult2neumann


subroutine trisol2neumann(np2, k, dx, d, b, x)
    implicit none
    integer :: n, i, ii, np2
    real(8) :: k, dx, d(np2), b(np2), cp(np2), dp(np2), x(np2), mk2, m2dx

!f2py real(8), intent(out), dimension(np2) :: x
!f2py real(8), intent(in), dimension(np2) :: d
!f2py real(8), intent(in), dimension(np2) :: b
!f2py real(8), intent(in) :: k
!f2py real(8), intent(in) :: dx
!f2py integer, intent(in) :: np2

  n = np2-2

  mk2 = -0.5*k
  m2dx = -0.5*dx

  cp(1) = -1.

  !Forward Elimination 
  !nm1 = n-1
  do i = 2, n+1
    cp(i) = mk2/(d(i)-mk2*cp(i-1))
  end do 

  dp(1) = b(1)/m2dx

  do i=2, n+1
      dp(i) = (b(i)-mk2*dp(i-1))/(d(i)-mk2*cp(i-1))
  end do 
  
 
  dp(n+2) = (b(n+2)-m2dx*dp(n+1))/(d(n+2)-m2dx*cp(n+1))
  
  x(n+2) = dp(n+2)

  !Backward substitution
  ii = n+1
  do i=1, n+1
      !print *, ii ![1, 2, 3]
      x(ii) = dp(ii) - cp(ii)*x(ii+1)
      ii = ii-1
  end do 

end subroutine trisol2neumann

subroutine godunovstepneumann2nd(np2, h, k, dx, u, v)
    implicit none
    integer :: n, i, np2
    real(8) :: h, k, dx, u(np2), v(np2), ut(np2), ue(np2), ur(np2), dA(np2), sigmal, sigmar

!f2py integer, intent(in) :: np2
!f2py real(8), intent(in) :: h
!f2py real(8), intent(in) :: k
!f2py real(8), intent(in) :: dx
!f2py real(8), intent(in), dimension(np2) :: u
!f2py real(8), intent(out), dimension(np2) :: v

  n = np2-2

  !Fluxes
  sigmal = 0.
  sigmar = 0.

  call amult2neumann(np2, u, ut, dx)
  ut = k*ut
  ue = ut + h*(1.-u)*u
  ur = u + 0.5*ue !RHS
  ue = u+ue

  ur(1) = sigmal
  ur(np2) = sigmar
 
  dA(1) = -0.5*dx
  dA(np2) = 0.5*dx
  
  do i = 2,np2-1
      dA(i) = 1. + k - 0.5*h*(1.-ue(i))
      !print *, dA(i)
  end do 

  call trisol2neumann(np2,k,dx,dA,ur,v)

end subroutine godunovstepneumann2nd

subroutine amult3neumann(n, u, v)
  implicit none
  integer :: n, i
  real(8) :: u(n), v(n)

!f2py real(8), intent(out), dimension(n) :: v
!f2py real(8), intent(in), dimension(n) :: u
!f2py integer, intent(in) :: n

  v(1) = -2.*u(1)+u(2)
  v(n) = u(n-1)-2*u(n)
  
!Forward Elimination 
  !v = u(1:n-2)+u(3:n)-2*u(2:n-1)
  do i = 2, n-1 
      v(i) = u(i-1)+u(i+1)-2*u(i)
  end do 

end subroutine amult3neumann

subroutine trisol3neumann(n, k, d, b, x)
  implicit none
  integer :: n, i, ii
  real(8) :: k, d(n), b(n), s1(n), s2(n), x(n), mk2

!f2py real(8), intent(out), dimension(n) :: x
!f2py real(8), intent(in), dimension(n) :: b
!f2py real(8), intent(in) :: k
!f2py integer, intent(in) :: n

  mk2 = -k/2
  s2(1) = d(1)
  s1(2) = mk2/s2(1)
  s2(2) = d(2)+4.*s1(2)/3.
  !s1(3) = mk2/s2(1)
  !s2(2) = d(2)+4.*s1(3)/3.

!Forward Elimination 
  !nm1 = n-1
  do i = 3, n-1 
      s1(i) = mk2/s2(i-1)
      s2(i) = d(i)-mk2*s1(i)
  end do 

!Neumann
  s1(n) = -4./(3*s2(n-1))
  s2(n) = d(n)-mk2*s1(n) 

  x(1) = b(1)
  do i=2, n
      x(i) = b(i)- s1(i)*x(i-1)
  end do 

!Backward substitution
  x(n) = x(n)/s2(n)
  ii = n-1

  do i=1, n-2
      x(ii) = (x(ii) - mk2*x(ii+1))/s2(ii)
      ii = ii-1
  end do 

  !x(1) = x(1)/s2(1)

end subroutine trisol3neumann

subroutine godunovstepneumann3rd(n, h, k, u, v)
  implicit none
  integer :: n, i
  real(8) :: h, k, u(n), v(n), ut(n), ue(n), ur(n), dA(n), uest

!f2py integer, intent(in) :: n
!f2py real(8), intent(in) :: h
!f2py real(8), intent(in) :: k
!f2py real(8), intent(in), dimension(n) :: u
!f2py real(8), intent(out), dimension(n) :: v

  call amult3neumann(n, u, ut)
  ut = k*ut
  ue = ut + h*(1.-u)*u
  ur = u + 0.5*ue !RHS
  ue = u+ue
 
  !impose neumann on Euler estimate 
  ue(1) = 4.*ue(2)/3.-ue(3)/3.
  ue(n)=4.*ue(n-1)/3.-ue(n-2)/3.

  uest = u(3)+0.5*k*(ue(2)-2.*ue(3)+ue(4))+0.5*k*(u(2)-2.*u(3)+u(4))+0.5*h*((1-ue(3))*ue(3)+(1-u(3))*u(3))
  ur(1) = -uest/3

  uest = u(n-2)+0.5*k*(ue(n-3)-2.*ue(n-2)+ue(n-1))+0.5*k*(u(n-3)-2.*u(n-2)+u(n-1))+0.5*h*((1-ue(n-2))*ue(n-2)+(1-u(n-2))*u(n-2))
  ur(n) = -uest/3

  dA(1) = 1.
  dA(n) = 1.
  
  do i = 2,n-1
      dA(i) = 1 + k - 0.5*h*(1-ue(i))
      !print *, dA(i)
  end do 

  call trisol3neumann(n,k,dA,ur,v)

  !impose Neumann on output
  !use only if necessary  
  !v(1)=4.*v(2)/3.-v(3)/3
  !v(n)=4*v(n-1)/3.-v(n-2)/3

end subroutine godunovstepneumann3rd
